-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 13, 2015 at 02:20 PM
-- Server version: 5.6.14
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_jon`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_answer`
--

CREATE TABLE IF NOT EXISTS `tbl_answer` (
`id` int(11) NOT NULL,
  `answer` varchar(255) NOT NULL,
  `question_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_answer`
--

INSERT INTO `tbl_answer` (`id`, `answer`, `question_id`) VALUES
(15, '$1000 To $5000', 7),
(16, '$5000 To $10000', 7),
(17, '$10000 To $15000', 7),
(18, '$15000 To $20000', 7),
(19, '$20000 To $25000', 7),
(20, '3 Months', 8),
(21, '6 Months', 8),
(22, '9 Months', 8),
(23, '1 Year', 8),
(24, '2 Years', 8),
(25, '$1000', 9),
(26, '$2500', 9),
(27, '$5000', 9),
(28, '$7500', 9),
(29, '$10000', 9),
(30, '$1000', 10),
(31, '$2500', 10),
(32, '$5000', 10),
(33, '$7500', 10),
(34, '$10000', 10),
(35, '$5000', 11),
(36, '$10000', 11),
(37, '$15000', 11),
(38, '$20000', 11),
(39, '$25000', 11),
(40, '$30000', 11),
(41, '$100', 12),
(42, '$250', 12),
(43, '$500', 12),
(44, '$750', 12),
(45, '$1000', 12),
(46, '$1250', 12),
(47, '$1500', 12),
(48, '6 Months', 13),
(49, '1 Years', 13),
(50, '2 Years', 13),
(51, '3 Years', 13),
(52, '4 Years', 13),
(53, '5 Years', 13),
(54, 'Yes', 14),
(55, 'No', 14);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_answer`
--
ALTER TABLE `tbl_answer`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_answer`
--
ALTER TABLE `tbl_answer`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=56;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
