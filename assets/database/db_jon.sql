-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 17, 2015 at 05:14 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_jon`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `full_name` varchar(30) NOT NULL,
  `position` varchar(30) NOT NULL,
  `city` varchar(255) NOT NULL,
  `mobile_number` varchar(30) NOT NULL,
  `work_number` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `excerpt` text NOT NULL,
  `about_description` text NOT NULL,
  `experience` text NOT NULL,
  `marketing_des` text NOT NULL,
  `affiliations` text NOT NULL,
  `skills` text NOT NULL,
  `references` text NOT NULL,
  `profile_pic` varchar(100) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `linked_in` varchar(255) NOT NULL,
  `instagram` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `username`, `password`, `full_name`, `position`, `city`, `mobile_number`, `work_number`, `email`, `excerpt`, `about_description`, `experience`, `marketing_des`, `affiliations`, `skills`, `references`, `profile_pic`, `facebook`, `twitter`, `linked_in`, `instagram`) VALUES
(1, 'admin', 'a4cbb2f3933c5016da7e83fd135ab8a48b67bf61', 'Jon Crocilla', 'REAL ESTATE CONSULTANT-BROKER', '<p>676 N Michigan Ave </br>Chicago, IL 60611</p>\r\n', '(847) 452-9123 ', '(847) 452-9124 ', 'shrestha.nirmala100@gmail.com', '<p>Jon Crocilla always finds a way. When it comes to his clients he stops at nothing to help them achieve their goals. His drive to succeed is something he learned early in life, both from his parents, who operate a mortgage firm, and from competing in football and hockey. He is passionate about real estate and about his clients&rsquo; welfare. There is simply no more committed individual than Jon.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Jon also understands the value of creating and maintaining relationships. Real estate is a relationship based enterprise. The power of relationships becomes apparent when you are looking for the right buyer or seller. Being networked with the top producing agents and the clients they represent can mean the difference between a successful or an unsuccessful outcome. Jon&rsquo;s focus is on success.</p>\r\n\r\n<p>&nbsp;</p>\r\n', '<p>Jon Crocilla always finds a way. When it comes to his clients he stops at nothing to help them achieve their goals. His drive to succeed is something he learned early in life, both from his parents, who operate a mortgage firm, and from competing in football and hockey. He is passionate about real estate and about his clients&rsquo; welfare. There is simply no more committed individual than Jon.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Jon also understands the value of creating and maintaining relationships. Real estate is a relationship based enterprise. The power of relationships becomes apparent when you are looking for the right buyer or seller. Being networked with the top producing agents and the clients they represent can mean the difference between a successful or an unsuccessful outcome. Jon&rsquo;s focus is on success.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>He grew up with real estate and is highly knowledgeable about the Chicago market. He also has a strong understanding of the role of marketing technology and how to use it to the advantage of his clients. With 90% of consumers looking for their next home online, Jon has expert knowledge of eMarketing and social media best practices. He can get your home the greatest amount of exposure possible so that it sells quickly and at the best possible price.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Jon is a lifelong resident of Chicago and graduated from Elmhurst College with a BA in Business and History.</p>\r\n', '<p style="box-sizing: border-box; margin: 0px; padding: 0px; line-height: 21px; color: rgb(0, 0, 0); font-family: Ubuntu, sans-serif; font-size: 14px;">\r\n	Anyone can call themselves a Luxury Real Estate Agent, but typically the proof is in the transactions. Handling a multimillion dollar sale can have some very complicated components, both on the listing side and the selling side. High end buyers and sellers are typically handling large transactions in their daily business lives, and they expect the same business acumen.</p>\r\n<div>\r\n	&nbsp;</div>\r\n', '<p style="box-sizing: border-box; margin: 0px; padding: 0px; line-height: 21px; color: rgb(0, 0, 0); font-family: Ubuntu, sans-serif; font-size: 14px;">\r\n	A Luxury Broker should be able to speak about their market with utter fluency. If they cannot give you an educated and thoughtful answer, then you probably need to keep looking for a more knowledgeable agent.</p>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n', '<p style="box-sizing: border-box; margin: 0px; padding: 0px; line-height: 21px; color: rgb(0, 0, 0); font-family: Ubuntu, sans-serif; font-size: 14px;">\r\n	Luxury Real Estate Agents are involved in their communities, as well as in national organizations. Many transactions are done off the public MLS, and you want to have an agent that has their ear to the ground. Whether you are a buyer or a seller, you want whoever is representing you to get the phone call when there is a desirable property available.</p>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n', '<p style="box-sizing: border-box; margin: 0px; padding: 0px; line-height: 21px; color: rgb(0, 0, 0); font-family: Ubuntu, sans-serif; font-size: 14px;">\r\n	Luxury Transactions typically have some complicated components and require refined negation skills. You&#39;re hiring an agent to get you the best value for your hard earned dollar. Make sure they represent you!</p>\r\n<div>\r\n	&nbsp;</div>\r\n', '<p>\r\n	Any agent should have a listof references available to you you should you feel the need to perform some due diligence.</p>\r\n', 'file_1434007070.jpg', 'http://www.facebook.com', '', '', 'https://instagram.com/'),
(3, 'admin1', 'a4cbb2f3933c5016da7e83fd135ab8a48b67bf61', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_answer`
--

CREATE TABLE IF NOT EXISTS `tbl_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer` varchar(255) NOT NULL,
  `question_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=56 ;

--
-- Dumping data for table `tbl_answer`
--

INSERT INTO `tbl_answer` (`id`, `answer`, `question_id`) VALUES
(15, '$1000 To $5000', 7),
(16, '$5000 To $10000', 7),
(17, '$10000 To $15000', 7),
(18, '$15000 To $20000', 7),
(19, '$20000 To $25000', 7),
(20, '3 Months', 8),
(21, '6 Months', 8),
(22, '9 Months', 8),
(23, '1 Year', 8),
(24, '2 Years', 8),
(25, '$1000', 9),
(26, '$2500', 9),
(27, '$5000', 9),
(28, '$7500', 9),
(29, '$10000', 9),
(30, '$1000', 10),
(31, '$2500', 10),
(32, '$5000', 10),
(33, '$7500', 10),
(34, '$10000', 10),
(35, '$5000', 11),
(36, '$10000', 11),
(37, '$15000', 11),
(38, '$20000', 11),
(39, '$25000', 11),
(40, '$30000', 11),
(41, '$100', 12),
(42, '$250', 12),
(43, '$500', 12),
(44, '$750', 12),
(45, '$1000', 12),
(46, '$1250', 12),
(47, '$1500', 12),
(48, '6 Months', 13),
(49, '1 Years', 13),
(50, '2 Years', 13),
(51, '3 Years', 13),
(52, '4 Years', 13),
(53, '5 Years', 13),
(54, 'Yes', 14),
(55, 'No', 14);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact`
--

CREATE TABLE IF NOT EXISTS `tbl_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `inquiry_type` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `tbl_contact`
--

INSERT INTO `tbl_contact` (`id`, `name`, `email`, `phone`, `inquiry_type`, `message`, `ip_address`, `create_date`) VALUES
(1, '', '', '', '', '', '', '0000-00-00 00:00:00'),
(2, 'Nirmala', 'shrestha_nirmala100@yahoo.com', '435436', '4654', 'dgdg', '', '0000-00-00 00:00:00'),
(3, 'gngf', 'shrestha.nirmala100@yahoo.com', '3123122131', 'rgddddd', 'rdgtrete', '', '0000-00-00 00:00:00'),
(4, 'Nirmala uyhuihjiu', 'shrestha.nirmala100@yahoo.com', '3123122131', 'gj', 'hgj', '', '0000-00-00 00:00:00'),
(5, 'samina', 'samina_tuladhar@yahoo.com', '345435', '46456', 'jhdghdkg', '', '0000-00-00 00:00:00'),
(6, 'Nirmala uyhuihjiu', 'shrestha.nirmala100@yahoo.com', '3123122131', '  hjgjhgj', 'gjkgjk', '', '0000-00-00 00:00:00'),
(7, 'nirmala', 'nirmala@smarttech.com.np', '3435', 'jlkh', 'jhkh', '', '0000-00-00 00:00:00'),
(8, 'nirmala shresta', 'nirmala@smarttech.com.np', '34r324', 'wrer', 'dgddgs', '', '0000-00-00 00:00:00'),
(9, 'Nirmala uyhuihjiu', 'shrestha.nirmala100@yahoo.com', '3123122131', 'fdfs', 'sfsd', '', '0000-00-00 00:00:00'),
(10, 'Nirmala uyhuihjiu', 'shrestha.nirmala100@yahoo.com', '3123122131', 'rtret', 'ertrt', '', '0000-00-00 00:00:00'),
(11, 'nirmala', 'shrestha.nirmala100@yahoo.com', '3123122131', 'hjhkh', 'kjhlk', '', '0000-00-00 00:00:00'),
(12, 'jhjk', 'shrestha.nirmala100@yahoo.com', 'jhhkj', 'uhuoyi', 'hgjhhjjg', '', '0000-00-00 00:00:00'),
(13, 'ni', 'shrestha.nirmala100@yahoo.com', '3123122131', 'kjjk', 'what is this?? jon', '', '0000-00-00 00:00:00'),
(14, 'Nirmala uyhuihjiu', 'shrestha.nirmala100@yahoo.com', '3123122131', 'hgjgkj', 'hgjkkjg', '', '0000-00-00 00:00:00'),
(15, 'samina', 'samina.tuladhar7@gmail.com', '567587', 'jhlkl', 'this is a test message', '', '0000-00-00 00:00:00'),
(16, 'gaurav', 'gauroughh@gmail.com', '65887', 'hggj', 'hgkjg', '', '0000-00-00 00:00:00'),
(17, 'nirmala', 's@gmail.com', 'sss', 't5yr5tytryt', 'dghf', '', '0000-00-00 00:00:00'),
(18, 'Nirmala uyhuihjiu', 'shrestha.nirmala100@yahoo.com', '3123122131', 'sdfdf', 'dfd', '', '0000-00-00 00:00:00'),
(19, 'nimrala', 'nitu@gmail.com', '345456', '57567', '54757', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_days`
--

CREATE TABLE IF NOT EXISTS `tbl_days` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `day_no` int(11) NOT NULL COMMENT 'starting from sunday as 1-7',
  `time` tinyint(4) NOT NULL COMMENT '0=>am; 1=>pm',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tbl_days`
--

INSERT INTO `tbl_days` (`id`, `name`, `day_no`, `time`) VALUES
(1, 'Sunday Morning', 7, 0),
(2, 'Sunday Evening', 7, 1),
(3, 'Monday Morning', 1, 0),
(4, 'Monday Evening', 1, 1),
(5, 'Tuesday Morning', 2, 0),
(6, 'Tuesday Evening', 2, 1),
(7, 'Wednesday Morning', 3, 0),
(8, 'Wednesday Evening', 3, 1),
(9, 'Thursday Morning', 4, 0),
(10, 'Thursday Evening', 4, 1),
(11, 'Friday Morning', 5, 0),
(12, 'Friday Evening', 5, 1),
(13, 'Saturday Morning', 6, 0),
(14, 'Saturday Evening', 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_day_slider`
--

CREATE TABLE IF NOT EXISTS `tbl_day_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day_id` int(11) NOT NULL,
  `slider_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tbl_day_slider`
--

INSERT INTO `tbl_day_slider` (`id`, `day_id`, `slider_id`) VALUES
(1, 3, 141),
(2, 4, 142),
(3, 5, 143),
(4, 6, 144),
(5, 7, 145),
(6, 8, 146),
(7, 9, 147),
(8, 10, 148),
(9, 11, 149),
(10, 12, 150),
(11, 13, 151),
(12, 14, 152),
(13, 1, 153),
(14, 2, 154);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_footerbox`
--

CREATE TABLE IF NOT EXISTS `tbl_footerbox` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `f_description` text NOT NULL,
  `url` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `is_active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_footerbox`
--

INSERT INTO `tbl_footerbox` (`id`, `title`, `image`, `f_description`, `url`, `type`, `is_active`) VALUES
(1, 'Current Blog', 'file_1434021212.png', '<p>this is testing</p>\r\n', 'dgdf', 'lending', 0),
(2, 'Current Blog', 'file_1434021591.png', '<p>test</p>\r\n', 'dfdf', 'lending', 1),
(3, 'Next Lising', 'file_1434021793.png', '<p>testing</p>\r\n', 'wwww', 'lending', 1),
(4, 'Current Listings', 'file_1434022862.jpg', '<p>As a leader in Westside real estate, Sacha is always adding new listings. Browse a gallery of Sacha&#39;s hottest current listings and be sure to contact her directly to inquire about exclusive pocket listings or upcoming properties not yet on the.</p>\r\n', '', 'contact', 0),
(5, 'Find Your Next Home', 'file_1434022928.jpg', '<p>From Beverly Hills to Brentwood, Marina del Rey to Malibu, there is simply no shortage of exceptional neighborhoods to explore within the Los Angeles luxury band. Click the button below to begin searching for your next home today.</p>\r\n', '', 'contact', 0),
(6, 'Connecting Buyers & Sellers', 'file_1434022998.jpg', '<p>Sacha Radford&#39;s intimate understanding of Southern California&#39;s luxury communities, combined with her undying commitment to meeting her clients&#39; needs, make her the ideal representative in the fierce world of high-end, Los Angeles real estate.</p>\r\n', '', 'contact', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pages`
--

CREATE TABLE IF NOT EXISTS `tbl_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `description` text NOT NULL,
  `is_active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_property`
--

CREATE TABLE IF NOT EXISTS `tbl_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `p_name` varchar(30) NOT NULL,
  `p_description` text NOT NULL,
  `p_price` varchar(20) NOT NULL,
  `p_featured` tinyint(1) NOT NULL,
  `p_location` varchar(30) NOT NULL,
  `p_sq_feet` varchar(30) NOT NULL,
  `p_bedroom` int(11) NOT NULL,
  `p_bathroom` int(11) NOT NULL,
  `p_lot_size` varchar(30) NOT NULL,
  `p_image` varchar(30) DEFAULT NULL,
  `is_active` int(1) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tbl_property`
--

INSERT INTO `tbl_property` (`id`, `p_name`, `p_description`, `p_price`, `p_featured`, `p_location`, `p_sq_feet`, `p_bedroom`, `p_bathroom`, `p_lot_size`, `p_image`, `is_active`, `slug`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(6, 'New Property', '<p>thi sis&nbsp;</p>\r\n', '23', 1, 'Chicago', '4646', 6768, 6, '575', 'lending3.jpg', 1, '', '', '', ''),
(7, 'ghdhdf', '<p>thtfj</p>\r\n', '44', 1, 'dvdgd', 'yy', 44, 66, 'ryy', 'listing1.jpg', 1, 'ghdhdf', '', '', ''),
(8, 'hello', '<p>soon</p>\r\n', '4546', 1, 'fffddg', '4646', 5656, 5657, 't657', 'lending31.jpg', 1, 'hello', '', '', ''),
(9, 'property', '<p>soon</p>\r\n', '33', 1, 'Chicago', '4546', 6768, 67, '5y56', 'lending3.jpg', 1, 'property', '', '', ''),
(10, 'prop', '<p>grg</p>\r\n', '1', 1, 'Chicago', 'e', 5, 5, 'e', 'lending3.jpg', 1, 'prop', '', '', ''),
(11, 'Property111', '<p>soom</p>\r\n', '1', 1, 'fg', '111', 1, 1, '11', 'lending31.jpg', 1, 'property111', '', '', ''),
(13, 'fdgfd', '<p>testing</p>\r\n', '1', 1, 'fgd', 'ete', 2, 3, 'etet', 'file_1428461374.jpg_main_280_4', 1, 'dfg', '', '', ''),
(14, 'dgdxg', '<p>ss</p>\r\n', '1', 1, 'sfs', 'dfdg', 5, 4, 'ff', 'file_1433746939.jpg', 1, 'rgdg', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_property_images`
--

CREATE TABLE IF NOT EXISTS `tbl_property_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_question`
--

CREATE TABLE IF NOT EXISTS `tbl_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(255) NOT NULL,
  `short_question` varchar(255) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tbl_question`
--

INSERT INTO `tbl_question` (`id`, `question`, `short_question`, `is_active`) VALUES
(7, 'Price range of home you are thinking about?', 'Price range of home', 1),
(8, 'How soon do you want to purchase a home?', 'Time to purchase', 1),
(9, 'How much of a down payment for a purchase?', 'Down payment', 1),
(10, 'How is your credit?', 'Credit', 1),
(11, 'Estimated annual income', 'Annual income', 1),
(12, 'Salary/hourly or self employed?', 'Hourly salary/Self-employed?', 1),
(13, 'How many years on job?', 'Years on job', 1),
(14, 'Do you have a home to sell before you purchase a new home?', 'Home to sell?', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_seo`
--

CREATE TABLE IF NOT EXISTS `tbl_seo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_seo`
--

INSERT INTO `tbl_seo` (`id`, `page`, `meta_title`, `meta_keyword`, `meta_description`, `create_date`, `update_date`) VALUES
(1, 'generic', 'Jon Crocilla', 'Property', 'This is property renting site.', '2015-03-29 12:03:18', '2015-05-21 09:37:24');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_setting`
--

CREATE TABLE IF NOT EXISTS `tbl_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `is_permanent` tinyint(4) NOT NULL,
  `order` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_setting`
--

INSERT INTO `tbl_setting` (`id`, `type`, `slug`, `value`, `is_permanent`, `order`) VALUES
(1, 'Site Name', 'site-name', '<p>GOLD COAST CHICAGO REAL ESTATE&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', 0, 0),
(2, 'Website Title', 'website-title', '<p>\r\n	Making your<br />\r\n	home buying</p>\r\n<p>\r\n	experience again</p>\r\n', 0, 0),
(3, 'Email', 'email', 'shrestha.nirmala100@gmail.com', 0, 0),
(4, 'Banner text', 'banner-text', 'aLOREM IPM DOLOR SIT AMET, CONSEC TETUR ADI CING ELIT, SED DO EIUSMOD TEMPOR IDUNT UT LABORE ET DOLORE MAGNA DOLORE MAGNAMR INCIDIDUNT UT LABORE ETORE DOLORE MAGNA ALIQUA. UQUAT. UISOREORE AUIRURE DEHENDERIT IN VOLUPT”', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slider`
--

CREATE TABLE IF NOT EXISTS `tbl_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(100) NOT NULL,
  `type` varchar(30) NOT NULL,
  `is_active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=187 ;

--
-- Dumping data for table `tbl_slider`
--

INSERT INTO `tbl_slider` (`id`, `image`, `type`, `is_active`) VALUES
(96, '3131.jpg', 'listing', 0),
(120, '10872136_10203395132376738_342607283_n.jpg', 'listing', 0),
(121, '10872136_10203395132376738_342607283_n3.jpg', '', 0),
(122, 'gold_color_blouse5.jpg', '', 0),
(125, 'block19.jpg', 'listing', 0),
(126, 'block320.jpg', 'listing', 0),
(138, 'main-bg1.jpg', 'home', 0),
(139, 'main-bg21.jpg', 'home', 0),
(141, 'about1.jpg', 'home', 0),
(142, 'about.jpg', 'home', 0),
(143, 'bg2.jpg', 'home', 0),
(144, 'bg3.jpg', 'home', 0),
(145, '1487871_10203433714221142_6785241606009387215_o1.jpg', 'home', 0),
(146, 'main-bg23.jpg', 'home', 0),
(147, 'about2.jpg', 'home', 0),
(148, 'main-bg24.jpg', 'home', 0),
(149, 'main-bg.jpg', 'home', 0),
(150, 'main-bg2.jpg', 'home', 0),
(151, 'main-bg7.jpg', 'home', 0),
(152, 'main-bg26.jpg', 'home', 0),
(153, 'main-bg8.jpg', 'home', 0),
(154, 'bg1.jpg', 'home', 0),
(176, 'carousel_home_1400_10001.jpg', 'about', 0),
(177, 'carousel1_about_703_3051.jpg', 'about', 0),
(178, 'lending3.jpg', 'about', 0),
(179, 'listing1.jpg', 'about', 0),
(181, 'lending11.jpg', 'about', 0),
(182, 'lending35.jpg', 'about', 0),
(183, 'lending-background5.jpg', 'about', 0),
(184, 'listing15.jpg', 'about', 0),
(185, 'carousel1.jpg', 'about', 0),
(186, 'coldwell-banker1.png', 'about', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_testimonials`
--

CREATE TABLE IF NOT EXISTS `tbl_testimonials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `t_description` text NOT NULL,
  `address` varchar(50) NOT NULL,
  `address2` varchar(50) NOT NULL,
  `author` varchar(30) NOT NULL,
  `is_active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=104 ;

--
-- Dumping data for table `tbl_testimonials`
--

INSERT INTO `tbl_testimonials` (`id`, `t_description`, `address`, `address2`, `author`, `is_active`) VALUES
(1, '<p>Lorem ipm dolor sit amet, consec tetur adi cing elit, sed do eiusmod tempor idunt ut labore et dolore magna dolore magnamr incididunt ut labore etore dolore magna aliqua. Uquat. uisoreore auirure dehenderit in volupt Lorem ipm dolor sit amet, consec tetur adi cing elit, sed do eiusmod tempor idunt ut labore et dolore magna dolore magnamr incididunt ut labore etore dolore magna aliqua. Uquat. uisoreore auirure dehenderit in volupt</p>\r\n', '', 'Chicago Area', 'John S.', 1),
(2, '“Lorem ipm dolor sit amet, consec tetur adi cing elit, sed do eiusmod tempor idunt ut labore et dolore magna dolore magnamr incididunt ut labore etore dolore magna aliqua. Uquat. uisoreore auirure dehenderit in volupt”', 'xyz, Illinois,', 'Chicago Area', 'John S. Doe', 1),
(3, '“Lorem ipm dolor sit amet, consec tetur adi cing elit, sed do eiusmod tempor idunt ut labore et dolore magna dolore magnamr incididunt ut labore etore dolore magna aliqua. Uquat. uisoreore auirure dehenderit in volupt”', 'xyz, Illinois,', 'Chicago Area', 'John S. Doe', 1),
(4, '“Lorem ipm dolor sit amet, consec tetur adi cing elit, sed do eiusmod tempor idunt ut labore et dolore magna dolore magnamr incididunt ut labore etore dolore magna aliqua. Uquat. uisoreore auirure dehenderit in volupt”', 'xyz, Illinois,', 'Chicago Area', 'John S. Doe', 1),
(5, '“Lorem ipm dolor sit amet, consec tetur adi cing elit, sed do eiusmod tempor idunt ut labore et dolore magna dolore magnamr incididunt ut labore etore dolore magna aliqua. Uquat. uisoreore auirure dehenderit in volupt”', 'xyz, Illinois,', 'Chicago Area', 'John S. Doe', 1),
(6, '“Lorem ipm dolor sit amet, consec tetur adi cing elit, sed do eiusmod tempor idunt ut labore et dolore magna dolore magnamr incididunt ut labore etore dolore magna aliqua. Uquat. uisoreore auirure dehenderit in volupt”', 'asdf', 'asdf', 'asdf', 1),
(7, '“Lorem ipm dolor sit amet, consec tetur adi cing elit, sed do eiusmod tempor idunt ut labore et dolore magna dolore magnamr incididunt ut labore etore dolore magna aliqua. Uquat. uisoreore auirure dehenderit in volupt”', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(8, '“Lorem ipm dolor sit amet, consec tetur adi cing elit, sed do eiusmod tempor idunt ut labore et dolore magna dolore magnamr incididunt ut labore etore dolore magna aliqua. Uquat. uisoreore auirure dehenderit in volupt”', 'asdf', 'asdf', 'asdf', 1),
(9, '“Lorem ipm dolor sit amet, consec tetur adi cing elit, sed do eiusmod tempor idunt ut labore et dolore magna dolore magnamr incididunt ut labore etore dolore magna aliqua. Uquat. uisoreore auirure dehenderit in volupt”', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(10, '“Lorem ipm dolor sit amet, consec tetur adi cing elit, sed do eiusmod tempor idunt ut labore et dolore magna dolore magnamr incididunt ut labore etore dolore magna aliqua. Uquat. uisoreore auirure dehenderit in volupt”', 'asdf', 'asdf', 'asdf', 1),
(11, '“Lorem ipm dolor sit amet, consec tetur adi cing elit, sed do eiusmod tempor idunt ut labore et dolore magna dolore magnamr incididunt ut labore etore dolore magna aliqua. Uquat. uisoreore auirure dehenderit in volupt”', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(12, '“Lorem ipm dolor sit amet, consec tetur adi cing elit, sed do eiusmod tempor idunt ut labore et dolore magna dolore magnamr incididunt ut labore etore dolore magna aliqua. Uquat. uisoreore auirure dehenderit in volupt”', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(13, '“Lorem ipm dolor sit amet, consec tetur adi cing elit, sed do eiusmod tempor idunt ut labore et dolore magna dolore magnamr incididunt ut labore etore dolore magna aliqua. Uquat. uisoreore auirure dehenderit in volupt”', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(14, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(15, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(16, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(17, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(18, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(19, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(20, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(21, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(22, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(23, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(24, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(25, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(26, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(27, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(28, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(29, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(30, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(31, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(32, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(33, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(34, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(35, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(36, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(37, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(38, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(39, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(40, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(41, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(42, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(43, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(44, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(45, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(46, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(47, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(48, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(49, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(50, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(51, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(52, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(53, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(54, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(55, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(56, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(57, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(58, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(59, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(60, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(61, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(62, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(63, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(64, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(65, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(66, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(67, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(68, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(69, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(70, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(71, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(72, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(73, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(74, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(75, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(76, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(77, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(78, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(79, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(80, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(81, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(82, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(83, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(84, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(85, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(86, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(87, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(88, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(89, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(90, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(91, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(92, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(93, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(94, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(95, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(96, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(97, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(98, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(99, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(100, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(101, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(102, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(103, '<p>test</p>\r\n', 'ereer', 'errer', 'Nirmala', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_thumbnail`
--

CREATE TABLE IF NOT EXISTS `tbl_thumbnail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL,
  `width` int(10) NOT NULL,
  `height` int(10) NOT NULL,
  `manager` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=65 ;

--
-- Dumping data for table `tbl_thumbnail`
--

INSERT INTO `tbl_thumbnail` (`id`, `type`, `width`, `height`, `manager`) VALUES
(40, 'featured', 200, 133, 'property'),
(41, 'listing', 479, 300, 'property'),
(43, 'album', 205, 205, 'Gallery'),
(44, 'listing', 240, 240, 'Gallery'),
(45, 'detail', 800, 800, 'Gallery'),
(50, 'main', 280, 480, 'profile'),
(53, 'main', 118, 143, 'aboutjon'),
(54, 'listing', 205, 205, 'galleryalbum'),
(55, 'about', 703, 305, 'slider'),
(56, 'main', 300, 300, 'Lending'),
(57, 'small', 98, 147, 'profile'),
(58, 'main', 300, 300, 'Listing'),
(59, 'Detail', 970, 437, 'slider'),
(60, 'listing', 240, 240, 'slider'),
(61, 'home', 1400, 1000, 'slider'),
(62, 'detailslider', 970, 437, 'property'),
(63, 'detaillisting', 306, 149, 'property'),
(64, 'main', 261, 134, 'footerbox');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_why_me`
--

CREATE TABLE IF NOT EXISTS `tbl_why_me` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_why_me`
--

INSERT INTO `tbl_why_me` (`id`, `title`, `description`) VALUES
(1, 'Experience', 'Anyone can call themselves a Luxury Real Estate Agent, but typically the proof is in the transactions. Handling a multimillion dollar sale can have some very complicated components, both on the listing side and the selling side. High end buyers and sellers are typically handling large transactions in their daily business lives, and they expect the same business acumen.\r\n'),
(2, 'COMMUNITY & NATIONAL AFFILIATIONS', 'Luxury Real Estate Agents are involved in their communities, as well as in national organizations. Many transactions are done off the public MLS, and you want to have an agent that has their ear to the ground. Whether you are a buyer or a seller, you want whoever is representing you to get the phone call when there is a desirable property available.\r\n'),
(3, 'MARKETING KNOWLEDGE', 'A Luxury Broker should be able to speak about their market with utter fluency. If they cannot give you an educated and thoughtful answer, then you probably need to keep looking for a more knowledgeable agent.\r\n'),
(4, 'NEGOTIATING SKILLS', 'Luxury Transactions typically have some complicated components and require refined negation skills. You''re hiring an agent to get you the best value for your hard earned dollar. Make sure they represent you!\r\n'),
(5, 'REFERNCES', 'Any agent should have a list of references available to you you should you feel the need to perform some due diligence.');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
