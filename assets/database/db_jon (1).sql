-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 29, 2015 at 12:17 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_jon`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `full_name` varchar(30) NOT NULL,
  `position` varchar(30) NOT NULL,
  `mobile_number` varchar(30) NOT NULL,
  `work_number` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `experience` text NOT NULL,
  `marketing_des` text NOT NULL,
  `affiliations` text NOT NULL,
  `skills` text NOT NULL,
  `references` text NOT NULL,
  `profile_pic` varchar(100) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `linked_in` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `username`, `password`, `full_name`, `position`, `mobile_number`, `work_number`, `email`, `description`, `experience`, `marketing_des`, `affiliations`, `skills`, `references`, `profile_pic`, `facebook`, `twitter`, `linked_in`) VALUES
(1, 'nirmala', '96876ef57d3ed215720c9001c43e6e61a00a1472', 'Jon Crocilla', 'Ceo', '', '', '', '', '   THis is my experience.', '    How someone makes a life gives deeper insight into who they are, what makes them tick, and how they spend their time they aren''t getting paid for…where their heart is. It is seeing all sides to truly know someone completely. My site is designed to give viewers insight into both what I do and who I am. ', '    How someone makes a life gives deeper insight into who they are, what makes them tick, and how they spend their time they aren''t getting paid for…where their heart is. It is seeing all sides to truly know someone completely. My site is designed to give viewers insight into both what I do and who I am. ', '    How someone makes a life gives deeper insight into who they are, what makes them tick, and how they spend their time they aren''t getting paid for…where their heart is. It is seeing all sides to truly know someone completely. My site is designed to give viewers insight into both what I do and who I am. ', '    How someone makes a life gives deeper insight into who they are, what makes them tick, and how they spend their time they aren''t getting paid for…where their heart is. It is seeing all sides to truly know someone completely. My site is designed to give viewers insight into both what I do and who I am. ', 'file_1427360998.png', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pages`
--

CREATE TABLE IF NOT EXISTS `tbl_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `description` text NOT NULL,
  `is_active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_property`
--

CREATE TABLE IF NOT EXISTS `tbl_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `p_name` varchar(30) NOT NULL,
  `p_description` text NOT NULL,
  `p_price` varchar(20) NOT NULL,
  `p_featured` tinyint(1) NOT NULL,
  `p_location` varchar(30) NOT NULL,
  `p_sq_feet` varchar(30) NOT NULL,
  `p_bedroom` int(11) NOT NULL,
  `p_bathroom` int(11) NOT NULL,
  `p_lot_size` varchar(30) NOT NULL,
  `p_image` varchar(30) DEFAULT NULL,
  `is_active` int(1) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `tbl_property`
--

INSERT INTO `tbl_property` (`id`, `p_name`, `p_description`, `p_price`, `p_featured`, `p_location`, `p_sq_feet`, `p_bedroom`, `p_bathroom`, `p_lot_size`, `p_image`, `is_active`, `slug`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(25, 'testing', '<p>\r\n	this is testing</p>\r\n', '$4299900', 0, 'nayabazar', '3454356', 4, 8, '3454545', 'file_1427345017.jpg', 1, '', '', '', ''),
(26, 'lake side', '<p>\r\n	tyty</p>\r\n', '35445', 1, 'nayabazar', '4656', 46, 454, '454', '31.jpg', 1, '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_seo`
--

CREATE TABLE IF NOT EXISTS `tbl_seo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_seo`
--

INSERT INTO `tbl_seo` (`id`, `page`, `meta_title`, `meta_keyword`, `meta_description`, `create_date`, `update_date`) VALUES
(1, 'main', 'jon', 'jon cocilla', 'this is', '2015-03-29 12:03:18', '2015-03-29 12:04:38');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_setting`
--

CREATE TABLE IF NOT EXISTS `tbl_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slider`
--

CREATE TABLE IF NOT EXISTS `tbl_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `sub_caption` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(100) NOT NULL,
  `type` varchar(30) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL,
  `is_active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `tbl_slider`
--

INSERT INTO `tbl_slider` (`id`, `title`, `caption`, `sub_caption`, `link`, `image`, `type`, `created_date`, `updated_date`, `is_active`) VALUES
(5, 'Slider3', 'Slider caption3', 'This is sub caption3', 'www.link/myslider.com2', 'file_1427438759.jpg', '', '2015-03-27', '0000-00-00', 1),
(6, 'Testing10', 'caption10', 'subcaption10', 'www.link/myslider.com2', 'file_1427546004.jpg', 'listing', '2015-03-28', '2015-03-28', 1),
(7, 'testing', 'caption10', '', 'www.link/myslider.com2', 'file_1427546124.jpg', 'listing', '2015-03-28', '2015-03-28', 1),
(10, 'Testing10', 'caption10', 'subcaption10', 'www.link/myslider.com2', '', 'about', '2015-03-28', '0000-00-00', 1),
(22, 'dggg', 'Slider caption2', 'subcaption10', 'www.link/myslider.com2', '', 'listing', '2015-03-28', '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_testimonials`
--

CREATE TABLE IF NOT EXISTS `tbl_testimonials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `address` varchar(50) NOT NULL,
  `address2` varchar(50) NOT NULL,
  `author` varchar(30) NOT NULL,
  `is_active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_testimonials`
--

INSERT INTO `tbl_testimonials` (`id`, `description`, `address`, `address2`, `author`, `is_active`) VALUES
(1, 'this is awesome', 'nayabazar', 'new road2', 'Jon Crocilla', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_thumbnail`
--

CREATE TABLE IF NOT EXISTS `tbl_thumbnail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL,
  `width` int(10) NOT NULL,
  `height` int(10) NOT NULL,
  `manager` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59 ;

--
-- Dumping data for table `tbl_thumbnail`
--

INSERT INTO `tbl_thumbnail` (`id`, `type`, `width`, `height`, `manager`) VALUES
(40, 'featured', 129, 83, 'property'),
(41, 'listing', 310, 207, 'property'),
(42, 'detail', 670, 447, 'property'),
(43, 'album', 205, 205, 'Gallery'),
(44, 'listing', 240, 240, 'Gallery'),
(45, 'detail', 800, 800, 'Gallery'),
(50, 'main', 170, 170, 'profile'),
(53, 'main', 118, 143, 'aboutjon'),
(54, 'listing', 205, 205, 'galleryalbum'),
(55, 'main', 525, 656, 'slider'),
(56, 'main', 300, 300, 'Lending'),
(57, 'small', 100, 147, 'profile'),
(58, 'main', 300, 300, 'Listing');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
