-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 09, 2015 at 11:00 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_jon`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `full_name` varchar(30) NOT NULL,
  `position` varchar(30) NOT NULL,
  `mobile_number` varchar(30) NOT NULL,
  `work_number` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `excerpt` text NOT NULL,
  `description` text NOT NULL,
  `experience` text NOT NULL,
  `marketing_des` text NOT NULL,
  `affiliations` text NOT NULL,
  `skills` text NOT NULL,
  `references` text NOT NULL,
  `profile_pic` varchar(100) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `linked_in` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `username`, `password`, `full_name`, `position`, `mobile_number`, `work_number`, `email`, `excerpt`, `description`, `experience`, `marketing_des`, `affiliations`, `skills`, `references`, `profile_pic`, `facebook`, `twitter`, `linked_in`) VALUES
(1, 'nirmala', '96876ef57d3ed215720c9001c43e6e61a00a1472', 'Jon Crocilla', 'REAL ESTATE AGENT', '(847) 452-9123 ', '(847) 452-9123 ', 'jon.crocilla@gmail.com', '<p>\r\n	Jon Crocilla always finds a way. When it comes to his clients he stops at nothing to help them achieve their goals. His drive to succeed is something he learned early in life, both from his parents, who operate a mortgage firm, and from competing in football and hockey. He is passionate about real estate and about his clients’ welfare. There is simply no more committed individual than Jon.</p>\r\n<p>\r\n	 </p>\r\n<p>\r\n	Jon also understands the value of creating and maintaining relationships. Real estate is a relationship based enterprise. The power of relationships becomes apparent when you are looking for the right buyer or seller. Being networked with the top producing agents and the clients they represent can mean the difference between a successful or an unsuccessful outcome. Jon’s focus is on success.</p>\r\n<p>\r\n	 </p>', '<p>\r\n	Jon Crocilla always finds a way. When it comes to his clients he stops at nothing to help them achieve their goals. His drive to succeed is something he learned early in life, both from his parents, who operate a mortgage firm, and from competing in football and hockey. He is passionate about real estate and about his clients&rsquo; welfare. There is simply no more committed individual than Jon.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Jon also understands the value of creating and maintaining relationships. Real estate is a relationship based enterprise. The power of relationships becomes apparent when you are looking for the right buyer or seller. Being networked with the top producing agents and the clients they represent can mean the difference between a successful or an unsuccessful outcome. Jon&rsquo;s focus is on success.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	He grew up with real estate and is highly knowledgeable about the Chicago market. He also has a strong understanding of the role of marketing technology and how to use it to the advantage of his clients. With 90% of consumers looking for their next home online, Jon has expert knowledge of eMarketing and social media best practices. He can get your home the greatest amount of exposure possible so that it sells quickly and at the best possible price.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Jon is a lifelong resident of Chicago and graduated from Elmhurst College with a BA in Business and History.</p>\r\n', '\r\n\r\nAnyone can call themselves a Luxury Real Estate Agent, but typically the proof is in the transactions. Handling a multimillion dollar sale can have some very complicated components, both on the listing side and the selling side. High end buyers and sellers are typically handling large transactions in their daily business lives, and they expect the same business acumen.\r\n\r\n', 'A Luxury Broker should be able to speak about their market with utter fluency. If they cannot give you an educated and thoughtful answer, then you probably need to keep looking for a more knowledgeable agent.\r\n', 'Luxury Real Estate Agents are involved in their communities, as well as in national organizations. Many transactions are done off the public MLS, and you want to have an agent that has their ear to the ground. Whether you are a buyer or a seller, you want whoever is representing you to get the phone call when there is a desirable property available.\r\n\r\n', 'Luxury Transactions typically have some complicated components and require refined negation skills. You''re hiring an agent to get you the best value for your hard earned dollar. Make sure they represent you!\r\n\r\n', 'Any agent should have a listof references available to you you should you feel the need to perform some due diligence.', 'file_1428461374.jpg', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact`
--

CREATE TABLE IF NOT EXISTS `tbl_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `inquiry_type` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_contact`
--

INSERT INTO `tbl_contact` (`id`, `name`, `email`, `phone`, `inquiry_type`, `message`, `ip_address`, `create_date`) VALUES
(1, '', '', '', '', '', '', '0000-00-00 00:00:00'),
(2, 'Nirmala', 'shrestha_nirmala100@yahoo.com', '435436', '4654', 'dgdg', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_days`
--

CREATE TABLE IF NOT EXISTS `tbl_days` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `day_no` int(11) NOT NULL COMMENT 'starting from sunday as 1-7',
  `time` tinyint(4) NOT NULL COMMENT '0=>am; 1=>pm',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tbl_days`
--

INSERT INTO `tbl_days` (`id`, `name`, `day_no`, `time`) VALUES
(1, 'Sunday Morning', 7, 0),
(2, 'Sunday Evening', 7, 1),
(3, 'Monday Morning', 1, 0),
(4, 'Monday Evening', 1, 1),
(5, 'Tuesday Morning', 2, 0),
(6, 'Tuesday Evening', 2, 1),
(7, 'Wednesday Morning', 3, 0),
(8, 'Wednesday Evening', 3, 1),
(9, 'Thursday Morning', 4, 0),
(10, 'Thursday Evening', 4, 1),
(11, 'Friday Morning', 5, 0),
(12, 'Friday Evening', 5, 1),
(13, 'Saturday Morning', 6, 0),
(14, 'Saturday Evening', 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_day_slider`
--

CREATE TABLE IF NOT EXISTS `tbl_day_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day_id` int(11) NOT NULL,
  `slider_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pages`
--

CREATE TABLE IF NOT EXISTS `tbl_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `description` text NOT NULL,
  `is_active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_property`
--

CREATE TABLE IF NOT EXISTS `tbl_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `p_name` varchar(30) NOT NULL,
  `p_description` text NOT NULL,
  `p_price` varchar(20) NOT NULL,
  `p_featured` tinyint(1) NOT NULL,
  `p_location` varchar(30) NOT NULL,
  `p_sq_feet` varchar(30) NOT NULL,
  `p_bedroom` int(11) NOT NULL,
  `p_bathroom` int(11) NOT NULL,
  `p_lot_size` varchar(30) NOT NULL,
  `p_image` varchar(30) DEFAULT NULL,
  `is_active` int(1) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `tbl_property`
--

INSERT INTO `tbl_property` (`id`, `p_name`, `p_description`, `p_price`, `p_featured`, `p_location`, `p_sq_feet`, `p_bedroom`, `p_bathroom`, `p_lot_size`, `p_image`, `is_active`, `slug`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(25, 'Once in a Lifetime', '<p>\r\n	Palazzo di Amore represents the ultimate lifestyle. Its extraordinary 25 acres of spectacular vistas over sculpted canyons and jeweled city lights, is both exceedingly private and exceptionally convenient to world famous shopping. The estate is able to comfortably host up</p>\r\n', '4,299,900', 1, 'Lania Lane, Beverly Hills, CA ', '3454356', 4, 8, '3454545', 'file_1428409266.jpg', 1, 'once-in-a-lifetime', '', '', ''),
(27, 'Property', '<p>\r\n	Palazzo di Amore represents the ultimate lifestyle. Its extraordinary 25 acres of spectacular vistas over sculpted canyons and jeweled city lights, is both exceedingly private and exceptionally convenient to world famous shopping. The estate is able to comfortably host up</p>\r\n', '4,554', 0, 'nayabazar', '46436', 4, 46, '46564', 'file_1428409503.jpg', 1, 'testing-property', 'tjkrtjkr', 'rtrty', 'rtttry'),
(30, 'Property15', '<p>\r\n	Palazzo di Amore represents the ultimate lifestyle. Its extraordinary 25 acres of spectacular vistas over sculpted canyons and jeweled city lights, is both exceedingly private and exceptionally convenient to world famous shopping. The estate is able to comfortably host up</p>\r\n', '34,343', 1, 'Lake ', '35454', 6, 5, '5445', 'file_1428409306.jpg', 1, 'property', 'title', 'this is description', 'keyword'),
(32, 'SEASIDE HOUSE', '<p>\r\n	Palazzo di Amore represents the ultimate lifestyle. Its extraordinary 25 acres of spectacular vistas over sculpted canyons and jeweled city lights, is both exceedingly private and exceptionally convenient to world famous shopping. The estate is able to comfortably host up</p>\r\n<p>\r\n	&nbsp;</p>\r\n', '689,990', 1, 'Chicago', '45464', 12, 3, '46546', 'file_1428409333.jpg', 1, 'seaside-house', '', '', ''),
(33, 'New Property', '<p>\r\n	this is new</p>\r\n', '4354', 1, 'Lake Side', '56546', 567567, 676, '56546', 'file_1428489666.jpg', 1, 'new-property', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_property_images`
--

CREATE TABLE IF NOT EXISTS `tbl_property_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `tbl_property_images`
--

INSERT INTO `tbl_property_images` (`id`, `property_id`, `image`) VALUES
(31, 25, '13.jpg'),
(32, 25, '30.jpg'),
(33, 25, '131.jpg'),
(34, 25, '20_1.jpg'),
(35, 25, '301.jpg'),
(36, 25, '31.jpg'),
(37, 25, 'banner1-images.png'),
(38, 25, 'banner-left.jpg'),
(39, 25, 'bg1-bannersequence.jpg'),
(40, 25, 'bg2-bannersequence.jpg'),
(41, 25, 'block1.jpg'),
(42, 25, 'block2.jpg'),
(43, 25, 'block3.jpg'),
(44, 25, 'orange_-_Copy.png'),
(45, 25, 'orange.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_seo`
--

CREATE TABLE IF NOT EXISTS `tbl_seo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_seo`
--

INSERT INTO `tbl_seo` (`id`, `page`, `meta_title`, `meta_keyword`, `meta_description`, `create_date`, `update_date`) VALUES
(1, 'generic', 'Jon Crocilla', 'Property', 'This is property renting site.', '2015-03-29 12:03:18', '2015-04-03 11:34:05');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_setting`
--

CREATE TABLE IF NOT EXISTS `tbl_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_setting`
--

INSERT INTO `tbl_setting` (`id`, `type`, `slug`, `value`) VALUES
(1, 'Site Name', 'site-name', 'GOLD COAST CHICAGO  REAL ESTATE PROPERTIES'),
(2, 'Website Title', 'website-title', 'MAKING YOUR  HOME BUYING EXPERIENCE MY PRIORITY'),
(3, 'Email', 'email', 'shrestha.nirmala100@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slider`
--

CREATE TABLE IF NOT EXISTS `tbl_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(100) NOT NULL,
  `type` varchar(30) NOT NULL,
  `is_active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=141 ;

--
-- Dumping data for table `tbl_slider`
--

INSERT INTO `tbl_slider` (`id`, `image`, `type`, `is_active`) VALUES
(96, '3131.jpg', 'listing', 0),
(120, '10872136_10203395132376738_342607283_n.jpg', 'listing', 0),
(121, '10872136_10203395132376738_342607283_n3.jpg', '', 0),
(122, 'gold_color_blouse5.jpg', '', 0),
(125, 'block19.jpg', 'listing', 0),
(126, 'block320.jpg', 'listing', 0),
(134, 'carousel2.jpg', 'about', 0),
(135, 'carousel3.jpg', 'about', 0),
(138, 'main-bg1.jpg', 'home', 0),
(139, 'main-bg21.jpg', 'home', 0),
(140, 'listing2.jpg', 'about', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_testimonials`
--

CREATE TABLE IF NOT EXISTS `tbl_testimonials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `address` varchar(50) NOT NULL,
  `address2` varchar(50) NOT NULL,
  `author` varchar(30) NOT NULL,
  `is_active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=104 ;

--
-- Dumping data for table `tbl_testimonials`
--

INSERT INTO `tbl_testimonials` (`id`, `description`, `address`, `address2`, `author`, `is_active`) VALUES
(1, 'Lorem ipm dolor sit amet, consec tetur adi cing elit, sed do eiusmod tempor idunt ut labore et dolore magna dolore magnamr incididunt ut labore etore dolore magna aliqua. Uquat. uisoreore auirure dehenderit in volupt', 'xyz, Illinois', 'Chicago Area', 'John S. Doe', 1),
(2, '“Lorem ipm dolor sit amet, consec tetur adi cing elit, sed do eiusmod tempor idunt ut labore et dolore magna dolore magnamr incididunt ut labore etore dolore magna aliqua. Uquat. uisoreore auirure dehenderit in volupt”', 'xyz, Illinois,', 'Chicago Area', 'John S. Doe', 1),
(3, '“Lorem ipm dolor sit amet, consec tetur adi cing elit, sed do eiusmod tempor idunt ut labore et dolore magna dolore magnamr incididunt ut labore etore dolore magna aliqua. Uquat. uisoreore auirure dehenderit in volupt”', 'xyz, Illinois,', 'Chicago Area', 'John S. Doe', 1),
(4, '“Lorem ipm dolor sit amet, consec tetur adi cing elit, sed do eiusmod tempor idunt ut labore et dolore magna dolore magnamr incididunt ut labore etore dolore magna aliqua. Uquat. uisoreore auirure dehenderit in volupt”', 'xyz, Illinois,', 'Chicago Area', 'John S. Doe', 1),
(5, '“Lorem ipm dolor sit amet, consec tetur adi cing elit, sed do eiusmod tempor idunt ut labore et dolore magna dolore magnamr incididunt ut labore etore dolore magna aliqua. Uquat. uisoreore auirure dehenderit in volupt”', 'xyz, Illinois,', 'Chicago Area', 'John S. Doe', 1),
(6, '“Lorem ipm dolor sit amet, consec tetur adi cing elit, sed do eiusmod tempor idunt ut labore et dolore magna dolore magnamr incididunt ut labore etore dolore magna aliqua. Uquat. uisoreore auirure dehenderit in volupt”', 'asdf', 'asdf', 'asdf', 1),
(7, '“Lorem ipm dolor sit amet, consec tetur adi cing elit, sed do eiusmod tempor idunt ut labore et dolore magna dolore magnamr incididunt ut labore etore dolore magna aliqua. Uquat. uisoreore auirure dehenderit in volupt”', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(8, '“Lorem ipm dolor sit amet, consec tetur adi cing elit, sed do eiusmod tempor idunt ut labore et dolore magna dolore magnamr incididunt ut labore etore dolore magna aliqua. Uquat. uisoreore auirure dehenderit in volupt”', 'asdf', 'asdf', 'asdf', 1),
(9, '“Lorem ipm dolor sit amet, consec tetur adi cing elit, sed do eiusmod tempor idunt ut labore et dolore magna dolore magnamr incididunt ut labore etore dolore magna aliqua. Uquat. uisoreore auirure dehenderit in volupt”', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(10, '“Lorem ipm dolor sit amet, consec tetur adi cing elit, sed do eiusmod tempor idunt ut labore et dolore magna dolore magnamr incididunt ut labore etore dolore magna aliqua. Uquat. uisoreore auirure dehenderit in volupt”', 'asdf', 'asdf', 'asdf', 1),
(11, '“Lorem ipm dolor sit amet, consec tetur adi cing elit, sed do eiusmod tempor idunt ut labore et dolore magna dolore magnamr incididunt ut labore etore dolore magna aliqua. Uquat. uisoreore auirure dehenderit in volupt”', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(12, '“Lorem ipm dolor sit amet, consec tetur adi cing elit, sed do eiusmod tempor idunt ut labore et dolore magna dolore magnamr incididunt ut labore etore dolore magna aliqua. Uquat. uisoreore auirure dehenderit in volupt”', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(13, '“Lorem ipm dolor sit amet, consec tetur adi cing elit, sed do eiusmod tempor idunt ut labore et dolore magna dolore magnamr incididunt ut labore etore dolore magna aliqua. Uquat. uisoreore auirure dehenderit in volupt”', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(14, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(15, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(16, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(17, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(18, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(19, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(20, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(21, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(22, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(23, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(24, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(25, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(26, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(27, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(28, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(29, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(30, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(31, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(32, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(33, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(34, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(35, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(36, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(37, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(38, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(39, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(40, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(41, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(42, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(43, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(44, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(45, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(46, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(47, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(48, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(49, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(50, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(51, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(52, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(53, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(54, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(55, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(56, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(57, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(58, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(59, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(60, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(61, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(62, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(63, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(64, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(65, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(66, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(67, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(68, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(69, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(70, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(71, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(72, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(73, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(74, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(75, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(76, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(77, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(78, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(79, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(80, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(81, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(82, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(83, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(84, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(85, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(86, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(87, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(88, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(89, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(90, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(91, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(92, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(93, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(94, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(95, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(96, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(97, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(98, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(99, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(100, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(101, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(102, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1),
(103, 'asgaergfsf', 'adfasd', 'fasdfasddfas', 'dfasdf', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_thumbnail`
--

CREATE TABLE IF NOT EXISTS `tbl_thumbnail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL,
  `width` int(10) NOT NULL,
  `height` int(10) NOT NULL,
  `manager` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=64 ;

--
-- Dumping data for table `tbl_thumbnail`
--

INSERT INTO `tbl_thumbnail` (`id`, `type`, `width`, `height`, `manager`) VALUES
(40, 'featured', 200, 133, 'property'),
(41, 'listing', 479, 300, 'property'),
(43, 'album', 205, 205, 'Gallery'),
(44, 'listing', 240, 240, 'Gallery'),
(45, 'detail', 800, 800, 'Gallery'),
(50, 'main', 280, 450, 'profile'),
(53, 'main', 118, 143, 'aboutjon'),
(54, 'listing', 205, 205, 'galleryalbum'),
(55, 'about', 703, 305, 'slider'),
(56, 'main', 300, 300, 'Lending'),
(57, 'small', 98, 147, 'profile'),
(58, 'main', 300, 300, 'Listing'),
(59, 'Detail', 970, 437, 'slider'),
(60, 'listing', 240, 240, 'slider'),
(61, 'home', 1400, 1000, 'slider'),
(62, 'detailslider', 970, 437, 'property'),
(63, 'detaillisting', 306, 149, 'property');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
