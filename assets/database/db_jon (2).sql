-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 05, 2015 at 12:36 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_jon`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `full_name` varchar(30) NOT NULL,
  `position` varchar(30) NOT NULL,
  `mobile_number` varchar(30) NOT NULL,
  `work_number` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `experience` text NOT NULL,
  `marketing_des` text NOT NULL,
  `affiliations` text NOT NULL,
  `skills` text NOT NULL,
  `references` text NOT NULL,
  `profile_pic` varchar(100) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `linked_in` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `username`, `password`, `full_name`, `position`, `mobile_number`, `work_number`, `email`, `description`, `experience`, `marketing_des`, `affiliations`, `skills`, `references`, `profile_pic`, `facebook`, `twitter`, `linked_in`) VALUES
(1, 'nirmala', '96876ef57d3ed215720c9001c43e6e61a00a1472', 'Jon Crocilla', 'Ceo', '', '', '', '', '<p>\r\n	THis is my experience.</p>\r\n', '<p>\r\n	How someone makes a life gives deeper insight into who they are, what makes them tick, and how they spend their time they aren&#39;t getting paid for&hellip;where their heart is. It is seeing all sides to truly know someone completely. My site is designed to give viewers insight into both what I do and who I am.</p>\r\n', '<p>\r\n	How someone makes a life gives deeper insight into who they are, what makes them tick, and how they spend their time they aren&#39;t getting paid for&hellip;where their heart is. It is seeing all sides to truly know someone completely. My site is designed to give viewers insight into both what I do and who I am.</p>\r\n', '<p>\r\n	How someone makes a life gives deeper insight into who they are, what makes them tick, and how they spend their time they aren&#39;t getting paid for&hellip;where their heart is. It is seeing all sides to truly know someone completely. My site is designed to give viewers insight into both what I do and who I am.</p>\r\n', '<p>\r\n	How someone makes a life gives deeper insight into who they are, what makes them tick, and how they spend their time they aren&#39;t getting paid for&hellip;where their heart is. It is seeing all sides to truly know someone completely. My site is designed to give viewers insight into both what I do and who I am.</p>\r\n', 'file_1427360998.png', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pages`
--

CREATE TABLE IF NOT EXISTS `tbl_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `description` text NOT NULL,
  `is_active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_property`
--

CREATE TABLE IF NOT EXISTS `tbl_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `p_name` varchar(30) NOT NULL,
  `p_description` text NOT NULL,
  `p_price` varchar(20) NOT NULL,
  `p_featured` tinyint(1) NOT NULL,
  `p_location` varchar(30) NOT NULL,
  `p_sq_feet` varchar(30) NOT NULL,
  `p_bedroom` int(11) NOT NULL,
  `p_bathroom` int(11) NOT NULL,
  `p_lot_size` varchar(30) NOT NULL,
  `p_image` varchar(30) DEFAULT NULL,
  `is_active` int(1) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `tbl_property`
--

INSERT INTO `tbl_property` (`id`, `p_name`, `p_description`, `p_price`, `p_featured`, `p_location`, `p_sq_feet`, `p_bedroom`, `p_bathroom`, `p_lot_size`, `p_image`, `is_active`, `slug`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(25, 'testing featured', '<p>\r\n	this is testing</p>\r\n', '$4299900', 1, 'nayabazar', '3454356', 4, 8, '3454545', 'file_1427345017.jpg', 0, '', '', '', ''),
(27, 'New testing111 not featured', '<p>\r\n	this is awesome33</p>\r\n', '4554', 0, 'nayabazar', '46436', 4, 46, '46564', 'file_1427732417.png', 0, 'testing-property', 'tjkrtjkr', 'rtrty', 'rtttry'),
(30, 'Property15', '<p>\r\n	this is test</p>\r\n', '34343', 1, 'Lake ', '35454', 6, 5, '5445', 'file_1427978109.jpg', 1, 'property', 'title', 'this is description', 'keyword');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_seo`
--

CREATE TABLE IF NOT EXISTS `tbl_seo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_seo`
--

INSERT INTO `tbl_seo` (`id`, `page`, `meta_title`, `meta_keyword`, `meta_description`, `create_date`, `update_date`) VALUES
(1, 'generic', 'Jon Crocilla', 'Property', 'This is property renting site.', '2015-03-29 12:03:18', '2015-04-03 11:34:05');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_setting`
--

CREATE TABLE IF NOT EXISTS `tbl_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_setting`
--

INSERT INTO `tbl_setting` (`id`, `type`, `slug`, `value`) VALUES
(1, 'Site Name', 'site-name', 'GOLD COAST CHICAGO  REAL ESTATE PROPERTIES'),
(2, 'Website Title', 'website-title', 'MAKING YOUR  HOME BUYING EXPERIENCE MY PRIORITY'),
(3, 'Email', 'email', 'shrestha.nirmala100@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slider`
--

CREATE TABLE IF NOT EXISTS `tbl_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(100) NOT NULL,
  `type` varchar(30) NOT NULL,
  `is_active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=129 ;

--
-- Dumping data for table `tbl_slider`
--

INSERT INTO `tbl_slider` (`id`, `image`, `type`, `is_active`) VALUES
(96, '3131.jpg', 'listing', 0),
(99, 'block3.jpg', 'about', 0),
(104, 'block16.jpg', 'about', 0),
(105, '3132.jpg', 'about', 0),
(110, '3010.jpg', 'about', 0),
(115, 'banner1-images9.png', 'about', 0),
(117, 'block319.jpg', 'about', 0),
(119, 'orange3.png', 'about', 0),
(120, '10872136_10203395132376738_342607283_n.jpg', 'listing', 0),
(121, '10872136_10203395132376738_342607283_n3.jpg', '', 0),
(122, 'gold_color_blouse5.jpg', '', 0),
(123, '10872136_10203395132376738_342607283_n14.jpg', 'about', 0),
(124, 'gold_color_blouse16.jpg', 'about', 0),
(125, 'block19.jpg', 'listing', 0),
(126, 'block320.jpg', 'listing', 0),
(127, '961428_10203395117816374_455808569_n.jpg', 'about', 0),
(128, '10872136_10203395132376738_342607283_n15.jpg', 'about', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_testimonials`
--

CREATE TABLE IF NOT EXISTS `tbl_testimonials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `address` varchar(50) NOT NULL,
  `address2` varchar(50) NOT NULL,
  `author` varchar(30) NOT NULL,
  `is_active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_testimonials`
--

INSERT INTO `tbl_testimonials` (`id`, `description`, `address`, `address2`, `author`, `is_active`) VALUES
(1, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 'xyz', 'chicago', 'Jon Crocilla', 1),
(2, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 'abc ', 'Lake side', 'Jon Crocilla', 1),
(3, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 'abc', 'Lake view', 'Jon', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_thumbnail`
--

CREATE TABLE IF NOT EXISTS `tbl_thumbnail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL,
  `width` int(10) NOT NULL,
  `height` int(10) NOT NULL,
  `manager` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=61 ;

--
-- Dumping data for table `tbl_thumbnail`
--

INSERT INTO `tbl_thumbnail` (`id`, `type`, `width`, `height`, `manager`) VALUES
(40, 'featured', 129, 83, 'property'),
(41, 'listing', 310, 207, 'property'),
(42, 'detail', 670, 447, 'property'),
(43, 'album', 205, 205, 'Gallery'),
(44, 'listing', 240, 240, 'Gallery'),
(45, 'detail', 800, 800, 'Gallery'),
(50, 'main', 170, 170, 'profile'),
(53, 'main', 118, 143, 'aboutjon'),
(54, 'listing', 205, 205, 'galleryalbum'),
(55, 'About', 670, 291, 'slider'),
(56, 'main', 300, 300, 'Lending'),
(57, 'small', 100, 147, 'profile'),
(58, 'main', 300, 300, 'Listing'),
(59, 'Detail', 970, 437, 'slider'),
(60, 'listing', 240, 240, 'slider');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
