-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 03, 2015 at 07:40 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_jon`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `full_name` varchar(30) NOT NULL,
  `position` varchar(30) NOT NULL,
  `city` varchar(255) NOT NULL,
  `mobile_number` varchar(30) NOT NULL,
  `work_number` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `excerpt` text NOT NULL,
  `about_description` text NOT NULL,
  `experience` text NOT NULL,
  `marketing_des` text NOT NULL,
  `affiliations` text NOT NULL,
  `skills` text NOT NULL,
  `references` text NOT NULL,
  `profile_pic` varchar(100) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `linked_in` varchar(255) NOT NULL,
  `instagram` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `username`, `password`, `full_name`, `position`, `city`, `mobile_number`, `work_number`, `email`, `excerpt`, `about_description`, `experience`, `marketing_des`, `affiliations`, `skills`, `references`, `profile_pic`, `facebook`, `twitter`, `linked_in`, `instagram`) VALUES
(1, 'jonadmin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Jon Crocilla', 'Real Estate Consultant-Broker', '<p>676 N Michigan Ave<br />\nChicago, IL 60611</p>\n', '847.452.9123 ', '847.452.9123 ', 'jon.crocilla@gmail.com', '<p>JCLP was established to bridge the gap between buyers &amp; sellers and the processes, market conditions and experience needed to be succesful in todays real estate market. In order to be successful in the guiding clients properly it is essential that an agent is highly trained and educated in his market. JCLP takes pride in consistently following not only the macro-economic markets of Chicagoland, but the many micro-enviroments that exist with the city of Chicago. &nbsp;</p>\n', '<p>Jon was groomed for a career in real estate as the son of a successful mortgage broker and President of the company. He has gained valuable experience by growing up in the industry, allowing his clients to feel secure and taken care of while pursuing their real estate dreams. With a degree in Business and History from Elmhurst College, Jon understands the fundamental concepts of all real estate transactions and how history has shaped our markets and will continue to do so into the future. His clients quickly realize that there is no agent more dedicated or motivated to produce the results they seek.&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>Listing Specialist<br />\n&nbsp;</p>\n\n<p>Jon understands that listing a property for his clients is much more than simply entering data into the MLS. He is extremely proactive in his approach and is consistently challenging himself to creatively market and create attention to his clients properties in order to maximize exposure. With a highly diverse network of agents at his disposal, he is able to create the market his clients desire through consistent networking. It is essential that his clients feel their home is shed in the best light possible so they are able to receive the highest possible offer for their home. Simply put, Jon is able to help his clients realize their properties full potential in the market. With extremely strong negotiation tactics, Jon is able to deliver what his clients seek in the current market.</p>\n\n<p>&nbsp;</p>\n\n<p>Buyer Consultant &amp; Investment Specialist<br />\n&nbsp;</p>\n\n<p>Jon is able to match his client&#39;s lifestyle and needs to their budget with great care and efficiency. Jon will help his clients get qualified with expert service, identify the desired property and secure the property through agressive and skillful negotiation. Jon takes the initiative to follow the market closely so he is able to guide his client&#39;s through the acquisition of his clients investment, whether that be single-family homes, multi-unit or income producing properties.&nbsp;</p>\n', '<p style="text-align: justify;">\n	Jon was groomed for a career in real estate when his family started a mortgage company over 25 years ago. His background in the financial world has trained him to see beyond the surface and identify the most successful path to making his clients dreams become a reality. Jon has experianced the trials and tribulations of the ever changing markets and uses this experiance to give his clients the ultimate trust in him in their search for their dream home. With zero foreclosures on his and his families business record, his clients can be sure they will be held in the highest regard and with great care.&nbsp;</p>\n', '<div style="text-align: justify;">\n	Coldwell Banker Residential Brokerage grossed $9.6 billion* in sales in 2013 making us one of the most successful real estate companies in the nation.</div>\n<div style="text-align: justify;">\n	&nbsp;</div>\n<div style="text-align: justify;">\n	Coldwell Banker Previews International&reg; agents are closely allied with a powerful network of top real estate professionals in areas all over Chicagoland, the nation and the world &mdash; agents who are likely to be working with buyers who are in the market for properties like yours. To showcase your incomparable property discreetly, powerfully and internationally...choose JCLP.</div>\n', '<p>\n	Coming Soon</p>\n', '<p>\n	Coming Soon</p>\n', '<p>\n	Coming Soon</p>\n', 'file_1434095263.jpg', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_answer`
--

CREATE TABLE IF NOT EXISTS `tbl_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer` varchar(255) NOT NULL,
  `question_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=63 ;

--
-- Dumping data for table `tbl_answer`
--

INSERT INTO `tbl_answer` (`id`, `answer`, `question_id`) VALUES
(15, '$250,000 - $500,000', 7),
(16, '$500,000 - $1,000,000', 7),
(17, '$1,000,000 - $5,000,000', 7),
(18, '$5,000,000 - $10,000,000', 7),
(20, 'ASAP', 8),
(21, '3-6 Months', 8),
(22, '6-12 Months', 8),
(23, '1 Year +', 8),
(25, '3.5%', 9),
(26, '5%', 9),
(27, '5-10%', 9),
(28, '10-25%', 9),
(29, '25%+', 9),
(30, '400 or Under', 10),
(31, '400-550', 10),
(32, '550-650', 10),
(33, '650-750', 10),
(34, '750-850', 10),
(35, '$25,000-$50,000', 11),
(36, '$50,000-$75,000', 11),
(37, '$75,000-$125,000', 11),
(38, '$125,000-$200,000', 11),
(39, '$200,000-$350,000', 11),
(41, 'Salary/Hourly', 12),
(42, 'Self-employed', 12),
(48, '6 Months or less', 13),
(49, '6-12 Months', 13),
(50, '1-4 Years', 13),
(51, '5-10 Years', 13),
(54, 'Yes', 14),
(55, 'No', 14),
(57, 'Other', 7),
(58, '850+', 10),
(59, '$350,000-$500,000', 11),
(60, '$500,000+', 11),
(61, '10 Years+', 13),
(62, '', 13);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blog`
--

CREATE TABLE IF NOT EXISTS `tbl_blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `excerpt` text NOT NULL,
  `b_description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `is_active` int(1) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `total_views` int(11) NOT NULL DEFAULT '0',
  `session_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tbl_blog`
--

INSERT INTO `tbl_blog` (`id`, `title`, `slug`, `excerpt`, `b_description`, `image`, `meta_title`, `meta_keyword`, `meta_description`, `author`, `is_active`, `create_date`, `update_date`, `total_views`, `session_id`) VALUES
(5, 'test', 'test', '<p>test</p>\r\n', '<p>test</p>\r\n', 'file_1435752643.jpg', '', '', '', '', 1, '2015-03-02 08:26:58', '0000-00-00 00:00:00', 5, 'a8504182f2eed347cf817933eb28596c'),
(6, 'housing', 'housing', '<p>testing</p>\r\n', '<p>test</p>\r\n', 'file_1435812309.jpg', '', '', '', '', 1, '2015-04-02 08:26:58', '0000-00-00 00:00:00', 9, 'effe6c6a7ec15b0061051d686f32b582'),
(7, 'testing', 'testing', '<p>test</p>\r\n', '<p>testing</p>\r\n', 'file_1435812518.jpg', '', '', '', '', 1, '2015-07-02 06:48:39', '0000-00-00 00:00:00', 0, 'effe6c6a7ec15b0061051d686f32b582'),
(8, 'next test', 'next-test', '<p>testing</p>\r\n', '<p>new</p>\r\n', 'file_1435812554.jpg', '', '', '', '', 1, '2015-07-02 06:49:15', '0000-00-00 00:00:00', 4, 'a5dd1c1f90b04a60b09f54d23b03e6e4'),
(9, 'latest blog', 'latest-blog', '<p>I mean, most of them eventually reduce the price, right? Time and time again, we see builders of new homes, primarily those over $2,000,000, price the property far higher than anything resembling fair market value, and reduce over, and over, and over, until the property finally sells.</p>\r\n', '<p>tessting</p>\r\n', 'file_1435818418.jpg', '', '', '', '', 1, '2015-07-02 08:26:58', '0000-00-00 00:00:00', 23, 'c4a2d58b956a00508d03c0b282d4df4d');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact`
--

CREATE TABLE IF NOT EXISTS `tbl_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `inquiry_type` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tbl_contact`
--

INSERT INTO `tbl_contact` (`id`, `name`, `email`, `phone`, `inquiry_type`, `message`, `ip_address`, `create_date`) VALUES
(1, '', '', '', '', '', '', '0000-00-00 00:00:00'),
(2, 'Nirmala', 'shrestha_nirmala100@yahoo.com', '435436', '4654', 'dgdg', '', '0000-00-00 00:00:00'),
(3, 'Nirmala uyhuihjiu', 'shrestha.nirmala100@yahoo.com', '3123122131', 'dgfg', 'sdgdfg', '', '0000-00-00 00:00:00'),
(4, 'Nirmala uyhuihjiu', 'shrestha.nirmala100@yahoo.com', '3123122131', 'dgfg', 'sdgdfg', '', '0000-00-00 00:00:00'),
(5, 'Nirmala uyhuihjiu', 'shrestha.nirmala100@yahoo.com', '3123122131', 'dgfg', 'sdgdfg', '', '0000-00-00 00:00:00'),
(6, 'Nirmala uyhuihjiu', 'shrestha.nirmala100@yahoo.com', '3123122131', 'dgfg', 'sdgdfg', '', '0000-00-00 00:00:00'),
(7, 'Nirmala uyhuihjiu', 'shrestha.nirmala100@yahoo.com', '3123122131', 'fgdfg', 'fgfg', '', '0000-00-00 00:00:00'),
(8, 'Nirmala uyhuihjiu', 'shrestha.nirmala100@yahoo.com', '3123122131', 'fgdfg', 'fgfg', '', '0000-00-00 00:00:00'),
(9, 'nirmala', 'shrestha.nirmala100@gmail.com', '354346', '5644', 'gszhxs', '', '0000-00-00 00:00:00'),
(10, 'Nirmala uyhuihjiu', 'shrestha.nirmala100@yahoo.com', '', 'Home Purchase', 'dfgdfhf', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_days`
--

CREATE TABLE IF NOT EXISTS `tbl_days` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `day_no` int(11) NOT NULL COMMENT 'starting from sunday as 1-7',
  `time` tinyint(4) NOT NULL COMMENT '0=>am; 1=>pm',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tbl_days`
--

INSERT INTO `tbl_days` (`id`, `name`, `day_no`, `time`) VALUES
(1, 'Sunday Morning', 7, 0),
(2, 'Sunday Evening', 7, 1),
(3, 'Monday Morning', 1, 0),
(4, 'Monday Evening', 1, 1),
(5, 'Tuesday Morning', 2, 0),
(6, 'Tuesday Evening', 2, 1),
(7, 'Wednesday Morning', 3, 0),
(8, 'Wednesday Evening', 3, 1),
(9, 'Thursday Morning', 4, 0),
(10, 'Thursday Evening', 4, 1),
(11, 'Friday Morning', 5, 0),
(12, 'Friday Evening', 5, 1),
(13, 'Saturday Morning', 6, 0),
(14, 'Saturday Evening', 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_day_slider`
--

CREATE TABLE IF NOT EXISTS `tbl_day_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day_id` int(11) NOT NULL,
  `slider_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tbl_day_slider`
--

INSERT INTO `tbl_day_slider` (`id`, `day_id`, `slider_id`) VALUES
(1, 7, 140),
(2, 8, 141),
(3, 9, 142),
(4, 10, 143),
(5, 3, 163),
(6, 4, 164),
(7, 5, 165),
(8, 6, 166),
(9, 11, 167),
(10, 12, 168),
(11, 13, 169),
(12, 14, 170),
(13, 1, 171),
(14, 2, 172);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_footerbox`
--

CREATE TABLE IF NOT EXISTS `tbl_footerbox` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `f_description` text NOT NULL,
  `url` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `is_active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_footerbox`
--

INSERT INTO `tbl_footerbox` (`id`, `title`, `image`, `f_description`, `url`, `type`, `is_active`) VALUES
(1, 'Current Blog', 'file_1434026790.jpg', '<p>As a leader in Westside real estate, Sacha is always adding new listings. Browse a gallery of Sacha&#39;s hottest current listings and be sure to contact her directly to inquire about exclusive pocket listings or upcoming properties not yet on the.</p>\n', 'dgdf', 'lending', 0),
(2, 'Current Blog', 'file_1434026822.jpg', '<p>As a leader in Westside real estate, Sacha is always adding new listings. Browse a gallery of Sacha&#39;s hottest current listings and be sure to contact her directly to inquire about exclusive pocket listings or upcoming properties not yet on the.</p>\n', 'dfdf', 'lending', 1),
(3, 'Next Lising', 'file_1434026835.jpg', '<p>As a leader in Westside real estate, Sacha is always adding new listings. Browse a gallery of Sacha&#39;s hottest current listings and be sure to contact her directly to inquire about exclusive pocket listings or upcoming properties not yet on the.</p>\n', 'wwww', 'lending', 1),
(4, 'Current Listings', 'file_1434083871.jpg', '<p>As a leader in Westside real estate, Sacha is always adding new listings. Browse a gallery of Sacha&#39;s hottest current listings and be sure to contact her directly to inquire about exclusive pocket listings or upcoming properties not yet on the.</p>\n', '', 'contact', 0),
(5, 'Find Your Next Home', 'file_1434083899.jpg', '<p>From Beverly Hills to Brentwood, Marina del Rey to Malibu, there is simply no shortage of exceptional neighborhoods to explore within the Los Angeles luxury band. Click the button below to begin searching for your next home today.</p>\n', '', 'contact', 0),
(6, 'Connecting Buyers & Sellers', 'file_1434083968.jpg', '<p>Sacha Radford&#39;s intimate understanding of Southern California&#39;s luxury communities, combined with her undying commitment to meeting her clients&#39; needs, make her the ideal representative in the fierce world of high-end, Los Angeles real estate.</p>\n', '', 'contact', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pages`
--

CREATE TABLE IF NOT EXISTS `tbl_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `description` text NOT NULL,
  `is_active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_property`
--

CREATE TABLE IF NOT EXISTS `tbl_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `p_name` varchar(30) NOT NULL,
  `p_description` text NOT NULL,
  `p_price` varchar(20) NOT NULL,
  `p_featured` tinyint(1) NOT NULL,
  `p_location` varchar(30) NOT NULL,
  `p_sq_feet` varchar(30) NOT NULL,
  `p_bedroom` int(11) NOT NULL,
  `p_bathroom` int(11) NOT NULL,
  `p_lot_size` varchar(30) NOT NULL,
  `p_image` varchar(30) DEFAULT NULL,
  `is_active` int(1) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `tbl_property`
--

INSERT INTO `tbl_property` (`id`, `p_name`, `p_description`, `p_price`, `p_featured`, `p_location`, `p_sq_feet`, `p_bedroom`, `p_bathroom`, `p_lot_size`, `p_image`, `is_active`, `slug`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(25, 'Coming Soon', '<p>Coming Soon</p>\n', '4,299,900', 1, 'Chicago', '3454356', 4, 8, '3454545', 'file_1428582842.jpg', 1, 'once-in-a-lifetime', '', '', ''),
(30, 'Coming Soon ', '<p>Coming Soon</p>\n', '34,343', 1, 'Lake ', '35454', 6, 5, '5445', 'file_1428582875.jpg', 1, 'property', 'title', 'this is description', 'keyword');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_property_images`
--

CREATE TABLE IF NOT EXISTS `tbl_property_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=56 ;

--
-- Dumping data for table `tbl_property_images`
--

INSERT INTO `tbl_property_images` (`id`, `property_id`, `image`) VALUES
(46, 25, 'gallery-banner11.jpg'),
(47, 25, 'gallery-banner12.jpg'),
(48, 25, 'gallery-banner13.jpg'),
(49, 27, 'property.jpg'),
(52, 30, 'property3.jpg'),
(53, 25, 'property4.jpg'),
(54, 25, 'gallery-banner14.jpg'),
(55, 25, 'listing1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_question`
--

CREATE TABLE IF NOT EXISTS `tbl_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(255) NOT NULL,
  `short_question` varchar(255) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tbl_question`
--

INSERT INTO `tbl_question` (`id`, `question`, `short_question`, `is_active`) VALUES
(7, 'What is the price range of your desired property?', 'Pricing of the property?', 1),
(8, 'How soon would you like to purchase a property?', 'Time to purchase', 1),
(9, 'How much do you anticipate as a down payment on the property? ', 'Down payment', 1),
(10, 'What is your credit score? (Estimated if not known)', 'Credit score', 1),
(11, 'What is your estimated annual income?', 'Annual income', 1),
(12, 'Salary/hourly or self-employed?', 'Hourly salary/Self-employed?', 1),
(13, 'How many years on job?', 'Years on job', 1),
(14, 'Do you have a home to sell before you purchase a new home?', 'Home to sell?', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_seo`
--

CREATE TABLE IF NOT EXISTS `tbl_seo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_seo`
--

INSERT INTO `tbl_seo` (`id`, `page`, `meta_title`, `meta_keyword`, `meta_description`, `create_date`, `update_date`) VALUES
(1, 'generic', 'Jon Crocilla Luxury Properties', 'Property', 'This is property renting site.', '2015-03-29 12:03:18', '2015-04-14 15:59:02');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_setting`
--

CREATE TABLE IF NOT EXISTS `tbl_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `order` int(11) NOT NULL,
  `is_permanent` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_setting`
--

INSERT INTO `tbl_setting` (`id`, `type`, `slug`, `value`, `order`, `is_permanent`) VALUES
(1, 'Site Name', 'site-name', '<p>REAL ESTATE Listing&nbsp;&amp;<br />\r\nINVESTMENT&nbsp;SPECIALISTS</p>\r\n', 0, 1),
(2, 'Website Title', 'website-title', 'Jon Crocilla Luxury Properties', 0, 1),
(3, 'Email', 'email', 'jon.crocilla@cbexchange.com', 0, 1),
(5, 'Banner text', 'banner-text', 'LOREM IPM DOLOR SIT AMET, CONSEC TETUR ADI CING ELIT, SED DO EIUSMOD TEMPOR IDUNT UT LABORE ET DOLORE MAGNA DOLORE MAGNAMR INCIDIDUNT UT LABORE ETORE DOLORE MAGNA ALIQUA. UQUAT. UISOREORE AUIRURE DEHENDERIT IN VOLUPT”', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slider`
--

CREATE TABLE IF NOT EXISTS `tbl_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(100) NOT NULL,
  `type` varchar(30) NOT NULL,
  `is_active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=178 ;

--
-- Dumping data for table `tbl_slider`
--

INSERT INTO `tbl_slider` (`id`, `image`, `type`, `is_active`) VALUES
(96, '3131.jpg', 'listing', 0),
(120, '10872136_10203395132376738_342607283_n.jpg', 'listing', 0),
(121, '10872136_10203395132376738_342607283_n3.jpg', '', 0),
(122, 'gold_color_blouse5.jpg', '', 0),
(125, 'block19.jpg', 'listing', 0),
(126, 'block320.jpg', 'listing', 0),
(138, 'main-bg1.jpg', 'home', 0),
(139, 'main-bg21.jpg', 'home', 0),
(140, 'wednesday-141.jpg', 'home', 1),
(141, 'jon-crocilla-23.jpg', 'home', 1),
(142, 'jon-crocilla-11.jpg', 'home', 0),
(143, 'jon-crocilla-2.jpg', 'home', 0),
(163, 'jon-crocilla-1.jpg', 'home', 0),
(164, 'jon-crocilla-22.jpg', 'home', 0),
(165, 'jon-crocilla-14.jpg', 'home', 0),
(166, 'jon-crocilla-24.jpg', 'home', 0),
(167, 'jon-crocilla-12.jpg', 'home', 0),
(168, 'jon-crocilla-21.jpg', 'home', 0),
(169, 'wednesday-14.jpg', 'home', 0),
(170, 'jon-crocilla-25.jpg', 'home', 0),
(171, 'wednesday-142.jpg', 'home', 0),
(172, 'jon-crocilla-26.jpg', 'home', 0),
(177, 'jclp-about.jpg', 'about', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_testimonials`
--

CREATE TABLE IF NOT EXISTS `tbl_testimonials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `t_description` text NOT NULL,
  `address` varchar(50) NOT NULL,
  `address2` varchar(50) NOT NULL,
  `author` varchar(30) NOT NULL,
  `is_active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=105 ;

--
-- Dumping data for table `tbl_testimonials`
--

INSERT INTO `tbl_testimonials` (`id`, `t_description`, `address`, `address2`, `author`, `is_active`) VALUES
(1, '<p>Jon is knowledgable, reliable, hungry for oppourtunity, and professional. It has been a pleasure to work with him on multiple transactions. He adapts to every situation and does not shy away from complicated matters. He is quick to repsond to concerns and he possesses a realistic approach to each issue presented. We hold Coldwell Banker in high regard as a result of our relationship with Jon</p>\n', '', 'Chicago, IL', 'Megan H.', 1),
(104, '<p>We are so pleased with Jon and his outstanding service given when we were selling our home. His market expertise, maximizing exposure and screening of qualified buyers was paramount in getting our home sold extremely quickly. Jon was extremely aggressive in negotiations and we had a solid contract within hours and could not have been more pleased!</p>\n', '', 'Hoffman Estates, IL', 'Deborah B. ', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_thumbnail`
--

CREATE TABLE IF NOT EXISTS `tbl_thumbnail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL,
  `width` int(10) NOT NULL,
  `height` int(10) NOT NULL,
  `manager` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=68 ;

--
-- Dumping data for table `tbl_thumbnail`
--

INSERT INTO `tbl_thumbnail` (`id`, `type`, `width`, `height`, `manager`) VALUES
(40, 'featured', 200, 133, 'property'),
(41, 'listing', 479, 300, 'property'),
(43, 'album', 205, 205, 'Gallery'),
(44, 'listing', 240, 240, 'Gallery'),
(45, 'detail', 800, 800, 'Gallery'),
(50, 'main', 280, 480, 'profile'),
(53, 'main', 118, 143, 'aboutjon'),
(54, 'listing', 205, 205, 'galleryalbum'),
(55, 'about', 703, 305, 'slider'),
(56, 'main', 300, 300, 'Lending'),
(57, 'small', 98, 147, 'profile'),
(58, 'main', 300, 300, 'Listing'),
(59, 'Detail', 970, 437, 'slider'),
(60, 'listing', 240, 240, 'slider'),
(61, 'home', 1400, 1000, 'slider'),
(62, 'detailslider', 970, 437, 'property'),
(63, 'detaillisting', 306, 149, 'property'),
(64, 'main', 261, 134, 'footerbox'),
(65, 'large', 702, 351, 'blog'),
(66, 'small', 250, 250, 'blog'),
(67, 'top_stories', 70, 70, 'blog');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_why_me`
--

CREATE TABLE IF NOT EXISTS `tbl_why_me` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_why_me`
--

INSERT INTO `tbl_why_me` (`id`, `title`, `description`) VALUES
(1, 'Experience', 'Anyone can call themselves a Luxury Real Estate Agent, but typically the proof is in the transactions. Handling a multimillion dollar sale can have some very complicated components, both on the listing side and the selling side. High end buyers and sellers are typically handling large transactions in their daily business lives, and they expect the same business acumen.\r\n'),
(2, 'COMMUNITY & NATIONAL AFFILIATIONS', 'Luxury Real Estate Agents are involved in their communities, as well as in national organizations. Many transactions are done off the public MLS, and you want to have an agent that has their ear to the ground. Whether you are a buyer or a seller, you want whoever is representing you to get the phone call when there is a desirable property available.\r\n'),
(3, 'MARKETING KNOWLEDGE', 'A Luxury Broker should be able to speak about their market with utter fluency. If they cannot give you an educated and thoughtful answer, then you probably need to keep looking for a more knowledgeable agent.\r\n'),
(4, 'NEGOTIATING SKILLS', 'Luxury Transactions typically have some complicated components and require refined negation skills. You''re hiring an agent to get you the best value for your hard earned dollar. Make sure they represent you!\r\n'),
(5, 'REFFERNCES', '<p>Any agent should have a list of references available to you you should you feel the need to perform some due diligence.</p>\n');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
