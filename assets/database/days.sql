-- --------------------------------------------------------

--
-- Table structure for table `tbl_days`
--

CREATE TABLE IF NOT EXISTS `tbl_days` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `day_no` int(11) NOT NULL COMMENT 'starting from sunday as 1-7',
  `time` tinyint(4) NOT NULL COMMENT '0=>am; 1=>pm',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tbl_days`
--

INSERT INTO `tbl_days` (`id`, `name`, `day_no`, `time`) VALUES
(1, 'Sunday Morning', 7, 0),
(2, 'Sunday Evening', 7, 1),
(3, 'Monday Morning', 1, 0),
(4, 'Monday Evening', 1, 1),
(5, 'Tuesday Morning', 2, 0),
(6, 'Tuesday Evening', 2, 1),
(7, 'Wednesday Morning', 3, 0),
(8, 'Wednesday Evening', 3, 1),
(9, 'Thursday Morning', 4, 0),
(10, 'Thursday Evening', 4, 1),
(11, 'Friday Morning', 5, 0),
(12, 'Friday Evening', 5, 1),
(13, 'Saturday Morning', 6, 0),
(14, 'Saturday Evening', 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_day_slider`
--

CREATE TABLE IF NOT EXISTS `tbl_day_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day_id` int(11) NOT NULL,
  `slider_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
