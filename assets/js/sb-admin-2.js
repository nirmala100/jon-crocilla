

$(function() {
    
    $( "#datepicker" ).datepicker();

    $('a.delete_ans').click(function (e){ 
        e.preventDefault();
        var that = $(this);
        var id = that.data('id');

        if(!isNaN(id) && id != ''){
            $.ajax({
                url: $('#baseurl').val() + 'admin/questionnaire/delete-ans',
                type: 'POST',
                data: {id: id},
                success: function(res){
                    if(res == 1){
                        that.parent().remove();
                    }
                    else{
                        alert('can not delete at the moment');
                    }
                    
                }
            });
        }
        else{
            that.parent().find('input[type=text]').val('');
            that.parent().remove();
        }
    });

    $('#side-menu').metisMenu();

    var counter = 1;
    $("#add_th").click(function() {
        var table = '<tr id = "thumb_div_' + counter + '">';
        table += '<td><input  type="text" class="form-control" name="type[]"></td>';
        table += '<td><input type="text" class="form-control" name="width[]" value=""></td>';
        table += '<td><input type="text" class="form-control" name="height[]" value=""></td>';
        table += '</tr>';
        counter++;
        $(".table").append(table);
    });

    var ans_counter = 1;
    $("#add_th_for_ans").click(function() {
        var field = '<div class="form-group" id = "answer_' + ans_counter + '" >';
        field += '<input  type="text" class="form-control" name="answer_new[]" autofocus>';
        field += '<a href="#" class="delete_ans">Delete</a>';
        field += '</div>';
        ans_counter++;
        $(".ans_field").append(field);

        $('a.delete_ans').click(function (e){ 
            e.preventDefault();
            var that = $(this);
            that.parent().find('input[type=text]').val('');
            that.parent().remove();
        });                
    });

    


});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse')
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse')
        }

        height = (this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height;
        height = height - topOffset;
        if (height < 1)
            height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    })
});

$(document).ready(function() {
    function change_status() {
    alert("okay");     
    } 



    $('.for_change_status select').change( function (){ 
            
        var that = $(this);         
        var id = that.data('id');
          if(!isNaN(id) && id > 0){
            $.ajax({
                url: that.data('baseurl'),
                type: 'POST',
                data: {id: id, active: that.val()},
                success: function(res){ console.log(res);
                    if(res == 0){
                        alert('error! try again');
                    }
                }
            });
        }
    });        


    $('.dataTables-table').dataTable();
    
    $('#blog_type').change(function(){
         var type = $(this).val();
           $.ajax({
               data:'type='+type,
               method:'post',
               url:$("#categories_url").val(),
               success:function(data){
                   $('#category_id').html(data);
               }
           })
     });


});
