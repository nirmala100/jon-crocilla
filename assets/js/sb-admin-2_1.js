

$(function() {

    $('#side-menu').metisMenu();

    var counter = 1;
    $("#add_th").click(function() {
        var table = '<tr id = "thumb_div_' + counter + '">';
        table += '<td><input  type="text" class="form-control" name="type[]"></td>';
        table += '<td><input type="text" class="form-control" name="width[]" value=""></td>';
        table += '<td><input type="text" class="form-control" name="height[]" value=""></td>'
        table += '</tr>';
        counter++;
        $(".table").append(table);
    });
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse')
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse')
        }

        height = (this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height;
        height = height - topOffset;
        if (height < 1)
            height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    })
});

$(document).ready(function() {
    function change_status() {
    alert("okay");             
} 

    $('.dataTables-table').dataTable();
    
    $('#blog_type').change(function(){
         var type = $(this).val();
           $.ajax({
               data:'type='+type,
               method:'post',
               url:$("#categories_url").val(),
               success:function(data){
                   $('#category_id').html(data);
               }
           })
     });
});
