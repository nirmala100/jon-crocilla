$(document).ready(function() {

    $('#fetch-ts').click(function() {
        var _this = this;
        var url = $(_this).data('url');
        var totalStory = $(_this).attr('data-total-story');
        $.get(url, {
            'totalStory': totalStory
        }, function(res) {
            $('.stories').append(res).hide().fadeIn();
            $('#fetch-ts').attr('data-total-story', parseInt(totalStory)+3);
        });
    });

    $('.bg-dropshadow').matchHeight({
        property: 'height',
        target: null,
        remove: false
    });

    var $active = $('#accordion .panel-collapse.in').prev().addClass('active');
    $active.find('a').prepend('<i class="glyphicon glyphicon-minus pull-right"></i>');


    $('#accordion .panel-heading')
        .not($active).find('a')
        .prepend('<i class="glyphicon glyphicon-plus pull-right"></i>');

    $('.collapse').on('show.bs.collapse', function() {
        $(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
    }).on('hide.bs.collapse', function() {
        $(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
    });


    var getSlideWidth = 217;

    if ($(window).width() <= 415) {
        getSlideWidth = $(window).width();
    }
    $('.featured-slider').bxSlider({
        nextText: '<i class="fa fa-caret-right"></i>',
        prevText: '<i class="fa fa-caret-left"></i>',
        maxSlides: 3,
        minSlides: 1,
        infiniteLoop: false,
        slideMargin: 10,
        slideSelector: $('.featured-slider li'),
        slideWidth: getSlideWidth
    });

    var getSlideWidth = 282;

    if ($(window).width() <= 415) {
        getSlideWidth = $(window).width();
    }
    $('.testi-monials').bxSlider({
        nextText: '<i class="fa fa-caret-right"></i>',
        prevText: '<i class="fa fa-caret-left"></i>',
        maxSlides: 1,
        minSlides: 1,
        slideMargin: 5,
        moveSlides: 1,
        slideWidth: getSlideWidth,
        infiniteLoop: false
    });

    $('a[rel=gallery-images]').colorbox({
        maxWidth: '100%',
        maxHeight: '100%',
        scrolling: false,
    });

    if($(window).width() > 767){

        $('.custom-jselect').selectbox()
        /*for (i = 1; i <= clas; i++) {
            $("#custom-select-box-" + i).selectbox();
        }*/
        $(".custom-select-box-city").selectbox();
        $(".custom-select-box-time").selectbox();

    }


    jQuery.validator.addMethod("notEqual", function(value, element, param) {
        return this.optional(element) || value !== param;
    },function(value, element, param) {
        return "Please choose an option!";
    });

    $('#questionnaire-form').validate({
        onfocusout: false,
            onkeyup: false,
            onclick: false,
        ignore:[],
        errorPlacement: function(error, element) {
              element.parent().append(error);
        }
    });
    $('#questionnaire-form .custom-jselect').each(function(){
        $(this).rules("add", {
            onfocusout: false,
            onkeyup: false,
            onclick: false,
            required: true,
            notEqual:'N/A',
            messages: {
                required: "Please choose an option!",
            }
        });
        //$(this).siblings('.sbHolder').find('li>a[rel="N/A"]').parent().hide();
    });

    $('.contact_info_form').validate({
        rules: {
            first_name: {
                required: true
            },
            last_name: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            inquiry_type: {
                required: true
            },
            phone_no: {
                required: true,
                number: true,
                min: 7
            },
            address_1: {
                required: true
            },
            city: {
                required: true
            },
            zip: {
                required: true,
                number: true
            }
        },
        messages: {
            first_name: {
                required: "Please enter your first name."
            },
            last_name: {
                required: "Please enter your last name."
            },
            email: {
                required: "Please enter your email id.",
                email: "Please enter a valid email id."
            },
            phone_no: {
                required: "Please enter your phone no.",
                number: "Please enter a valid phone no.",
                min: "Please enter a valid phone no."
            },
            address_1: {
                required: "Please enter your address."
            },
            city: {
                required: "Please enter your city."
            },
            zip: {
                required: "Please enter your Zip Code",
                number: "Please enter a valid Zip Code."
            }
        },
        submitHandler: function(form) {
            submitContactInfoForm(form);
        }
    });


    function submitContactInfoForm(form) {
        if (typeof form !== 'undefined') {
            $.ajax({
                data: $(form).serialize(),
                url: $('#baseurl').val() + 'contact-info',
                type: 'post',
                success: function(data) {
                    console.log(data);
                    var parseData = jQuery.parseJSON(data);
                    if (parseData.status == 'success') {
                        location.href = $('#baseurl').val() + 'submission';
                    } else {
                        alert(parseData.message);
                    }
                }
            });
        }
    }


    /**************************states of usa dropdown--contact info*****************************/

    var url = document.URL;
    var segmentArr = url.split('/');
    var lastSegment = segmentArr[segmentArr.length - 1]; //console.log(lastSegment);
    if (lastSegment == 'contact-info') {
        $.getJSON(segmentArr[0] + segmentArr[1] + "assets/json/states_of_usa.json", function(data) {
            var items = [];
            $.each(data, function(key, val) {
                items.push("<option value='" + val + "'>" + val + "</option>");
            });

            $('.form-states-usa').append(items);
        });
    }



    $('.info_form_submit').validate({
        rules: {
            first_name: {
                required: true,

            },
            email: {
                required: true,
                email: true
            },
            last_name: {
                required: true
            }
        },
        /*messages: {
            first_name: {
                required: "Please enter your First Name."
            },
            email: {
                required: "Please enter your email address."
            },
            last_name: {
                required: "Please enter your Last Name."
            }
        },*/
        errorPlacement: function() {
            return false; // suppresses error message text
        },
        submitHandler: function(form) {
            submitInfoForm(form);

        }


    });

    function submitInfoForm(form) {
        $.ajax({
            data: $(form).serialize(),
            url: $('#base-url').val() + 'home/more_info',
            type: 'post',
            success: function(data) {
                    console.log(data);
                    if (data) {

                        var response = $.parseJSON(data);
                        //var response = jQuery.parseJSON(data)
                        if (response.status == 'success') {
                            //alert(response.message);
                            $(form)[0].reset();
                            var msg = "Thankyou for the information ";
                            $("#listing_info_msg").html(msg);

                        } else if (response.status == 'failed') {
                            alert(response.message);
                        }

                    } //data close

                } //success close
        }); //ajax close
    }

 
 //for testimonials box equal height
 $('.textimonials-box').matchHeight({
        property: 'height',
        target: null,
        remove: false
    });


   
   $('.approved_form').validate({ // initialize the plugin

        // your rules and options,
        rules:{
            first_name: {
                required: true,

            },
            email: {
                required: true,
                email: true
            },
            last_name: {
                required: true
            },
            phone_no:{
                number:true
            }


        },
        
        highlight: function(element){
            $(element).addClass('infoerror');
        },
        errorPlacement: function(){
            return false;  // suppresses error message text
        },

        submitHandler: function(form) { //alert('ok');
            form.submit();
        }
    });


     $('.property-box').matchHeight({
        property: 'height',
        target: null,
        remove: false
    });

    if($(".for-index").length>0){
        var fixednav=$(".for-index").offset().top;
         $(window).scroll(function(){
            var wScroll=$(this).scrollTop();
        
           
               if(wScroll > fixednav){
                    if(!$(".for-index").hasClass('fixed-nav')){
                        $(".for-index").addClass('fixed-nav');
                    }
                    // $(".fixed-nav").sticky({topSpacing:0});

                }else{
                    $(".for-index").removeClass('fixed-nav');
                }
            
           
         });
    }
     

   

});



