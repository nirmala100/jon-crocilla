<main>
    <div class="container contain contain-white testimonials">


        <div class="row">
            <div class="col-md-12">
                <h1>testimonials:</h1>
            </div>
        </div><!-- row -->
        <?php
        if ($all_testimonial):
            $i = 1; //for numbering value S.N.
            ?>
            <div class="testimonial" >
                <?php
                foreach ($all_testimonial as $testimonial):
                    ?>
                    <div class="testimonial-post" id ="<?php echo "testimonial_" . $testimonial->id; ?>" >
                        <div class="row" >

                            <div class="col-md-10 col-md-offset-1 text-center">

                                <div class="testimonials-wrap">
                                    <p><?php echo $testimonial->t_description; ?></p>
                                    <img src="<?php echo base_url('assets/images/testimonials-comma.jpg'); ?>" alt="">
                                </div>

                                <div class="person-details">
                                    <div class="person-name">
                                        <?php echo $testimonial->author; ?>
                                    </div>
                                    <div class="person-address">
                                        <?php echo  $testimonial->address2; ?>
                                    </div>
                                </div><!-- person-details -->

                            </div><!-- column -->

                        </div><!-- row -->
                    </div>
                    <?php
                    $i++;

                endforeach;
                ?>
                <a href="<?php echo base_url('next-testimonial/' . $start); ?>" id="next_url"></a>
            </div>
        <?php else:
            ?>
           <!--  <div>There are no testimonials.</div> -->
        <?php endif; ?>

    </div><!-- container -->
</main>
</div><!-- bg-pattern-grey -->

