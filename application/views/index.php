
<div class="container contain cold-well-logo">

    <div class="row mb27 text-center">
        <div class="col-md-12 mt17">
            <img src="<?php echo base_url('assets/images/previews.png');?>" alt="">
        </div><!-- column -->
    </div><!-- row -->

    <div class="row">
        <div class="col-md-8 col-md-offset-2 bg-fade">
            <div class="heading text-center uppercase">
                <?php //echo $websitetitle; ?>
                DELIVERING <Br>LIFESTYLES WITH <br><span class="big">EXCELLENCE </span>
            </div>
        </div>
    </div><!-- row -->
    </div><!--ends static banner-->
<?php /*
    <div class="row">
        <div class="col-md-12 search-bx">

            <div class="search-heading">
                <h3>search for property</h3>
            </div><!-- search-heading -->
            <div class="search-body">

                <script type="text/javascript" src="http://idxre.com/idx/searchquick_js.cfm?cid=79193&style=horizontal"></script>
            </div>
            <?php /* <form class="search-body">

<div class="row">

<div class="col-md-4 col-sm-2 col-xs-12">
<h4>located in:</h4>

<select class="form-control customised-select" id="state_id">
<optgroup>
<option>Chicago</option>
<option>Alabama</option>
</optgroup>
</select>

</div><!-- column -->

<div class="col-md-2 col-sm-4 col-xs-12 text-center">
<h4 class="line">cost between</h4>
<div class="row">
<div class="col-md-6 col-sm-6 col-xs-6" id="min_id">
<select class="form-control customised-select">
<optgroup>
<option>Min. Price</option>
<option>$2,000,000</option>
<option>$2,000,000</option>
<option>$2,000,000</option>
</optgroup>
</select>
</div>
<div class="col-md-6 col-sm-6 col-xs-6" id="max_id">
<select class="form-control customised-select">
<option>Max. Price</option>
<option>$3,000,000</option>
<option>$3,000,000</option>
<option>$3,000,000</option>
</select>
</div>
</div>
</div><!-- column -->

<div class="col-md-2 col-sm-4 col-xs-12">
<div class="row">
<div class="col-md-6 col-sm-6 col-xs-6">
<h4># of beds:</h4>
<select class="form-control customised-select" id="beds_id">
<option>&lt;select&gt;</option>
</select>
</div>
<div class="col-md-6 col-sm-6 col-xs-6">
<h4># of baths:</h4>
<select class="form-control customised-select" id="baths_id">
<option>&lt;select&gt;</option>
</select>
</div>
</div>
</div><!-- column -->

<div class="col-md-4 col-sm-2 col-xs-12 chng-width">
<button type="submit" class="btn srch-btn"><i class="fa fa-search"></i>&nbsp;&nbsp;search</button>
</div><!-- column -->

</div><!-- row -->

</form><!-- search-body --> //close?>

        </div><!-- column -->
    </div><!-- row -->

</div><!-- contain -->
*/?>
</div><!-- bg-main-img -->

<div class="container contain for-nav main-nav visible-xs">
    <div class="row">
        <div class="col-md-12">


            <nav class="navbar navbar-default custom-nav">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#fixed-navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?php echo base_url();?>"><img width="260" src="<?php echo base_url('assets/images/mobile-nav-btm.png');?>" alt="" class="img-responsive"></a>
                    </div><!-- navbar-header -->

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="fixed-navbar">
                        <ul class="nav navbar-nav">
                            <li><a onclick="" href="<?php echo base_url();?>" class="active">home</a></li>
                            <li><a onclick="" href="<?php echo base_url('about-jclp');?>">about jclp</a></li>
                            <li><a onclick="" href="<?php echo base_url('listing');?>">our listings</a></li>
                            <li><a onclick="" href="<?php echo base_url('testimonial');?>">testimonials</a></li>
                            <li><a onclick="" href="<?php echo base_url('lending');?>">lending</a></li>
                             <li><a onclick="" href="<?php echo base_url('blog');?>">blog</a></li>
                            <li><a onclick="" href="<?php echo base_url('contact');?>">contact</a></li>
                        </ul><!--nav-closed-->
                    </div><!--collapse-->
                </div><!-- container-fluid -->
            </nav>

        </div><!-- column-closed -->
    </div><!-- row-closed -->
</div>

<main>
    <div class="bg-grey">
        <div class="container contain">
            <div class="row">
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <h2>featured properties</h2>
                    <?php if ($property) {
	?>
                        <ul class="featured-slider">
                            <?php foreach ($property as $prop): ?>
                                <li>
                                    <a href="<?php echo base_url('listing-detail') . '/' . $prop->slug;?> ">
                                        <div class="border">
                                            <div class="property-box">
                                                <div class="img-wrap">
                                                    <?php if ($prop && $prop->p_image && $image_name = $this->misc_library->get_image_filename($prop->p_image, 'property', 'featured')) {
		?>
                                                        <?php if (file_exists(BASEPATH . '../assets/images/property/' . $image_name)) {?>
                                                            <img src="<?php echo base_url() . '/assets/images/property/' . $image_name;?>" class = "img-responsive" alt=""/>
                                                            <?php
}
	}
	?>
                                                </div>
                                                <h4><?php echo $prop->p_name;?></h4>
                                                <p><?php echo $prop->city;?></p>
                                            </div>
                                            <div class="features">
                                                <p><?php echo $prop->p_bedroom . ' beds' . ' + ' . $prop->p_bathroom . ' baths' . ' + ' . $prop->p_sq_feet . ' sqft';?></p>
                                            </div>
                                        </div>
                                        <div class="rate"><i class="fa fa-usd"></i> <?php echo $prop->p_price;?></div>
                                    </a>

                                </li>
                            <?php endforeach;?>
                        </ul>
                    <?php } else {?>
                        There are no properties added yet!
                    <?php }
?>
                </div><!-- column -->

                <div class="col-md-4 col-sm-4 col-xs-12">
                    <?php include "includes/profile-widget.php";?>
                </div>

            </div><!-- row -->
        </div><!-- container -->
    </div><!-- bg-grey -->

    <div class="bg-white">
        <div class="bg-img">
            <img src="<?php echo base_url('assets/images/faded-img.jpg');?>" alt="">
        </div>
        <div class="container contain">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    <h1>ABOUT JON CROCILLA:</h1>
                    <div class="about-box">
                        <?php echo content_limiter($all_about->excerpt, 900);?>
                        <p><?php //echo $all_about->excerpt;   ?></p><br>
                        <a href="<?php echo base_url('home/about');?>" class="btn btn-style uppercase">learn more</a>
                    </div>
                </div><!-- column -->

                <div class="col-md-4 col-sm-4">
                    <?php if ($all_testimonials): ?>
                        <h1>TESTIMONIALS:</h1>
                        <ul class="testi-monials">
                            <?php foreach ($all_testimonials as $test):
?>
                                <li>
                                    <div class="textimonials-box">
                                     <p><?php echo content_limiter($test->t_description, 195, '<a href="' . base_url('testimonial') . '">...[+]</a>');?>
                                     </p>
                                    </div>

                                    <div class="info text-center">
                                        <h2><?php echo $test->author;?></h2>
                                        <p><?php echo $test->address . $test->address2;?></p>
                                    </div>
                                </li>
                            <?php endforeach;?>
                        </ul>
                    <?php endif;?>
                </div>

            </div><!-- row -->
        </div><!-- container -->
    </div><!--bg-white-->
</main>

<style>
    .field-label{
        color: orange !important;
        text-transform: uppercase !important;
    }

    #searchProfile input, #searchProfile select{
        height: 32px !important;
    }

    .search-body{
        padding: 20px 10px 0 10px !important;
    }

    #ihf-main-container .mb-25{
        margin-bottom: 0 !important;
    }
</style>

<script>
    //$('#ihf-quicksearch-submit3').attr('type', 'submit');
</script>

<script>
    var mn = $(".main-nav");
    mns = "main-nav-scrolled";
    hdr = $('.bg-main-img').height();
    $(window).scroll(function () {
        if ($(this).scrollTop() > hdr) {
            mn.addClass(mns);
        } else {
            mn.removeClass(mns);
        }
    });
</script>
