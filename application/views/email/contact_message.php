<div style="width:600px; margin:0 auto; font-size:13px; line-height:1.2; font-family:Arial, Helvetica, sans-serif">
    <p style="margin:0 0 20px 0">New Enquiry Posted</p>

    <div style="margin-bottom:20px;">You have a new enquiry posted from the <?php echo $this->misc_library->setting_links('site-name', 'value'); ?> website.</div>

    <div style="margin-bottom:5px;">Name: <?php echo $name; ?></div>
    <div style="margin-bottom:5px;">Company: <?php echo $company; ?></div>
    <div style="margin-bottom:5px;">E-mail Address: <?php echo $email; ?></div>
    <div style="margin-bottom:20px;">Phone Number: <?php echo $phone; ?></div>

    <div style="margin-bottom:5px;">Message:</div>
    <div style="margin-bottom:20px;"><?php echo nl2br($message); ?></div>

    <div style="margin-bottom:5px;">This message was generated from the following page on the website:</div>
    <div style="margin-bottom:20px;"><a href="<?php echo $link; ?>" target="_blank" style="color:#E50600; text-decoration:none;"><?php echo $link; ?></a></div>

    <div style="margin-bottom:5px;">Regards</div>
    <div style="margin-bottom:12px;"><?php echo $this->misc_library->setting_links('site-name', 'value'); ?></div>
</div>