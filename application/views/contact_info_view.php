<?php
if ($this->session->userdata('contact_info') != '')
    $info_contact = $this->session->userdata('contact_info');
?>
<main>
    <div class="container contain contain-white form-submission-page">

        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class="border-bottom">contact info</h1>
            </div><!-- column -->
        </div><!-- row -->

        <div class="row">
            <div class="col-md-12">    
                <ul class="page-links">
                    <li class="active"><a href="<?php echo base_url('questionnaire'); ?>">1.&nbsp;lending questionnaire</a></li>
                    <li class="active"><a href="#">2.&nbsp;contact info</a></li>
                    <li><a>3.&nbsp;submission</a></li>
                </ul>
            </div><!-- column -->
        </div><!-- row -->

        <div class="row text-center">
            <div class="col-md-12">
                <p>FILL OUT THE CONTACT DETAILS AND A REPRESENTATIVE WILL FOLLOW UP AS SOON AS POSSIBLE:</p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <form class="form-horizontal contact_info_form" method="post" action="<?php echo base_url('contact-info'); ?>">

                    <div class="form-group">
                        <label class="col-md-2 col-md-offset-2 control-label">First name<span class="red">*</span></label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="first_name" value="<?php echo isset($info_contact['first_name']) ? $info_contact['first_name'] : set_value('first_name'); ?>">
                            <?php echo form_error('first_name'); ?>
                        </div>
                        <div class="clearfix"></div>
                    </div><!-- form-group -->

                    <div class="form-group">
                        <label class="col-md-2 col-md-offset-2 control-label">Last name<span class="red">*</span></label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="last_name" value="<?php echo isset($info_contact['last_name']) ? $info_contact['last_name'] : set_value('last_name'); ?>">
                            <?php echo form_error('last_name'); ?>
                        </div>
                        <div class="clearfix"></div>
                    </div><!-- form-group -->

                    <div class="form-group">
                        <label class="col-md-2 col-md-offset-2 control-label">Email<span class="red">*</span></label>
                        <div class="col-md-6">
                            <input type="email" class="form-control" name="email" value="<?php echo isset($info_contact['email']) ? $info_contact['email'] : set_value('email'); ?>">
                            <?php echo form_error('email'); ?>
                        </div>
                        <div class="clearfix"></div>
                    </div><!-- form-group -->

                    <div class="form-group">
                        <label class="col-md-2 col-md-offset-2 control-label">Phone number<span class="red">*</span></label>
                        <div class="col-md-6">
                            <input type="tel" class="form-control" name="phone_no" value="<?php echo isset($info_contact['phone_no']) ? $info_contact['phone_no'] : set_value('phone_no'); ?>">
                            <?php echo form_error('phone_no'); ?>
                        </div>
                        <div class="clearfix"></div>
                    </div><!-- form-group -->

                    <div class="form-group">
                        <label class="col-md-2 col-md-offset-2 control-label">Address 1<span class="red">*</span></label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="address_1" value="<?php echo isset($info_contact['address_1']) ? $info_contact['address_1'] : set_value('address_1'); ?>">
                            <?php echo form_error('address_1'); ?>
                        </div>
                        <div class="clearfix"></div>
                    </div><!-- form-group -->

                    <div class="form-group">
                        <label class="col-md-2 col-md-offset-2 control-label">Address 2</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="address_2" value="<?php echo isset($info_contact['address_2']) ? $info_contact['address_2'] : set_value('address_2'); ?>">
                        </div>
                        <div class="clearfix"></div>
                    </div><!-- form-group -->

                    <div class="form-group">
                        <label class="col-md-2 col-md-offset-2 control-label">City, State, Zip<span class="red">*</span></label>
                        <div class="col-md-6">

                            <div class="row">

                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <input type="text" class="form-control" name="city" value="<?php echo isset($info_contact['city']) ? $info_contact['city'] : set_value('city'); ?>">
                                </div><!-- column -->

                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <!-- <select class="form-control form-states-usa custom-select-box-city" name="state">
                                        <option value="Alabama">Alabama</option><option value="Alaska">Alaska</option><option value="American Samoa">American Samoa</option><option value="Arizona">Arizona</option><option value="Arkansas">Arkansas</option><option value="California">California</option><option value="Colorado">Colorado</option><option value="Connecticut">Connecticut</option><option value="Delaware">Delaware</option><option value="District Of Columbia">District Of Columbia</option><option value="Federated States Of Micronesia">Federated States Of Micronesia</option><option value="Florida">Florida</option><option value="Georgia">Georgia</option><option value="Guam">Guam</option><option value="Hawaii">Hawaii</option><option value="Idaho">Idaho</option><option selected="selected" value="Illinois">Illinois</option><option value="Indiana">Indiana</option><option value="Iowa">Iowa</option><option value="Kansas">Kansas</option><option value="Kentucky">Kentucky</option><option value="Louisiana">Louisiana</option><option value="Maine">Maine</option><option value="Marshall Islands">Marshall Islands</option><option value="Maryland">Maryland</option><option value="Massachusetts">Massachusetts</option><option value="Michigan">Michigan</option><option value="Minnesota">Minnesota</option><option value="Mississippi">Mississippi</option><option value="Missouri">Missouri</option><option value="Montana">Montana</option><option value="Nebraska">Nebraska</option><option value="Nevada">Nevada</option><option value="New Hampshire">New Hampshire</option><option value="New Jersey">New Jersey</option><option value="New Mexico">New Mexico</option><option value="New York">New York</option><option value="North Carolina">North Carolina</option><option value="North Dakota">North Dakota</option><option value="Northern Mariana Islands">Northern Mariana Islands</option><option value="Ohio">Ohio</option><option value="Oklahoma">Oklahoma</option><option value="Oregon">Oregon</option><option value="Palau">Palau</option><option value="Pennsylvania">Pennsylvania</option><option value="Puerto Rico">Puerto Rico</option><option value="Rhode Island">Rhode Island</option><option value="South Carolina">South Carolina</option><option value="South Dakota">South Dakota</option><option value="Tennessee">Tennessee</option><option value="Texas">Texas</option><option value="Utah">Utah</option><option value="Vermont">Vermont</option><option value="Virgin Islands">Virgin Islands</option><option value="Virginia">Virginia</option><option value="Washington">Washington</option><option value="West Virginia">West Virginia</option><option value="Wisconsin">Wisconsin</option><option value="Wyoming">Wyoming</option>
                                    </select> -->
                                    <?php $states = $this->misc_library->get_all_us_states();
                                        if(!isset($info_contact['state'])){
                                            $info_contact['state'] = 'Illinois';
                                        }?>
                                        <select class="form-control form-states-usa custom-select-box-city" id="custom-select-box-1" name='state'>
                                        <?php foreach($states as $state){
                                                $selected = false;
                                                if(isset($info_contact['state']) && $info_contact['state']==$state){
                                                    $selected = true;
                                                }?>
                                            <option <?php if($selected==true){?>selected="selected"<?php }?> value="<?php echo $state; ?>"><?php echo $state; ?></option>
                                        <?php }?>
                                     </select>
                                </div><!-- column -->

                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <input type="text" class="form-control" name="zip" value="<?php echo isset($info_contact['zip']) ? $info_contact['zip'] : set_value('zip'); ?>">
                                </div><!-- column -->

                            </div><!-- row -->

                        </div><!-- column -->
                        <div class="clearfix"></div>
                    </div><!-- form-group -->

                    <div class="form-group">
                        <label class="col-md-2 col-md-offset-2 control-label">Best time to reach you</label>
                          <div class="col-md-6">
                            <select class="form-control custom-select-box-time" name="best_time">
                                <option value="10:00AM to 12:00PM" <?php if(isset($info_contact['best_time']) && $info_contact['best_time']=="10:00AM to 12:00PM"){?>selected="selected"<?php }?>>10:00AM to 12:00PM </option>                                                                                             
                                <option value="12:00PM to 02:00PM" <?php if(isset($info_contact['best_time']) && $info_contact['best_time']=="12:00PM to 02:00PM"){?>selected="selected"<?php }?>>12:00PM to 02:00PM </option>  
                                <option value="02:00PM to 04:00PM" <?php if(isset($info_contact['best_time']) && $info_contact['best_time']=="02:00PM to 04:00PM"){?>selected="selected"<?php }?>>02:00PM to 04:00PM </option>  
                                <option value="04:00PM to 06:00PM" <?php if(isset($info_contact['best_time']) && $info_contact['best_time']=="04:00PM to 06:00PM"){?>selected="selected"<?php }?>>04:00PM to 06:00PM</option>

                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div><!-- form-group -->

                    <div class="col-md-12 text-center">
                        <input type="hidden" id="baseurl" value="<?php echo base_url(); ?>" name="baseurl">
                        <button class="btn btn-default btn-pattern" type="submit">continue</button>
                    </div>

                </form>

            </div><!-- column -->
        </div><!-- row -->

    </div><!-- contain-white -->
</main>

</div><!-- bg-pattern-grey -->