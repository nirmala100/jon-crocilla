<main>
    <div class="container contain contain-white blog blog-detail">
        <div class="row">
            <div class="col-md-8 mb15">
                <div class="the-post">
                    <?php if ($blog_info): ?>
                        <?php if ($blog_info && $blog_info->image && $image_name = $this->misc_library->get_image_filename($blog_info->image, 'blog', 'large')) {
                            ?> 
                            <?php if (file_exists(BASEPATH . '../assets/images/blog/' . $image_name)) { ?>
                                <img src="<?php echo base_url() . '/assets/images/blog/' . $image_name; ?>" class="img-responsive" alt="<?php echo $blog_info->title; ?>"/>
                                <?php
                            }
                        }
                        ?>
                        <div class="blog-date"> <?php $timestamp = strtotime($blog_info->create_date); ?>
                            <big><?php echo date('d', $timestamp); ?></big>
                            <?php echo date('M', $timestamp) ?> ’<?php echo date('y', $timestamp); ?>
                        </div>
                                <h1 class="title"><?php echo $blog_info->title; ?></h1>
                                <div class="desc ckeditor-style"><?php echo $blog_info->b_description; ?></div>
                    <?php endif; ?>
                                <div class="disqus-comment-section">
                                <div id="disqus_thread"></div>
                    <script type="text/javascript">
                        /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
                        var disqus_shortname = 'joncrocillacom'; // Required - Replace '<example>' with your forum shortname
                        var disqus_identifier = 'blog_<?php  echo $blog_info->id;?>';
                        /* * * DON'T EDIT BELOW THIS LINE * * */
                        (function () {
                            var dsq = document.createElement('script');
                            dsq.type = 'text/javascript';
                            dsq.async = true;
                            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                        })();
                    </script>
                    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="top-stories">
                    <header>
                        <h2>TOP STORIES</h2>
                    </header>
                    <div class="stories">
                    <?php if ($top_stories): ?>
                        <?php foreach ($top_stories as $top): ?>   
                            <div class="top-story">
                                <div class="row">
                                    <div class="col-md-3 col-xs-3">
                                        <a href="<?php echo base_url("blog-detail/$top->slug"); ?>">
                                            <?php if ($top && $top->image && $image_name = $this->misc_library->get_image_filename($top->image, 'blog', 'top_stories')) {
                                                ?> 
                                                <?php if (file_exists(BASEPATH . '../assets/images/blog/' . $image_name)) { ?>
                                                    <img src="<?php echo base_url() . '/assets/images/blog/' . $image_name; ?>" class="img-responsive"/>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </a>
                                    </div>
                                    <div class="col-md-9 col-xs-9">
                                        <div><a href="<?php echo base_url("blog-detail/$top->slug"); ?>"><?php echo $top->title; ?></a></div>
                                       <?php /* <p class="comments"><span class="disqus-comment-count" data-disqus-url="<?php echo base_url("blog-detail/".$top->slug); ?>"> <!-- Count will be inserted here --> </span></p> */?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
</div>
                    <div class="more"><a href="javascript:void(0)" id ="fetch-ts" data-total-story = "3" data-url = "<?php echo base_url("top-stories"); ?>">More Stories</a></div>
                </div><!-- top stories ends-->
                <div id="about-fb-page">
                    <h2><img src="<?php echo base_url('assets/images/fb-header.jpg'); ?>"></h2>
                    <div class="main-fb-plugin">
                        <div class="fb-page"  data-width="100%"  data-href="https://www.facebook.com/joncrocillaproperty" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/joncrocillaproperty"><a href="https://www.facebook.com/joncrocillaproperty">Jon Crocilla Luxury Properties</a></blockquote></div></div>
                    </div>
                </div> <!--fb plugin ends-->
            </div>

        </div>
    </div>
</main>
