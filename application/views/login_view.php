<div class="container">
    <div class="row login-logo">
        <div class="col-md-4 col-md-offset-4">
            <img src="<?php echo base_url('assets/images/logo.png'); ?>" alt="Logo" class="img-responsive">
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Sign In</h3>
                </div>
                <div class="panel-body">
                    <form role="form" method="post" action="">
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control" placeholder="Username" name="username" type="text" autofocus>
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Password" name="password" type="password" value="">
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                </label>
                            </div>
                            <a href="<?php echo base_url("admin/forgot-password"); ?>">Forgot Password? Click here.</a>
                            <br/><br/>
                            <!-- Change this to a button or input when using this as a form -->
                            <input type="submit" name="login" class="btn btn-lg btn-success btn-block" value="Login">
                        </fieldset>

                    </form>
                    <div class="error-message"><?php echo isset($msg) ? $msg : ''; ?></div>
                </div>
            </div>
        </div>
    </div> <!--end of row-->
</div> <!--end of container-->