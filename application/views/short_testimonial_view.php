<?php
if ($next_testimonial):
    foreach ($next_testimonial as $testimonial):
        ?>
        <div id ="<?php echo "testimonial_" . $testimonial->id; ?>" class="testimonial-post">
            <div class="row" >

                <div class="col-md-10 col-md-offset-1 text-center">

                    <div class="testimonials-wrap">
                        <p><?php echo $testimonial->t_description; ?></p>
                        <img src="<?php echo base_url('assets/images/testimonials-comma.jpg'); ?>" alt="">
                    </div>

                    <div class="person-details">
                        <div class="person-name">
                            <?php echo $testimonial->author; ?>
                        </div>
                        <div class="person-address">
                            <?php echo  $testimonial->address2; ?>
                        </div>
                    </div><!-- person-details -->

                </div><!-- column -->

            </div><!-- row -->
        </div>
    <?php endforeach; ?>
    <a href="<?php echo base_url('next-testimonial/' . $start); ?>" id="next_url"></a>
    <?php
else:
    ?>
    <!-- <div class="row">
        <div class="col-md-10 col-md-offset-1 text-center">
            <p class='bg-primary-cust'>There are no more testimonials.</p>
        </div>
    </div> -->
<?php
endif;
?>