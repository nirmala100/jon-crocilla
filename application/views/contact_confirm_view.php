<?php foreach ($basic_info as $basic): ?>

    <main>
        <div class="container contain contain-white">
            <div class="row mb27">
                <div class="col-md-4 col-sm-4 col-xs-4 alter-width">
                    <?php if ($basic->profile_pic && $image = $this->misc_library->get_image_filename($basic->profile_pic, 'profile', 'main')) {
	?>
                        <?php if (file_exists(BASEPATH . '../assets/images/basic/' . $image)) {?>
                            <img src="<?php echo base_url() . '/assets/images/basic/' . $image;?>" alt="" class="img-responsive img-full" />
                            <?php
}
}
?>
                </div><!-- column -->
                <div class="col-md-8 col-sm-8 col-xs-8 alter-width">
                    
                    <div class="confirm-box">
                        <i class="confirm_check"></i>
                        <h1 class="text-center">thank you</h1>
                        <p>For contacting Jon Crocilla Luxury Properties. <br/>
                            We will get in touch with you as soon as possible.
                        </p>
                        <ul class="social-media">
                                <li><a target="_blank" href="<?php if(!empty( $all_about->facebook)){echo $all_about->facebook;}else { echo '#';}?>" class="circle fb"><i class="fa fa-facebook"></i></a></li>
                                <li><a target="_blank" href="<?php if(!empty( $all_about->twitter)){echo $all_about->twitter;}else { echo '#';}?>" class="circle twt"><i class="fa fa-twitter"></i></a></li>
                                <li><a target="_blank" href="<?php if(!empty( $all_about->linked_in)){echo $all_about->linked_in;}else { echo '#';}?>" class="circle lin"><i class="fa fa-linkedin-square"></i></a></li>
                                <!--<li><a target="_blank" href="" class="circle lin"><i class="fa fa-instagram"></i></a></li>-->
                        </ul>

                    </div>

                </div><!-- column -->
            </div><!-- row -->

            <div class="row mb27">

                <div class="col-md-4 col-sm-4 alter-width jc-details">
                    <h2><?php echo $basic->full_name;?> <br><?php echo $basic->position;?> </h2>
                    <address><?php echo $basic->city;?></address>
                    <div class="contact-info">
                        cell: <?php echo $basic->mobile_number;?><br>
                        Office: <?php echo $basic->work_number;?>
                    </div>
                </div><!-- column -->
                <div class="col-md-8 col-sm-8 alter-width">
<!--                    <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d1484.9573362250098!2d-87.6258482234871!3d41.89469212143846!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sne!2snp!4v1429510030893" width="100%" height="230" frameborder="0" style="border:1px solid #919191;padding-right:1px;"></iframe>-->
               <div id="googleMap" style="width:100%;height:228px;"></div>

                </div><!-- column -->

            </div><!-- row -->

            <div class="row bg-pattern-brick">
<?php if ($contactboxs): ?>
    <?php foreach ($contactboxs as $lending): ?>
            <div class="col-md-4 col-sm-4 col-xs-12 alter-width-listings">
                <div class="bg-dropshadow">
                    <h2><?php echo $lending->title;?></h2>
                     <?php if ($lending && $lending->image && $image_name = $this->misc_library->get_image_filename($lending->image, 'footerbox', 'main')) {
	?>
                                            <?php if (file_exists(BASEPATH . '../assets/images/footerbox/' . $image_name)) {?>
                    <img src="<?php echo base_url() . '/assets/images/footerbox/' . $image_name;?>" class="img-responsive" alt="">
                     <?php
}
}
?>
                    <p><?php echo $lending->f_description;?></p>
                    <a href="<?php echo $lending->url;?>" class="btn btn-default btn-pattern"><span class="after"><img src="<?php echo base_url('assets/images/icon-btn.png');?>" alt=""></span>read more</a>
                </div>
            </div><!-- column -->
            <?php endforeach;?>
<?php endif;?>

        </div><!-- row -->

        </div><!-- contain-white -->
    </main>

    </div><!-- bg-pattern-grey -->

<?php endforeach;?>

<script
src="http://maps.googleapis.com/maps/api/js">
</script>

<script>
var myCenter=new google.maps.LatLng('<?php echo $officelatitude;?>','<?php echo $officelongitude;?>');
var marker;

function initialize()
{
var mapProp = {
  zoomControl: false,
  scaleControl: false,
  scrollwheel: false,
  disableDoubleClickZoom: true,
  center:myCenter,
  zoom:14,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

var marker=new google.maps.Marker({
  position:myCenter,
  title:'676 N Michigan Ave Chicago',
  });

marker.setMap(map);
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>
