<footer>

    <div class="bg-black">
        <div class="container contain">
            <div class="row">
                <div class="col-md-12">
                    <ul class="nav navbar-nav footer-nav mobile-center"> 
                        <li>BUCKTOWN</li>
                        <li>GOLD COAST</li>
                        <li>LAKE VIEW</li>
                        <li>LINCOLN PARK </li>
                        <li>RIVER NORTH </li>
                        <li>SOUTH LOOP</li>
                        <li>STREETERVILLE </li>
                     </ul><!-- nav -->
                </div><!-- column -->
            </div><!-- row -->
        </div><!-- container -->
    </div><!-- bg-black -->

    <div class="bg-grey hidden-xs">
        <div class="container contain">
            <div class="row">
                <div class="col-md-9 col-sm-12">
                    <ol class="breadcrumb mobile-center">
                        <li class="active" href="<?php echo base_url(); ?>">Home</li>
                        <li><a href="<?php echo base_url('about-jclp'); ?>">about jclp</a></li>
                        <li><a href="<?php echo base_url('listing'); ?>">our listings</a></li>
                        <li><a href="<?php echo base_url('testimonial'); ?>">testimonials</a></li>
                        <li><a href="<?php echo base_url('questionnaire'); ?>">lending</a></li>
                        <li><a href="<?php echo base_url('blog'); ?>">blog</a></li>
                        <li><a href="<?php echo base_url('contact'); ?>">contact</a></li>
                    </ol>
                    <div class="mobile-center"><img src="<?php echo base_url('assets/images/associated-logo.png'); ?>" alt=""></div>
                </div><!-- column -->

                <div class="col-md-3 col-sm-12 mobile-center">
                    <a href=""><img src="<?php echo base_url('assets/images/footer-logo.png'); ?>" alt=""></a>
                </div>
            </div><!-- row -->
        </div><!-- container -->
    </div><!-- bg-grey -->

    <div class="bg-white">
        <div class="container contain">
            <div class="row">
                <div class="col-md-11 col-sm-10 col-xs-10">
                    <p><i class="fa fa-copyright"></i>&nbsp;2015 Jon Crocilla Luxury Properties. All rights reserved. </p>
                </div>
                <div class="col-md-1 col-sm-2 col-xs-2">
                    <a href="https://www.briancozzi.com/" target="_blank"> <img src="<?php echo base_url('assets/images/bc-logo.png'); ?>" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</footer>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.bxslider.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.colorbox-min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.selectbox-0.2.js'); ?>"></script>
<!--jQuery for equal heights-->
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.matchHeight-min.js'); ?>"></script>

<!-- jQuery for js scroller -->
<script src="<?php echo base_url('assets/js/jquery.jscroll.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.validate.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom.js'); ?>"></script>
<script>
    $(document).ready(function () {
        var mobileHover = function () {
            $('*').on('touchstart', function () {
                $(this).trigger('hover');
            }).on('touchend', function () {
                $(this).trigger('hover');
            });
        };

        mobileHover();
        $('.bxslider').bxSlider({
            maxSlides: 3
        });
        $('.carousel').carousel({interval: 7000}); // carousel
        /*jscroll*/

        $('.testimonial').jscroll({
            //debug: true,
            loadingHtml: '<div id="loader"><img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" /></div>',
        });
        $('.listing').jscroll({
            //debug: true,
            loadingHtml: '<div id="loader"><img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" /></div>',
        });

        /*end of jscroll*/



        $('#contact_us_form').validate({
            rules: {
                name: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                message: {
                    required: true
                },
                
                inquiry_type: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "Please enter your name."
                },
                email: {
                    required: "Please enter your email address."
                },
                message: {
                    required: "Please enter your message."
                },
               
                inquiry_type: {
                    required: "Please enter the inquiry type."
                },
            },
        });

$('#info_btn').click(function(){   

    $(".information-wrapper input.req").keyup(function(){
       $(this).css({ 'border':'1px solid green'});
    });
    $(".information-wrapper input.req:blank").css({
    'border':'1px solid #f00'
});
});





});
</script>
<script type="text/javascript">
/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
var disqus_shortname = 'joncrocillacom'; // required: replace example with your forum shortname

/* * * DON'T EDIT BELOW THIS LINE * * */
(function () {
var s = document.createElement('script'); s.async = true;
s.type = 'text/javascript';
s.src = '//' + disqus_shortname + '.disqus.com/count.js';
(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
}());
</script>
</body>
</html>