<?php
//print_r($all_testimonials);
if ($all_testimonials):

    foreach ($all_testimonials as $test):
        ?>
        <h1>TESTIMONIALS:</h1>

        <ul class="bxslider">

            <li>
                <div class="textimonials-box">
                    <div class="uppercase"><p><?php echo $this->misc-library->limit_text($test->description, 20); ?></p></div>
                </div>
                <div class="info text-center">
                    <h2><?php echo $test->author; ?></h2>
                    <p><?php echo $test->address . $test->address2; ?></p>
                </div>
            </li>



        </ul>
        <?php
    endforeach;
endif;
?>