<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <meta name="title" content="<?php echo $meta_title;?>">
        <meta name="keywords" content="<?php echo $meta_keywords;?>">
        <meta name="description" content="<?php echo $meta_description;?>">
        <title><?php echo $websitetitle;?></title>

        <!-- Normalize -->
        <link href="<?php echo base_url('assets/css/normalize.css');?>" rel="stylesheet">

        <!-- Bootstrap -->
        <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">

        <!-- bx.slider -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/jquery.bxslider.css');?>">

        <!--Style-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css');?>">

        <!--responsive-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/responsive.css');?>">

        <!--Fontawesome-4.3.0-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/font-awesome.min.css');?>">

        <link href='http://fonts.googleapis.com/css?family=Questrial|Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel='stylesheet' type='text/css'>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->
    </head>
    <body>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65320352-1', 'auto');
  ga('send', 'pageview');

</script>
        <?php
$img_displayed = false;
$background = "";
if ($home_image && $home_image->image && $image = $this->misc_library->get_image_filename($home_image->image, 'slider', 'home')) {
	?>
            <?php
if (file_exists(BASEPATH . '../assets/images/slider/' . $image)) {
		$img_displayed = true;
		$background = base_url() . '/assets/images/slider/' . $image;
	}
}
?>
        <div>
          <div class="bg-main-img bg-img-home static-banner" style="background:url(<?php echo $background;?>)  no-repeat; background-size:cover; height:100vh">
            <header class="<?php if ($img_displayed) {?>push-top<?php }
?>">

                <div class="container contain">
                    <div class="row">

                        <div class="col-md-4 col-sm-4">
                            <!--              <div class="heading-dtls uppercase">
                                            GOLD COAST CHICAGO <br>
                                            REAL ESTATE PROPERTIES
                                          </div>
                             <div class="heading-dtls uppercase">
                            <?php //echo $sitename; ?>
                                          </div>-->
                        </div><!-- column-closed -->

                        <div class="col-md-4 col-sm-4 text-center logo">
                            <a href="" class="hidden-mobile"><img src="<?php echo base_url('assets/images/logo.png');?>" alt=""></a>
                            <a href="" class="visible-xs mobile-logo"><img src="<?php echo base_url('assets/images/mobile-logo.png');?>" alt=""></a>
                        </div><!-- column-closed -->

                        <div class="col-md-4 col-sm-4 text-right index-responsive">

                            <ul class="social-media">
                                <li><a target="_blank" href="<?php echo $all_about->facebook;?>"  class="circle fb"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="<?php echo $all_about->twitter;?>" class="circle twt"><i class="fa fa-twitter"></i></a></li>
                                <li><a target="_blank" href="<?php echo $all_about->linked_in;?>"  class="circle lin"><i class="fa fa-linkedin-square"></i></a></li>
                                <!-- <li><a target="_blank"href="<?php echo $all_about->instagram;?>" class="circle lin"><i class="fa fa-instagram"></i></a></li>-->
                            </ul>
                            <a href="" class="cb-logo"><img src="<?php echo base_url('assets/images/coldwell-banker.png');?>" alt=""></a>

<!--              <div class="sm-txt uppercase"><a href="<?php //echo base_url('contact'); ?>">book an appointment</a></div> -->
                            <?php /* <?php //print_r($all_about);die;?>
<?php //foreach($all_about as $about): ?>
<div class="lg-txt"><?php echo $all_about->work_number;?></div>
<?php //endforeach;?>
 */?>
                        </div><!-- column-closed -->

                    </div><!-- row -->

                    <div class="row">
                        <div class="col-md-12">
                            <nav class="navbar navbar-default home-nav">
                                <div class="container-fluid">
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                    </div><!-- navbar-header -->

                                    <!-- Collect the nav links, forms, and other content for toggling -->
                                    <div class="collapse navbar-collapse" id="navbar-collapse-1">
                                        <ul class="nav navbar-nav ">
                                            <li><a onclick="" href="<?php echo base_url();?>" class="active">home</a></li>
                                            <li><a onclick="" href="<?php echo base_url('about-jclp');?>">about jclp</a></li>
                                            <li><a onclick="" href="<?php echo base_url('listing');?>">our listings</a></li>
                                            <li><a onclick="" href="<?php echo base_url('testimonial');?>">testimonials</a></li>
                                            <li><a onclick="" href="<?php echo base_url('lending');?>">lending</a></li>
                                            <li><a onclick="" href="<?php echo base_url('blog');?>">Blog</a></li>
                                            <li><a onclick="" href="<?php echo base_url('contact');?>">contact</a></li>

                                        </ul><!--nav-closed-->
                                    </div><!--collapse-->
                                </div><!-- container-fluid -->
                            </nav>
                        </div><!-- column-closed -->
                    </div><!-- row-closed -->

                </div><!-- container-closed -->
            </header>