<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <?php if ($this->uri->segment(1) == 'listing-detail') {?>
            <meta name="title" content="<?php echo $meta_title_p;?>">
            <meta name="keywords" content="<?php echo $meta_keywords_p;?>">
            <meta name="description" content="<?php echo $meta_description_p;?>">
        <?php } else {?>
            <meta name="title" content="<?php echo $meta_title;?>">
            <meta name="keywords" content="<?php echo $meta_keywords;?>">
            <meta name="description" content="<?php echo $meta_description;?>">
        <?php }
?>
        <title><?php echo $title . ' | ' . $websitetitle;?></title>

        <!-- Normalize -->
        <link href="<?php echo base_url('assets/css/normalize.css');?>" rel="stylesheet">

        <!-- Bootstrap -->
        <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">

        <!-- bx.slider -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/jquery.bxslider.css');?>">

        <!--Style-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css');?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/colorbox.css');?>">
        <!--responsive-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/responsive.css')?>">

        <!--Fontawesome-4.3.0-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/font-awesome.min.css')?>">

        <link href='http://fonts.googleapis.com/css?family=Questrial|Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="<?php echo base_url('assets/css/jquery.selectbox.css');?>">
    


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->

    </head>
    <body>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65320352-1', 'auto');
  ga('send', 'pageview');

</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=328968707296833";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
        <div class="bg-pattern-grey">
            <div class="container contain for-nav main-nav for-inner-page visible-xs">
                <div class="row">
                    <div >
                        <nav class="navbar navbar-default navbar-fixed-top custom-nav inner-nav">
                            <div class="container-fluid">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#fixed-navbar">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a class="navbar-brand" href="<?php echo base_url();?>"><img width="260" src="<?php echo base_url('assets/images/mobile-nav-btm.png');?>" alt="" class="img-responsive"></a>
                                </div><!-- navbar-header -->

                                <!-- Collect the nav links, forms, and other content for toggling -->
                                <div class="collapse navbar-collapse" id="fixed-navbar">
                                    <ul class="nav navbar-nav">
                                        <li><a onclick="" href="<?php echo base_url();?>">home</a></li>
                                        <li><a onclick="" href="<?php echo base_url('about-jclp');?>" <?php
if ($this->uri->segment(1) == 'about-jclp') {
	echo "class='active'";
}
?>>about jclp</a></li>
                                        <li><a onclick="" href="<?php echo base_url('listing');?>" <?php
if ($this->uri->segment(1) == 'listing') {
	echo "class='active'";
}
?>>our listings</a></li>
                                        <li><a onclick="" href="<?php echo base_url('testimonial');?>" <?php
if ($this->uri->segment(1) == 'testimonial') {
	echo "class='active'";
}
?>>testimonial</a></li>
                                        <li><a onclick="" href="<?php echo base_url('lending');?>" <?php
if ($this->uri->segment(1) == 'lending' || $this->uri->segment(1) == 'questionnaire' || $this->uri->segment(1) == 'contact-info') {
	echo "class='active'";
}
?>>lending</a></li>
                                          <li><a onclick="" href="<?php echo base_url('blog');?>" <?php
if ($this->uri->segment(1) == 'blog') {
	echo "class='active'";
}

?>>blog</a></li>

                                        <li><a onclick="" href="<?php echo base_url('contact');?>" <?php
if ($this->uri->segment(1) == 'contact') {
	echo "class='active'";
}
?>>contact</a></li>

                                    </ul><!--nav-closed-->
                                </div><!--collapse-->
                            </div><!-- container-fluid -->
                        </nav>

                    </div><!-- column-closed -->
                </div><!-- row-closed -->
            </div>

            <header>
                <div class="container contain">

                    <div class="row">

                        <div class="col-md-3 col-sm-4">
                            <div class="heading-dtls uppercase"><?php echo $sitename;?></div>
                        </div><!-- column-closed -->

                        <div class="col-md-4 col-md-offset-1  col-sm-4 text-center logo">
                            <a href="<?php echo base_url('home');?>" class="hidden-mobile"><img src="<?php echo base_url('assets/images/logo.png');?>" alt=""></a>
                        </div><!-- column-closed -->

                        <div class="col-md-4 col-sm-4 text-right">

                            <ul class="social-media">
                                <li><a target="_blank" href="<?php echo $all_about->facebook;?>" class="circle fb"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="<?php echo $all_about->twitter;?>" class="circle twt"><i class="fa fa-twitter"></i></a></li>
                                <li><a target="_blank" href="<?php echo $all_about->linked_in;?>" class="circle lin"><i class="fa fa-linkedin-square"></i></a></li>
                                <!--<li><a target="_blank" href="<?php echo $all_about->instagram;?>" class="circle lin"><i class="fa fa-instagram"></i></a></li>-->
                            </ul>
                            <a href="" class="cb-logo"><img src="<?php echo base_url('assets/images/coldwell-banker.png');?>" alt=""></a>

                            <div class="sm-txt uppercase"><a href="<?php echo base_url('contact');?>">book an appointment <i class="fa fa-angle-double-right"></i></a></div>

                            <div class="lg-txt"><?php echo $all_about->work_number;?></div>

                        </div><!-- column-closed -->

                    </div><!-- row -->

                    <div class="row">

                        <div class="col-md-12">
                            <nav class="navbar navbar-default hidden-xs">
                                <div class="container-fluid">

                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                    </div><!-- navbar-header -->

                                    <!-- Collect the nav links, forms, and other content for toggling -->
                                    <div class="collapse navbar-collapse" id="navbar-collapse-1">
                                        <ul class="nav navbar-nav">
                                            <li><a href="<?php echo base_url();?>">home</a></li>
                                            <li><a href="<?php echo base_url('about-jclp');?>" <?php
if ($this->uri->segment(1) == 'about-jclp') {
	echo "class='active'";
}
?>>about jclp</a></li>
                                            <li><a href="<?php echo base_url('listing');?>" <?php
if ($this->uri->segment(1) == 'listing') {
	echo "class='active'";
}
?>>our listings</a></li>
                                            <li><a href="<?php echo base_url('testimonial');?>" <?php
if ($this->uri->segment(1) == 'testimonial') {
	echo "class='active'";
}
?>>testimonials</a></li>
                                            <li><a href="<?php echo base_url('lending');?>" <?php
if ($this->uri->segment(1) == 'lending') {
	echo "class='active'";
}
?>>lending</a></li>
                                            <li><a href="<?php echo base_url('blog');?>" <?php
if ($this->uri->segment(1) == 'blog') {
	echo "class='active'";
}
?>>blog</a></li>
                                            <li><a href="<?php echo base_url('contact');?>" <?php
if ($this->uri->segment(1) == 'contact') {
	echo "class='active'";
}
?>>contact</a></li>
                                        </ul><!--nav-closed-->
                                    </div><!--collapse-->

                                </div><!-- container-fluid -->
                            </nav>
                        </div><!-- column-closed -->
                    </div><!-- row-closed -->

                </div><!-- container-closed -->
            </header>
