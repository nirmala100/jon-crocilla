<?php
if ($this->session->userdata('questionnaire') != ''):
    $info_qest = $this->session->userdata('questionnaire');

    if ($this->session->userdata('contact_info') != ''):
        $info_contact = $this->session->userdata('contact_info');
        ?>
        <main>
            <div class="container contain contain-white form-submission-page">

                <div class="row">
                    <div class="col-md-12 text-center">
                        <h1 class="border-bottom">submission</h1>
                    </div><!-- column -->
                </div><!-- row -->

                <div class="row">
                    <?php if ($this->session->flashdata('email_status')): ?>
                        <div class="col-md-12 text-center">
                            <h1 class="border-bottom">
                                <?php echo $this->session->flashdata('email_status'); ?>
                            </h1>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="row">
                    <div class="col-md-12">    
                        <ul class="page-links">
                            <li class="active"><a href="<?php echo base_url('questionnaire'); ?>">1.&nbsp;lending questionnaire</a></li>
                            <li class="active"><a href="<?php echo base_url('contact-info'); ?>">2.&nbsp;contact info</a></li>
                            <li class="active"><a href="#">3.&nbsp;submission</a></li>
                        </ul>
                    </div><!-- column -->
                </div><!-- row -->

                <div class="row text-center">
                    <div class="col-md-12">
                        <p>VERIFY THE DETAILS AND PRESS SUBMIT. WE’LL GET BACK TO YOU AS SOON AS POSSIBLE.</p>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-5 col-sm-6 col-md-offset-1 customizable-form">

                        <div class="heading">
                            questionnaire
                            <a href="<?php echo base_url('questionnaire'); ?>"><img src="<?php echo base_url('assets/images/pencil.png'); ?>" alt=""></a>
                        </div>
                        <div class="body">
                            <?php if (!empty($info_qest)): ?>
                                <div class="table-responsive">
                                    <table class="table">
                                        <?php foreach ($info_qest as $qest => $ans): ?>
                                            <tr>
                                                <td>
                                                    <?php
                                                    if (isset($for_short_questions)):
                                                        foreach ($for_short_questions as $short):
                                                            if (trim($short->question) == trim($qest)):
                                                                echo isset($short->short_question) ? $short->short_question : $qest;
                                                            endif;
                                                        endforeach;

                                                    else:
                                                        echo $qest;
                                                    endif;
                                                    ?>
                                                </td>
                                                <td class="text-right">
                                                    <?php echo $ans; ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>

                                    </table>
                                </div>
                            <?php endif; ?>
                        </div><!-- body -->

                    </div><!-- column -->
                    <div class="col-md-5 col-sm-6 customizable-form">

                        <div class="heading">
                            contact info
                            <a href="<?php echo base_url('contact-info'); ?>"><img src="<?php echo base_url('assets/images/pencil.png'); ?>" alt=""></a>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <td>First name</td>
                                        <td class="text-right"><?php echo isset($info_contact['first_name']) ? $info_contact['first_name'] : ''; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Last name</td>
                                        <td class="text-right"><?php echo isset($info_contact['last_name']) ? $info_contact['last_name'] : ''; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td class="text-right"><?php echo isset($info_contact['email']) ? $info_contact['email'] : ''; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Phone number</td>
                                        <td class="text-right"><?php echo isset($info_contact['phone_no']) ? $info_contact['phone_no'] : ''; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Address 1</td>
                                        <td class="text-right"><?php echo isset($info_contact['address_1']) ? $info_contact['address_1'] : ''; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Address 2</td>
                                        <td class="text-right"><?php echo isset($info_contact['address_2']) ? $info_contact['address_2'] : ''; ?></td>
                                    </tr>
                                    <tr>
                                        <td>City, State, Zip</td>
                                        <td class="text-right">
                                            <?php echo isset($info_contact['city']) ? $info_contact['city'] . ', ' : ' --- ,'; ?>
                                            <?php echo isset($info_contact['state']) ? $info_contact['state'] . ', ' : ' --- ,'; ?>
                                            <?php echo isset($info_contact['zip']) ? $info_contact['zip'] : ' --- '; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Best time to reach you</td>
                                        <td class="text-right"><?php echo isset($info_contact['best_time']) ? $info_contact['best_time'] : ''; ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div><!-- column -->

                </div><!-- row -->

                <div class="row mb27">
                    <div class="col-md-12 text-center">
                        <form action="<?php echo base_url('add-submission'); ?>" method="post">
                            <button class="btn btn-default btn-pattern" type="submit" id="save_for_submission">
                                <?php
                                if ($this->session->flashdata('email_status')) {
                                    echo $this->session->flashdata('email_status');
                                } else {
                                    echo "submit";
                                }
                                ?></button>
                        </form>
                    </div>
                </div>

            </div><!-- contain-white -->
        </main>

        </div><!-- bg-pattern-grey -->

    <?php endif;
endif;
?>

