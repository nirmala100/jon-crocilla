

<main>
    <div class="container contain contain-white listing-details">

        <div class="row mb27">

            <div class="col-md-9 col-sm-6 col-xs-6">
                <h1 class="banner-head"><span><?php echo $detail->p_name;?> </span><small><?php echo $detail->city;?> </small></h1>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-6 text-right btn-right">
                <div class="pull-right">
                    <a class="btn btn-default btn-ps"  onclick="window.print()"><i class="print"></i></a>
                    <a class="btn btn-default  btn-ps ml" href="mailto:?subject=Check out this property--www.joncrocilla.com&body=Hi, Check out this property : <?php echo base_url('listing-detail/' . $detail->slug);?>"><i class="share"></i></a>
                    <a class="btn btn-default ml bg-color btn-req" href="<?php echo base_url('contact');?>">request info</a></a>
                </div>
            </div>
        </div><!-- row -->

        <div class="row mb27">
            <div class="col-md-7 details-slider">
                <?php $i = 1;?>
                <?php if ($images) {
	                 ?>
                    <div id="carousel-example-generic" class="carousel slide details-main-slider" data-ride="carousel">


                        <div class="carousel-inner" role="listbox">

                            <?php foreach ($images as $image):
	                          ?>

                                <?php $item_class = ($i == 1) ? 'item active' : 'item';?>
                                <div class="<?php echo $item_class;?>">
                                    <?php
                                    if ($image && $image->image && $image_name = $this->misc_library->get_filename_without_ext($image->image, 'property', 'detailslider')) {
		                              ?>
                                        <?php if (file_exists(BASEPATH . '../assets/images/property/' . $image_name)) {
			                                ?>
                                            <img src="<?php echo base_url() . '/assets/images/property/' . $image_name;?>" class="img-thumbnail" class="img-responsive" alt=""/>
                                            <?php
                                               }
	                                           }
	                                          ?>

                                </div>
                                <?php $i++;?>
                            <?php endforeach;?>


                        </div><!-- carousel-inner -->

                        <!-- Controls -->
                        <?php foreach ($images as $image):?>
                         <?php
                          if ($image && $image->image && $image_name = $this->misc_library->get_filename_without_ext($image->image, 'property', 'detailslider')) {
                           ?>
                                        <?php if (file_exists(BASEPATH . '../assets/images/property/' . $image_name)) {
                             ?>
                        <a class="left carousel-control details-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left left-arrow" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control details-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right right-arrow" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                        <?php  }
                           }
                           endforeach;?>
                    </div><!-- carousel -->
                <?php } else {?>
                    <div class="carousel slide details-main-slider">
                        There are no images.
                    </div>

                <?php }
                   ?>


            </div><!-- column -->
            
            <div class="col-md-2 information">
                <h1>Get More Information</h1>
                <div class="information-wrapper">
                <div class="row">
                    <form class="form info_form_submit" method="post" action="">
                    <div class="col-md-6 fn">
                        <div class="form-group">
                            <input type="text" name="first_name" placeholder="First Name" class="form-control req ">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" name="last_name" placeholder="Last Name" class="form-control req">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="email" name="email" placeholder="Email Address" class="form-control req">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="text" name="phone" placeholder="Phone (Optional) " class="form-control">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group check">
                            <input type="checkbox" name="see" id="see" value="yes"><label for="see"><span></span></label> <label for="see" class="see-lbl"> See this property in person</label>
                        </div>
                    </div>
                     <div class="col-md-12">
                        <div class="form-group">
                            <textarea class="form-control" name="comments" placeholder="Comments (Optional)"></textarea>
                        </div>
                    </div>
                    <div class="col-md-6 col-md-offset-7">
                        <div class="form-group">
                            <input type="hidden" name="prop_address" value="<?php echo $detail->p_location;?>">
                            <input type="hidden" id="base-url" value="<?php echo base_url(); ?>" name="base-url">
                            <input type="submit" name="submit" value="submit" class="btn btn-submit btn-block push-left" id="info_btn">
                        </div>
                    </div>
                    <div id="listing_info_msg">
                            
                    </div><!--listing_info_msg-->
                    </form>

                </div>
                </div>
            </div>
        </div><!-- row -->

        <div class="row border-down">

            <div class="col-md-10 col-sm-12">
                <ul class="details inner-details">
                    <li>bedrooms&nbsp;&nbsp;<span class="big-txt"><?php echo $detail->p_bedroom;?></span></li>
                    <li>bathrooms&nbsp;&nbsp;<span class="big-txt"><?php echo $detail->p_bathroom;?></span></li>
                    <li>sq. feet&nbsp;&nbsp;<span class="big-txt"><?php echo $detail->p_sq_feet;?></span></li>
                    <li>lot size&nbsp;&nbsp;<span class="big-txt"><?php echo $detail->p_lot_size;?></span></li>
                </ul>
            </div>

            <div class="col-lg-2 col-md-2 property-rate">
                $<?php echo $detail->p_price;?>
            </div>

        </div><!-- row -->

        <div class="row mb27">

            <div class="col-md-9">

                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#tab1" aria-controls="home" role="tab" data-toggle="tab">property details</a>
                        </li>
                        <li role="presentation"><a href="#tab2" aria-controls="profile" role="tab" data-toggle="tab">photos</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane active" id="tab1">
                            <div class="ckeditor-style"><?php echo $detail->p_description;?></div>
                        </div>

                        <div role="tabpanel" class="tab-pane photo-gallery" id="tab2">
                            <?php
                         if ($images) {
	                      $i = 1;
	                         ?>
                                <div id="photogallery" class="carousel slide photo-slider" data-ride="carousel">

                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner" role="listbox">
                                        <?php
                            foreach ($images as $image):
	                                ?>
                             <?php if ($i == 1 || $i % 6 == 1): ?>
                                                <div class="item <?php echo $i == 1 ? 'active' : '';?>">
                                                    <div class="row">
                                                    <?php endif;?>
                                                        <?php if ($i == 1 || $i % 3 == 1): ?>
                                                        <div class="col-md-6 col-sm-6">
                                                        <?php endif;?>
                                                        <?php
                                                    if ($image && $image->image && $image_name = $this->misc_library->get_filename_without_ext($image->image, 'property', 'detaillisting')) {
		                                                 $image_slider_name = $this->misc_library->get_filename_without_ext($image->image, 'property', 'detailslider')
		                                                    ?>
                                                            <?php if (file_exists(BASEPATH . '../assets/images/property/' . $image_name)) {
			                                                 ?>
                                                                <a href="<?php echo base_url() . '/assets/images/property/' . $image_slider_name;?>" rel="gallery-images">
                                                                    <img src="<?php echo base_url() . '/assets/images/property/' . $image_name;?>" class="img-thumbnail" class="img-responsive" alt=""/>
                                                                </a>
                                                                <?php
                                                                 }
	                                                             }
	                                                            ?>
                                                    <?php if ($i == 3 || $i % 3 == 0 || $i == count($images)): ?>
                                                        </div><!-- col-md-6 com-sm-6-->
                                                    <?php endif;?>

                                                       <?php if ($i % 6 == 0 || $i == count($images)): ?>
                                                    </div>
                                                </div>
                                            <?php endif;?>
                                            <?php
                                          $i++;
	                                       endforeach;
	                                      ?>

                                    </div><!-- carousel-inner -->

                                    <!-- Controls -->
                                    <a class="left carousel-control" href="#photogallery" role="button" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#photogallery" role="button" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>

                            <?php } else {?>
                                <div class="carousel slide photo-slider" > There are no photos in photo gallery.</div>
                              <?php }
                                ?>

                        </div><!-- tab2 -->

                    </div><!-- tab-content -->
                </div><!-- tabpanel -->
            </div><!-- column -->

            <div class="col-md-3">

                <div class="row">
                    <div class="col-md-12">
                        <!--<img src="<?php echo base_url('assets/images/map.jpg');?>" class="img-responsive img-thumbnail" alt="">-->
                        <?php if($lat && $long){?>                        
                            <div id="googleMap" style="width:100%;height:228px;"></div>
                        <?php }?>
                        <ul class="location">
                            <li><img src="<?php echo base_url('assets/images/location.jpg');?>" alt=""></li>
                            <li><p><?php echo $detail->city;?></p></li>
                        </ul>
                    </div>
                </div><!-- row -->

                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="contact-bg">
                            <h2>get in touch</h2>
                            <div class="contact-info">
                                <div class="img-wrap">
                                   <?php  foreach ($basic_info as $basic): ?>
                                        <?php /*if ($basic->profile_pic && $image = $this->misc_library->get_image_filename($basic->profile_pic, 'profile', 'small')) {
	?>
                                            <?php if (file_exists(BASEPATH . '../assets/images/basic/' . $image)) {?>
                                                <img src="<?php echo base_url() . '/assets/images/basic/' . $image;?>" alt="" class="img-responsive img-full" />
                                                
                                                <?php
}
}*/
endforeach;

?>
                            <img src="<?php echo base_url().'assets/images/pp.png'?>" width=110>
                                </div><!-- img-wrap -->
                                <div class="txt-wrap">
                                    <!--<h1><?php echo $basic->full_name;?></h1>
                                    <span class="sm-heading"><?php echo $basic->position;?></span>
                                    <a href="mailto:jon.crocilla@cbexchange.com"><img src="<?php echo base_url('assets/images/mail.png');?>" alt="">&nbsp;&nbsp;&nbsp;&nbsp;send email</a>
                                    <p><img src="<?php echo base_url('assets/images/call.png');?>" alt="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $all_about->work_number;?></p>

                                    -->
                                    <h1><?php echo $basic->full_name;?></h1>
                                    <span class="sm-heading"><?php echo $basic->position;?></span>
                                    <a href="mailto:jon.crocilla@cbexchange.com" class="mail"><?php if(!empty($email)){echo $email;}?></a>
                                    <p><?php echo $all_about->work_number;?></p>
                                </div>
                                <div class="clearfix"></div>
                            </div><!-- contact-info -->

                        </div><!-- contact-bg -->
                     
                    </div><!-- column -->
                </div><!-- row -->

            </div><!-- column -->

        </div><!-- row -->

    </div><!-- contain-white -->
</main>

</div><!-- bg-pattern-grey -->
<?php if($lat && $long){?> 
<script src="http://maps.googleapis.com/maps/api/js"></script>

<script type="text/javascript">
var myCenter=new google.maps.LatLng('<?php echo $lat;?>','<?php echo $long;?>');
var marker;

function initialize()
{
var mapProp = {
  zoomControl: false,
  scaleControl: false,
  scrollwheel: false,
  disableDoubleClickZoom: true,
  center:myCenter,
  zoom:14,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

var marker=new google.maps.Marker({
  position:myCenter,
  title:'676 N Michigan Ave Chicago',
  });

marker.setMap(map);
}

google.maps.event.addDomListener(window, 'load', initialize);






</script>
<?php }?>
