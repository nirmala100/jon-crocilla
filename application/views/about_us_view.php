<main class="about-page">
    <div class="container contain contain-white">

        <div class="row mb27">

            <div class="col-md-12 col-sm-12 col-xs-12">

                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <!--                    <ol class="carousel-indicators">
                    <?php $counter = count($sliders); ?>
                    <?php
                    for ($i = 0; $i < $counter; $i++) {
                        ?>
                                                            <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php echo $i == 0 ? 'active' : ''; ?>"></li>
                    <?php } ?>                                                    
                    
                                        </ol>-->

                    <!-- Wrapper for slides -->

                    <div class="carousel-inner" role="listbox">
                        <?php $i = 1; ?>
                        <?php if ($sliders): ?>
                            <?php foreach ($sliders as $slider):
                                ?>

                                <?php $item_class = ($i == 1) ? 'item active' : 'item'; ?>
                                <div class="<?php echo $item_class; ?>">
                                    <?php if ($slider && $slider->image && $image_name = $this->misc_library->get_filename_without_ext($slider->image, 'slider', 'about')) {
                                        ?>  
                                        <?php if (file_exists(BASEPATH . '../assets/images/slider/' . $image_name)) {
                                            ?>

                                            <img src="<?php echo base_url() . '/assets/images/slider/' . $image_name; ?>" class="img-thumbnail" class="img-responsive" alt=""/>
                                            <?php
                                        }
                                    }
                                    ?>

                                </div>
                                <?php $i++; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>

                    </div><!-- carousel-inner -->

                </div><!-- carousel -->
            </div><!-- column -->

        </div><!-- row -->  

        <div class="row mb27">

            <div class="col-md-12 col-xs-12">
                <h1 class="text-center">about jon crocilla</h1>

                <div class="about-box ckeditor-style">
                    <p><?php echo $all_about->about_description; ?></p>
                </div><!-- about-box -->
            </div><!-- column -->
        </div>
        <div class="row">
            <div class="col-md-8">
                <h1 class="heading-tlnt-box text-center">why to choose me?</h1>

                <div class="panel-group about-panel" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <?php echo $why_me_1->title; ?>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                <?php echo $why_me_1->description; ?>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <?php echo $why_me_2->title; ?>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body">
                                <?php echo $why_me_2->description; ?>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    <?php echo $why_me_3->title; ?>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body">
                                <?php echo $why_me_3->description; ?>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFour">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    <?php echo $why_me_4->title; ?>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                            <div class="panel-body">
                                <?php echo $why_me_4->description; ?>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFive">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <?php echo $why_me_5->title; ?>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                            <div class="panel-body">
                                <?php echo $why_me_5->description; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-12">
                <?php /*
                  <?php include("includes/profile-widget.php"); ?>
                  <?php
                  if ($all_testimonials):
                  ?>
                  <h1>TESTIMONIALS:</h1>
                  <ul class="bxslider">
                  <?php foreach ($all_testimonials as $test):
                  ?>


                  <li>
                  <div class="textimonials-box">
                  <p><?php echo content_limiter($test->t_description, 250, '..<a href="' . base_url('testimonial') . '">[+]</a>'); ?></p>
                  </div>
                  <div class="info text-center">
                  <h2><?php echo $test->author; ?></h2>
                  <p><?php echo $test->address . $test->address2; ?></p>
                  </div>
                  </li>




                  <?php endforeach; ?>
                  </ul>
                  <?php endif; ?>
                 */ ?>
                <h1 class="heading-tlnt-box text-center">&nbsp;</h1>
                <div id="about-fb-page">
                    <h2><img src="<?php echo base_url('assets/images/fb-header.jpg'); ?>"></h2>
                    <div class="main-fb-plugin">
                        <div class="fb-page" data-href="https://www.facebook.com/joncrocillaproperty" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/joncrocillaproperty"><a href="https://www.facebook.com/joncrocillaproperty">Jon Crocilla Luxury Properties</a></blockquote></div></div>
                    </div>
                </div>
            </div><!-- column -->

        </div><!-- row -->

    </div><!-- container -->
</main>
</div><!--bg-pattern-grey -->