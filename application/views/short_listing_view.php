<?php
if ($next_listing):
    foreach ($next_listing as $listing):
        ?>  

        <div id ="<?php echo "listing_" . $listing->id; ?>" >
            <div class="row mb27">

                <div class="col-md-5 col-sm-5">
                    <?php if ($listing && $listing->p_image && $image_name = $this->misc_library->get_image_filename($listing->p_image, 'property', 'listing')) {
                        ?> 
                        <?php if (file_exists(BASEPATH . '../assets/images/property/' . $image_name)) { ?>
                            <a href="<?php echo base_url('listing-detail/') . '/' . $listing->slug;?>">
                            <img src="<?php echo base_url() . '/assets/images/property/' . $image_name; ?>" class="img-thumbnail" class="img-responsive" alt=""/>
                            </a>
                        <?php
                        }
                    }
                    ?>
                </div><!-- column -->

                <div class="col-md-7 col-sm-7 property-text-wrap">       
                    <div class="property-name"><a href="<?php echo base_url('listing-detail/') . '/' . $listing->slug; ?>"><?php echo $listing->p_name; ?></a></div>
                    <p><?php echo $listing->city; ?></p>
                    <div class="rate"><i class="fa fa-usd"></i><?php echo $listing->p_price; ?></div>
                    <ul class="details">
                        <li><b><?php echo $listing->p_bedroom; ?></b> beds</li>
                        <li><b><?php echo $listing->p_bathroom; ?></b> baths</li>
                        <li><b><?php echo $listing->p_sq_feet; ?></b> sq.fts</li>
                    </ul>
                    <p><?php echo content_limiter($listing->short_desc,200,'...'); ?></p>
                    <input type="hidden" name="id" value="<?php echo $listing->id; ?>">
                    <a href="<?php echo base_url('listing-detail/') . '/' . $listing->slug; ?>">property details&nbsp;&nbsp;<i class="fa fa-caret-right"></i></a>
                </div><!-- column -->

            </div><!-- row -->
        </div>
    <?php endforeach; ?>
    <a href="<?php echo base_url('next-listing/' . $startagain); ?>" ></a>
    <?php
else:
    ?>
   <!--  <div class="row">
        <div class="col-md-12 text-center">
            <p class='bg-primary-cust'>There are no more listings.</p>
        </div>
    </div> -->
<?php
endif;
?>
