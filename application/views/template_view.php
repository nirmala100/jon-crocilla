<?php

if ($this->uri->segment(1) == 'admin') {
    $header = 'admin/includes/header_view';
    $footer = 'admin/includes/footer_view';
} else {
    $data['all_about'] = $this->crud_model->select_row('*', 'tbl_admin', array('id' => 1));
    $data['is_home'] = ($this->router->fetch_class() === 'home' && $this->router->fetch_method() === 'index') ? true : false;
    if ($this->router->fetch_class() === 'home' && $this->router->fetch_method() === 'index') {
        $header = 'includes/header_index_view';
    } else {
        $header = 'includes/header_inner_view';
    }


    $footer = 'includes/footer_view';
}

$data['is_home'] = ($this->router->fetch_class() === 'home' && $this->router->fetch_method() === 'index') ? true : false;

$this->load->view($header, $data);
$this->load->view($template, $data);
$this->load->view($footer, $data);
?>