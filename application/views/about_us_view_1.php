<main>
    <div class="container contain contain-white">

        <div class="row mb27">

            <div class="col-md-8 col-sm-8 col-xs-12">

                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <?php $counter = count($sliders); ?>
                        <?php
                        for ($i = 0; $i < $counter; $i++) {
                            ?>
                            <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php echo $i == 0 ? 'active' : ''; ?>"></li>
                        <?php } ?>                                                    

                    </ol>

                    <!-- Wrapper for slides -->

                    <div class="carousel-inner" role="listbox">
                        <?php $i = 1; ?>
                        <?php if ($sliders): ?>
                            <?php foreach ($sliders as $slider):
                                ?>

                                <?php $item_class = ($i == 1) ? 'item active' : 'item'; ?>
                                <div class="<?php echo $item_class; ?>">
                                    <?php if ($slider && $slider->image && $image_name = $this->misc_library->get_filename_without_ext($slider->image, 'slider', 'about')) {
                                        ?>  
                                        <?php if (file_exists(BASEPATH . '../assets/images/slider/' . $image_name)) {
                                            ?>

                                            <img src="<?php echo base_url() . '/assets/images/slider/' . $image_name; ?>" class="img-thumbnail" class="img-responsive" alt=""/>
                                            <?php
                                        }
                                    }
                                    ?>

                                </div>
                                <?php $i++; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>

                    </div><!-- carousel-inner -->

                </div><!-- carousel -->
            </div><!-- column -->

            <div class="col-md-4 col-sm-4 col-xs-12 profile">
                <?php include("includes/profile-widget.php"); ?>
            </div> 

        </div><!-- row -->  

        <div class="row mb27">

            <div class="col-md-8 col-xs-12">
                <h1>about jon crocilla</h1>

                <div class="about-box">
                    <p><?php echo $all_about->about_description; ?></p><br>

                    <h1 class="heading-tlnt-box">why to choose me?</h1>
           
                    <div class="row">
                                
                          <div class="col-md-6">
                            
                            <div class="tlnt-box">
                                <div class="heading mw180">
                                    <?php echo $why_me_1->title;?>
                                </div>
                                <div class="body">
                                    <p><?php echo $why_me_1->description; ?> </p>
                                    <div class="bg_img">
                                        <img src="<?php echo base_url('assets/images/faded-bg.png'); ?>" alt="">
                                    </div>
                                </div><!-- heading -->
                            </div><!-- tlnt-box -->
                            <div class="clearfix"></div>
                           <div class="tlnt-box">
                                <div class="heading no-img">
                                    <?php echo $why_me_2->title;?>
                                </div>
                                <div class="body">
                                    <p><?php echo $why_me_2->description;?></p>
                                    <div class="bg_img">
                                        <img src="<?php echo base_url('assets/images/faded-bg.png'); ?>" alt="">
                                    </div>
                                </div> <!-- heading -->
                            </div> <!-- tlnt-box -->

                        </div><!-- column -->
                      
             
                       
                      <div class="col-md-6">

                            <div class="tlnt-box">
                                <div class="heading mw240">
                                    <?php echo $why_me_3->title;?>
                                </div>
                                <div class="body">
                                    <p> <?php echo $why_me_3->description;?></p>
                                    <div class="bg_img">
                                        <img src="<?php echo base_url('assets/images/faded-bg.png'); ?>" alt="">
                                    </div>
                                </div> <!--heading--> 
                            </div> <!--tlnt-box--> 

                            <div class="tlnt-box">
                                <div class="heading mw225">
                                    <?php echo $why_me_4->title;?>
                                </div>
                                <div class="body">
                                    <p> <?php echo $why_me_4->description;?></p>
                                    <div class="bg_img">
                                        <img src="<?php echo base_url('assets/images/faded-bg.png'); ?>" alt="">
                                    </div>
                                </div> <!--  heading -->
                            </div><!--   tlnt-box--> 

                            <div class="tlnt-box">
                                <div class="heading mw150">
                                     <?php echo $why_me_5->title;?>
                                </div>
                                <div class="body">
                                    <p> <?php echo $why_me_5->description;?></p>
                                    <div class="bg_img">
                                        <img src="<?php echo base_url('assets/images/faded-bg.png'); ?>" alt="">
                                    </div>
                                </div><!--   heading -->
                            </div> <!--  tlnt-box -->

                        </div> <!--  column -->

                    </div><!-- row -->

                </div><!-- about-box -->
            </div><!-- column -->

            <div class="col-md-4 col-sm-12">
                <div class="row">
                    <div class="col-md-12 col-sm-6 col-xs-12 fb-lb"><!-- facebook like box code here-->
                        <img src="<?php echo base_url('assets/images/fb-lb.jpg'); ?>" class="img-responsive" alt="">
                    </div>

                    <div class="col-md-12 col-sm-6 col-xs-12 margin" style="">


                        <?php
                        if ($all_testimonials):
                            ?>


                            <h1>TESTIMONIALS:</h1>
                            <ul class="bxslider">
                                <?php foreach ($all_testimonials as $test):
                                    ?>


                                    <li>
                                        <div class="textimonials-box">
                                            <p><?php echo content_limiter($test->t_description, 250, '..<a href="'.base_url('testimonial').'">[+]</a>'); ?></p>
                                        </div>
                                        <div class="info text-center">
                                            <h2><?php echo $test->author; ?></h2>
                                            <p><?php echo $test->address . $test->address2; ?></p>
                                        </div>
                                    </li>




                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>

                    </div><!-- column -->

                </div>

            </div><!-- column -->

        </div><!-- row -->

    </div><!-- container -->
</main>
</div><!--bg-pattern-grey -->








