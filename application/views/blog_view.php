<main>  
    <div class="container contain contain-white blog">
        <div class="row">
            <div class="col-md-8 mb15">

                <?php
                if ($all_blogs):
                    $i = 1;
                    foreach ($all_blogs as $all_blog): 
                        ?>
                        <?php if ($i == 1) { ?>
                            <a href="<?php echo base_url("blog-detail/$all_blog->slug"); ?>" class="hidden-sm hidden-xs hidden-sm"> 
                                <div class="latest-post">
                                    <?php if ($all_blog && $all_blog->image && $image_name = $this->misc_library->get_image_filename($all_blog->image, 'blog', 'large')) {
                                        ?> 
                                        <?php if (file_exists(BASEPATH . '../assets/images/blog/' . $image_name)) { ?>
                                            <img src="<?php echo base_url() . '/assets/images/blog/' . $image_name; ?>" class="img-responsive" alt="<?php echo $all_blog->title; ?>"/>
                                            <?php
                                        }
                                    }
                                    ?>
                                    <div class="blog-date"> <?php $timestamp = strtotime($all_blog->create_date); ?>
                                        <big><?php echo date('d', $timestamp); ?></big>
                                        <?php echo date('M', $timestamp) ?>’ <?php echo date('y', $timestamp); ?>
                                    </div>
                                    <h2 class="title"><?php echo $all_blog->title; ?></h2>
                                    <p class="comment"><span class="disqus-comment-count" data-disqus-url="<?php echo base_url("blog-detail/".$all_blog->slug."/"); ?>"> <!-- Count will be inserted here --> </span></p>
                                </div>
                            </a>
                        <div class="blog-post visible-sm visible-xs">
                                <div class="row">
                                    <div class="col-md-3 col-sm-3">
                                        <a href="<?php echo base_url("blog-detail/$all_blog->slug"); ?>"> 
                                            <?php if ($all_blog && $all_blog->image && $image_name = $this->misc_library->get_image_filename($all_blog->image, 'blog', 'small')) {
                                                ?> 
                                                <?php if (file_exists(BASEPATH . '../assets/images/blog/' . $image_name)) { ?>
                                                    <img src="<?php echo base_url() . '/assets/images/blog/' . $image_name; ?>" class="img-responsive" alt="<?php echo $all_blog->title; ?>"/>
                                                    <?php
                                                }
                                            }
                                            ?>


                                                                                                                                       <!-- <img src="<?php echo base_url('assets/images/blog-med.jpg'); ?>" alt="" class="img-responsive">--></a>
                                        <div class="blog-date"><?php $timestamp = strtotime($all_blog->create_date); ?>
                                            <big><?php echo date('d', $timestamp); ?></big>
                                            <?php echo date('M', $timestamp); ?> ’<?php echo date('y', $timestamp); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-9">
                                        <h3><a href="<?php echo base_url("blog-detail/$all_blog->slug"); ?>"> <?php echo $all_blog->title; ?></a></h3>
                                        <p class="comments"><span class="disqus-comment-count" data-disqus-url="<?php echo base_url("blog-detail/$all_blog->slug"); ?>"> <!-- Count will be inserted here --> </span></p>
                                        <div class="desc">
                                            <?php echo $all_blog->excerpt; ?>
                                        </div>

                                    </div>
                                </div>
                                <a href="<?php echo base_url("blog-detail/$all_blog->slug"); ?>" class="read-more">READ MORE</a>
                            </div>

                        <?php } else { ?>

                            <div class="blog-post">
                                <div class="row">
                                    <div class="col-md-3 col-sm-3">
                                        <a href="<?php echo base_url("blog-detail/$all_blog->slug"); ?>"> 
                                            <?php if ($all_blog && $all_blog->image && $image_name = $this->misc_library->get_image_filename($all_blog->image, 'blog', 'small')) {
                                                ?> 
                                                <?php if (file_exists(BASEPATH . '../assets/images/blog/' . $image_name)) { ?>
                                                    <img src="<?php echo base_url() . '/assets/images/blog/' . $image_name; ?>" class="img-responsive" alt="<?php echo $all_blog->title; ?>"/>
                                                    <?php
                                                }
                                            }
                                            ?>
                                                                                                                                       <!-- <img src="<?php echo base_url('assets/images/blog-med.jpg'); ?>" alt="" class="img-responsive">--></a>
                                        <div class="blog-date"><?php $timestamp = strtotime($all_blog->create_date); ?>
                                            <big><?php echo date('d', $timestamp); ?></big>
                                            <?php echo date('M', $timestamp); ?> ’<?php echo date('y', $timestamp); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-9">
                                        <h3><a href="<?php echo base_url("blog-detail/$all_blog->slug"); ?>"> <?php echo $all_blog->title; ?></a></h3>
                                        <p class="comments"><span class="disqus-comment-count" data-disqus-url="<?php echo base_url("blog-detail/$all_blog->slug"); ?>"> <!-- Count will be inserted here --> </span></p>
                                        <div class="desc">
                                            <?php echo $all_blog->excerpt; ?>
                                        </div>
                                    </div>
                                </div>
                                <a href="<?php echo base_url("blog-detail/$all_blog->slug"); ?>" class="read-more">READ MORE</a>
                            </div>

                        <?php } ?>
                        <?php
                        $i++;
                    endforeach;
                    ?>
                <?php endif; ?>
                        <?php echo $this->pagination->create_links(); ?>
            </div>
            <div class="col-md-4">
                <div class="top-stories">
                    <header>
                        <h2>TOP STORIES</h2>
                    </header>
                    <div class="stories">
                    <?php if ($top_stories): ?>
                        <?php foreach ($top_stories as $top): ?>    
                            <div class="top-story">
                                <div class="row">
                                    <div class="col-md-3 col-xs-3">
                                        <a href="<?php echo base_url("blog-detail/$top->slug"); ?>">
                                            <?php if ($top && $top->image && $image_name = $this->misc_library->get_image_filename($top->image, 'blog', 'top_stories')) {
                                                ?> 
                                                <?php if (file_exists(BASEPATH . '../assets/images/blog/' . $image_name)) { ?>
                                                    <img src="<?php echo base_url() . '/assets/images/blog/' . $image_name; ?>" class="img-responsive"/>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </a>
                                    </div>
                                    <div class="col-md-9 col-xs-9">
                                        <div><a href="<?php echo base_url("blog-detail/$top->slug"); ?>"><?php echo $top->title; ?></a></div>
                                      <?php /*  <p class="comments"><span class="disqus-comment-count" data-disqus-url="<?php echo base_url("blog-detail/".$top->slug); ?>"> <!-- Count will be inserted here --> </span></p> */?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </div>
                    <div class="more"><a href="javascript:void(0)" id ="fetch-ts" data-total-story = "3" data-url = "<?php echo base_url("top-stories"); ?>">More Stories</a></div>
                </div><!-- top stories ends-->
                <div id="about-fb-page">
                    <h2><img src="<?php echo base_url('assets/images/fb-header.jpg'); ?>"></h2>
                    <div class="main-fb-plugin">
                        <div class="fb-page" data-href="https://www.facebook.com/joncrocillaproperty" data-width="100%" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/joncrocillaproperty"><a href="https://www.facebook.com/joncrocillaproperty">Jon Crocilla Luxury Properties</a></blockquote></div></div>
                    </div>
                </div> <!--fb plugin ends-->
            </div>
        </div>
    </div>
</main>