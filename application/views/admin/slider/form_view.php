<div class="row">
    <div class="col-sm-6">
        <div class="add-container">
            <p>
                <a class="btn btn-info" href="<?php echo base_url('admin/slider'); ?>"><span class="fa fa-fw fa-lg fa-mail-reply"></span> Back</a>
            </p>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title"><span class="fa fa-lg fa-edit"></span> <?php echo $slider_info ? $slider_info->title . ' : edit slider' : ' add new slider'; ?></h4>
                </div>
                <div class="panel-body">

                    <?php echo validation_errors(); ?>



                    <form action="" enctype="multipart/form-data" id="signupForm" class="form"  method="post">



                        <!--   <div class="form-group">
           
                               <label for="title">Title:</label>
           
                               <input class="form-control" type="text" name="title" value="<?php echo $slider_info ? $slider_info->title : '' ?>" id="title" >
           
                           </div>
           
           
           
                           <div class="form-group">
           
                               <label for="caption">Caption:</label>
           
                               <input class="form-control" type="text" name="caption" value="<?php echo $slider_info ? $slider_info->caption : ''; ?>" id="caption">
           
                           </div>
           
           
           
                           <div class="form-group">
           
                               <label for="sub_caption">Sub Caption:</label>
           
                               <input class="form-control" type="text" name="sub_caption" value="<?php echo $slider_info ? $slider_info->sub_caption : ''; ?>" id="sub_caption">
           
                           </div>-->

                        <div class="form-group">
                            <label for="type" >Type:</label>
                            <select class="form-control" name="type" id="slider_type">
                                <option value='about' <?php echo set_select('type', 'about', $slider_info ? ($slider_info->type == 'about' ? true : false ) : false ); ?>>About Slider</option>
                                <option value='listing' <?php echo set_select('type', 'listing', $slider_info ? ($slider_info->type == 'listing' ? true : false ) : false ); ?>>Listing Slider</option>
                                <option value='home' <?php echo set_select('type', 'home', $slider_info ? ($slider_info->type == 'home' ? true : false ) : false ); ?>>Home Slider</option>
                            </select>
                        </div>

                        <!--<div class="form-group">
                        
                            <label for="link_caption">Link Caption:</label>
                        
                                <input class="form-control" type="text" name="link_caption" value="<?php //echo set_value('link_caption', $slider_info ? $slider_info->link_caption : '');   ?>" id="link_caption">
                        
                            </div>
                        
                        
                        
                            <div class="form-group">
                        
                                <label for="link">Link:</label>
                        
                                <input class="form-control" type="text" name="link" value="<?php echo $slider_info ? $slider_info->link : ''; ?>" id="link">
                        
                            </div>-->





                        <div class="form-group">

                            <label for="image">Image:</label>

                            <input type="file" name="image" value="<?php echo $slider_info ? $slider_info->image : ''; ?>" id="image">
                            <div class="form_group">
                                <?php if ($slider_info && $slider_info->image && $image_name = $this->misc_library->get_image_filename($slider_info->image, 'slider', 'main')) {
                                    ?> 
                                    <?php if (file_exists(BASEPATH . '../assets/images/slider/' . $image_name)) { ?>
                                        <img src="<?php echo base_url() . '/assets/images/slider/' . $image_name; ?>" class="img-thumbnail"/>
                                        <?php
                                    }
                                }
                                ?>
                            </div>                
                        </div>







                        <div class="form-group">

                            <label for="is_active" > Visible:</label>

                            <select class="form-control" name="is_active" id="is_active">

                                <option value=1 <?php echo set_select('is_active', '1', $slider_info ? ($slider_info->is_active == 1 ? true : false ) : false ); ?>>Yes</option>

                                <option value=0 <?php echo set_select('is_active', '0', $slider_info ? ($slider_info->is_active == 0 ? true : false ) : false ); ?>>No</option>

                            </select>

                        </div>



                </div>
            </div> <!-- end panel -->







            <p>
                <input type="hidden" value="<?php echo set_value('id', $slider_info ? $slider_info->id : ''); ?>" name="id">



                <button type="submit" name="btn_slider" class="btn btn-success"><span class="fa fa-fw fa-lg fa-check"></span> Done </button>

            </p>


            </form>

        </div>
    </div><!-- /.col-sm-8 -->
</div><!-- /.row -->







<script src="<?php echo base_url() . 'assets/js/jquery.js'; ?>" type="text/javascript"></script>

<script src="<?php echo base_url() . 'assets/js/validation.js'; ?>" type="text/javascript"></script>

<script src="<?php echo base_url() . 'assets/js/jquery.validate.js'; ?>" type="text/javascript"></script>

<link rel="stylesheet" href="<?php echo base_url() . 'assets/css/screen.css'; ?>" type="text/css" media="screen">

