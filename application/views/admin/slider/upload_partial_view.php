<div id="file-uploader_<?php echo $index; ?>">
    <div class="qq-uploader">
        <div id="uploadFile_<?php echo $index; ?>">
            <div class="qq-uploader" >
                <div class="qq-sn"></div>
                <div class="qq-input-type"><textarea id="textarea_<?php echo $index; ?>" class="form-control"></textarea></div>
                <div class="qq-image" id="image_<?php echo 5; ?>"></div>
                <div class="qq-upload-drop-area" style="display: none;"><span>Drop files here to upload</span></div>
                <div class="qq-upload-button btn btn-primary mb10" id="<?php echo $index; ?>" ><span class="uploadControl">Browse</span>
                    <input type="file" name="file" style="position: absolute;height:30px; width:105px; right: 0pt; top: 0pt; font-family: Arial; font-size: 118px; margin: 0pt; padding: 0pt; cursor: pointer; opacity: 0;"/>
                </div>
                <ul  class="qq-upload-list">
                    <li>
                        <span class="qq-progress-bar"></span>
                        <span class="qq-upload-file"></span>
                        <span class="qq-upload-spinner"></span>
                        <span class="qq-upload-size"></span>
                        <a class="qq-upload-cancel" href="#">Cancel</a>
                        <span class="qq-upload-failed-text">Failed</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>