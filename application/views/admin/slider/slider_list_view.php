<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo ($type == 'about' ? "About" : "Listing"); ?></h3>
    </div>
    <div class="panel-body">
        <p>
            <a href="<?php echo base_url('admin/slider/add_edit'); ?>" class="btn btn-primary"><span class="fa fa-plus"></span> Add New Slider</a><br>
        </p><?php //echo 'hi';  ?>
        <?php if ($slider_info) { ?>

            <table class="table table-bordered <?php
            if ($slider_info) {
                echo "dataTables-table";
            }
            ?>">
                <thead>
                    <tr>
                        <th>S.N.</th>
                        <th>Name</th>
                        <th>Publish</th>
                        <th class="action-col">Option</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $i = 1;
                    //echo '<pre>';print_r($gallery_info);die;

                    foreach ($slider_info as $slider):
                        ?>

                        <tr>

                            <td ><?php echo $i; ?></td>

                            <td><?php echo $slider->title ?></td>

                            <td>



                                <select class="form-control" name="is_active" id="change_status_<?php echo $slider->id; ?>" class="content_status" onchange="return change_status(this, '<?php echo base_url($this->config->item('backend')); ?>/slider/change-status')">

                                    <option value="1" <?php echo $slider ? (($slider->is_active == 1 ) ? 'selected' : '' ) : '' ?>>Published</option>

                                    <option value="0" <?php echo $slider ? (($slider->is_active == 0) ? 'selected' : '' ) : '' ?>>Unpublished</option>

                                </select>  

                            </td>

                            <td class="action-col">

                                <div class="action-pos">

                                    <form method="post" action="<?php echo base_url('admin/slider/add_edit'); ?>" class="pull-left">

                                        <input type="hidden" value="<?php echo $slider->id; ?>" name="id">
                                        <button class="btn btn-info" type="submit" title="Edit"><span class="fa fa-edit fa-lg"></span></button>

                                    </form>

                                    <form method="post" action="<?php echo base_url($this->config->item('backend')) . '/slider/delete' ?>" class="pull-left" onsubmit="return confirm('Are you sure?')">

                                        <input type="hidden" value="<?php echo $slider->id; ?>" name="id">

                                        <button type="submit" class="del_con btn btn-danger" title="Delete"><span class="fa fa-trash-o fa-lg"></span></button>

                                    </form>

                                    <?php /*
                                      <form method="post" action="<?php echo base_url($this->config->item('backend')) . '/gallery/gallery_open' ?>">

                                      <input type="hidden" value="<?php echo $gallery->id; ?>" name="id">

                                      <button type="submit" class="btn btn-success" title="Open"><span class="fa fa-external-link fa-lg"></span></button>

                                      </form> */ ?>
                                    <a href="<?php echo base_url($this->config->item('backend')) . '/slider/slider_open/' . $slider->id ?>" title="Upload Images to slider" class="btn btn-success"><span class="fa fa-external-link fa-lg"></span></a>

                                </div>

                            </td>

                        </tr>

                        <?php
                        $i++;

                    endforeach;
                    ?>
                </tbody>
            </table>


            <?php
        }else {
            ?><tr><td colspan="5"><?php echo 'Slider Not Available.'; ?></td></tr>

            <?php
        }
        ?>

    </div>
</div>