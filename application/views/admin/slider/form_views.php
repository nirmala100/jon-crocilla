
<body>
    <div class="row">
        <div class = "col-md-12">
            <!-- Generate the form using form helper function: form_open_multipart(); -->
            <?php echo form_open_multipart('admin/slider/do_upload', array('class' => 'upload-image-form')); ?>
            <!--        <div class="form-group">
                        <label for="type" >Type:</label>
                        <select class="form-control" name="type" id="slider_type">
                            <option value='about' <?php //echo set_select('type', 'about', $slider_info ? ($slider_info->type == 'about' ? true : false ) : false );            ?>>About Slider</option>
                            
                        </select>
                    </div>-->

            <br>Please upload image of size 700px X 300px <br>
            <input type="file" multiple = "multiple" accept = "image/*" name="uploadfile[]" size="20" /><br />

            <input type="submit" name = "submit" value="Upload" class = "btn btn-primary" />
            </form>

            <!-- AJAX Response will be outputted on this DIV container -->
            <div class = "upload-image-messages row"></div>
            <div class="imagesubmmit row"></div>
        </div>
    </div>   
    <script>
        jQuery(document).ready(function ($) {
            var img_arr = [];
            var image_count = 0;
            var options = {
                beforeSend: function () {

// Replace this with your loading gif image
                    // $(".upload-image-messages").html('<p><img src = "<?php //echo base_url();        ?>assets/images/loading.gif" class = "loader" /></p>');
                },
                complete: function (response) {
                    // Output AJAX response to the div container
                    //var result = $.parseJSON(response.responseText);
                    //json string into json object
                    if ($('input[type=file]').val() == '') {
                        // alert ('test');
                        return false;

                    }
                    var JSONObject = JSON.parse(response.responseText);
                    //console.log(JSONObject);      // Dump all data of the Object in the console
                    //alert(JSONObject[0]["file_name"]);


                    var counter = 0;
                    $.each(JSONObject, function (index, data) {
                        counter++;
                        //  $('.upload-image-messages').append(data.index);
                        img_arr.push(data.file_name);
                        var append_content = '<div class="col-md-3">'
                                + '<img src="<?php echo base_url(); ?>assets/images/slider/' + data.file_name + '" width=100% alt="Image" title="' + data.file_name + '" id="upload_img_' + counter + '"/>'
                                + '<div class="delete" id="delete_btn_' + counter + '"><button type="button" class="btn-block btn btn-danger btn-small btn_delete" data-id="' + counter + '" data-image_name="' + data.file_name + '"> X </button></div>'
                                + '</div>';

                        if (counter % 4 == 0) {
                            append_content += '<div class="clearfix"></div>';
                        }

                        $('.upload-image-messages').append(append_content);

                        console.log(append_content);

                    });

                    //console.log(img_arr);
                    var type = $('#slider_type').val();

                    var btn_count = $('.imageSubmitForm').length;

                    if (btn_count < 1) {
                        $('.imagesubmmit').append('<form class="imageSubmitForm" action="<?php echo base_url(); ?>admin/slider/add" method="post">' +
                                '<input type="submit"  value="submit" class="btn btn-success">' +
                                '<input type="hidden" name="imagecap" value="" id="img_arr_p"> ' +
                                // '<input type="hidden" name="slider_type" value="' + type + '"> ' +
                                '<input type="hidden" name="slider_type" value="about">' +
                                '</form>');
                    }

                    $('#img_arr_p').val(img_arr);

                    $('input[type=file]').val('');
                    //$('input[type=file]').hide();
                    //$('#btn_upload').hide();


                    //to delete the image
                    $('.delete button').click(function () {

                        var name = $(this).data('image_name');
                        var id = $(this).data('id');

                        if (image_count > 0) {
                            return false;
                        }

                        if (!confirm("Are you sure to delete?")) {
                            return false;
                        }
                        else {
                            image_count++;
                        }


                        $.ajax({
                            type: 'post',
                            url: '<?php echo base_url('admin/slider/delete_image'); ?>',
                            data: {image_name: name},
                            success: function (res) {
                                if (res != 0) {
                                    var img_arr = $('#img_arr_p').val();

                                    var the_arr = img_arr.split(',');

                                    var final_arr = "";
                                    $.each(the_arr, function (ind, val) {
                                        if (val != name) {
                                            final_arr += val + ",";
                                        }
                                    });

                                    $('#img_arr_p').val(rtrim(final_arr, ','));
                                    $('#upload_img_' + id).parent().remove();
                                    // $('#delete_btn_' + id).remove();

                                    if ($('.btn_delete').length == 0) {
                                        // $('input[type=file]').show();
                                        // $('#btn_upload').show();
                                        $('.imagesubmmit').html('');
                                    }
                                    image_count = 0;

                                } else {
                                    alert('Cannot be deleted.');
                                }
                            }

                        });

                    });


                    // console.log(result);
//                        if ($.isArray(result)) {
//                            alert('is_array');
//                        } else {
//                            alert('not array');
//                        }
                    //$('.upload-image-messages').html(response);
                    //$(".upload-image-messages").html(response.responseText);
                    $('html, body').animate({scrollTop: $(".upload-image-messages").offset().top - 100}, 150);

                }
            };
            // Submit the form
            $(".upload-image-form").ajaxForm(options);

            return false;

        });
        function rtrim(str, chr) {
            var rgxtrim = (!chr) ? new RegExp('\\s+$') : new RegExp(chr + '+$');
            return str.replace(rgxtrim, '');
        }
    </script>

    <!--</body>
    </html> -->

