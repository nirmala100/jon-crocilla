<div class="row">
    <div class="col-lg-12">
        <a class="btn btn-info" href="<?php echo base_url('admin/slider/add-edit'); ?>"> <span class="fa fa-fw fa-plus"></span> Add new slider</a><br>
    </div>
</div>
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-comments fa-lg fa-fw"></i> List of all About Slider

            </div>
            <!-- /.panel-heading -->

            <div class="panel-body">
                <table class="table table-bordered ">
                    <thead>
                        <tr>
                            <th>S.N.</th>
                            <th>Type</th>


                            <th>Action</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if ($slider_row):

                            $i = 1; //for numbering value S.N.
                            foreach ($slider_row as $slider):
                                ?>
                                <tr>
                                    <td ><?php echo $i; ?></td>


                                    <td>

                                        <?php if (file_exists(BASEPATH . '../assets/images/slider/' . $slider->image)) { ?>
                                            <img src="<?php echo base_url() . '/assets/images/slider/' . $slider->image; ?>" class="img-thumbnail" height="100" width="100"/>
                                            <?php
                                        }
                                        // }
                                        ?>
                                    </td>

                                    <td class="action-col">
                                        <div class="action-pos">

                                            <form method="post" action="<?php echo base_url($this->config->item('backend')) . '/slider/delete' ?>" onsubmit="return confirm('Are you sure?')">
                                                <input type="hidden" value="<?php echo $slider->id; ?>" name="id">
                                                <button type="submit" class="del_con btn btn-danger" title="Delete"><span class="fa fa-trash-o fa-lg"></span></button>
                                            </form>

                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            endforeach;
                        else:
                            ?>
                            <tr>
                                <td colspan="5">There are no images.</td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<?php
echo $this->session->flashdata('msg');
?>

