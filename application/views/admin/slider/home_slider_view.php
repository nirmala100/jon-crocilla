<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-comments fa-lg fa-fw"></i> Home Slider

            </div>
            <!-- /.panel-heading -->

            <div class="panel-body">
                <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url('admin/slider/post_home_slider'); ?>">
                    <table class="table table-bordered ">
                        <thead>
                            <tr>
                                <th width="5%">S.N.</th>
                                <th width="15%">Day</th>
                                <th width="40%">Morning(am)</th>

                                <th width="40%">Evening(pm)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($days) {
                                $i = 0;
                                $count = 0;
                                ?>
                                <?php
                                foreach ($days as $day) {
                                    $slider_am = $this->misc_library->get_slider_image_by_day($day->day_no, 0,'home');
                                    $slider_pm = $this->misc_library->get_slider_image_by_day($day->day_no, 1,'home');
                                    ?>
                                    <tr>
                                        <td><?php echo ++$i; ?></td>
                                        <td>
        <?php echo date('l', strtotime("Sunday +" . $day->day_no . " days")); ?>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-7 text-left">
                                                        <label>Upload: Please upload image atleast of size 1500px X 1100px </label> 
                                                        <input type="file" placeholder="Upload" name="sliders[<?php echo $count; ?>][image]">
                                                        <input type="hidden" name="sliders[<?php echo $count; ?>][id]" value="<?php echo set_value('id', ($slider_am) ? $slider_am->id : ''); ?>">
                                                        <input type="hidden" name="sliders[<?php echo $count; ?>][time]" value="0">
                                                        <input type="hidden" name="sliders[<?php echo $count; ?>][day_no]" value="<?php echo $day->day_no; ?>">
                                                    </div>
                                                    <div class="col-lg-5">
                                                        <?php echo $slider_am->image;if ($slider_am && $slider_am->image && $image = $this->misc_library->get_image_filename($slider_am->image, 'slider', 'listing')) { ?> 
                                                            <?php if (file_exists(BASEPATH . '../assets/images/slider/' . $image)) { ?>
                                                                <img src="<?php echo base_url() . '/assets/images/slider/' . $image; ?>" alt="" class="img-responsive img-full" />
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
        <?php $count++; ?>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-7 text-left">
                                                        <label>Upload : Please upload image atleast of size 1500px X 1100px</label>
                                                        <input type="file" placeholder="Upload" name="sliders[<?php echo $count; ?>][image]">
                                                        <input type="hidden" name="sliders[<?php echo $count; ?>][id]" value="<?php echo set_value('id', ($slider_pm) ? $slider_pm->id : ''); ?>">
                                                        <input type="hidden" name="sliders[<?php echo $count; ?>][time]" value="1">
                                                        <input type="hidden" name="sliders[<?php echo $count; ?>][day_no]" value="<?php echo $day->day_no; ?>">
                                                    </div>
                                                    <div class="col-lg-5">
                                                        <?php echo $slider_pm->image;if ($slider_pm && $slider_pm->image && $image = $this->misc_library->get_image_filename($slider_pm->image, 'slider', 'listing')) { ?> 
                                                            <?php if (file_exists(BASEPATH . '../assets/images/slider/' . $image)) { ?>
                                                                <img src="<?php echo base_url() . '/assets/images/slider/' . $image; ?>" alt="" class="img-responsive img-full" />
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                    <?php $count++; ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            <?php } else { ?>
                                <tr colspan="4">No Days</tr>
<?php }
?>
                        </tbody>
                    </table>
                    <input type="submit" class="btn btn-primary" value="Submit"/>
                </form>

            </div>
        </div>
    </div>

</div>

<?php
echo $this->session->flashdata('msg');
?>

