<script>

    /* add section */
    var toTal = 1;
    var coUnt = $('.images [id^=image_]').length + 1;
    toTal = coUnt;
    var index;
    $('.qq-upload-button').each(function () {
        var counter = $(".images [id^=image_]").size();
        if (counter <= 0) {
            counter = 0;
            index = counter;
        } else {
            var last = $(".images [id^=image_]:last").attr("id");
            index = last.split('_')[1];
            index++;
        }
        initiateUpload(index);
    });
    /*$("#add_image_field").click(function() {
     var _this = this;
     $(_this).attr('disabled', 'disabled');
     $(_this).html('loading...');
     $.ajax({
     success: function(html) {
     $(".images").append(html);
     initiateUpload(coUnt);
     },
     complete: function() {
     $(_this).removeAttr('disabled');
     $(_this).html('+Add New');
     //$('html,body').animate({
     // scrollTop: $("#remove_" + coUnt).offset().top},          1000
     //);
     },
     type: 'get',
     url: '<?php echo base_url('admin/gallery/addimagefield'); ?>',
     data: {index: coUnt},
     cache: false,
     dataType: 'html'
     });
     });
     
     /*function initiateUpload(index)
     {
     
     new qq.FileUploader({
     'element': document.getElementById('uploadFile_' + index),
     'debug': true,
     'multiple': false,
     'action': '<?php echo base_url('admin/gallery/upload_files'); ?>/' + index,
     'allowedExtensions': ['jpg', 'jpeg', 'gif', 'png', 'pdf', 'doc', 'docx'],
     'sizeLimit': 6000 * 5000,
     'params': {
     id: 1,
     value: 52
     },
     'onSubmit': function()
     {
     $('#uploadFile_' + index).find('.uploadControl').text('Uploading...');
     },
     'template': '<div class="qq-uploader">' +
     '<ul class="qq-upload-list"></ul>' +
     '<div class="qq-upload-drop-area" style="display:none"><span>Drop files here to upload</span></div>' +
     '<input type="hidden" name="image[' + coUnt + '][filename]" id="filename_' + coUnt + '">' +
     '</div>'+
     '<table class="table table-bordered table-striped">' +
     '<tr>' +
     '<td><div class="qq-sn">' + toTal + '</div></td>' +
     '<td><div class="qq-input-type"><textarea name="image[' + coUnt + '][text]" id="textarea_' + coUnt + '" class="form-control" ></textarea></div></td>' +
     '<td style="width:100px;"><div class="qq-image"  id="image_' + coUnt + '"><img src="<?php echo base_url('assets/images/no-img-200.png'); ?>" class="img-thumbnail" width="100px"></div></td>' +
     '<td style="width:110px;"><div class="qq-upload-button btn btn-primary mb10" id="' + coUnt + '"><span class="uploadControl">Browse</span></div></td>' +
     '<td style="width:50px;"><button class="btn btn-danger del_image" type="button" id="delete_' + coUnt + '"> X </button></td>' +
     '</tr>' +
     '</table>' ,
     // template for one item in file list
     'fileTemplate': '<li>' +
     '<span class="qq-progress-bar"></span>' +
     '<span class="qq-upload-file"></span>' +
     '<span class="qq-upload-spinner"></span>' +
     '<span class="qq-upload-size"></span>' +
     '<a class="qq-upload-cancel" href="#">Cancel</a>' +
     '<span class="qq-upload-failed-text" style="display:none">Failed</span>' +
     '</li>',
     'onComplete': function(id, fileName, responseJSON) {
     $('#uploadFile_' + index).find('.uploadControl').text('Browse');
     $('#uploadControl_' + index).text('Browse');
     if (responseJSON.success)
     {
     if (responseJSON.errorSize)
     {
     alert(responseJSON.errorMsg);
     }
     else
     {
     $('#filename_' + index).val(responseJSON.filename);
     $('#image_' + index).html('<img src="' + responseJSON.imageThumb + '" width="100px"/>');
     }
     }
     else
     {
     alert('something went wrong!');
     }
     },
     'messages': {'typeError': '{file} has invalid extension. Only {extensions} are allowed.', 'sizeError': '{file} is too large, maximum file size is {sizeLimit}.', 'minSizeError': '{file} is too small, minimum file size is {minSizeLimit}.', 'emptyError': '{file} is empty, please select files again without it.', 'onLeave': 'The files are being uploaded, if you leave now the upload will be cancelled.'}, 'showMessage': function(message) {
     alert(message);
     }});
     
     coUnt++;
     toTal++;
     }*/

    function initiateUpload(index)
    {
        new qq.FileUploader({'element': document.getElementById('uploadFile'),
            'debug': true,
            'multiple': true,
            'action': '<?php echo base_url('admin/slider/upload_files'); ?>',
            'allowedExtensions': ['jpg', 'jpeg', 'gif', 'png'],
            'sizeLimit': 10485760,
            'onSubmit': function ()
            {
                alert('okay');
                $('.uploadControl').text('Uploading...');
            },
            'onComplete': function (id, fileName, responseJSON) {
                $('.uploadControl').text('Browse');
                console.log(responseJSON);
                if (responseJSON.success)
                {
                    //$('#image-gallery').append('<div class="items form-group col-md-8 well" id="image_'+index+'"><div class="col-md-4"><img src="'+responseJSON.imageThumb+'" class="img-thumbnail"/></div><div class="col-md-6"><textarea class="form-control" placeholder="Caption" name="images[' + index + '][title]"></textarea></div><div class="col-md-2"><button type="button" class="btn btn-danger remove_image">Remove</button><input type="hidden" name="images[' + index + '][id]" value=""/><input type="hidden" name="images[' + index + '][filename]" value="' + responseJSON.filename + '" class="image-filename"/></div><div class="clearfix"></div></div>');
                    $('.images').append('<table class="table table-bordered table-striped" id="gal_img_' + index + '">' +
                            '<tr>' +
                            '<td><div class="qq-sn">' + index + '</div></td>' +
                            '<td><div class="qq-input-type"><textarea name="image[' + index + '][text]" id="textarea_' + index + '" class="form-control" ></textarea></div></td>' +
                            '<td style="width:100px;"><div class="qq-image"  id="image_' + index + '"><img src="' + responseJSON.imageThumb + '" class="img-thumbnail" width="100px"></div></td>' +
                            '<td style="width:50px;"><button class="btn btn-danger del_image" type="button" id="delete_' + index + '"> X </button></td>' +
                            '<input type="hidden" name="image[' + index + '][filename]" id="filename_' + index + '" value="' + responseJSON.filename + '">' +
                            '</tr>' +
                            '</table>');
                    index++;
                }
                else
                {
                    alert('something went wrong ok!');
                }
            },
            'template': '<div class="qq-uploader">' +
                    '<div class="qq-upload-drop-area" style="display:none;"><span>Drop files here to upload</span></div>' +
                    '<div class="qq-upload-button btn btn-primary mb10"><span class="uploadControl">Browse</span></div>' +
                    '<div class="blue">Add one or more</div>' +
                    '<ul class="qq-upload-list"></ul>' +
                    '</div>',
            // template for one item in file list
            'fileTemplate': '<li>' +
                    '<span class="qq-upload-file"></span> ' +
                    '<span class="qq-upload-spinner"></span>' +
                    '<span class="qq-upload-size"></span>' +
                    '<span class="qq-progress-bar"></span>' +
                    '<a class="qq-upload-cancel" href="#">Cancel</a>' +
                    '<span class="qq-upload-failed" style="display:none;">Failed</span>' +
                    '</li>',
            'messages': {'typeError': '{file} has invalid extension. Only {extensions} are allowed.', 'sizeError': '{file} is too large, maximum file size is {sizeLimit}.', 'minSizeError': '{file} is too small, minimum file size is {minSizeLimit}.', 'emptyError': '{file} is empty, please select files again without it.', 'onLeave': 'The files are being uploaded, if you leave now the upload will be cancelled.'}, 'showMessage': function (message) {
                alert(message);
            }});
    }

    $('.del_gallery_img').click(function () {
        if (!confirm("Are you sure to delete?")) {
            return false;
        }
        var button_id = $(this).attr('id');
        var id_arr = button_id.split('_');
        var id = id_arr[1];
        var img_id = $(this).attr('data-id');
        $.ajax({
            type: 'post',
            url: '<?php echo base_url('admin/gallery/delete_gallery_image'); ?>',
            data: 'id=' + img_id,
            success: function (res) {
                if (res == 1) {
                    $('#gal_img_' + id).remove();
                    $('.no-image').hide();
                } else {
                    alert('Cannot be deleted.');
                }
            }
        });
    });
    $(document).on('click', '.del_image', function () {
        if (!confirm("Are you sure to delete?")) {
            return false;
        }
        var button_id = $(this).attr('id');
        var id_arr = button_id.split('_');
        var id = id_arr[1];
        var image_name = $('#filename_' + id).val();
        $.ajax({
            type: 'get',
            url: '<?php echo base_url('admin/gallery/delete_image'); ?>',
            data: 'image=' + image_name,
            success: function (res) {
                if (res == 1) {
                    $('#gal_img_' + id).remove();
                    console.log('#gal_img_' + id);
                } else {
                    alert('Cannot be deleted.');
                }
            }
        });
    });
</script>