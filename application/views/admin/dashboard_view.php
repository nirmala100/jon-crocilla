<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-globe fa-lg fa-fw"></i> Website Settings
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <form role="form" method="post" action="<?php echo base_url('admin/postWebInformation'); ?>">

                    <div class="form-group">
                        <label>Top Left Text</label>
                        <textarea class="form-control" name="site-name" ><?php echo $sitename; ?></textarea>
                    </div>
                    <?php echo $this->ckeditor->replace("site-name"); ?>
                    <!--<div class="form-group">
                        <label>Middle Area Text</label>
                        <textarea class="form-control" name="website-title" ><?php //echo $websitetitle;   ?></textarea>
                    </div>
                    <?php //echo $this->ckeditor->replace("website-title"); ?>
                    -->
                    <div class="form-group">
                        <label>Website Email</label>
                        <input class="form-control" type="text" name="email" value="<?php echo $email; ?>" placeholder="Enter Website Email">
                    </div>

                    <button type="submit" class="btn btn-default">Save</button>
                </form>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-tags fa-lg fa-fw"></i> Meta Tags
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <?php //if($generic){?>
                <form role="form"  method="post" action="<?php echo base_url('admin/seo/updategeneric'); ?>">
                    <div class="form-group">
                        <label>Meta Title</label>
                        <input class="form-control" name="meta_title" type="text" placeholder="Enter Meta Title" value="<?php echo $generic ? $generic->meta_title : ''; ?>">
                    </div>
                    <div class="form-group">
                        <label>Meta Keywords</label>
                        <input class="form-control" name="meta_keyword" type="text" placeholder="Enter Meta Keywords" value="<?php echo $generic ? $generic->meta_keyword : ''; ?>">
                    </div>
                    <div class="form-group">
                        <label>Meta Description</label>
                        <textarea class="form-control" name="meta_description" rows="3"><?php echo $generic ? $generic->meta_description : ''; ?></textarea>
                    </div>
                    <button type="submit" class="btn btn-default" name="savegeneric" value="submit">Save</button>
                </form>
                <?php //}?>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-8 -->
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-phone fa-lg fa-fw"></i> Quick Property
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">

                <form role="form"  enctype="multipart/form-data" method="post" action="<?php echo base_url('admin/property/addQuickProperty'); ?>">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Featured</label>
                                <div class="checkbox" style="margin-top:-5px">  
                                    <label for="featured" ><input type="checkbox" class="" name="p_featured" id="featured" value='1' <?php if (isset($featured) && $featured) { ?>checked="checked"<?php } ?> />
                                        Yes</label>                                                                                 
                                </div>
                                <div class="form-group"><label for="title">Title:</label>
                                    <input type="text" name="p_name" class="form-control" value="" id="title">
                                </div>
                                 <div class="form-group"><label for="slug">Slug:</label>
                                    <input type="text" name="slug" class="form-control" value="" id="title">
                                </div>
                                <div class="form-group"> <label for="location">Location:</label>
                                    <input type="text" name="p_location" class="form-control" value="" id="slug">
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group"> <label for="bathroom">Price:</label>
                                            <input type="text" name="p_price" class="form-control" value="" id="slug">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="bathroom">Bathroom:</label>
                                            <input type="text" name="p_bathroom" class="form-control" value="" id="slug">

                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="bedroom">Bedroom:</label>
                                            <input type="text" name="p_bedroom" class="form-control" value="" id="slug">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group"> <label for="sqfeet">Square Feet:</label>
                                    <input type="text" name="p_sq_feet" class="form-control" value="" id="slug">
                                </div>
                                <div class="form-group"> <label for="lotsize">Lot Size:</label>
                                    <input type="text" name="p_lot_size" class="form-control" value="" id="slug">
                                </div>
                                <!-- /.col-sm-8 -->
                            </div><!-- /.row -->
                        </div>
                    </div>
                     <div class="form-group"><label for="short_desc">Short Description:</label>
                        <textarea name="short_desc" class="form-control"  /></textarea>
                    </div>
                    <?php echo $this->ckeditor->replace("short_desc"); ?>
                    <div class="form-group"><label for="p_description">Description:</label>
                        <textarea name="p_description" class="form-control"  /></textarea>
                    </div>
                    <?php echo $this->ckeditor->replace("p_description"); ?>

                    <div class="form-group">
                        <label for="image">Image:</label> Please upload image at least of size 463px X 290px
                        <input type="file" name="p_image"  value="" id="image">
                    </div>

                    <div class="form-group">
                        <label for="is_active" >Page Visible:</label>
                        <select class="form-control" name="is_active" id="is_active">
                            <option value=1 <?php //echo set_select('is_active', '1', $property_info ? ($property_info->is_active == 1 ? true : false ) : false );    ?>>Yes</option>
                            <option value=0 <?php //echo set_select('is_active', '0', $property_info ? ($property_info->is_active == 0 ? true : false ) : false );    ?>>No</option>
                        </select></div>



                    <button type="submit" class="btn btn-success" value="submit" > <span class="fa fa-fw fa-lg fa-check"></span> Submit </button>
                </form>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->



    </div>
    <!-- /.col-lg-4 -->
</div>