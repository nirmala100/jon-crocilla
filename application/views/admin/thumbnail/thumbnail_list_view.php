﻿﻿﻿<a href="<?php echo base_url('admin/thumbnail/add'); ?>" class="btn btn-primary">Add New Thumbnail</a><br>
<br>
<?php
if ($distinct_info):
    ?>
    <div class="row">
        <?php foreach ($distinct_info as $distinct): ?>
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-bar-chart-o fa-fw"></i> <?php echo ucfirst($distinct->manager) . ' Manager'; ?>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="pull-right">
                            <form method="post" action="<?php echo base_url($this->config->item('backend')) . '/thumbnail/edit' ?>">
                                <input type="hidden" value="<?php echo $distinct->manager; ?>" name="manager">
                                <input class="btn btn-info" type="submit" value="Edit">
                            </form>
                        </div>
                        <br class="clearfix">
                        <table class="table table-bordered">
                            <tr>
                                <th>Type</th>
                                <th>Width</th>
                                <th>Height</th>
                                <th>Action</th>
                            </tr>
                            <?php
                            $thumbnail_info = $this->misc_library->get_thumbnail($distinct->manager);
                            foreach ($thumbnail_info as $thumbnail):
                                ?>
                                <tr><td><?php echo $thumbnail->type; ?></td><td><?php echo $thumbnail->width; ?></td><td><?php echo $thumbnail->height; ?></td><td>
                                        <div class="action-pos">
                                            <form method="post" onsubmit="return confirm('Are you sure?');" action="<?php echo base_url($this->config->item('backend')) . '/thumbnail/delete' ?>">
                                                <input type="hidden" value="<?php echo $thumbnail->id ?>" name="id">
                                                <input type="submit" class="del_con btn btn-danger"  value="Delete"></form>
                                        </div>
                                    </td></tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php else: ?>
    <hr>
    <div class="alert alert-info alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        Thumbnails are not defined yet.</div>
<?php endif; ?>