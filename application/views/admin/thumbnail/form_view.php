<?php
echo validation_errors() . '<br>';
if (isset($thumbnail_edit)) {
    ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i>  Edit Thumbnail for <strong><?php echo ucfirst($thumbnail_manager->manager); ?></strong>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <form action="<?php echo base_url('admin/thumbnail/edit') ?>" enctype="multipart/form-data" id="signupForm"  method="post" role="form">
                        <table class="table table-bordered">
                            <tr>
                                <td><label>Type:</label>
                                </td>
                                <td><label>Width:</label></td>
                                <td><label>Height:</label></td>
                            </tr>
                            <?php foreach ($thumbnail_edit as $thumbnail_e) {
                                ?>
                                <tr>
                                    <td><input class='form-control' type="text" name="type[]" value="<?php echo $thumbnail_e->type ?>">&nbsp;&nbsp;</td>
                                    <td><input class="form-control" type="text" name="width[]" value="<?php echo $thumbnail_e->width ?>">&nbsp;&nbsp;</td>
                                    <td><input type="text" class="form-control" name="height[]" value="<?php echo $thumbnail_e->height ?>">&nbsp;&nbsp;&nbsp;</td>
                                </tr>
                            <?php }
                            ?>
                        </table>
                        <div>
                            <div class="pull-right">
                                <input class="btn btn-success" type="submit" name="btn_thumbnail" value="Save">
                            </div>
                            <div class="pull-left">
                                <label>Size:</label>
                                <input type="button" class="btn btn-primary" name="btn_add" id="add_th" value="+Add"></td>
                            </div>
                        </div>
                        <input type="hidden" name="manager" value="<?php echo $thumbnail_e->manager; ?>">
                    </form>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-8 -->
    </div>
    <!-- row-->

    <a class="btn btn-danger" href="<?php echo base_url('admin/thumbnail'); ?>">Back</a>

    <br>
    <?php
} else {
    ?>


    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i>  Add New Thumbnail
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <form action="" id="signupForm"  method="post" role="form">
                        <table class="table table-bordered">
                            <tr>
                                <td><label for="manager">Manager:</label></td>
                                <td colspan="2"><input type="text" class="form-control" name="manager" value="" id="manager" ></td>
                            </tr>
                            <tr>
                                <td><label>Type:</label>
                                </td>
                                <td><label>Width:</label></td>
                                <td><label>Height:</label></td>
                            </tr>

                            <tr>
                                <td><input class='form-control' type="text" name="type[]" value="">&nbsp;&nbsp;</td>
                                <td><input class="form-control" type="text" name="width[]" value="">&nbsp;&nbsp;</td>
                                <td><input type="text" class="form-control" name="height[]" value="">&nbsp;&nbsp;&nbsp;</td>
                            </tr>
                        </table>
                        <div>
                            <div class="pull-right">
                                <input class="btn btn-success" type="submit" name="btn_thumbnail" value="Save">
                            </div>
                            <div class="pull-left">
                                <label>Size:</label>
                                <input type="button" class="btn btn-primary" name="btn_add" id="add_th" value="+Add"></td>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-8 -->
    </div>

    <a class="btn btn-danger" href="<?php echo base_url('admin/thumbnail'); ?>">Back</a>


    <?php
}
?>

