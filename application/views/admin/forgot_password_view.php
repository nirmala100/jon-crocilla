<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Forgot Password? Enter your email here:</h3>
                </div>
                <div class="panel-body">
                    <?php echo $this->session->flashdata('msg'); ?>
                    <form role="form" method="post" action="" id="forgot_password_form">
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control required email" placeholder="Enter your email" name="email" type="text" autofocus>
                            </div>
                            <!-- Change this to a button or input when using this as a form -->
                            <input type="submit" name="submit" class="btn btn-lg btn-success btn-block" value="Submit">
                        </fieldset>
                    </form>
                    <div class="error-message"><?php echo isset($msg) ? $msg : ''; ?></div>
                </div>
            </div>
        </div>
    </div> <!--end of row-->
</div> <!--end of container-->
<script src="<?php echo base_url() . 'assets/js/jquery.validate.js'; ?>" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#forgot_password_form').validate();

        // alert("okay");
    });
</script>