<?php if ($this->misc_library->is_logged()): ?>
    </div>
    <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
<?php endif; ?>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url() . 'assets/js/bootstrap.min.js'; ?>"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url() . 'assets/js/plugins/metisMenu/metisMenu.min.js'; ?>"></script>
<script src="<?php echo base_url() . 'assets/js/jquery.validate.js'; ?>" type="text/javascript"></script>
<script src="<?php echo base_url() . 'assets/js/plugins/dataTables/jquery.dataTables.js'; ?>"></script>
<script src="<?php echo base_url() . 'assets/js/plugins/dataTables/dataTables.bootstrap.js'; ?>"></script>

<!--jquery for calender-->
<script src="<?php echo base_url() . 'assets/js/jquery-ui.js'; ?>"></script>
<?php if ($this->uri->segment(2) == 'slider' && $this->uri->segment(3) == 'about'): ?>
    <script src="<?php echo base_url('assets/js/plugins/ajaxUploader/fileuploader.js'); ?>"></script>
    <?php $this->load->view('admin/slider/upload_script'); ?>
<?php endif; ?>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url() . 'assets/js/sb-admin-2.js'; ?>"></script>
</body>
</html>