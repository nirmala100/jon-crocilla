<?php
$dashboard = $page_open = $property = $listproperty = $listfeatured = $about = $basic = $whychooseme = $testimonial = $slideradd = $sliderabout = $sliderhome = 
$sliderdetail = $property_open = $slider_open = $facebook = $lending_open = $questionnaire = $footer =
$lending = $contact = $lendingboxtext = $blog_open  =  $blog_add =  $blog = "";
$url = $this->uri->uri_string();
switch ($url) {
    case 'admin/dashboard':
        $dashboard = 'active';
        break;
    case 'admin/property/add_edit':
        $property_open = 'active';
        $property = "active";
        break;
    case 'admin/property/index':
        $property_open = 'active';
        $listproperty = "active";
        break;

    case 'admin/about/why-choose-me':
        $about = 'active';
        $whychooseme = "active";
        break;
    case 'admin/about/basic':
        $about = "active";
        $basic = "active";
        break;
    case 'admin/testimonial':
        $page_open = "active";
        $testimonial = "active";
        break;
    case 'admin/slider/add_edit':
        $slider_open = 'active';
        $slideradd = 'active';
        break;
    case 'admin/slider/about-slider':
        $slider_open = 'active';
        $sliderabout = 'active';
        break;
    case 'admin/slider/home-slider':
        $slider_open = 'active';
        $sliderhome = 'active';
        break;
    case 'admin/slider/detail-slider':
        $slider_open = 'active';
        $sliderdetail = 'active';
        break;
    case 'admin/property/featured-property':
        $property_open = 'active';
        $listfeatured = 'active';
        break;
    case 'admin/about/facebook-like':
        $about = 'active';
        $facebook = 'active';
        break;
    case 'admin/questionnaire':
        $lending_open = 'active';
        $questionnaire = 'active';
        break;
    case 'admin/home/lending-box':
        $lending_open = 'active';
        $lendingboxtext = 'active';
        break;
    case 'admin/footerbox':
        $footer = 'active';
        $lending = "active";
        break;
    case 'admin/footerbox':
        $footer = "active";
        $contact = "active";
        break;
     case 'admin/blog':
        $blog = "active";
        $blog_open ="active";
        break;

      case 'admin/blog/add-edit':
        $blog = "active";
        $blog_add ="active";
        break;  
      
}
?>

<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php echo base_url('admin/dashboard'); ?>">Jon Admin</a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">

        <li class="dropdown right">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">

                <li><a href="<?php echo base_url('admin/setting'); ?>"><i class="fa fa-gear fa-fw"></i> Settings</a>
                </li>
                <li class="divider"></li>
                <li><a href="<?php echo base_url('admin/logout'); ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">

                <li>
                    <a class="<?php echo $dashboard; ?>" href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                </li>


                <li class="<?php echo $about; ?>">
                    <a href="#"><i class="fa fa-info fa-fw"></i> About<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?php echo base_url('admin/about/basic'); ?>" class="<?php echo $basic; ?>">About JCLP</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('admin/about/why-choose-me'); ?>" class="<?php echo $whychooseme; ?>">Why Choose Me?</a>
                        </li>
                    </ul>
                </li>
                <li class="<?php echo $property_open; ?>">
                    <a href="#"><i class="fa fa-link fa-fw"></i>Our Properties<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">

                        <li>
                            <a class="<?php echo $listproperty; ?>" href="<?php echo base_url('admin/property/index'); ?>">List property</a>
                        </li>
                        <li>
                            <a class="<?php echo $listfeatured; ?>" href="<?php echo base_url('admin/property/featured-property'); ?>">List featured property</a>
                        </li>

                    </ul>
                </li>

                <li class="<?php echo $page_open; ?>">
                    <a class="<?php echo $testimonial; ?>"href="<?php echo base_url('admin/testimonial/'); ?>"><i class="fa fa-users fa-fw"></i> Testimonials<span class="fa arrow"></span></a>
                   
                </li>

                <li class="<?php echo $slider_open; ?>">
                    <a href="#"><i class="fa fa-sliders fa-fw"></i> Banner Management<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?php echo base_url('admin/slider/about-slider'); ?>" class="<?php echo $sliderabout; ?>">JCLP Banners</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('admin/slider/home-slider'); ?>" class="<?php echo $sliderhome; ?>">Home Page Banners</a>
                        </li>
                    </ul>
                </li>

                <li class="<?php echo $lending_open; ?>">
                    <a href="#"><i class="fa fa-legal fa-fw"></i> Lending<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a class="<?php echo $questionnaire; ?>" href="<?php echo base_url('admin/questionnaire'); ?>"><i class="fa fa-question-circle fa-fw"></i> Questionnaire</a>
                        </li>
                        
                    </ul>

                </li>
                
                
                 <li class="<?php echo $footer; ?>">
                    <a href="#"><i class="fa fa-info fa-fw"></i> Footer Boxes<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?php echo base_url('admin/footerbox'); ?>" class="<?php echo $lending; ?>">Manage</a>
                        </li>
                        
                    </ul>
                </li>
                 <li class="<?php echo $blog; ?>">
                    <a href="#"><i class="fa fa-book fa-fw"></i>Blogs<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">

                        <li>
                            <a class="<?php echo $blog_open; ?>" href="<?php echo base_url('admin/blog'); ?>">List blogs</a>
                        </li>
                        <li>
                            <a class="<?php echo $blog_add; ?>" href="<?php echo base_url('admin/blog/add-edit'); ?>">Add blog</a>
                        </li>
                        
                    </ul>
                </li>


            </ul>
            <!-- /.nav-third-level  -->

        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>
