<div class="row">

    <div class="col-lg-12">

        <div class="panel panel-default">

            <div class="panel-heading">

                <i class="fa fa-paperclip fa-fw fa-lg"></i> Basic Information

            </div>

            <!-- /.panel-heading -->

            <div class="panel-body">

                <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url('admin/about/postBasicInfo'); ?>">

                    <div class="row">

                        <div class="col-lg-6">



                            <div class="form-group">

                                <label>Full name</label><?php //echo '<pre>'; print_r($basic_info);//die;            ?>

                                <input class="form-control" type="text" value="<?php echo set_value('full_name', isset($basic_info) ? $basic_info->full_name : ''); ?>" name="full_name" placeholder="Enter Full Name">

                            </div>

                            <div class="form-group">

                                <label>Position</label>

                                <input class="form-control" name="position" type="text" value="<?php echo set_value('position', isset($basic_info) ? $basic_info->position : ''); ?>" placeholder="Enter Position">

                            </div>
                            <div class="form-group">

                                <label>Short Description</label>

                                <textarea class="form-control" name="excerpt"><?php echo set_value('excerpt', isset($basic_info) ? $basic_info->excerpt : ''); ?></textarea>

                                <?php echo $this->ckeditor->replace("excerpt"); ?>

                            </div>

                            <div class="form-group">

                                <label>About Description</label>

                                <textarea class="form-control" name="about_description"><?php echo set_value('about_description', isset($basic_info) ? $basic_info->about_description : ''); ?></textarea>

                                <?php echo $this->ckeditor->replace("about_description"); ?>

                            </div>
                            
                             <div class="form-group">

                                <label>City</label>

                                <textarea class="form-control" name="city" placeholder="Enter Your City"><?php echo set_value('city', isset($basic_info) ? $basic_info->city : ''); ?></textarea>
                                 <?php echo $this->ckeditor->replace("city"); ?>
                            </div>

                            <div class="form-group">

                                <label>Office</label>

                                <input class="form-control" type="text" value="<?php echo set_value('work_number', isset($basic_info) ? $basic_info->work_number : ''); ?>" name="work_number" placeholder="Enter Work Number">

                            </div>

                            <div class="form-group">

                                <label>Cell</label>

                                <input class="form-control" name="mobile_number" type="text" value="<?php echo set_value('mobile_number', isset($basic_info) ? $basic_info->mobile_number : ''); ?>" placeholder="Enter Mobile Number">

                            </div>



                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">

                                <label>Email</label>

                                <input class="form-control" name="email" type="email" value="<?php echo set_value('email', isset($basic_info) ? $basic_info->email : ''); ?>" placeholder="Enter Email">

                            </div>




                            <div class="form-group">

                                <label>Facebook</label>

                                <input class="form-control" type="text" value="<?php echo set_value('facebook', isset($basic_info) ? $basic_info->facebook : ''); ?>" name="facebook" placeholder="Enter Facebook URL">

                            </div>

                            <div class="form-group">

                                <label>Twitter</label>

                                <input class="form-control" name="twitter" type="text" value="<?php echo set_value('twitter', isset($basic_info) ? $basic_info->twitter : ''); ?>" placeholder="Enter Twitter URL">

                            </div>

                            <div class="form-group">

                                <label>Linkedin</label>

                                <input class="form-control" name="linked_in" type="text" value="<?php echo set_value('linked_in', isset($basic_info) ? $basic_info->linked_in : ''); ?>" placeholder="Enter Linkedin URL">

                            </div>


                              <div class="form-group">

                                <label>Instagram</label>

                                <input class="form-control" name="instagram" type="text" value="<?php echo set_value('instagram', isset($basic_info) ? $basic_info->instagram : ''); ?>" placeholder="Enter Instagram URL">

                            </div>

                        </div>





                        <div class="col-lg-6">

                            <div class="form-group">

                                <div class="row">

                                    <div class="col-lg-7 text-left">

                                        <label>Upload Profile Picture: </label> <br>Please upload image of size 278px X 477px <br><br>

                                        <input  type="file" placeholder="Upload" name="profile_pic" >

                                    </div>
                                    <div class="col-lg-5">

                                        <?php if ($basic_info->profile_pic && $image = $this->misc_library->get_image_filename($basic_info->profile_pic, 'profile', 'main')) {
                                            ?> 

                                            <?php if (file_exists(BASEPATH . '../assets/images/basic/' . $image)) { ?>

                                                <img src="<?php echo base_url() . '/assets/images/basic/' . $image; ?>" alt="" class="img-responsive img-full" />

                                                <?php
                                            }
                                        }
                                        ?>

                                    </div>


                                </div>

                            </div>

                        </div>

                    </div>

                    <input type="hidden" name="id" value="<?php echo $basic_info->id; ?>">

                    <button type="submit"  class="btn btn-default" value="" >Save</button>

                </form>

            </div>

            <!-- /.panel-body -->

        </div>

        <!-- /.panel -->

    </div>

    <!-- /.col-lg-12 -->



</div>