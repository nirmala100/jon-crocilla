<div class="row">
    <?php $i=0; foreach ($why_choose_me as $why): $i++;?>
    <div class="col-lg-6">
        <div class="panel panel-default">
            
            <div class="panel-heading">
                <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url('admin/about/why-to-choose-me'); ?>">
<!--                <i class="fa fa-comment fa-fw fa-lg"></i>  Experience-->
                 <i class="fa fa-comment fa-fw fa-lg"></i><input type="text" name="title" value="<?php echo set_value('title', isset($why) ? $why->title : ''); ?>">
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                

                    <div class="form-group">
                        <label>Description</label>
<!--                        <textarea class="form-control" name="experience"> <?php //echo set_value('experience', isset($why_choose_me) ? $why_choose_me->experience : ''); ?></textarea>-->
                    <textarea class="form-control" name="description<?php echo $i;?>"> <?php echo set_value('description', isset($why) ? $why->description : ''); ?></textarea>
                    </div>
                    <?php echo $this->ckeditor->replace("description".$i); ?>                         
                    <input type="hidden" name="id" value="<?php echo set_value('id', isset($why) ? $why->id : ''); ?>">
                    <button type="submit" class="btn btn-default">Save</button>
                
            </div>
            </form>
            <!-- /.panel-body -->

        </div>  
    </div>
    
     <?php endforeach;?>
    <!-- /.panel -->
<!--    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-graduation-cap fa-fw fa-lg"></i>  Marketing Knowledge
            </div>
             /.panel-heading 
            <div class="panel-body">


                <div class="form-group">
                    <label>Description</label>
                    <textarea class="form-control" name="marketing_des"> <?php echo set_value('marketing_des', isset($why_choose_me) ? $why_choose_me->marketing_des : ''); ?></textarea>
                </div>
                <?php echo $this->ckeditor->replace("marketing_des"); ?>                               
            </div>

             /.panel-body 

        </div>
    </div>-->
</div>
<!--<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-users fa-fw fa-lg"></i>  Community And National Affiliations
            </div>
             /.panel-heading 
            <div class="panel-body">


                <div class="form-group">
                    <label>Description</label>
                    <textarea class="form-control" name="affiliations"> <?php echo set_value('affiliations', isset($why_choose_me) ? $why_choose_me->affiliations : ''); ?></textarea>
                </div>
                <?php echo $this->ckeditor->replace("affiliations"); ?>                               
            </div>

             /.panel-body 

        </div>
    </div>
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-lightbulb-o fa-fw fa-lg"></i>  Negotiating skills
            </div>
             /.panel-heading 
            <div class="panel-body">


                <div class="form-group">
                    <label>Description</label>
                    <textarea class="form-control" name="skills"> <?php echo set_value('skills', isset($why_choose_me) ? $why_choose_me->skills : ''); ?></textarea>
                </div>
                <?php echo $this->ckeditor->replace("skills"); ?>                 
            </div>

             /.panel-body 

        </div>
    </div>
</div>-->
<!--<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-paperclip fa-fw fa-lg"></i>  References
            </div>
             /.panel-heading 
            <div class="panel-body">


                <div class="form-group">
                    <label>Description</label>
                    <textarea class="form-control" name="references"> <?php echo set_value('references', isset($why_choose_me) ? $why_choose_me->references : ''); ?></textarea>
                </div>
                <?php echo $this->ckeditor->replace("references"); ?>
            </div>

             /.panel-body 

        </div>
    </div>
</div>-->

<!--<input type="hidden" name="id" value="<?php echo set_value('id', isset($why_choose_me) ? $why_choose_me->id : ''); ?>">-->


              

</div>
<!-- /.col-lg-4 -->
</div>