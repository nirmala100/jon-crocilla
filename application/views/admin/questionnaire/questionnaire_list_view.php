
<div class="row">
    <div class="col-lg-12">
        <a class="btn btn-info" href="<?php echo base_url('admin/questionnaire/add'); ?>"> <span class="fa fa-fw fa-plus"></span> Add new questionnaire</a><br>
    </div>
</div>
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-comments fa-lg fa-fw"></i> List of all Questionnaire

            </div>
            <!-- /.panel-heading -->

            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered 
                    <?php
                    if (!empty($questionnaires)) {
                        echo 'dataTables-table';
                    }
                    ?>">
                        <thead>
                            <tr>
                                <th>S.N.</th>
                                <th>Questions</th>
                                <th>Short Questions</th>
                                <th>Answers</th>
                                <th>Publish</th>
                                <th class="action-col">Option</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($questionnaires)):
                                $i = 1; //for numbering value S.N.
                                foreach ($questionnaires as $questionnaire):
                                    ?>
                                    <tr>
                                        <td ><?php echo $i; ?></td>
                                        <td><?php echo $questionnaire->question ?></td>
                                        <td><?php echo isset($questionnaire->short_question) ? $questionnaire->short_question : '' ?></td>
                                        <td>
                                            <?php if ($questionnaire->answers): ?>
                                                <ul>
                                                    <?php foreach ($questionnaire->answers as $ans): ?>
                                                        <li> <?php echo $ans->answer; ?></li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            <?php endif; ?>
                                        </td>
                                        <td class="for_change_status">
                                            <select class="form-control" name="is_active" id="change_status_<?php echo $questionnaire->id; ?>" class="content_status" 
                                                    data-id="<?php echo $questionnaire->id; ?>" data-baseurl="<?php echo base_url() . 'admin/questionnaire/change-status'; ?>">

                                                <option value="1" <?php echo $questionnaire ? (($questionnaire->is_active == 1 ) ? 'selected' : '' ) : '' ?>>Published</option>
                                                <option value="0" <?php echo $questionnaire ? (($questionnaire->is_active == 0) ? 'selected' : '' ) : '' ?>>Unpublished</option>
                                            </select>
                                        </td>
                                        <td class="action-col">
                                            <div class="action-pos">
                                                <nobr>
                                                    <form method="post" action="<?php echo base_url($this->config->item('backend')) . '/questionnaire/add-edit' ?>" class="pull-left">
                                                        <input type="hidden" value="<?php echo $questionnaire->id; ?>" name="id">
                                                        <button title="Edit" type="submit" class="btn btn-info"><span class="fa fa-edit fa-lg"></span></button>
                                                    </form>

                                                    <form method="post" action="<?php echo base_url($this->config->item('backend')) . '/questionnaire/delete' ?>" onsubmit="return confirm('Are you sure?')" class="pull-left">
                                                        <input type="hidden" value="<?php echo $questionnaire->id; ?>" name="id">
                                                        <button type="submit" class="del_con btn btn-danger" title="Delete"><span class="fa fa-trash-o fa-lg"></span></button>
                                                    </form>
                                                    <div class="clearfix"></div>
                                                </nobr>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                endforeach;
                            else:
                                ?>
                                <tr>
                                    <td colspan="5">There are no Questionnaire.</td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
