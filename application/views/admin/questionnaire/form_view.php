
<div class="add-container">
    <p>
        <a class="btn btn-info" href="javascript:window.history.go(-1);"<h3><span class="fa fa-fw fa-lg fa-mail-reply"></span> Back</h3></a>
    </p>
    <form action="<?php echo base_url('admin/questionnaire/add-edit'); ?>" enctype="multipart/form-data" id="signupForm"  method="post" role="form">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">

                        <h3 class="panel-title"> <span class="fa fa-fw  fa-plus"></span> <?php echo isset($question) ? ' Edit questionnaire' : ' Add new questionnaire'; ?></h3>
                    </div>
                    <div class="panel-body">

                        <div class="form-group"><label for="question">Question:</label>
                            <input type="text" name="question" class="form-control" value="<?php echo isset($question->question) ? $question->question : set_value('question'); ?>" id="question">
                        </div>

                        <div class="form-group"><label for="short_question">Short Question:</label>
                            <input type="text" name="short_question" class="form-control" value="<?php echo isset($question->short_question) ? $question->short_question : set_value('short_question'); ?>" id="short_question">
                        </div>


                        <div class="clearfix"></div> 
                        <div class="ans_field">
                            <label for="answer">Answers:</label>
                            <?php
                            if (isset($answers)):
                                foreach ($answers as $ans):
                                    ?>
                                    <div class="form-group">
                                        <input type="text" name="answer[]" class="form-control" value="<?php echo isset($ans->answer) ? $ans->answer : set_value('answer'); ?>" id="answer">
                                        <a href="#" class="delete_ans" data-id="<?php echo isset($ans->id) ? $ans->id : ''; ?>">Delete</a>
                                    </div>
                                <?php endforeach;
                            else:
                                ?>
                                <div class="form-group">
                                    <input type="text" name="answer[]" class="form-control" value="<?php echo set_value('answer'); ?>" id="answer">
                                    <a href="#" class="delete_ans" >Delete</a>
                                </div>
<?php endif; ?>
                        </div>


                        <div class="form-group"><label for="add_th_for_ans">Answer: </label>
                            <input type="button" class="btn btn-primary" name="btn_add" id="add_th_for_ans" value="+Add">
                        </div>

                        <div class="form-group">
                            <label for="is_active" >Visible:</label>
                            <select class="form-control" name="is_active" id="is_active">
                                <option value="1" <?php echo isset($question) ? (($question->is_active == 1 ) ? 'selected' : '' ) : '' ?>>Yes</option>
                                <option value="0" <?php echo isset($question) ? (($question->is_active == 0) ? 'selected' : '' ) : '' ?>>No</option>
                            </select>
                        </div>
                    </div>

                </div>
            </div><!-- /.col-sm-7 -->

        </div><!-- /.row -->
        <p>
            <input type="hidden" value="<?php echo isset($question) ? $question->id : ''; ?>" name="id">
            <input type="hidden" value="<?php echo base_url(); ?>" id="baseurl">
            <button type="submit" name="btn_questionnaire" class="btn btn-success" value="submit" > <span class="fa fa-fw fa-lg fa-check"></span> Submit </button>
        </p>
    </form>
</div><!-- end panel -->

