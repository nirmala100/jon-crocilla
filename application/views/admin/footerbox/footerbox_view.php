<div class="row">
  
    <div class="col-lg-6">
        <div class="panel panel-default">
            
            <div class="panel-heading">
                <form role="form" method="post" enctype="multipart/form-data">
              <i class="fa fa-comment fa-fw fa-lg"></i>  Lending Box 1
                 
            </div>
            <!-- /.panel-heading -->
          <div class="panel-body">

                        <div class="error-panel">
                            <?php echo validation_errors(); ?>
                        </div>
                        
                        <div class="form-group"><label for="title">Title:</label>
                            <input type="text" name="title" class="form-control" value="<?php echo $footer_info1 ? $footer_info1->title : '' ?>" id="title">
                        </div>
                         <div class="form-group"><label for="url">Url:</label>
                            <input type="text" name="url" class="form-control" value="<?php echo $footer_info1 ? $footer_info1->url : '' ?>" id="title">
                        </div>
                     
                   
                       
                        <!-- /.row -->
                        <div class="form-group">
                            <label for="image">Image:</label> Please upload image at least of size 1024px X 500px
                            <input type="file" name="image"  value="<?php echo $footer_info1 ? $footer_info1->image : ''; ?>" id="image">
                        </div>
                        <div class="col-lg-5">
                            <?php if ($footer_info1 && $footer_info1->image && $image_name = $this->misc_library->get_image_filename($footer_info1->image, 'footer')) {
                                ?> 
                                <?php if (file_exists(BASEPATH . '../assets/images/footerbox/' . $image_name)) { ?>
                                    <img src="<?php echo base_url() . '/assets/images/footerbox/' . $image_name; ?>" class="img-thumbnail"/>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <div class="clearfix"></div> <br><br>   
                        <div class="form-group"><label for="description">Description:</label>
                            <textarea name="f_description" class="form-control"  /><?php echo set_value('f_description', $footer_info1 ? $footer_info1->f_description : ''); ?></textarea>
                        </div>
                        <?php echo $this->ckeditor->replace("f_description"); ?>

                        <div class="form-group">
                            <label for="is_active" >Page Visible:</label>
                            <select class="form-control" name="is_active" id="is_active">
                                <option value=1 <?php echo set_select('is_active', '1', $footer_info1 ? ($footer_info1->is_active == 1 ? true : false ) : false ); ?>>Yes</option>
                                <option value=0 <?php echo set_select('is_active', '0', $footer_info1 ? ($footer_info1->is_active == 0 ? true : false ) : false ); ?>>No</option>
                            </select></div>
                    </div>
              <p>
<!--            <input type="hidden" value="<?php echo set_value('id', $footer_info ? $footer_info->id : ''); ?>" name="id">-->
            <button type="submit" name="btn_footer" class="btn btn-success" value="submit" > <span class="fa fa-fw fa-lg fa-check"></span> Submit </button>
        </p>
            </form>
            <!-- /.panel-body -->

        </div>  
    </div>
    
     <?php //endforeach;?>
    <!-- /.panel -->
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-graduation-cap fa-fw fa-lg"></i> Lending Box 2
            </div>
<!--             /.panel-heading -->
             <div class="panel-body">

                        <div class="error-panel">
                            <?php echo validation_errors(); ?>
                        </div>
                        
                        <div class="form-group"><label for="title">Title:</label>
                            <input type="text" name="title" class="form-control" value="<?php echo $footer_info2 ? $footer_info2->title : '' ?>" id="title">
                        </div>
                         <div class="form-group"><label for="url">Url:</label>
                            <input type="text" name="url" class="form-control" value="<?php echo $footer_info2 ? $footer_info2->url : '' ?>" id="title">
                        </div>
                     
                   
                       
                        <!-- /.row -->
                        <div class="form-group">
                            <label for="image">Image:</label> Please upload image at least of size 1024px X 500px
                            <input type="file" name="image"  value="<?php echo $footer_info1 ? $footer_info2->image : ''; ?>" id="image">
                        </div>
                        <div class="col-lg-5">
                            <?php if ($footer_info2 && $footer_info2->image && $image_name = $this->misc_library->get_image_filename($footer_info2->image, 'footer','featured')) {
                                ?> 
                                <?php if (file_exists(BASEPATH . '../assets/images/footerbox/' . $image_name)) { ?>
                                    <img src="<?php echo base_url() . '/assets/images/footerbox/' . $image_name; ?>" class="img-thumbnail"/>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <div class="clearfix"></div> <br><br>   
                        <div class="form-group"><label for="description">Description:</label>
                            <textarea name="f_description" class="form-control"  /><?php echo set_value('f_description', $footer_info2 ? $footer_info2->f_description : ''); ?></textarea>
                        </div>
                        <?php echo $this->ckeditor->replace("f_description"); ?>

                        <div class="form-group">
                            <label for="is_active" >Page Visible:</label>
                            <select class="form-control" name="is_active" id="is_active">
                                <option value=1 <?php echo set_select('is_active', '1', $footer_info2 ? ($footer_info2->is_active == 1 ? true : false ) : false ); ?>>Yes</option>
                                <option value=0 <?php echo set_select('is_active', '0', $footer_info2 ? ($footer_info1->is_active == 0 ? true : false ) : false ); ?>>No</option>
                            </select></div>
                    </div>

<!--             /.panel-body -->
 <p>
<!--            <input type="hidden" value="<?php echo set_value('id', $footer_info ? $footer_info->id : ''); ?>" name="id">-->
            <button type="submit" name="btn_footer" class="btn btn-success" value="submit" > <span class="fa fa-fw fa-lg fa-check"></span> Submit </button>
        </p>

        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-users fa-fw fa-lg"></i>  Lending Box 3
            </div>
<!--             /.panel-heading -->
         <div class="panel-body">

                        <div class="error-panel">
                            <?php echo validation_errors(); ?>
                        </div>
                        
                        <div class="form-group"><label for="title">Title:</label>
                            <input type="text" name="title" class="form-control" value="<?php echo $footer_info1 ? $footer_info1->title : '' ?>" id="title">
                        </div>
                         <div class="form-group"><label for="url">Url:</label>
                            <input type="text" name="url" class="form-control" value="<?php echo $footer_info1 ? $footer_info1->url : '' ?>" id="title">
                        </div>
                     
                   
                       
                        <!-- /.row -->
                        <div class="form-group">
                            <label for="image">Image:</label> Please upload image at least of size 1024px X 500px
                            <input type="file" name="image"  value="<?php echo $footer_info1 ? $footer_info1->image : ''; ?>" id="image">
                        </div>
                        <div class="col-lg-5">
                            <?php if ($footer_info1 && $footer_info1->image && $image_name = $this->misc_library->get_image_filename($footer_info1->image, 'footer')) {
                                ?> 
                                <?php if (file_exists(BASEPATH . '../assets/images/footerbox/' . $image_name)) { ?>
                                    <img src="<?php echo base_url() . '/assets/images/footerbox/' . $image_name; ?>" class="img-thumbnail"/>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <div class="clearfix"></div> <br><br>   
                        <div class="form-group"><label for="description">Description:</label>
                            <textarea name="f_description" class="form-control"  /><?php echo set_value('f_description', $footer_info1 ? $footer_info1->f_description : ''); ?></textarea>
                        </div>
                        <?php echo $this->ckeditor->replace("f_description"); ?>

                        <div class="form-group">
                            <label for="is_active" >Page Visible:</label>
                            <select class="form-control" name="is_active" id="is_active">
                                <option value=1 <?php echo set_select('is_active', '1', $footer_info1 ? ($footer_info1->is_active == 1 ? true : false ) : false ); ?>>Yes</option>
                                <option value=0 <?php echo set_select('is_active', '0', $footer_info1 ? ($footer_info1->is_active == 0 ? true : false ) : false ); ?>>No</option>
                            </select></div>
                    </div>
<!--             /.panel-body -->
 <p>
<!--            <input type="hidden" value="<?php echo set_value('id', $footer_info ? $footer_info->id : ''); ?>" name="id">-->
            <button type="submit" name="btn_footer" class="btn btn-success" value="submit" > <span class="fa fa-fw fa-lg fa-check"></span> Submit </button>
        </p>

        </div>
    </div>

</div>


</div>
<!-- /.col-lg-4 -->
</div>
   