
<div class="add-container">
    <p>
        <a class="btn btn-info" href="javascript:window.history.go(-1);"<h3><span class="fa fa-fw fa-lg fa-mail-reply"></span> Back</h3></a>
    </p>
    <form action="" enctype="multipart/form-data" id="signupForm"  method="post" role="form">
        <div class="row">
            <div class="col-sm-7">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <?php //print_r($footer_info); die;?>
                        <h3 class="panel-title"> <span class="fa fa-fw  fa-plus"></span> <?php echo $footer_info ? $footer_info->title . ' : edit footer' : ' add new footer'; ?></h3>
                    </div>
                    <div class="panel-body">

                        <div class="error-panel">
                            <?php echo validation_errors(); ?>
                        </div>
                        
                        <div class="form-group"><label for="title">Title:</label>
                            <input type="text" name="title" class="form-control" value="<?php echo $footer_info ? $footer_info->title : '' ?>" id="title">
                        </div>
                         <div class="form-group"><label for="url">Url:</label>
                            <input type="text" name="url" class="form-control" value="<?php echo $footer_info ? $footer_info->url : '' ?>" id="title">
                        </div>
                     
                   
                       
                        <!-- /.row -->
                        <div class="form-group">
                            <label for="image">Image:</label> Please upload image at least of size 1024px X 500px
                            <input type="file" name="image"  value="<?php echo $footer_info ? $footer_info->image : ''; ?>" id="image">
                        </div>
                        <div class="col-lg-5">
                            <?php if ($footer_info && $footer_info->image && $image_name = $this->misc_library->get_image_filename($footer_info->image, 'footer', 'featured')) {
                                ?> 
                                <?php if (file_exists(BASEPATH . '../assets/images/footerbox/' . $image_name)) { ?>
                                    <img src="<?php echo base_url() . '/assets/images/footerbox/' . $image_name; ?>" class="img-thumbnail"/>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <div class="clearfix"></div> <br><br>   
                        <div class="form-group"><label for="description">Description:</label>
                            <textarea name="f_description" class="form-control"  /><?php echo set_value('f_description', $footer_info ? $footer_info->f_description : ''); ?></textarea>
                        </div>
                        <?php echo $this->ckeditor->replace("f_description"); ?>
                          <div class="form-group">
                            <label for="type" >Type</label>
                            <select class="form-control" name="type" id="is_active">
                                <option value='lending' <?php echo set_select('type', 'lending', $footer_info ? ($footer_info->type == 'lending' ? true : false ) : false ); ?>>Lending</option>
                                <option value='contact' <?php echo set_select('type', 'contact', $footer_info ? ($footer_info->type == 'contact' ? true : false ) : false ); ?>>Contact</option>
                            </select>
                        </div>
                        
                       
                        
                    </div>

                </div>
            </div><!-- /.col-sm-7 -->
      

        </div><!-- /.row -->
        <p>
            <input type="hidden" value="<?php echo set_value('id', $footer_info ? $footer_info->id : ''); ?>" name="id">
            <button type="submit" name="btn_footer" class="btn btn-success" value="submit" > <span class="fa fa-fw fa-lg fa-check"></span> Submit </button>
        </p>
    </form>
</div><!-- end panel -->

