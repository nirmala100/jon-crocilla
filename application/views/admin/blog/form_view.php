
<div class="add-container">
    <p>
        <a class="btn btn-info" href="javascript:window.history.go(-1);"<h3><span class="fa fa-fw fa-lg fa-mail-reply"></span> Back</h3></a>
    </p>
    <form action="" enctype="multipart/form-data" id="signupForm"  method="post" role="form">
        <div class="row">
            <div class="col-sm-7">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <?php //print_r($blog_info); die;?>
                        <h3 class="panel-title"> <span class="fa fa-fw  fa-plus"></span> <?php echo $blog_info ? $blog_info->title . ' : edit blog' : ' add new blog'; ?></h3>
                    </div>
                    <div class="panel-body">

                        <div class="error-panel">
                            <?php echo validation_errors(); ?>
                        </div>
                   
                        <div class="form-group"><label for="title">Title:</label>
                            <input type="text" name="title" class="form-control" value="<?php echo $blog_info ? $blog_info->title : '' ?>" id="title">
                        </div>
                        <div class="form-group"> <label for="slug">Slug:</label>
                            <input type="text" name="slug" class="form-control" value="<?php echo set_value('slug', $blog_info ? $blog_info->slug : ''); ?>" id="slug">
                        </div>
                        
                        
                        <!-- /.row -->
                        <div class="form-group">
                            <label for="image">Image:</label> Please upload image at least of size 1000px X 450px
                            <input type="file" name="image"  value="<?php echo $blog_info ? $blog_info->image : ''; ?>" id="image">
                        </div>
                        <div class="col-lg-5">
                            <?php if ($blog_info && $blog_info->image && $image_name = $this->misc_library->get_image_filename($blog_info->image, 'blog', 'small')) {
                                ?> 
                                <?php if (file_exists(BASEPATH . '../assets/images/blog/' . $image_name)) { ?>
                                    <img src="<?php echo base_url() . '/assets/images/blog/' . $image_name; ?>" class="img-thumbnail"/>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <div class="clearfix"></div> <br><br> 
                        <div class="form-group"><label for="description">Short Description:</label>
                            <textarea name="excerpt" class="form-control"  /><?php echo set_value('excerpt', $blog_info ? $blog_info->excerpt : ''); ?></textarea>
                        </div>
                        <?php echo $this->ckeditor->replace("excerpt"); ?>  
                        <div class="form-group"><label for="description">Description:</label>
                            <textarea name="b_description" class="form-control"  /><?php echo set_value('b_description', $blog_info ? $blog_info->b_description : ''); ?></textarea>
                        </div>
                        <?php echo $this->ckeditor->replace("b_description"); ?>
                        <?php /*<div class="form-group"> <label for="slug">Author:</label>
                            <input type="text" name="author" class="form-control" value="<?php echo set_value('author', $blog_info ? $blog_info->author : ''); ?>" id="slug">
                        </div>*/?>
                        <div class="form-group">
                          <label for="date">Created Date:</label>
                          <input type="text" name="create_date" class="form-control" value="<?php echo $blog_info ? $blog_info->create_date : '' ?>" id="datepicker">
                        </div>
                        
                        <div class="form-group">
                            <label for="is_active" >Page Visible:</label>
                            <select class="form-control" name="is_active" id="is_active">
                                <option value=1 <?php echo set_select('is_active', '1', $blog_info ? ($blog_info->is_active == 1 ? true : false ) : false ); ?>>Yes</option>
                                <option value=0 <?php echo set_select('is_active', '0', $blog_info ? ($blog_info->is_active == 0 ? true : false ) : false ); ?>>No</option>
                            </select></div>
                    </div>

                </div>
            </div><!-- /.col-sm-7 -->
            <div class="col-sm-5">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="fa fa-fw fa-tag"></span> Meta Group</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group"><label for="meta_title" >Meta Title:</label>
                            <input type="text" class="form-control" id="meta_title" name="meta_title" maxlength="55" value="<?php echo set_value('meta_title', $blog_info ? $blog_info->meta_title : ''); ?>" id="meta_title"> <em class="text-muted">Should not exceed more than 55 words</em>
                        </div>
                        <div class="form-group"><label for="meta_keyword" >Meta Keyword:</label>
                            <textarea class="form-control" name="meta_keyword" id="meta_keyword" /><?php echo set_value('meta_keyword', $blog_info ? $blog_info->meta_keyword : ''); ?></textarea></div>

                        <div class="form-group"><label for="meta_description" >Meta Description:</label>
                            <textarea class="form-control" id="meta_description" name="meta_description" id="meta_description" maxlength="115"/><?php echo set_value('meta_description', $blog_info ? $blog_info->meta_description : ''); ?></textarea> <em class="text-muted">Should not exceed more than 115 words.</em>
                        </div>
                    </div>
                </div>
            </div><!-- /.col-sm-5 -->

        </div><!-- /.row -->
        <p>
            <input type="hidden" value="<?php echo set_value('id', $blog_info ? $blog_info->id : ''); ?>" name="id">
            <button type="submit" name="btn_blog" class="btn btn-success" value="submit" > <span class="fa fa-fw fa-lg fa-check"></span> Submit </button>
        </p>
    </form>
</div><!-- end panel -->

