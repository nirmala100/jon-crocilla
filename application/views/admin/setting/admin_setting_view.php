<div class="heading clearfix">
</div>
<div class="row">
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Admin Settings</h3>
            </div>
            <div class="panel-body">
                <form method="post" action="<?php echo base_url('admin/setting/change_admin'); ?>"  >
                    <table class="table table-bordered">
                        <tr>
                            <th colspan="2">Change Password</th>
                        </tr>
                        <tr>
                            <td>Username</td>
                            <td><input class="form-control" type="text" readonly="readonly" name="username" value="<?php echo set_value('username', isset($admin_info) ? $admin_info->username : ''); ?>"></td>
                        </tr>
                        <tr>
                            <td>Password</td>
                            <td><input class="form-control" class="input-5" type="password" name="password" value="password"></td>
                        </tr>
                    </table>
                    <p>
                        <button type="submit" class="btn btn-success"> <span class="fa fa-fw fa-lg fa-check"></span> DONE </button>
                    </p>
                </form>
            </div>
        </div>

    </div><!-- /.col-md-4 -->
    <div class="col-md-8">
        <?php echo validation_errors(); ?>
        <form action="<?php echo base_url('admin/setting/edit'); ?>" method="post" id="signupForm">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Add Type</h3>
                </div>
                <div class="panel-body">

                    <table class="table table-bordered">
                        <tr>
                            <td>Type</td>
                            <td><input class="form-control" type="text"  name="type" id="type" value="<?php echo set_value('type', isset($stng_info) ? $stng_info->type : ''); ?>"></td>
                        </tr>

                        <tr>
                            <td>Value</td>
                            <td><input class="form-control"  type="text" name="value" value="<?php echo set_value('value', isset($stng_info) ? $stng_info->value : ''); ?>"></td>
                        </tr>
                        <tr>
                            <td><select class="form-control" name="insert_type" >
                                    <option value="after" selected="selected" >Insert After</option>
                                    <option value="before" >Insert Before</option>
                                </select>
                            </td>
                            <td>
                                <select class="form-control" name="select_type" >
                                    <option value="0" selected="selected" >Select Type</option>
                                    <?php foreach ($setting_info as $sett):
                                        ?>
                                        <option value="<?php echo $sett->order ?>" ><?php echo $sett->type ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"><input type="checkbox" value="1" <?php echo isset($stng_info) ? ( $stng_info->is_permanent == 1 ? 'checked' : '' ) : ''; ?> name="is_permanent"> Permanent </td>
                        </tr>
                    </table>

                    <input type="hidden" value="<?php echo isset($stng_info) ? $stng_info->slug : ''; ?>" name="slug">

                    <input type="hidden" value="<?php echo isset($stng_info) ? $stng_info->id : ''; ?>" name="id">
                    <p>
                        <button class="btn btn-info" type="submit" name="add_edit_form"><span class="fa fa-lg fa-fw fa-check"></span> Done</button>
                    </p>
                    </form>
                </div> 
            </div>  <!--  end panel -->
            <?php //echo $this->session->flashdata('msg'); ?>
    </div> <!--/.col-md-8 -->
</div><!-- /.row --> 

<br />

<table class="table table-bordered">
    <tr>
        <th>S.No.</th>
        <th>Type</th>
        <th>Value</th>
        <th>Action</th>
    </tr>
    <?php
    if ($setting_info):
        $i = 1;
        foreach ($setting_info as $setting):
            ?>
            <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $setting->type; ?></td>
                <td><?php echo htmlspecialchars($setting->value); ?></td>
                <td>
                    <div class="action-pos">
                        <form method="post" action="<?php echo base_url($this->config->item('backend')) . '/setting/edit' ?>">
                            <input type="hidden" value="1" name="edit">
                            <input type="hidden" value="<?php echo $setting->id; ?>" name="id">
                            <button class="btn btn-info btn-block" type="submit"> <span class="fa fa-fw fa-lg fa-edit"></span></button>
                        </form>
                        <?php if ($setting->is_permanent == '0'): ?>
                            <form method="post" action="<?php echo base_url($this->config->item('backend')) . '/setting/delete' ?>">
                                <input type="hidden" value="<?php echo $setting->id; ?>" name="id">
                                <button type="submit" class="del_con btn btn-danger btn-block" onclick="return confirm('Are you sure?')"><span class="fa fa-lg fa-fw fa-trash-o"></span> </button>
                            </form>
                        <?php endif; ?>
                    </div>
                </td>
            <tr>
                <?php
                $i++;
            endforeach;
        else:
            ?>
        <tr>
            <td colspan="5">No Settings</td>
        </tr>
    <?php
    endif;
    ?>
</table>