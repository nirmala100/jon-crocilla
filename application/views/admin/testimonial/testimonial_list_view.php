<div class="row">
    <div class="col-lg-12">
        <a class="btn btn-info" href="<?php echo base_url('admin/testimonial/add_edit'); ?>"> <span class="fa fa-fw fa-plus"></span> Add new testimonial</a><br>
    </div>
</div>
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-comments fa-lg fa-fw"></i> List of all Testimonial

            </div>
            <!-- /.panel-heading -->

            <div class="panel-body">
                <table class="table table-bordered <?php
                if ($all_testimonial) {
                    echo "dataTables-table";
                }
                ?>">
                    <thead>
                        <tr>
                            <th>S.N.</th>
                            <th>Author</th>
                            <th>Description</th>
                            <th>Publish</th>
                            <th class="action-col">Option</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        //echo '<pre>';print_r($all_testimonial);die;
                        if ($all_testimonial):
                            $i = 1; //for numbering value S.N.
                            foreach ($all_testimonial as $testimonial):
                                ?>
                                <tr>
                                    <td ><?php echo $i; ?></td>
                                    <td><?php echo $testimonial->author; ?></td>
                                    <td><?php echo $testimonial->description; ?></td>
                                    <td>
                                        <select class="form-control" name="is_active" id="change_status_<?php echo $testimonial->id; ?>" class="content_status" onchange="return change_status(this, '<?php echo base_url($this->config->item('backend')); ?>/testimonial/change-status')">
                                            <option value="1" <?php echo $testimonial ? (($testimonial->is_active == 1 ) ? 'selected' : '' ) : '' ?>>Published</option>
                                            <option value="0" <?php echo $testimonial ? (($testimonial->is_active == 0) ? 'selected' : '' ) : '' ?>>Unpublished</option>
                                        </select>
                                    </td>
                                    <td class="action-col">
                                        <div class="action-pos">
                                            <form method="post" action="<?php echo base_url($this->config->item('backend')) . '/testimonial/add_edit' ?>" class="pull-left">
                                                <input type="hidden" value="<?php echo $testimonial->id; ?>" name="id">
                                                <button title="Edit" type="submit" class="btn btn-info"><span class="fa fa-edit fa-lg"></span></button>
                                            </form>
                                            <form method="post" action="<?php echo base_url($this->config->item('backend')) . '/testimonial/delete' ?>" onsubmit="return confirm('Are you sure?')">
                                                <input type="hidden" value="<?php echo $testimonial->id; ?>" name="id">
                                                <button type="submit" class="del_con btn btn-danger" title="Delete"><span class="fa fa-trash-o fa-lg"></span></button>
                                            </form>

                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            endforeach;
                        else:
                            ?>
                            <tr>
                                <td colspan="5">There are no testimonials.</td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<?php
echo $this->session->flashdata('msg');
?>
