<div class="row">
    <div class="col-lg-5">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-paperclip fa-fw fa-lg"></i> Add Testimonial
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url('admin/testimonial/post_testimonial'); ?>">
                    <div class="form-group">
                        <label>Author</label>
                        <input class="form-control" type="text" value="<?php echo set_value('author', isset($testimonial_row) ? $testimonial_row->author : ''); ?>" name="author" placeholder="Enter Author">
                    </div>

                    <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control" name="t_description"> <?php echo set_value('t_description', isset($testimonial_row) ? $testimonial_row->t_description : ''); ?></textarea>
                    </div>
                    <?php echo $this->ckeditor->replace("t_description"); ?>
                   
                    <div class="form-group">
                        <label>City</label>
                        <input class="form-control" name="address2" type="text" value="<?php echo set_value('address2', isset($testimonial_row) ? $testimonial_row->address2 : ''); ?>" placeholder="Enter Address">
                    </div>    
                    <div class="form-group">
                        <label for="is_active" > Visible:</label>
                        <select class="form-control" name="is_active" id="is_active">
                            <option value=1 <?php echo set_select('is_active', '1', isset($testimonial_row) ? ($testimonial_row->is_active == 1 ? true : false ) : false ); ?>>Yes</option>
                            <option value=0 <?php echo set_select('is_active', '0', isset($testimonial_row) ? ($testimonial_row->is_active == 0 ? true : false ) : false ); ?>>No</option>
                        </select></div>


                    <input type="hidden" name="id" value="<?php echo set_value('id', isset($testimonial_row) ? $testimonial_row->id : ''); ?>">
                    <button type="submit" class="btn btn-default">Save</button>
                </form>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-8 -->
    <div class="col-lg-7">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-briefcase fa-fw fa-lg"></i> Testimonials
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-condensed">
                        <tr>
                            <th>#</th>
                            <th>Author</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                        <?php
                        if ($testimonial):
                            $i = 1;

                            foreach ($testimonial as $test):
                                ?>
                                <tr>
                                    <td><?php echo $i++; ?></td>
                                    <td><?php echo $test->author; ?></td>
                                    <td><?php echo $test->t_description; ?></td>
                                    <td>
                                        <form action="" method="post">
                                            <input type="hidden" value="<?php echo $test->id; ?>" name="id">
                                            <button title="Edit" class="btn btn-primary btn-block"><span class="fa fa-edit fa-lg"></span></button>
                                        </form>
                                        <form action="<?php echo base_url('admin/testimonial/delete'); ?>" method="post" onsubmit="return confirm('Are you sure?')">
                                            <input type="hidden" value="experience" name="type">
                                            <input type="hidden" value="<?php echo $test->id; ?>" name="id">
                                            <button title="Delete" class="btn btn-danger btn-block"><span class="fa fa-trash-o fa-lg"></span></button>
                                        </form>
                                    </td>
                                </tr>
                                <?php
                            endforeach;

                        else:
                            ?>
                            <tr>
                                <td colspan="6">There are no testimonials</td>
                            </tr>
                        <?php endif; ?>
                    </table>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-4 -->
</div>