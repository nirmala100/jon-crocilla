
<div class="add-container">
    <p>
        <a class="btn btn-info" href="<?php echo base_url('admin/testimonial'); ?>"<h3><span class="fa fa-fw fa-lg fa-mail-reply"></span> Back</h3></a>
    </p>
    <form action="" enctype="multipart/form-data" id="signupForm"  method="post" role="form">
        <div class="row">
            <div class="col-sm-7">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <?php //print_r($testimonial_info);?>
                        <h3 class="panel-title"> <span class="fa fa-fw  fa-plus"></span> <?php echo $testimonial_info ? $testimonial_info->author . ' : edit testimonial' : ' add new testimonial'; ?></h3>
                    </div>
                    <div class="panel-body">

                        <div class="error-panel">
                            <?php echo validation_errors(); ?>
                        </div>
                        <div class="form-group"><label for="author">Author:</label>
                            <input type="text" name="author" class="form-control" value="<?php echo $testimonial_info ? $testimonial_info->author : '' ?>" id="title">
                        </div>
                        <div class="form-group"><label for="description">Description:</label>
                            <textarea name="description" class="form-control"  /><?php echo $testimonial_info ? $testimonial_info->description : ''; ?></textarea>
                        </div>
                        <?php //echo $this->ckeditor->replace("description"); ?>
                        <div class="form-group"><label for="address">Address:</label>
                            <input type="text" name="address" class="form-control" value="<?php echo $testimonial_info ? $testimonial_info->address : '' ?>" id="title">
                        </div>
                        <div class="form-group"><label for="address2">Address2:</label>
                            <input type="text" name="address2" class="form-control" value="<?php echo $testimonial_info ? $testimonial_info->address2 : '' ?>" id="title">
                        </div>




                        <!-- /.row -->

                        <div class="clearfix"></div> <br><br>   


                        <div class="form-group">
                            <label for="is_active" > Visible:</label>
                            <select class="form-control" name="is_active" id="is_active">
                                <option value=1 <?php echo set_select('is_active', '1', $testimonial_info ? ($testimonial_info->is_active == 1 ? true : false ) : false ); ?>>Yes</option>
                                <option value=0 <?php echo set_select('is_active', '0', $testimonial_info ? ($testimonial_info->is_active == 0 ? true : false ) : false ); ?>>No</option>
                            </select></div>
                    </div>

                </div>
            </div><!-- /.col-sm-7 -->
            <!-- /.col-sm-5 -->
        </div><!-- /.row -->
        <p>
            <input type="hidden" value="<?php echo set_value('id', $testimonial_info ? $testimonial_info->id : ''); ?>" name="id">
            <button type="submit" name="btn_testimonial" class="btn btn-success" value="submit" > <span class="fa fa-fw fa-lg fa-check"></span> Submit </button>
        </p>
    </form>
</div><!-- end panel -->

