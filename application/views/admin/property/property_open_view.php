<body>
    <?php if ($old_image): ?>
        <div id="showmsg"></div>
        <div class="row" id ="property-images">

            <?php
            foreach ($old_image as $old):
                if (file_exists(BASEPATH . '../assets/images/property/' . $old->image)) {
                    ?>
                    <div class="property-image col-md-3" data-id="<?php echo $old->id; ?>">
                        <div class="image-holder" id="holder_<?php echo $old->id; ?>">
                            <img src="<?php echo base_url() . '/assets/images/property/' . $old->image; ?>" class="" height="100" width="100%"/>
                            <div class="delete_old"><button type="button"   class="btn-block btn btn-danger btn-small" data-name="<?php echo $old->image; ?>" id="image_<?php echo $old->id; ?>">Delete</button></div>
                        </div>
                    </div>
                    <?php
                }
            endforeach;
            ?>
        </div>
    <?php endif;
    ?>
    <!-- AJAX Response will be outputted on this DIV container -->

    <div class="row">
        <div class = "col-md-12">
            <!-- Generate the form using form helper function: form_open_multipart(); -->
            <?php echo form_open_multipart('admin/property/do_upload', array('class' => 'upload-image-form')); ?><br/>
            Please upload image of size 1024px X 500px 
            <input type="file" multiple = "multiple" accept = "image/*"  name="uploadfile[]" size="20" /><br/>
            <input type="submit" name = "submit" value="Upload" class = "btn btn-primary" /><br/>
            <?php echo form_close(); ?>
            <div class = "upload-image-messages row"></div>
            <div class="imagesubmmit">
            </div>
        </div>
    </div>


    <script>
        jQuery(document).ready(function ($) {

            $('#property-images').sortable({
                helper: function(event, ui){
                    var $clone = $(ui).clone();
                    $clone.css('position', 'absolute');
                    return $clone.get(0);
                },
                 update : function (event,ui) {
                    var order=[];// array to hold the id of all the child li of the selected parent
                    $('.property-image').each(function(index) {
                            //var item=$(this).attr('id').split('_');
                            var val=$(this).data('id');
                            order.push(val); 
                        });
                    var itemList="list="+order;
                   
                    $("#showmsg").load("<?php echo base_url('admin/property/sort');?>",itemList); 
                 }
            });

            $('.delete_old button').click(function () {
                 if(!confirm ("Are you sure to delete?")){
                         return false;

                         }
                var id = $(this).attr('id');// name of image

                var that = $(this);
                var old_name = that.data('name');
                

                //console.log(old_name);
                $.ajax({
                    type: 'post',
                    url: '<?php echo base_url('admin/property/delete_old_image'); ?>',
                    data: {name: old_name},
                    success: function (res) {
                        //console.log(res);
                        var old_image_array = id.split('_');
                        //console.log(old_image_array[1]);
                        $('#holder_' + old_image_array[1]).remove();

                    }

                });

                //console.log(that.data('name'));

            });

            var options = {
                dataType: "json",
                beforeSend: function () {
                    // Replace this with your loading gif image
                     $(".upload-image-messages").html('<p><img src = "<?php echo base_url() ?>/assets/images/loading.gif" class = "loader" /></p>');
                },
                complete: function (response) {
                    $(".upload-image-messages").html('');
                    if ($('input[type=file]').val() == '') {
                        // alert ('test');
                        return false;

                    }
                    // Output AJAX response to the div container
                    var JSONObject = JSON.parse(response.responseText);
                    //Console.log(JSONObject);return;
                    var img_arr = [];
                    var counter = 0;
                    $.each(JSONObject, function (index, data) {
                        counter++;
                        img_arr.push(data.file_name);
                        var append_content = '<div class="col-md-3">'
                                + '<img src="<?php echo base_url(); ?>assets/images/property/' + data.file_name + '" width="100%"  alt="Image" title="' + data.file_name + '" id="upload_img_' + counter + '"/>'
                                + '<div class="delete" id="delete_btn_' + counter + '"><button type="button" class="btn-block btn btn-danger btn-small"  data-id="' + counter + '" data-image_name="' + data.file_name + '"> Delete Image </button></div>'
                                + '</div>';
                        $('.upload-image-messages').append(append_content);
                        $('input[type=file]').val('');

                        //console.log(img_arr);
                    });


                    $('.imagesubmmit').append('<form action="<?php echo base_url('admin/property/add_images/' . $this->uri->segment(4)); ?>" method="post">' +
                            '<br/><input class="btn btn-success" type="submit"  value="submit">' +
                            '<input type="hidden" name="imagecap" value="' + img_arr + '" id="img_arr_p"> ' +
                            '</form>');

                    $('.delete button').click(function () {

                        var name = $(this).data('image_name');
                        var id = $(this).data('id');

                         if(!confirm ("Are you sure to delete?")){
                         return false;

                         }


                        $.ajax({
                            type: 'post',
                            url: '<?php echo base_url('admin/property/delete_image'); ?>',
                            data: {image_name: name},
                            success: function (res) {
                                if (res != 0) {
                                    //console.log('ok');
                                    var img_arr = $('#img_arr_p').val();
                                    // console.log(img_arr);
                                    var the_arr = img_arr.split(',');
                                    // console.log(the_arr);
                                    var final_arr = "";
                                    $.each(the_arr, function (ind, val) {
                                        if (val != name) {

                                            final_arr += val + ",";
                                        }
                                    });
                                    //console.log(final_arr);

//                                    console.log(final_arr);
//                                    var final = trim(final_arr, ',');
//                                    console.log(final);
//                                    $('#img_arr_p').val(final);
                                    $('#img_arr_p').val(rtrim(final_arr, ','));

                                    $('#upload_img_' + id).remove();
                                    $('#delete_btn_' + id).remove();
                                } else {
                                    alert('Cannot be deleted.');
                                }
                            }

                        });

                    });













                    //$(".upload-image-messages").html(response.responseText);
                    $('html, body').animate({scrollTop: $(".upload-image-messages").offset().top - 100}, 150);

                }
            };
            // Submit the form
            $(".upload-image-form").ajaxForm(options);

            return false;

        });
        function rtrim(str, chr) {
            var rgxtrim = (!chr) ? new RegExp('\\s+$') : new RegExp(chr + '+$');
            return str.replace(rgxtrim, '');
        }
    </script>