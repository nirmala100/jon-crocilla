<div class="row">
    <div class="col-lg-12">
        <a class="btn btn-info" href="<?php echo base_url('admin/property/add-edit'); ?>"> <span class="fa fa-fw fa-plus"></span> Add new property</a><br>
    </div>
</div>
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-comments fa-lg fa-fw"></i> List of all Property

            </div>
            <!-- /.panel-heading -->

            <div class="panel-body">
                <table class="table table-bordered <?php
                if ($all_property) {
                    echo "dataTables-table";
                }
                ?>">
                    <thead>
                        <tr>
                            <th>S.N.</th>
                            <th>Title</th>
                            <th>City</th>
                            <th>Image</th>
                            <th>Publish</th>
                            <th class="action-col">Option</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if ($all_property):

                            $i = 1; //for numbering value S.N.
                            foreach ($all_property as $property):
                                ?>
                                <tr>
                                    <td ><?php echo $i; ?></td>
                                    <td><?php echo $property->p_name ?></td>
                                    <td><?php echo $property->city ?></td>
                                    <td>
                                        <?php if ($property && $property->p_image && $image_name = $this->misc_library->get_image_filename($property->p_image, 'property', 'featured')) {
                                            ?> 
                                            <?php if (file_exists(BASEPATH . '../assets/images/property/' . $image_name)) { ?>
                                                <img src="<?php echo base_url() . '/assets/images/property/' . $image_name; ?>" class="img-thumbnail" height="100" width="100"/>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </td>
                                    <td class="for_change_status">
                                        <select class="form-control" name="is_active" id="change_status_<?php echo $property->id; ?>" class="content_status" 
                                                data-id="<?php echo $property->id; ?>" data-baseurl="<?php echo base_url() . 'admin/property/change_status_for'; ?>">
                                                <!--onchange="return change_status(this, '<?php //echo base_url($this->config->item('backend'));   ?>/property/change_status')"> -->
                                            <option value="1" <?php echo $property ? (($property->is_active == 1 ) ? 'selected' : '' ) : '' ?>>Published</option>
                                            <option value="0" <?php echo $property ? (($property->is_active == 0) ? 'selected' : '' ) : '' ?>>Unpublished</option>
                                        </select>
                                    </td>
                                    <td class="action-col">
                                        <div class="action-pos">
                                            <nobr>
                                                <form method="post" action="<?php echo base_url($this->config->item('backend')) . '/property/add_edit' ?>" class="pull-left">
                                                    <input type="hidden" value="<?php echo $property->id; ?>" name="id">
                                                    <button title="Edit" type="submit" class="btn btn-info"><span class="fa fa-edit fa-lg"></span></button>
                                                </form>

                                                <a href="<?php echo base_url('admin/property/property_open') . '/' . $property->id; ?>" title="Upload Images to property" class="btn btn-success pull-left"><span class="fa fa-external-link fa-lg"></span></a>
                                                <form method="post" action="<?php echo base_url($this->config->item('backend')) . '/property/delete' ?>" onsubmit="return confirm('Are you sure?')" class="pull-left">
                                                    <input type="hidden" value="<?php echo $property->id; ?>" name="id">
                                                    <button type="submit" class="del_con btn btn-danger" title="Delete"><span class="fa fa-trash-o fa-lg"></span></button>
                                                </form>
                                                <div class="clearfix"></div>
                                            </nobr>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            endforeach;
                        else:
                            ?>
                            <tr>
                                <td colspan="5">There are no propetites.</td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<?php
echo $this->session->flashdata('msg');
?>
<script type="text/javascript">
    $(document).ready(function () {
        //alert('okay');return;
        //var $myInput = $('#change_status_<?php //echo $property->id;   ?>').on('change', change_status);

//                                        function change_status($id, url) {
//                                            alert('okay');
//                                        }
        $('.for_change_status select').change(function () {

            var that = $(this);

            var id = that.data('id');
            if (!isNaN(id) && id > 0) {
                $.ajax({
                    url: that.data('baseurl'),
                    type: 'POST',
                    data: {id: id, active: that.val()},
                    success: function (res) {
                        console.log(res);
                        if (res == 0) {
                            alert('error! try again');
                        }
//                                                        else{
//                                                            if(res == 'do_reload'){
//                                                                location.reload();
//                                                            }
//                                                        }
                    }
                });
            }
        });

    });
</script>