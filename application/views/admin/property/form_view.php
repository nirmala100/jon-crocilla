
<div class="add-container">
    <p>
        <a class="btn btn-info" href="javascript:window.history.go(-1);"<h3><span class="fa fa-fw fa-lg fa-mail-reply"></span> Back</h3></a>
    </p>
    <form action="" enctype="multipart/form-data" id="signupForm"  method="post" role="form">
        <div class="row">
            <div class="col-sm-7">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <?php //print_r($property_info); die;?>
                        <h3 class="panel-title"> <span class="fa fa-fw  fa-<?php echo $property_info ?"edit":"plus";?>"></span> <?php echo $property_info ? $property_info->p_name . ' : edit property' : ' add new property'; ?></h3>
                    </div>
                    <div class="panel-body">
            
                        <div class="error-panel">
                            <?php //echo validation_errors(); ?>
                        </div>
                        <?php if($this->uri->segment(4)=='featured'){ ?>
                            <div class="form-group">
                            <label>Featured</label>
                            <div class="checkbox" style="margin-top:-5px">  
                                <label for="featured" ><input type="checkbox" class="" name="p_featured" id="featured" value='1'  checked="checked" />
                                    Yes</label>

                            </div>
                        </div>


                        <?php }else{ ?>
                        <div class="form-group">
                            <label>Featured</label>
                            <div class="checkbox" style="margin-top:-5px">  
                                <label for="featured" ><input type="checkbox" class="" name="p_featured" id="featured" value='1'  <?php if (isset($property_info->p_featured) && $property_info->p_featured == 1) { ?>checked="checked"<?php } ?> />
                                    Yes</label>

                            </div>
                        </div>
                        <?php } ?>
                        <div class="form-group"><label for="title">Title:</label>
                            <input type="text" name="p_name" class="form-control" value="<?php echo set_value('p_name', $property_info ? $property_info->p_name : '') ?>" id="title">
                        </div>
                        <div class="form-group"> <label for="slug">Slug:</label>
                            <input type="text" name="slug" class="form-control" value="<?php echo set_value('slug', $property_info ? $property_info->slug : ''); ?>" id="slug">
                        </div>
                       
                        <div class="row"> 
                            <div class="col-md-4">
                                <div class="form-group"><label for="bathroom">Price:</label>
                                    <input type="text" name="p_price" class="form-control" value="<?php echo set_value('p_price', $property_info ? $property_info->p_price : ''); ?>" id="slug">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group"><label for="bathroom">Bathroom:</label>
                                    <input type="text" name="p_bathroom" class="form-control" value="<?php echo set_value('p_bathroom', $property_info ? $property_info->p_bathroom : ''); ?>" id="slug">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group"><label for="bedroom">Bedroom:</label>
                                    <input type="text" name="p_bedroom" class="form-control" value="<?php echo set_value('p_bedroom', $property_info ? $property_info->p_bedroom : ''); ?>" id="slug">
                                </div>
                            </div>
                        </div>
                        <div class="form-group"> <label for="sqfeet">Square Feet:</label>
                            <input type="text" name="p_sq_feet" class="form-control" value="<?php echo set_value('p_sq_feet', $property_info ? $property_info->p_sq_feet : ''); ?>" id="slug">
                        </div>
                        <div class="form-group"> <label for="lotsize">Lot Size:</label>
                            <input type="text" name="p_lot_size" class="form-control" value="<?php echo set_value('p_lot_size', $property_info ? $property_info->p_lot_size : ''); ?>" id="slug">
                        </div>
                        <?php if (!empty($authors)) { ?>
                            <div class="form-group"> <label for="author_id" >Author:</label>
                                <select class="form-control" name="author_id" id="author_id">
                                    <?php foreach ($authors as $author) { ?>
                                        <option value='<?php echo $author->id; ?>' <?php echo set_select('author_id', $author->id, $blog_info ? ($blog_info->author_id == $author->id ? true : false) : false ); ?>><?php echo $author->title; ?></option>
                                    <?php } ?>
                                </select>
                            </div>  
                        <?php } ?>
                        <!-- /.row -->
                        <div class="form-group">
                            <label for="image">Image:</label> Please upload image at least of size 1024px X 500px
                            <input type="file" name="p_image"  value="<?php echo $property_info ? $property_info->p_image : ''; ?>" id="image">
                        </div>
                        <div class="col-lg-5">
                            <?php if ($property_info && $property_info->p_image && $image_name = $this->misc_library->get_image_filename($property_info->p_image, 'property', 'featured')) {
                                ?> 
                                <?php if (file_exists(BASEPATH . '../assets/images/property/' . $image_name)) { ?>
                                    <img src="<?php echo base_url() . '/assets/images/property/' . $image_name; ?>" class="img-thumbnail"/>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <div class="clearfix"></div> <br><br>   
						 <div class="form-group"><label for="short_desc">Short Description:</label>
                            <textarea name="short_desc" class="form-control"  /><?php echo set_value('short_desc', $property_info ? $property_info->short_desc : ''); ?></textarea>
                        </div>
                        <?php echo $this->ckeditor->replace("short_desc"); ?>
                        <div class="form-group"><label for="description">Description:</label>
                            <textarea name="p_description" class="form-control"  /><?php echo set_value('p_description', $property_info ? $property_info->p_description : ''); ?></textarea>
                        </div>
                        <?php echo $this->ckeditor->replace("p_description"); ?>
                        

                        <div class="form-group">
                            <label for="is_active" >Page Visible:</label>
                            <select class="form-control" name="is_active" id="is_active">
                                <option value=1 <?php echo set_select('is_active', '1', $property_info ? ($property_info->is_active == 1 ? true : false ) : false ); ?>>Yes</option>
                                <option value=0 <?php echo set_select('is_active', '0', $property_info ? ($property_info->is_active == 0 ? true : false ) : false ); ?>>No</option>
                            </select></div>
                    </div>

                </div>
            </div><!-- /.col-sm-7 -->
            <div class="col-sm-5">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="fa fa-fw fa-location-arrow"></span> Map</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group"> <label for="location">Location:</label>
                            <input type="text" name="p_location" id="location" class="form-control" value="<?php echo set_value('p_location', $property_info ? $property_info->p_location : ''); ?>">
                        </div>
                        <div class="form-group"> 
                            <label for="city">City:</label>
                            <input type="text" name="city" id="city" class="form-control" value="<?php echo set_value('city', $property_info ? $property_info->city : ''); ?>">
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group"> <label for="latitude">Latitude:</label>
                                    <input type="text" name="lat" id="lat" class="form-control" value="<?php echo set_value('lat', $property_info ? $property_info->lat : ''); ?>" readonly="true">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group"> <label for="longitude">Longitude:</label>
                                    <input type="text" name="long" id="long" class="form-control" value="<?php echo set_value('long', $property_info ? $property_info->long : ''); ?>" readonly="true">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <br>
                                <button id="clear-loc" type="button" class="btn btn-default" title="Clear"><i class="fa fa-trash-o"></i></button>
                            </div>

                        </div>
                        
                        <div id="location_map" style="width:100%; height:300px">
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="fa fa-fw fa-tag"></span> Meta Group</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group"><label for="meta_title" >Meta Title:</label>
                            <input type="text" class="form-control" id="meta_title" name="meta_title" maxlength="55" value="<?php echo set_value('meta_title', $property_info ? $property_info->meta_title : ''); ?>" id="meta_title"> <em class="text-muted">Should not exceed more than 55 words</em>
                        </div>
                        <div class="form-group"><label for="meta_keyword" >Meta Keyword:</label>
                            <textarea class="form-control" name="meta_keyword" id="meta_keyword" /><?php echo set_value('meta_keyword', $property_info ? $property_info->meta_keyword : ''); ?></textarea></div>

                        <div class="form-group"><label for="meta_description" >Meta Description:</label>
                            <textarea class="form-control" id="meta_description" name="meta_description" id="meta_description" maxlength="115"/><?php echo set_value('meta_description', $property_info ? $property_info->meta_description : ''); ?></textarea> <em class="text-muted">Should not exceed more than 115 words.</em>
                        </div>
                    </div>
                </div>
                
            </div><!-- /.col-sm-5 -->

        </div><!-- /.row -->
        <p>
            <input type="hidden" value="<?php echo set_value('id', $property_info ? $property_info->id : ''); ?>" name="id">
            <button type="submit" name="btn_property" class="btn btn-success" value="submit" > <span class="fa fa-fw fa-lg fa-check"></span> Submit </button>
        </p>
    </form>
</div><!-- end panel -->
<?php $latitude = ($property_info && $property_info->lat)?$property_info->lat:"41.881832";
$longitude = ($property_info && $property_info->long)?$property_info->long:"-87.623177";?>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=true&amp;libraries=places"></script>
<script type="text/javascript">
    var marker=new google.maps.Marker();
    var geocoder = new google.maps.Geocoder();
    var map; 
    /** marker **/
    marker.setMap(map);
    marker.setDraggable(true);
    marker.setVisible(true);

    function initialize() {
        var myLatlng = new google.maps.LatLng(<?php echo $latitude;?>,<?php echo $longitude;?>);
        var myOptions = {
            zoom: 8,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById("location_map"), myOptions); 
        marker.setMap(map);
        marker.setPosition(myLatlng);
        <?php if($property_info && $property_info->lat && $property_info->long)
        {?>
            map.setZoom(15);
        <?php }?>

        marker.setDraggable(true);
        marker.setMap(map);

        google.maps.event.addListener(marker, 'dragend', function() {
            $('#lat').val(marker.getPosition().lat().toFixed(7));
            $('#long').val(marker.getPosition().lng().toFixed(7));        
        });
  
    }

    function codeAddress() {
        var street_address = $("#location").val();
        var address = street_address+", United States";           
        if (geocoder) {
            geocoder.geocode( { 'address': address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setZoom(15);
                    map.setCenter(results[0].geometry.location);
                    marker.setPosition(results[0].geometry.location);
                     
                    $('#lat').val(marker.getPosition().lat().toFixed(7));
                    $('#long').val(marker.getPosition().lng().toFixed(7));       
                }else {
                    //console.log(address+' not found');
                }
            });
        }
    }
    google.maps.event.addDomListener(window, 'load', initialize);

    $(document).ready(function(){
        $('#location').on('keyup blur',function(){
            codeAddress();
        });
        $('#clear-loc').on('click',function(){
            if($('#lat').val()=="" && $('#long').val()==""){
                return false;
            }
            if(confirm("Are you sure to delete the latitude and longitude?")){
                $('#lat').val('');
                $('#long').val('');
            }
        });
    });
</script>