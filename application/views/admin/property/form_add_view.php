
<div class="add-container">
    <p>
        <a class="btn btn-info" href="<?php echo base_url('admin/property'); ?>"<h3><span class="fa fa-fw fa-lg fa-mail-reply"></span> Back</h3></a>
    </p>
    <form action="" enctype="multipart/form-data" id="signupForm"  method="post" role="form">
        <div class="row">
            <div class="col-sm-7">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <?php //print_r($property_info);?>
                        <h3 class="panel-title"> <span class="fa fa-fw  fa-plus"></span> <?php //echo $property_info ? $property_info->p_name . ' : edit blog' : ' add new blog';   ?></h3>
                    </div>
                    <div class="panel-body">

                        <div class="error-panel">
                            <?php //echo validation_errors(); ?>
                        </div>
                        <div class="form-group">
                            <label>Featured</label>
                            <div class="checkbox" style="margin-top:-5px">  
                                <label for="featured" ><input type="checkbox" class="" name="p_featured" id="featured" value='1' <?php if (isset($featured) && $featured) { ?>checked="checked"<?php } ?> />
                                    Yes</label>                                                                                 
                            </div>
                        </div>
                        <div class="form-group"><label for="title">Title:</label>
                            <input type="text" name="title" class="form-control" value="<?php //echo $property_info ? $property_info->p_name : ''   ?>" id="title">
                        </div>
                        <div class="form-group"><label for="slug">Slug:</label>
                            <input type="text" name="slug" class="form-control" value="<?php //echo $property_info ? $property_info->p_name : ''   ?>" id="title">
                        </div>
                        <div class="form-group"> <label for="location">Location:</label>
                            <input type="text" name="location" class="form-control" value="<?php //echo set_value('location', $property_info ? $property_info->p_location : '');   ?>" id="slug">
                        </div>
                        <div class="form-inline"> <label for="bathroom">Price:</label>
                            <input type="text" name="price" class="form-control" value="<?php //echo set_value('price', $property_info ? $property_info->p_price : '');   ?>" id="slug">
                            <label for="bathroom">Bathroom:</label>
                            <input type="text" name="bathroom" class="form-control" value="<?php //echo set_value('bathroom', $property_info ? $property_info->p_bathroom : '');   ?>" id="slug">
                            <label for="bedroom">Bedroom:</label>
                            <input type="text" name="bedroom" class="form-control" value="<?php //echo set_value('bedroom', $property_info ? $property_info->p_bedroom : '');   ?>" id="slug">
                        </div>
                        <div class="form-group"> <label for="sqfeet">Square Feet:</label>
                            <input type="text" name="sqfeet" class="form-control" value="<?php //echo set_value('squarefeet', $property_info ? $property_info->p_sq_feet : '');   ?>" id="slug">
                        </div>
                        <div class="form-group"> <label for="lotsize">Lot Size:</label>
                            <input type="text" name="lotsize" class="form-control" value="<?php //echo set_value('lotsize', $property_info ? $property_info->p_lot_size : '');   ?>" id="slug">
                        </div>
                        <?php if (!empty($authors)) { ?>
                            <div class="form-group"> <label for="author_id" >Author:</label>
                                <select class="form-control" name="author_id" id="author_id">
                                    <?php foreach ($authors as $author) { ?>
                                        <option value='<?php echo $author->id; ?>' <?php //echo set_select('author_id', $author->id, $blog_info ? ($blog_info->author_id == $author->id ? true : false) : false );   ?>><?php echo $author->title; ?></option>
                                    <?php } ?>
                                </select>
                            </div>  
                        <?php } ?>
                        <!-- /.row -->
                        <div class="form-group">
                            <label for="image">Image:</label> Please upload image at least of size 640px X 447px
                            <input type="file" name="image"  value="<?php echo $property_info ? $property_info->p_image : ''; ?>" id="image">
                        </div>
                        <div class="col-lg-5">
                            <?php //if ($property_info && $property_info->p_image && $image_name = $this->misc_library->get_image_filename($property_info->p_image, 'property', 'listing')) {
                            ?> 
                            <?php //if (file_exists(BASEPATH . '../assets/images/property/' . $image_name)) { ?>
                            <img src="<?php //echo base_url() . '/assets/images/property/' . $image_name;   ?>" class="img-thumbnail"/>
                            <?php
                            //}
                            // }
                            ?>
                        </div>
                        <div class="clearfix"></div> <br><br>   
                        <div class="form-group"><label for="description">Description:</label>
                            <textarea name="description" class="form-control"  /><?php //echo set_value('description', $property_info ? $property_info->p_description : '');   ?></textarea>
                        </div>
                        <?php echo $this->ckeditor->replace("description"); ?>

                        <div class="form-group">
                            <label for="is_active" >Page Visible:</label>
                            <select class="form-control" name="is_active" id="is_active">
                                <option value=1 <?php //echo set_select('is_active', '1', $property_info ? ($property_info->is_active == 1 ? true : false ) : false );   ?>>Yes</option>
                                <option value=0 <?php //echo set_select('is_active', '0', $property_info ? ($property_info->is_active == 0 ? true : false ) : false );   ?>>No</option>
                            </select></div>
                    </div>

                </div>
            </div><!-- /.col-sm-7 -->
            <div class="col-sm-5">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="fa fa-fw fa-tag"></span> Meta Group</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group"><label for="meta_title" >Meta Title:</label>
                            <input type="text" class="form-control" id="meta_title" name="meta_title" maxlength="55" value="<?php echo set_value('meta_title', $blog_info ? $blog_info->meta_title : ''); ?>" id="meta_title"> <em class="text-muted">Should not exceed more than 55 words</em>
                        </div>
                        <div class="form-group"><label for="meta_keyword" >Meta Keyword:</label>
                            <textarea class="form-control" name="meta_keyword" id="meta_keyword" /><?php echo set_value('meta_keyword', $blog_info ? $blog_info->meta_keyword : ''); ?></textarea></div>

                        <div class="form-group"><label for="meta_description" >Meta Description:</label>
                            <textarea class="form-control" id="meta_description" name="meta_description" id="meta_description" maxlength="115"/><?php echo set_value('meta_description', $blog_info ? $blog_info->meta_description : ''); ?></textarea> <em class="text-muted">Should not exceed more than 115 words.</em>
                        </div>
                    </div>
                </div>
            </div><!-- /.col-sm-5 -->

        </div><!-- /.row -->
        <p>
            <input type="hidden" value="<?php //echo set_value('id', $property_info ? $property_info->id : '');   ?>" name="id">
            <button type="submit" name="btn_property" class="btn btn-success" value="submit" > <span class="fa fa-fw fa-lg fa-check"></span> Submit </button>
        </p>
    </form>
</div><!-- end panel -->

