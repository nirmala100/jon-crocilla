
<main>
    <div class="container contain contain-white lending">

        <div class="row">
            <div class="col-md-12">
                <div class="lending-background">
                    <div class="text-caption">
                        JON CROCILLA LUXURY PROPERTIES LENDING
                       
                    </div><!-- text-caption -->
                   
                    <div class="text-bottom">
                        <div class="logo-lending">
                            <img src="<?php echo base_url('assets/images/ema-logo.png'); ?>" class="img-responsive" alt="">
                        </div>
                            <p>EXPERT MORTGAGE PROVIDES LENDING KNOWLEDGE TO BEST STRUCTURE YOUR MORTGAGE LOAN IN ORDER TO MAKE YOU LOOK AS FINANCIALLY SOLID AS POSSIBLE TO A BANK.</p>
<!--                       a Lorem ipm dolor sit amet, consec tetur adi cing elit, sed do eiusmod tempor idunt ut labore et dolore magna dolore magnamr incididunt ut labore etore dolore magna aliqua. Uquat. uisoreore auirure dehenderit in volupt”-->
                    </div><!-- text-bottom -->
                </div><!-- lending-background -->
            </div><!-- column -->
        </div><!-- row -->
    <div class="clearfix"></div>
        <div class="row">
        <div class="col-md-5 col-md-offset-1 col-sm-5  push-top-apply-form col-md-push-6 col-sm-12">
                <h1 class="large-heading"><a href="<?php echo base_url('questionnaire'); ?>">apply here</a></h1>

                <div class="row">
                    <div class="col-md-12 number-list">
                        <!--<div class="number-holder">
                            1
                        </div>
                        <div class="text-holder">
                            <h4>apply online</h4>
                            <p>Fill out the form and you will get a response as soon as possible.</p>
                        </div>
                    </div>-->
                    <!-- column -->
                <!--</div>--><!-- row --> 

                 <!--<div class="row">
                    <div class="col-md-12">
                        <div class="number-holder">
                            2
                        </div>
                        <div class="text-holder">
                            <h4>contract</h4>
                            <p>The process will not take more than
                                60 minutes. </p>
                        </div>
                    </div>--> <!-- column -->
                 <!--</div>--> <!-- row --> 

                 <!--<div class="row">
                    <div class="col-md-12">
                        <div class="number-holder no-img-after">
                            3
                        </div>
                        <div class="text-holder">
                            <h4>get your money!</h4>
                            <p>Once you have signed the contract.
                                money will be transferred to your
                                bank account within few hours or
                                you will be handed over immediately. </p>
                        </div>
                    </div>--> <!-- column -->
                 <!--</div>--> <!-- row --> 
             <div class="number-list-lists">
                <h1 class="number-list-heading">Why Expert Mortgage?</h1>
                <h2>LENDERS:</h2>
                <p>We shop your loan to dozens of lenders, which forces them to compete with each other to earn your business, so you get a better rate and better terms.</p>

                <h2>EXPERIENCE:</h2>
                <p>We stay by your side throughout the entire process, leveraging decades of experience to make everything smoother and easier. </p>

                <h2>RELIABILITY:</h2>
                <p>Our reliable, responsive, and easy to reach service allows you to know what's happening every step of the way.</p>

            </div>
            </div>

        </div><!-- row -->
   </div>
            
            <div class="col-md-5 col-md-offset-1 col-md-pull-6 col-sm-12 ">

                <form class="form-horizontal application-form approved_form" method="post" action="<?php echo base_url('get_approved');?>">
                    <h1 class="border-bottom">GET PRE APPROVED NOW!</h1>

                    <div class="form-group">
                        <label class="col-md-4 control-label">first name:</label>
                        <div class="col-md-8">
                            <input type="text" name="first_name" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">last name:</label>
                        <div class="col-md-8">
                            <input type="text" name="last_name" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">phone:</label>
                        <div class="col-md-8">
                            <input type="text" name="phone_no" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Email:</label>
                        <div class="col-md-8">
                            <input type="email" name="email" class="form-control">
                        </div>
                    </div>
                    <input type="hidden" id="base-url" value="<?php echo base_url();?>">
                    <div class="text-center">
                        <button class="btn btn-default btn-pattern" name="btn_approved"  value="okay"type="submit">apply now!</button>
                    </div>

                </form>
            </div><!-- column -->
          

     </div>        
        <div class="row bg-pattern-brick">
<?php if($lendings): ?>
    <?php foreach($lendings as $lending):?>
            <div class="col-md-4 col-sm-4 col-xs-12 alter-width-listings">
                <div class="bg-dropshadow">
                    <h2><?php echo $lending->title;?></h2>
                     <?php if ($lending && $lending->image && $image_name = $this->misc_library->get_image_filename($lending->image, 'footerbox', 'main')) {
                                            ?>  
                                            <?php if (file_exists(BASEPATH . '../assets/images/footerbox/' . $image_name)) { ?>
                    <img src="<?php echo base_url() . '/assets/images/footerbox/' . $image_name; ?>" class="img-responsive" alt="">
                     <?php
                                            }
                                        }
                                        ?>
                    <p><?php echo $lending->f_description;?></p>
                    <a href="<?php echo $lending->url;?>" class="btn btn-default btn-pattern"><span class="after"><img src="<?php echo base_url('assets/images/icon-btn.png'); ?>" alt=""></span>read more</a>
                </div>
            </div><!-- column -->
            <?php endforeach;?>
<?php endif;?>

        </div><!-- row -->


    </div><!-- contain-white -->
</main>

</div><!-- bg-pattern-grey -->
