
<?php 
if ($top_stories): 
    ?>
                        <?php foreach ($top_stories as $top): ?>    
                            <div class="top-story">
                                <div class="row">
                                    <div class="col-md-3 col-xs-3">
                                        <a href="<?php echo base_url("blog-detail/$top->slug"); ?>">
                                            <?php if ($top && $top->image && $image_name = $this->misc_library->get_image_filename($top->image, 'blog', 'top_stories')) {
                                                ?> 
                                                <?php if (file_exists(BASEPATH . '../assets/images/blog/' . $image_name)) { ?>
                                                    <img src="<?php echo base_url() . '/assets/images/blog/' . $image_name; ?>" class="img-responsive"/>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </a>
                                    </div>
                                    <div class="col-md-9 col-xs-9">
                                        <div><a href="<?php echo base_url("blog-detail/$top->slug"); ?>"><?php echo $top->title; ?></a></div>
                                       <?php /* <p class="comments"><span class="disqus-comment-count" data-disqus-url="<?php echo base_url("blog-detail/$top->slug"); ?>"> <!-- Count will be inserted here --> </span></p> */?>
                                    </div>
                                </div>
                            </div>
                            
                        <?php endforeach; ?>
                    <?php 
                    endif; 
                    ?>
<script type="text/javascript">
/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
var disqus_shortname = 'joncrocillacom'; // required: replace example with your forum shortname

/* * * DON'T EDIT BELOW THIS LINE * * */
(function () {
var s = document.createElement('script'); s.async = true;
s.type = 'text/javascript';
s.src = '//' + disqus_shortname + '.disqus.com/count.js';
(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
}());
</script>