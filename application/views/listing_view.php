<main>
    <?php
if ($all_listing):
	$i = 1; //for numbering value S.N.
	?>
			        <div class="container contain contain-white">
			            <div class="listing">
			                <?php
	//print_r($all_listing);die;
	foreach ($all_listing as $listing):
	?>
			                    <div id ="<?php echo "listing_" . $listing->id;?>" >
			                        <div class="row mb27">
			                            <div class="col-md-5 col-sm-5"><?php //$image_name = $this->misc_library->get_filename_without_ext($listing->p_image, 'property', 'listing'); print_r($image_name); ?>
			                                <?php if ($listing && $listing->p_image && $image_name = $this->misc_library->get_image_filename($listing->p_image, 'property', 'listing')) {
		?>
			                                    <?php if (file_exists(BASEPATH . '../assets/images/property/' . $image_name)) {?>
			                                        <a href="<?php echo base_url('listing-detail/') . '/' . $listing->slug;?>">
			                                        <img src="<?php echo base_url() . '/assets/images/property/' . $image_name;?>" class="img-thumbnail" class="img-responsive" alt=""/>
							                                        </a>
							                                        <?php
																}
																}
																?>

		                            </div><!-- column -->
	                            <div class="col-md-7 col-sm-7 property-text-wrap">
	                                <div class="property-name"><a href="<?php echo base_url('listing-detail/') . '/' . $listing->slug;?>"><?php echo $listing->p_name;?></a></div>
	                                <p><?php echo $listing->city;?></p>
	                                <div class="rate"><img src="<?php echo base_url() . '/assets/images/dollar.jpg';?>" alt=""> <?php echo $listing->p_price;?></div>
	                                <ul class="details">
	                                    <li><b><?php echo $listing->p_bedroom;?></b> beds</li>
	                                    <li><b><?php echo $listing->p_bathroom;?></b> baths</li>
	                                    <li><b><?php echo $listing->p_sq_feet;?></b> sq.fts</li>
	                                </ul>
	                                <p><?php echo content_limiter($listing->short_desc, 200, '...');?></p>
	                                <input type="hidden" name="id" value="<?php echo $listing->id;?>"><?php //echo $listing->slug; die;  ?>
	                                <a href="<?php echo base_url('listing-detail/') . '/' . $listing->slug;?>">property details&nbsp;&nbsp;<i class="fa fa-caret-right"></i></a>
	                            </div><!-- column -->


			                        </div><!-- row -->
			                    </div>
			                    <?php
	$i++;
	endforeach;
	?>
			                <a href="<?php echo base_url('next-listing/' . $startagain);?>" ></a>
			            </div>

			        </div><!-- contain-white -->
			    <?php else:
?>
        <!-- <div class="container contain contain-white">There are no Listing.</div> -->
<?php endif;?>
</main>

</div><!-- bg-pattern-grey -->

<?php
//include("footer.php"); ?>