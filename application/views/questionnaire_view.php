<?php
if ($this->session->userdata('questionnaire') != ''):
    $info_qest = $this->session->userdata('questionnaire');
endif;
?>
<main>
    <div class="container contain contain-white form-submission-page">
        
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class="border-bottom">lending questionnaire</h1>
            </div><!-- column -->
        </div><!-- row -->

        <div class="row">
            <?php if ($this->session->flashdata('email_status')): ?>
                <div class="col-md-12 text-center">
                    <h1 class="border-bottom">
                        <?php echo $this->session->flashdata('email_status'); ?>
                    </h1>
                </div>
            <?php endif; ?>
        </div>

        <div class="row">
            <div class="col-md-12">    
                <ul class="page-links">
                    <li class="active"><a href="#">1.&nbsp;lending questionnaire</a></li>
                    <li><a>2.&nbsp;contact info</a></li>
                    <li><a>3.&nbsp;submission</a></li>
                </ul>
            </div><!-- column -->
        </div><!-- row -->

        <div class="row text-center">
            <div class="col-md-12">
                <p>FILL OUT THE QUESTIONNAIRE TO UNDERSTAND MORE ABOUT YOUR LENDING NEEDS:</p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php if (!empty($questionnaires)): $i = 1; ?>
                    <form class="form-horizontal" method="post" action="<?php echo base_url('add-questionnaire'); ?>" id="questionnaire-form">
                        <?php foreach ($questionnaires as $ques): 
                        ?>

                            <div class="form-group">
                                <label class="col-md-5 col-md-offset-1 control-label"><?php echo isset($ques->question) ? $ques->question : ''; ?></label>
                                <div class="col-md-5">
                                    <input type="hidden" name="question[]" value="<?php echo isset($ques->question) ? $ques->question : ''; ?>">
                                    <?php if ($ques->answers): ?>
                                        <select class="form-control custom-jselect" id="custom-select-box-<?php echo $i; ?>" name='answer[<?php echo $i; ?>]'>
                                           <option <?php echo (!isset($info_qest[$ques->question]) || (isset($info_qest[$ques->question]) && $info_qest[$ques->question]=="N/A"))?'selected="selected"':'';?> value="N/A">--Select Option--</option>
                                            <?php foreach ($ques->answers as $ans): 
                                                    $selected = false;
                                                    if(isset($info_qest[$ques->question]) && $info_qest[$ques->question]==$ans->answer){
                                                        $selected = true;
                                                    }?>
                                                 
                                                    <option value="<?php echo $ans->answer; ?>" <?php echo $selected?"selected=selected":'';?>><?php echo $ans->answer; ?></option>
                                                <?php //if (!empty($info_qest) && $info_qest[$ques->question] == $ans->answer): ?>
                                                <?php //endif; ?>
                                            <?php endforeach; ?>
                                        </select>
                                    <?php endif; ?>
                                </div>
                                <div class="clearfix"></div>
                            </div><!-- form-group -->
                            <?php $i++;
                        endforeach;
                        ?>

                        <div class="col-md-12 text-center">
                            <button class="btn btn-default btn-pattern" type="submit" name="btn_continue">continue</button>
                        </div>

                    </form>
                <?php endif;
                ?>

            </div><!-- column -->
        </div><!-- row -->

    </div><!-- contain-white -->
</main>

</div><!-- bg-pattern-grey -->

