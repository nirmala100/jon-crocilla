<?php

class Main_Model extends CI_Model {

    public function __construct() {

        parent::__construct();
    }

    public function get_limited_testimonials() {
        $this->db->select('*');
        $this->db->from('tbl_testimonials');
        $this->db->where(array('is_active' => 1));
        $this->db->limit(5);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result() : false;
    }

    public function get_featured_property() {
        $this->db->select('*');
        $this->db->from('tbl_property');
        $this->db->where(array('is_active' => 1));
        $this->db->where(array('p_featured' => 1));
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result() : false;
    }

    public function getPropertyBySlug($slug) {

        $this->db->where(array('is_active' => 1, 'slug' => $slug));

        $this->db->from('tbl_property');

        $query = $this->db->get();
        //print_r($query);die;

        if ($query->num_rows > 0) {

            return $query->row();
        } else {

            return false;
        }
    }


    public function getBlogBySlug($slug) {

        $this->db->where(array('is_active' => 1, 'slug' => $slug));

        $this->db->from('tbl_blog');

        $query = $this->db->get();
        //print_r($query);die;

        if ($query->num_rows > 0) {

            return $query->row(); 
        } else {

            return false;
        }
    }

    public function get_all_day_no() {
        $this->db->select('*');
        $this->db->from('tbl_days');
        $this->db->order_by('day_no', "asc");
        $this->db->group_by('day_no');
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result() : false;
    }

     /*public function get_latest_blog(){
         $this->db->limit(1);
        $this->db->where(array('is_active' => 1));

        $this->db->from('tbl_blog');
        $this->db->group_by("create_date","desc");
        $query = $this->db->get();
        //print_r($query);die;

        if ($query->num_rows > 0) {

            return $query->row(); 
        } else {

            return false;
        }

     }
     

     public function get_other_blogs(){
        $this->db->select("*");
        $this->db->from('tbl_blog');
        $this->db->group_by("create_date","desc");
        $this->db->where(array('is_active'=>1));
        $this->db->except;
        $this->crud_model->select_by_order_limit("*", "tbl_blog", "create_date", "DESC", 1, array('is_active'=>1));
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result() : false;
     }*/
     
     public function get_count($table, $args){
        $this->db->select('id')->from($table)->where($args);
        $result = $this->db->get();

        return $result->num_rows > 0 ? $result->num_rows : false;
    }


     public function get_other_blogs(){
         $this->db->get_where('tbl_blog', array('is_active' => 1), 10, $offset);
     }
}