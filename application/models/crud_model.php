<?php

class Crud_Model extends CI_Model {

    public function __construct() {

        parent::__construct();
    }

    public function insert($tbl_name, $args) {
         // echo '<pre>';print_r($args);die;
        if ($this->db->insert($tbl_name, $args))
            return $this->db->insert_id();
        else
            return false;
    }

    public function update($tbl_name, $args, $where) {

        $this->db->where($where);

        // echo ($this->db->update($tbl_name, $args));die;
        //echo $this->db->last_query();die;

        if ($this->db->update($tbl_name, $args))
            return true;
        else
            return false;
    }

    public function delete($tbl_name, $where) {

        if ($this->db->delete($tbl_name, $where))
        // echo $this->db->last_query();die;
            return true;
        else
            return false;
    }

    public function select($select, $tbl_name, $where = '') {

        $this->db->select($select);

        $this->db->from($tbl_name);

        if ($where != '')
            $this->db->where($where);

        $query = $this->db->get();

        //echo $this->db->last_query();die;

        return $query->num_rows > 0 ? $query->result() : false;
    }

    public function select_where_in($select, $tbl_name, $col, $arr, $where = array(), $not = false) {

        $this->db->select($select);

        $this->db->from($tbl_name);

        if (!empty($where))
            $this->db->where($where);

        if ($not === true) {

            $this->db->where_not_in($col, $arr);
        } else {

            $this->db->where_in($col, $arr);
        }

        $query = $this->db->get();

        return $query->num_rows > 0 ? $query->result() : false;
    }

    public function select_greater_than($select, $tbl_name, $where1, $where2) {

        $this->db->select($select);

        $this->db->from($tbl_name);

        $this->db->where($where1, $where2);

        $query = $this->db->get();

        return $query->num_rows > 0 ? $query->result() : false;
    }

    public function select_row($select, $tbl_name, $where = '') {

        $this->db->limit(1);

        $this->db->select($select);

        if ($where != '')
            $this->db->where($where);

        $query = $this->db->get($tbl_name);

        //echo $this->db->last_query();die;

        return $query->num_rows > 0 ? $query->row() : false;
    }

    public function select_by_order($select, $tbl_name, $field_name, $type, $where = '') {

        $this->db->select($select);

        $this->db->from($tbl_name);

        if ($where != '')
            $this->db->where($where);

        $this->db->order_by($field_name, $type);

        $query = $this->db->get();

        return $query->num_rows > 0 ? $query->result() : false;
    }

    public function select_by_order_limit($select, $tbl_name, $field_name, $type, $limit = '', $where = '') {

        if ($limit)
            $this->db->limit($limit);

        $this->db->select($select);

        $this->db->from($tbl_name);

        if ($where != '')
            $this->db->where($where);

        $this->db->order_by($field_name, $type);

        $query = $this->db->get();

        return $query->num_rows > 0 ? $query->result() : false;
    }

    

    public function get_max($field_name, $variable, $table,$where='') {

        $this->db->select_max($field_name, $variable);
        if ($where != '')
            $this->db->where($where);

        $result = $this->db->get($table);

        $max_ord = $result->row()->max_order;

        return $max_ord;
    }

    public function content_count($tbl) {

        return $this->db->count_all($tbl);
    }

    public function get_all_content($limit, $start, $tbl) {

        $this->db->limit($limit, $start);

        $query = $this->db->order_by('id', 'desc')->get($tbl);

        //echo $this->db->last_query();

        if ($query->num_rows > 0) {

            return $query->result();
        } else {

            return false;
        }
    }

    public function get_all_content_asc($limit, $start, $tbl) {

        $this->db->limit($limit, $start);
        $this->db->where(array('is_active' => 1));

        $query = $this->db->order_by('id', 'asc')->get($tbl);

        //echo $this->db->last_query();

        if ($query->num_rows > 0) {

            return $query->result();
        } else {

            return false;
        }
    }

//    public function img_delete($array_detail, $img_detail, $manager, $extension) {
//
//        foreach ($array_detail as $detail):
//
//            @unlink(BASEPATH . '../assets/images/' . $manager . '/' . $img_detail . '_' . $detail->type . '_' . $detail->width . '_' . $detail->height . '.' . $extension);
//
//        endforeach;
//
//        
//        @unlink(BASEPATH . '../assets/images/' . $manager . '/' . $img_detail);
//
//    }


    public function img_delete($array_detail, $img_detail, $manager, $extension, $flag = false) {

        foreach ($array_detail as $detail):

            @unlink(BASEPATH . '../assets/images/' . $manager . '/' . $img_detail . '_' . $detail->type . '_' . $detail->width . '_' . $detail->height . '.' . $extension);

        endforeach;

        if (!$flag) {
            @unlink(BASEPATH . '../assets/images/' . $manager . '/' . $img_detail);
        } else {
            @unlink(BASEPATH . '../assets/images/' . $manager . '/' . $img_detail . '.' . $extension);
        }
    }

    public function search_info($get_info) {

        echo $get_info;

        die;

        //echo 'Search Text';
    }

    public function change_status($table) {
        $id = $this->input->post('id');
        $active = $this->input->post('active');
        $this->db->where('id', $id);
        if ($this->db->update($table, array('is_active' => $active))) {
            return true;
        }
        return false;
    }
	
	public function status($tbl) {

        $is_active = $this->input->post('status_val');
        print_r($is_active);
        die;
        $id = $this->input->post('the_id');

        $this->db->where('id', $id);

        if ($this->db->update($tbl, array('is_active' => (int) $is_active))) {

            return 1;
        } else {

            return 2;
        }
    }

     public function select_by_order_limit_start($select, $tbl_name, $field_name, $type, $limit = '', $start=0, $where = '') {
        if ($limit)
            $this->db->limit($limit, $start);
        $this->db->select($select);
        $this->db->from($tbl_name);
        if ($where != '')
            $this->db->where($where);
        $this->db->order_by($field_name, $type);
        $query = $this->db->get();
        return $query->num_rows > 0 ? $query->result() : false;
    }


    public function select_row_by_order($select, $tbl_name, $field_name,$type,$where = '') {

        $this->db->limit(1);

        $this->db->select($select);

        if ($where != '')
            $this->db->where($where);
        $this->db->order_by($field_name, $type);
        $query = $this->db->get($tbl_name);

        //echo $this->db->last_query();die;

        return $query->num_rows > 0 ? $query->row() : false;
    }

}
