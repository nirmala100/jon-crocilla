<?php

class Authorization_Model extends CI_Model {

    public function __construct() {

        parent::__construct();
    }

    public function check_if_user_exists() {

        $where = array('username' => $this->input->post('username'), 'password' => sha1($this->input->post('password')));

        return $this->crud_model->select_row('*', 'tbl_admin', $where);
    }

    public function set_session() {

        $sess_data = array('is_logged' => 1);


        $this->session->set_userdata($sess_data);

        return true;
    }

}

?>