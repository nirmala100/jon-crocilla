<?php
class Thumbnail_Model extends CI_Model {

    public function thumb_distinct($select, $tbl_name, $where = '') {


        $this->db->select($select);


        $this->db->distinct();


        $this->db->from($tbl_name);


        if ($where != '')
            $this->db->where($where);


        $query = $this->db->get();


        //echo $this->db->last_query();die;


        return $query->num_rows > 0 ? $query->result() : false;
    }

}