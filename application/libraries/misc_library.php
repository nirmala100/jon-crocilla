<?php

class Misc_Library {

    private $CI;

    public function __construct() {

        $this->CI = & get_instance();
    }

    public function is_logged() {

        if ($this->CI->session->userdata('is_logged'))
            return true;
        else
            return false;
    }

    public function set_logged_in_session($associative_array) {

        $this->CI->session->set_userdata($associative_array);
    }

    public function check_login($username, $password) {

        $password = sha1($password);

        $user_info = array('username' => $username, 'password' => $password);

        $query = $this->CI->db->get_where('tbl_admin', $user_info, 1);

        $this->CI->db->_error_message();

        if ($query->num_rows() > 0) {

            $row = $query->row();

            $sess['is_logged'] = $row->id;

            $this->set_logged_in_session($sess);

            return true;
        } else {

            return false;
        }
    }

    public function get_thumbnail($manager) {

        $this->CI->db->select('*');

        $this->CI->db->from("tbl_thumbnail");

        $this->CI->db->where('manager', $manager);

        $query = $this->CI->db->get();

        return $query->num_rows > 0 ? $query->result() : false;
    }

    public function get_image($select, $tbl_name, $where = '') {

        $this->CI->db->limit(1);

        $this->CI->db->select($select);

        $this->CI->db->from($tbl_name);

        if ($where != '')
            $this->CI->db->where($where);

        $query = $this->CI->db->get();

        return $query->num_rows > 0 ? $query->row() : false;
    }

    public function get_gallery_image($select, $tbl_name, $where = '') {

        $this->CI->db->select($select);

        $this->CI->db->from($tbl_name);

        if ($where != '')
            $this->CI->db->where($where);

        $query = $this->CI->db->get();

        return $query->num_rows > 0 ? $query->result() : false;
    }

    public function select_row_by_order($select, $tbl_name, $field_name, $type, $where = '') {

        $this->CI->db->limit(1);

        $this->CI->db->select($select);

        $this->CI->db->from($tbl_name);

        $this->CI->db->where($where);

        $this->CI->db->order_by($field_name, $type);

        $query = $this->CI->db->get();

        return $query->num_rows > 0 ? $query->row() : false;
    }

    public function setting_links($slug, $select = '') {

        $this->CI->db->limit(1);

        $this->CI->db->select($select);

        $this->CI->db->from('tbl_setting');

        $this->CI->db->where('slug', $slug);

        $query = $this->CI->db->get();

        if ($query->num_rows > 0) {

            if ($select == "") {

                return $query->row();
            } else {

                return $query->row()->$select;
            }
        } else {

            return false;
        }
    }

    /**

     * Limits the given paragraph and returns the given number of characters with trailing three dots(...)

     * @param string $text = paragraph

     * @param integer $limit = number of characters to limit/retrieve

     * @param boolean $includeTags =  if true includes the tags

     */
    public function limit_text($text, $limit = '200', $includeTags = false) {

        if (!$includeTags)
            $text = strip_tags($text);

        if (strlen($text) > $limit) {

            $text = substr($text, 0, $limit) . '...';
        }

        return $text;
    }

    /**

     * Gets the image of the given type

     * @param type $filename Name of the Image File

     * @param type $section Section on which image is used

     * @param type $type The type of Image : denotes the size of image

     */
    public function get_image_filename($filename, $section, $type) {

        $row = false;

        $this->CI->db->limit(1);

        $this->CI->db->from('tbl_thumbnail');

        $this->CI->db->where('manager', $section);

        $this->CI->db->where('type', $type);

        $query = $this->CI->db->get();

        if ($query->num_rows > 0) {

            $row = $query->row();
        }

        if ($row) {

            $imagearr = explode(".", $filename);

            $ext = end($imagearr);

            $image = $filename . '_' . $row->type . '_' . $row->width . '_' . $row->height . "." . $ext;

            return $image;
        }
    }

    public function get_first_image($id) {

        $this->CI->db->limit(1);

        $this->CI->db->from('tbl_gallery');

        $this->CI->db->where('gal_cat_id', $id);

        $query = $this->CI->db->get();

        if ($query->num_rows > 0) {

            return $query->row();
        }
    }

    public function countGalleryImages($id) {

        $this->CI->db->where(array('is_active' => 1, 'gal_cat_id' => $id));

        return $this->CI->db->count_all_results('tbl_gallery');
    }

    public function check_if_slug_exists($slug, $table_name, $id = "") {

        $where_arg['slug'] = $slug;

        if ($id != "") {

            $where_arg['id <>'] = $id;
        } else {

            $a = $this->CI->db->get($table_name)->last_row();

            if ($a)
                $id = $a->id + 1;
            else
                $id = 1;
        }

        $this->CI->db->where($where_arg);

        $same_slug = $this->CI->db->get($table_name)->row();

        if ($same_slug) {

            $text = $same_slug->slug . "-" . $id;
        } else {

            $text = $slug;
        }

        return $text;
    }

    public function get_basic_info($select = '') {

        $this->CI->db->limit(1);

        $this->CI->db->select($select);

        $this->CI->db->from('tbl_basic_info');

        $query = $this->CI->db->get();

        if ($query->num_rows > 0) {

            if ($select == "") {

                return $query->row();
            } else {

                return $query->row()->$select;
            }
        } else {

            return false;
        }
    }

    public function blogCategories($type = "", $limit = "") {

        if ($limit != "") {

            $this->CI->db->limit($limit);
        }

        if ($type != "") {

            $this->CI->db->where(array('type' => $type));
        }



        $this->CI->db->from('tbl_blog_category');

        $query = $this->CI->db->get();

        if ($query->num_rows > 0) {

            return $query->result();
        } else {

            return false;
        }
    }

    public function get_seo($page) {

        $this->CI->db->where(array('page' => $page));

        $this->CI->db->from('tbl_seo');

        $query = $this->CI->db->get();

        if ($query->num_rows > 0) {

            return $query->row();
        } else {

            return false;
        }
    }

    public function get_author_by_id($author_id, $select) {

        $this->CI->db->select($select);

        $this->CI->db->from('tbl_author');

        $this->CI->db->where('id', $author_id);

        $query = $this->CI->db->get();

        if ($query->num_rows > 0) {

            return $query->row()->$select;
        } else {

            return false;
        }
    }

    public function get_filename_without_ext($filename, $section, $type) {

        $row = false;

        $this->CI->db->limit(1);

        $this->CI->db->from('tbl_thumbnail');

        $this->CI->db->where('manager', $section);

        $this->CI->db->where('type', $type);

        $query = $this->CI->db->get();

        if ($query->num_rows > 0) {

            $row = $query->row();
        }

        if ($row) {

            $imagearr = explode(".", $filename);
            //print_r($imagearr);die;
            $ext = end($imagearr);
            $info = pathinfo($filename);
            $file_name = basename($filename, '.' . $info['extension']);

            $image = $file_name . '_' . $row->type . '_' . $row->width . '_' . $row->height . "." . $ext;
            // print_r($image);die;
            return $image;
        }
    }
	
	

    /** return the slider model
     * get the day and time
     */
    public function get_slider_image_by_day($day, $time, $type) {
        $this->CI->db->select('slider.*');
        $this->CI->db->where('day.day_no', $day);
        $this->CI->db->where('day.time', $time);
        $this->CI->db->where('slider.type', $type);
        $this->CI->db->join('tbl_day_slider as day_slider', 'slider.id=day_slider.slider_id');
        $this->CI->db->join('tbl_days as day', 'day.id=day_slider.day_id');
        $this->CI->db->from('tbl_slider as slider');
        $query = $this->CI->db->get();
        return $query->num_rows > 0 ? $query->row() : false;
    }
	
	
	
	 public function get_pagination_classes() {
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '<li>';
        $config['next_link'] = '&raquo;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last &raquo;';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        return $config;
    }

    /** for image resizing 
    * by Shreyata **/
    public function get_resized_width_height($width, $height, $resize_item) {
        if (is_array($resize_item)) {
            if (($resize_item["crop"] == "true")) {
                list($resize_width, $resize_height) = self::get_precrop_size($width, $height, $resize_item["width"], $resize_item["height"]);
            } else {
                if ($width > $resize_item["width"] || $height > $resize_item["height"]) {
                    if ($width > $height) {
                        $resize_width = $resize_item["width"];
                        $resize_height = (($resize_item["width"] / $width) * $height);
                    } else {
                        $resize_height = $resize_item["height"];
                        $resize_width = (($resize_item["height"] / $height) * $width);
                    }
                } else {
                    $resize_width = $width;
                    $resize_height = $height;
                }
            }
            return array($resize_width, $resize_height);
        }
    }
    
    public function get_precrop_size($width, $height, $resize_width, $resize_height){
        if($width<$height){
            $new_width = $resize_width;
            $new_height = ($new_width/$width)*$height;
            if($new_height > $height){
                $new_height = $height;
                $new_width = ($new_height/$height)*$width;
            }            
        }
        else if($width == $height){
            if($resize_width>$resize_height){
                $new_width = $resize_width;
                $new_height = ($new_width/$width)*$height;
            }
            else{
                $new_height = $resize_height;
                $new_width = ($new_height/$height)*$width;
            }
        }
        else{
            $new_height = $resize_height;
            $new_width = ($new_height/$height)*$width;
            if($new_width < $resize_width){
                $new_height  = ($resize_width/$new_width)*$new_height;
                $new_width = $resize_width;
            }
        }
        return array($new_width, $new_height);
    }

    public function get_all_us_states(){
        return array('Alabama','Alaska','American Samoa','Arizona','Arkansas','California','Colorado','Connecticut','Delaware',
            'District Of Columbia','Federated States Of Micronesia','Florida','Georgia','Guam','Hawaii','Idaho','Illinois',
            'Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Marshall Islands','Maryland','Massachusetts',
            'Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey',
            'New Mexico','New York','North Carolina','North Dakota','Northern Mariana Islands','Ohio','Oklahoma','Oregon',
            'Palau','Pennsylvania','Puerto Rico','Rhode Island','South Carolina','South Dakota','Tennessee','Texas',
            'Utah','Vermont','Virgin Islands','Virginia','Washington','West Virginia','Wisconsin','Wyoming');
    }

}

?>