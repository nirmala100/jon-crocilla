<?php

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('admin/authorization_model');
        $this->load->library('misc_library');
        $this->load->model('crud_model');
        $this->load->library('email');
        if ($this->uri->segment(2) != 'login' && $this->uri->segment(2) != 'forgot-password') {
            if (!$this->misc_library->is_logged()) {
                redirect('admin/login');
            }
        }
    }

    public function is_logged() {

        if ($this->session->userdata('is_logged'))
            return true;
        else
            return false;
    }

    public function index() {
        if (!$this->is_logged()) {
            redirect('admin/login');
        } else {
            redirect('admin/dashboard');
        }
    }

    public function login() {
        if ($this->is_logged()) {
            redirect('admin/dashboard');
        }
        if ($this->input->post('login')) {
            if ($this->authorization_model->check_if_user_exists()) {
                if ($this->authorization_model->set_session())
                    redirect('admin/dashboard');
            } else {
                //echo 'Login Unsuccessful.';
                $data['msg'] = 'Login Unsuccessful.';
            }
        }
        $data['title'] = 'Jon Crocilla';

        $data['template'] = 'login_view';
        $this->load->view('template_view', $data);
    }

    public function dashboard() {
        $data['title'] = 'Dashboard';
        $this->load->library('ckeditor');
        $this->load->library('ckFinder');
        $this->ckeditor->basePath = base_url() . 'assets/ckeditor/';
        $this->ckeditor->config['language'] = 'en';
        $this->ckeditor->config['width'] = '100%';
        $this->ckeditor->config['height'] = '100px';
        //configure ckfinder with ckeditor config
        $this->ckfinder->SetupCKEditor($this->ckeditor, '../../assets/ckfinder');
        $web_info = $this->crud_model->select_where_in('value, slug', 'tbl_setting', 'slug', array('email', 'website-title', 'site-name'));

        foreach ($web_info as $web) {
            $data[str_replace('-', '', $web->slug)] = $web->value;
        }

        $data['generic'] = $this->crud_model->select_row('*', 'tbl_seo', array('id' => 1));
        $data['property'] = $this->crud_model->select('*', 'tbl_property');


        $data['template'] = 'admin/dashboard_view';
        $this->load->view('template_view', $data);
    }

    public function postWebInformation() {
        if ($this->input->post()) {
            foreach ($this->input->post() as $key => $post) {
                $this->crud_model->update('tbl_setting', array('value' => $post), array('slug' => $key));
            }
            $this->session->set_flashdata('msg', 'Successfully Updated.');
            redirect('admin');
        } else {
            show_404();
        }
    }

    public function logout() {
        $sess_arr = array('is_logged' => '');
        $this->session->unset_userdata($sess_arr);
        redirect('admin/login');
    }

    public function forgot_password() {
        if ($this->misc_library->is_logged())
            redirect('admin/dashboard');
        if ($this->input->post('submit')) {
            $email = $this->input->post('email');
            $setting_email = $this->misc_library->setting_links('email', 'value');

            if ($email === $setting_email) {

                $admin = $this->crud_model->select_row("*", "tbl_admin", array('id' => 1));
                if ($admin) {
                    $values['username'] = $admin->username;
                }
                $rand_string = trim(substr(uniqid(), 5, 5));
                $values['newpassword'] = $rand_string;
                $message = $this->load->view('email/forgot_message', $values, true);
                $send_to = $this->misc_library->setting_links('email', 'value');
                $subject = "New Password for Jon Crocilla Admin";
                $config['mailtype'] = 'html';
                $config['charset'] = 'utf-8';
                $config['wordwrap'] = TRUE;


                $this->email->initialize($config); //print_r($config);die;
                $this->email->set_mailtype("html");

                $this->email->from($send_to);
                $this->email->to($email);

                $this->email->subject($subject);
                $this->email->message($message);

                if ($this->email->send()):
                    $this->crud_model->update("tbl_admin", array('password' => sha1($rand_string)), array('id' => 1));
                    $this->session->set_flashdata('msg', 'An email with your new password is sent to the email.');
                else:

                    // show_error($this->email->print_debugger());
                    $this->session->set_flashdata('error', 'Something went wrong!');
                endif;
            }else {

                $this->session->set_flashdata('msg', 'The email does not exist in our database!');
            }
            // redirect('admin/forgot-password');
        }
        $data['title'] = "Forgot Password";
        $data['template'] = 'admin/forgot_password_view';
        $this->load->view('template_view', $data);
    }
   
    
    
    
}

?>