<?php

class Thumbnail extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->library('misc_library');
        $this->load->model('crud_model');
        $this->load->model('admin/thumbnail_model');
        $this->load->library('form_validation');
        $this->load->helper('form');

        if (!$this->misc_library->is_logged()) {

            redirect('admin/login');
        }
    }

    public function index() {
        $data['title'] = "Thumbnail";
        $data['distinct_info'] = $this->thumbnail_model->thumb_distinct('manager', 'tbl_thumbnail', '');
        $data['template'] = 'admin/thumbnail/thumbnail_list_view';
        $this->load->view('template_view', $data);
    }

    public function add() {
        $data['title'] = "Add Thumbnail";
        if ($this->input->post('btn_thumbnail')) {
            $width_array = $this->input->post('width');
            $height_array = $this->input->post('height');
            $type_array = $this->input->post('type');
            $manager = $this->input->post('manager');
            foreach ($width_array as $width):
                $height = each($height_array);
                $type = each($type_array);
                $form_data = array(
                    'manager' => $manager,
                    'width' => $width,
                    'height' => $height['value'],
                    'type' => $type['value']
                );
                $this->crud_model->insert('tbl_thumbnail', $form_data);
            endforeach;
            $this->session->set_flashdata('msg', 'Successfully Added');
            redirect('admin/thumbnail');
        }
        $data['template'] = 'admin/thumbnail/form_view';
        $this->load->view('template_view', $data);
    }

    public function edit() {
        $data['title'] = "Edit Thumbnail";
        $manager_get = $this->input->post('manager');
        $data['thumbnail_edit'] = $this->crud_model->select('*', 'tbl_thumbnail', array('manager' => $manager_get));
        $data['thumbnail_manager'] = $this->crud_model->select_row('manager', 'tbl_thumbnail', array('manager' => $manager_get));
        if ($this->input->post('btn_thumbnail')) {
            $width_array = $this->input->post('width');
            $height_array = $this->input->post('height');
            $type_array = $this->input->post('type');
            $manager = $this->input->post('manager');
            foreach ($data['thumbnail_edit'] as $edit):
                $width = each($width_array);
                $height = each($height_array);
                $type = each($type_array);
                $form_data = array(
                    'manager' => $manager,
                    'width' => $width['value'],
                    'height' => $height['value'],
                    'type' => $type['value']
                );
                $this->crud_model->update('tbl_thumbnail', $form_data, array('id' => $edit->id));
            endforeach;
            $this->session->set_flashdata('msg', 'Successfully Updated');
            redirect('admin/thumbnail');
        }
        $data['template'] = 'admin/thumbnail/form_view';
        $this->load->view('template_view', $data);
    }

    public function delete() {
        $id = $this->input->post('id');
        $get_type_manager = $this->crud_model->select_row('*', 'tbl_thumbnail', array('id' => $id));
        if ($this->crud_model->delete('tbl_thumbnail', array('id' => $id))) {
            $this->session->set_flashdata('msg', 'Successfully deleted  "' . $get_type_manager->manager . ' " Manager  ' . $get_type_manager->type . ' Size');
            redirect('admin/thumbnail');
        }
    }

}

?>