<?php

class Testimonial extends CI_controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('misc_library');
        $this->load->model('crud_model');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('ckeditor');
        $this->load->library('ckFinder');
        $this->ckeditor->basePath = base_url() . 'assets/ckeditor/';
       // $this->ckeditor->config['toolbar'] = array(
          //  array('Source'),
         //   array('Bold', 'Italic', 'Underline', 'Strike', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Link', 'Unlink', 'Anchor'),
          //  array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock')
      //  );
        //$this->ckeditor->config['toolbar'] = 'Full';
        $this->ckeditor->config['language'] = 'en';
        $this->ckeditor->config['width'] = '100%';
        $this->ckeditor->config['height'] = '100%';
        //configure ckfinder with ckeditor config
        $this->ckfinder->SetupCKEditor($this->ckeditor, '../../assets/ckfinder');
    }

    public function index() {

        if ($this->input->post('id')) {

            $data['testimonial_row'] = $this->crud_model->select_row('*', 'tbl_testimonials', array('id' => $this->input->post('id')));

            if ($data['testimonial_row'] == false) {

                redirect('admin/testimonial/testimonial_view');
            }
        }


        $data['testimonial'] = $this->crud_model->select('*', 'tbl_testimonials');

        $data['title'] = 'Testimonial';

        $data['template'] = 'admin/testimonial/testimonial_view';

        $this->load->view('template_view', $data);
    }

     public function post_testimonial() {
        if ($this->input->post()) {
            $post = $this->input->post();
            $config = array(
                array(
                    'field' => 'author',
                    'label' => 'Author',
                    'rules' => 'required'
                ),
                array(
                    'field' => 't_description',
                    'label' => 'Description',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'address',
                    'label' => 'Address',
                    'rules' => ''
                ),
                array(
                    'field' => 'address2',
                    'label' => 'Adress2',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'is_active',
                    'label' => 'Status',
                    'rules' => ''
                )
            );


            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE) {

                $this->session->set_flashdata('error', validation_errors());

                redirect('admin/testimonial');
            } else {

                $args = array(
                    'author' => $post['author'],
                    't_description' => $post['t_description'],
                   
                    'address2' => $post['address2'],
                    'is_active'=>$post['is_active']
                );
                if ($post['id'] != '') {
                   
                    $this->crud_model->update('tbl_testimonials', $args, array('id' => $post['id']));

                    $this->session->set_flashdata('msg', 'Successfully Updated');
                } else {

                    $this->crud_model->insert('tbl_testimonials', $args);

                    $this->session->set_flashdata('msg', 'Successfully Added');
                }
                redirect('admin/testimonial');
            }
        } else {

            show_404();
        }
    }

    public function delete() {
        $id = $this->input->post('id');

        if ($this->crud_model->delete('tbl_testimonials', array('id' => $id))) {
            $this->session->set_flashdata('msg', 'Successfully Deleted');
            redirect('admin/testimonial');
        } else {
            $this->session->set_flashdata('error', 'Cannot delete ');
            redirect('admin/testimonial');
        }
    }

    

}

?>