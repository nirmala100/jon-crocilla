<?php

class Seo extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('crud_model');
        $this->load->library('misc_library');
    }

    public function index() {
        $data['title'] = "SEO Manager";
        $data['seo_info'] = $this->crud_model->select('*', 'tbl_seo');
        $data['template'] = 'admin/seo/seo_list_view';
        $this->load->view('template_view', $data);
    }

    public function add_edit() {
        $current_date = date('Y-m-d H:i:s');
        $id = $this->input->post('id');

        $data['seo_info'] = $this->crud_model->select('*', 'tbl_seo');

        if ($id != '') {
            $data['get_seo'] = $this->crud_model->select_row('*', 'tbl_seo', array('id' => $id));
        }

        if ($this->input->post() && !$this->input->post('edit')) {
            $config = array(
                array(
                    'field' => 'page',
                    'label' => 'Page Name',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'meta_title',
                    'label' => 'Meta Title',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'meta_keyword',
                    'label' => 'Meta Keyword',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'meta_description',
                    'label' => 'Meta Description',
                    'rules' => 'required'
                )
            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('msg', validation_errors());
                redirect('admin/seo');
            } else {
                $form_data = $this->input->post();
                foreach ($form_data as $key => $val):
                    if ($key != 'btn_seo') {
                        $basic_args[$key] = $val;
                    }
                endforeach;

                if ($id == '') {
                    $basic_args['create_date'] = $current_date;
                    $this->crud_model->insert('tbl_seo', $basic_args);
                    $this->session->set_flashdata('msg', 'Successfully Added');
                    redirect(base_url($this->config->item('backend')) . '/seo');
                } else {
                    $basic_args['update_date'] = $current_date;
                    $this->crud_model->update('tbl_seo', $basic_args, array('id' => $id));
                    $this->session->set_flashdata('msg', 'Sussessfully Updated');
                    redirect(base_url($this->config->item('backend')) . '/seo');
                }
            }
        }
        $data['template'] = 'admin/seo/seo_list_view';
        $this->load->view('template_view', $data);
    }

    public function delete() {
        $id = $this->input->post('id');
        $type = $this->crud_model->select_row('*', 'tbl_seo', array('id' => $id));
        if ($this->crud_model->delete('tbl_seo', array('id' => $id))) {
            $this->session->set_flashdata('msg', 'Successfully Deleted The ');
            redirect('admin/seo');
        } else {
            $this->session->set_flashdata('msg', 'Cannot Deleted.');
            redirect('admin/seo');
        }
    }

    public function updategeneric() {
        if ($this->input->post('savegeneric')) {

            $config = array(
                array(
                    'field' => 'meta_title',
                    'label' => 'Meta Title',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'meta_keyword',
                    'label' => 'Meta Keyword',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'meta_description',
                    'label' => 'Meta Description',
                    'rules' => 'required'
                )
            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('msg', validation_errors());
                redirect('admin/dashboard');
            } else {
                $form_data = $this->input->post();
                foreach ($form_data as $key => $val):
                    if ($key != 'savegeneric') {
                        $basic_args[$key] = $val;
                    }
                endforeach;
                // print_r($basic_args);die;
                $basic_args['update_date'] = date('Y-m-d H:i:s');
                $this->crud_model->update('tbl_seo', $basic_args, array('id' => 1));
                $this->session->set_flashdata('msg', 'Generic SEO Successfully Updated');
                redirect('admin/dashboard');
            }
        }
    }

}
