<?php

class About extends CI_controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("misc_library");
        $this->load->model('crud_model');
        $this->load->helper('form');
        $this->load->library('ckeditor');
        $this->load->library('ckFinder');
        $this->ckeditor->basePath = base_url() . 'assets/ckeditor/';
        $this->ckeditor->config['language'] = 'en';
        $this->ckeditor->config['width'] = '100%';
        $this->ckeditor->config['height'] = '100%';
        
        $this->ckfinder->SetupCKEditor($this->ckeditor, '../../assets/ckfinder');
        if (!$this->misc_library->is_logged()) {

            redirect('admin/login');
        }
    }

    public function basic() {
        $data['title'] = 'About';
        $data['basic_info'] = $this->crud_model->select_row('*', 'tbl_admin');
        $data['template'] = 'admin/about/basic_view';
        $this->load->view('template_view', $data);
    }

    public function postBasicInfo() {
        if ($this->input->post()) {
            foreach ($this->input->post() as $key => $post) {
                      
                if ($key != 'id') {
                 
                    $update_arr[$key] = $post;
                }
                     
            }

            $basic_info = $this->crud_model->select_row('*', 'tbl_admin', array('id' => $this->input->post('id')));

            if ($basic_info) {

                $old_profile = $basic_info->profile_pic;
            }

            if ($_FILES['profile_pic']['name']) {

                $update_arr['profile_pic'] = $this->add_basic_image('profile_pic', 'profile', $old_profile);
            }





            $this->crud_model->update('tbl_admin', $update_arr, array('id' => $this->input->post('id')));

            $this->session->set_flashdata('msg', 'Successfully Updated');

            redirect('admin/about/basic');
        } else {

            show_404();
        }
    }

    private function add_basic_image($image, $manager, $old_filename = '') {

        //for uploading image

        $newname = 'file_' . time();

        $config['allowed_types'] = "gif|png|jpg|jpeg";

        $config['upload_path'] = BASEPATH . '../assets/images/basic/';

        $config['file_name'] = $newname;

        $config['max_size'] = '2048';

        $config['max_width'] = '2024';

        $config['max_height'] = '1068';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($image)) {

            $error = array('error' => $this->upload->display_errors());

            print_r($error);

            die;
        } else {

            $data = $this->upload->data();

            $get_thumb = $this->crud_model->select('*', 'tbl_thumbnail', array('manager' => $manager));

            if ($get_thumb) {

                $ext = '';

                $img_filename = '';

                if ($data['file_name'] . $data['file_ext'] != $old_filename && $old_filename != '') {

                    list($img_filename, $ext) = explode(".", $old_filename);

                    @unlink($config['upload_path'] . $old_filename);
                }

                $this->load->library('image_lib');

                foreach ($get_thumb as $thumb):

                    $new_image = $data['file_name'] . '_' . $thumb->type . '_' . $thumb->width . '_' . $thumb->height . $data['file_ext'];

                    $old_image = $old_filename . '_' . $thumb->type . '_' . $thumb->width . '_' . $thumb->height . "." . $ext;

                    $config = array(
                        // 'image_library'=> 'gd2',

                        'source_image' => $data['full_path'],
                        // 'maintain_ration' => true,
                        // 'create_thumb'=> true,
                        'width' => $thumb->width,
                        'height' => $thumb->height,
                        'new_image' => $new_image,
                        'quality'=>'100%',
                    );

                    $this->image_lib->initialize($config);

                    $this->image_lib->fit();

                    if ($new_image != $old_image && $old_image != '' && file_exists(BASEPATH . '../assets/images/basic/' . $old_image)) {

                        @unlink(BASEPATH . '../assets/images/basic/' . $old_image);
                    }

                endforeach;
            }

            return $data['file_name'];
        }
    }

    public function whyChooseMe() {
         $data['title'] = 'Why Choose Me?';
         $data['why_choose_me'] = $this->crud_model->select('*', 'tbl_why_me');
         $data['template'] = 'admin/about/why_to_choose_me_view';

        $this->load->view('template_view', $data);
    }

    public function postWhyChooseMe() {
           if ($this->input->post()) {
            foreach ($this->input->post() as $key => $post) {

                if ($key != 'id') {
                    if(!strncmp($key, 'description', 11)){
                        $key = 'description';
                    }
                    $update_arr[$key] = $post;
                }
            }
             if($this->crud_model->update('tbl_why_me', $update_arr, array('id' => $this->input->post('id')))){
                 $this->session->set_flashdata('msg', 'Successfully Updated');
             }

             else{
                  $this->session->set_flashdata('error', 'Couldnot update.');
             }


            
            redirect('admin/about/why-choose-me');
        }
    }

}

?>