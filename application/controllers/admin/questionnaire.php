<?php

class Questionnaire extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("misc_library");
        $this->load->model('crud_model');
        $this->load->helper('url');
        $this->load->helper('misc_helper');

        if (!$this->misc_library->is_logged()) {

            redirect('admin/login');
        }
    }

    public function index() {
        $data['title'] = 'Questionnaire';

        $data['questionnaires'] = $this->crud_model->select('*', 'tbl_question');

        if (!empty($data['questionnaires'])):
            foreach ($data['questionnaires'] as $ques):
                $ques->answers = $this->crud_model->select('answer', 'tbl_answer', array('question_id' => $ques->id));
            endforeach;
        endif;

        $data['template'] = 'admin/questionnaire/questionnaire_list_view';
        $this->load->view('template_view', $data);
    }

    public function add() {
        if ($this->session->userdata('questionnaire_id')) {
            $this->session->unset_userdata('questionnaire_id');
        }
        redirect('admin/questionnaire/add-edit');
    }

    public function add_edit() {
        $this->load->library('form_validation');
        if ($this->input->post()) {
            $id = $this->input->post('id');
            $this->session->set_userdata('questionnaire_id', $id);
        } else if ($this->session->userdata('questionnaire_id')) {
            $id = $this->session->userdata('questionnaire_id');
            //$this->session->unset_userdata('questionnaire_id');        
        } else {
            $id = '';
        }


        if (empty($id)) {
            $data['title'] = 'Add New Questionnaire';
        } else {
            $data['question'] = $this->crud_model->select_row('*', 'tbl_question', array('id' => $id));
            $data['answers'] = $this->crud_model->select('*', 'tbl_answer', array('question_id' => $id));
            $edit = $data['answers'];
            $data['title'] = 'Edit Questionnaire';
        }

        if ($this->input->post('btn_questionnaire')) {
            $form_data = $this->input->post();
            $config = array(
                array(
                    'field' => 'question',
                    'label' => 'Question',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'answer[]',
                    'lable' => 'Answer',
                    'rules' => 'required'
                )
            );


            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error', validation_errors());
                redirect('admin/questionnaire/add-edit');
            } else {

                $form_question_data = array(
                    'question' => $this->input->post('question'),
                    'short_question' => $this->input->post('short_question'),
                    'is_active' => $this->input->post('is_active')
                );

                $answers = $this->input->post('answer');
                $answers_new = $this->input->post('answer_new');

                if (empty($id)) { //to add questionnaire
                    if ($qid = $this->crud_model->insert('tbl_question', $form_question_data)) {

                        //add answer of non generated field
                        $form_answer_data = array(
                            'answer' => $answers[0],
                            'question_id' => $qid
                        );

                        $this->crud_model->insert('tbl_answer', $form_answer_data);

                        //add answer of generated field
                        if (!empty($answers_new)):
                            foreach ($answers_new as $ans):
                                $form_answer_data = array(
                                    'answer' => $ans,
                                    'question_id' => $qid
                                );

                                $this->crud_model->insert('tbl_answer', $form_answer_data);
                            endforeach;
                        endif;

                        $this->session->set_flashdata('msg', 'Successfully Added');
                        $this->session->unset_userdata('questionnaire_id');
                        redirect('admin/questionnaire');
                    }
                    else {
                        $this->session->set_flashdata('error', 'Error adding questionnaire, try again!');
                        redirect('admin/questionnaire/add-edit');
                    }
                } else { //to edit questionnaire				
                    if ($this->crud_model->update('tbl_question', $form_question_data, array('id' => $id))) {

                        if (!empty($edit)) {
                            //update questionnaires answers
                            foreach ($edit as $e):
                                $ans = each($answers);

                                $form_answer_data = array(
                                    'answer' => $ans['value'],
                                    'question_id' => $id
                                );

                                $this->crud_model->update('tbl_answer', $form_answer_data, array('id' => $e->id));
                            endforeach;
                        }

                        //add newly generated questionnaire answers
                        if (!empty($answers_new)):
                            foreach ($answers_new as $ans):
                                $form_answer_data = array(
                                    'answer' => $ans,
                                    'question_id' => $id
                                );

                                $this->crud_model->insert('tbl_answer', $form_answer_data);
                            endforeach;
                        endif;

                        $this->session->set_flashdata('msg', 'Successfully Updated');
                        $this->session->unset_userdata('questionnaire_id');
                        redirect('admin/questionnaire');
                    }
                    else {
                        $this->session->set_flashdata('error', 'Error updating questionnaire, try again!');
                        redirect('admin/questionnaire/add-edit');
                    }
                }
            }
        }

        $data['template'] = 'admin/questionnaire/form_view';
        $this->load->view('template_view', $data);
    }

    public function delete() {
        if ($this->input->post()) {
            $id = $this->input->post('id');

            if ($this->crud_model->delete('tbl_question', array('id' => $id))) {
                $this->crud_model->delete('tbl_answer', array('question_id' => $id));
                $this->session->set_flashdata('msg', 'Successfully deleted.');
                return redirect('admin/questionnaire');
            }
        }

        $this->session->set_flashdata('error', 'Cannot deleted');
        return redirect('admin/questionnaire');
    }

    public function delete_ans() {
        if ($this->input->post()) {
            $id = $this->input->post('id');

            if ($this->crud_model->delete('tbl_answer', array('id' => $id))) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function change_status_for() {

        if ($this->crud_model->change_status('tbl_question')) {
            echo 1;
        } else {
            echo 0;
        }
    }

}
