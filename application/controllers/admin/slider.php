<?php

class Slider extends CI_controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('upload');
        $this->load->library('pagination');
        $this->load->library('misc_library');
        $this->load->model('crud_model');
        $this->load->model('main_model');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('MY_Upload');
        $this->load->library('image_lib');
        if (!$this->misc_library->is_logged()) {
            redirect('admin/login');
        }
    }

    public function add_edit() {
        $data['title'] = 'Add slider';
        $data['template'] = 'admin/slider/form_views';
        $this->load->view('template_view', $data);
    }

    public function delete_image() {
        $data = $this->input->post('image_name');
        $filename = BASEPATH . '../assets/images/slider/' . $data;
        if (file_exists($filename)) {
            @unlink($filename);
            echo $data;
            // echo "The file $filename exists";
        } else {
            echo 0;
        }
    }

    public function add() {
        $basic_args = array();
        $images = $this->input->post('imagecap');
        $upload = explode(",", $images);
        $image = '';
        foreach ($upload as $upl):
            $get_thumb = $this->crud_model->select('*', 'tbl_thumbnail', array('manager' => 'slider'));
            if ($get_thumb) {
                $ext = '';
                $img_filename = '';
                if ($upl != $image && $image != '') {
                    list($img_filename, $ext) = explode(".", $image);
                    @unlink($config['upload_path'] . $image);
                }
                $this->load->library('image_lib');
                $image_info = explode('.', $upl);
                $img_upl = $image_info[0];
                $ext_upl = $image_info[1];
                list($width, $height, $type, $attr) = getimagesize(BASEPATH . '../assets/images/slider/' . $upl);
                foreach ($get_thumb as $thumb):
                    $new_image = $img_upl . '_' . $thumb->type . '_' . $thumb->width . '_' . $thumb->height . "." . $ext_upl;
                    $old_image = $img_upl . '_' . $thumb->type . '_' . $width . '_' . $height . "." . $ext_upl;

                    $config = array(
                        'quality' => '100%',
                        'source_image' => BASEPATH . '../assets/images/slider/' . $upl,
                        'width' => $thumb->width,
                        'height' => $thumb->height,
                        'new_image' => $new_image,
                        
                    );
                    $this->image_lib->initialize($config);
                    $this->image_lib->fit();
                    if ($new_image != $old_image && $old_image != '' && file_exists(BASEPATH . '../assets/images/slider/' . $old_image)) {
                        @unlink(BASEPATH . '../assets/images/slider/' . $old_image);
                    }
                endforeach;
            }
            $basic_args['image'] = $upl;

            $basic_args['type'] = $this->input->post('slider_type');
            $this->crud_model->insert('tbl_slider', $basic_args);
        endforeach;
        $this->session->set_flashdata('msg', 'Successfully inserted ');
        redirect('admin/slider/add_edit');
    }

    public function do_upload() {

        // Detect form submission.

        if ($this->input->post('submit')) {


            //$path = '../assets/images/slider/';
            $path = BASEPATH . '../assets/images/slider';
            $this->upload->initialize(array(
                "upload_path" => $path,
                "allowed_types" => "gif|jpg|png",
                "max_size" => '2048',
                "max_width" => '1600',
                "max_height" => '1600'
            ));

            if ($this->upload->do_multi_upload("uploadfile")) {
                $upload_data = $this->upload->get_multi_upload_data();
                echo json_encode($upload_data);
            } else {

                // Output the errors
                $errors = array('error' => $this->upload->display_errors(), 'errorstatus' => 'true');
                echo json_encode($errors);

//                foreach ($errors as $k => $error) {
//                    echo $error;
//                }
            }
        } else {
            echo '<p class = "bg-danger">An error occured, please try again later.</p>';
        }
        // Exit to avoid further execution
        exit();
    }

    public function delete() {

        $id = $this->input->post('id');
        $type = $this->crud_model->select_row('*', 'tbl_slider', array('id' => $id));

        $thumb_detail = $this->crud_model->select('*', 'tbl_thumbnail', array('manager' => 'slider'));

        if ($this->crud_model->delete('tbl_slider', array('id' => $id))) {
            if (!empty($type->image)):

                $file_name = explode('.', $type->image);
                $get_ext = $file_name[1];

                $this->crud_model->img_delete($thumb_detail, $type->image, 'slider', $get_ext);
            endif;
            $this->session->set_flashdata('msg', 'Successfully deleted ');
            redirect('admin/slider/about-slider');
        } else {
            $this->session->set_flashdata('error', 'Cannot Deleted ');
            redirect('admin/slider/about-slider');
        }
    }

    public function about_slider() {
        $data['title'] = 'About Slider List';
        $data['slider_row'] = $this->crud_model->select('*', 'tbl_slider', array('type' => 'about'));

        $data['template'] = ('admin/slider/display_image_view');
        $this->load->view('template_view', $data);
    }

    public function home_slider() {
        $data['title'] = 'Home Slider List';
        $data['slider_row'] = $this->crud_model->select('*', 'tbl_slider', array('type' => 'home'));
        $data['days'] = $this->main_model->get_all_day_no();

        $data['template'] = ('admin/slider/home_slider_view');
        $this->load->view('template_view', $data);
    }

    public function post_home_slider() {
        if ($this->input->post()) {
            $sliders = $this->input->post('sliders');
            //echo "<pre>";print_r($_FILES);
            $error = '';
            if ($sliders) {
                $files = $_FILES;
                foreach ($sliders as $key => $slider) {
                   // echo $slider['id']." ".$slider['time']." ".$_FILES['sliders']['name'][$key]['image']."<br>";die;
                    $id = $slider['id'];
                    $old_image = '';
                    $slide = $this->crud_model->select_row('*', 'tbl_slider', array('id' => $id));
                    if ($slide) {
                        $old_image = $slide->image;
                    }
                    $model_arr = array('type' => 'home');
                    if ($filename = $files['sliders']['name'][$key]['image']) {
                        $_FILES['image']['name'] = $files['sliders']['name'][$key]['image'];
                        $_FILES['image']['type'] = $files['sliders']['type'][$key]['image'];
                        $_FILES['image']['tmp_name'] = $files['sliders']['tmp_name'][$key]['image'];
                        $_FILES['image']['error'] = $files['sliders']['error'][$key]['image'];
                        $_FILES['image']['size'] = $files['sliders']['size'][$key]['image'];
                        $result = $this->add_home_image($filename, 'image', 'slider', $old_image);
                        if (is_array($result)) {
                            $error = $filename . "-->" . implode(',', $result);
                            $filename = '';
                        } else {
                            $model_arr['image'] = $result;
                        }
                    }
                    if ($id && $slide) {
                        $this->crud_model->update('tbl_slider', $model_arr, array('id' => $id));
                    } else {
                        if ($filename) {
                            $timearr = array('day_no' => $slider['day_no'],
                                'time' => $slider['time']);
                            $day_time = $this->crud_model->select_row('*', 'tbl_days', $timearr);
                            if ($day_time) {/** insert only if the day is present * */
                                $check = $this->crud_model->select_row('*', 'tbl_day_slider', array('day_id' => $day_time->id));
                                if (!$check) {
                                    $id = $this->crud_model->insert('tbl_slider', $model_arr);
                                    $insert = array('slider_id' => $id, 'day_id' => $day_time->id);
                                    $this->crud_model->insert('tbl_day_slider', $insert);
                                }
                            }
                        }
                    }
                }
            }

            if ($error) {
                $this->session->set_flashdata('msg', 'Error:' . $error);
            } else {
                $this->session->set_flashdata('msg', 'Home slider successfully updated! ');
            }

            redirect('admin/slider/home-slider');
        } else {

            show_404();
        }
    }

    private function add_home_image($filename, $image, $manager, $old_filename = '') {

        $filearr = explode(".", $filename);
        $filename = $filearr[0];
        if ($filename && file_exists(BASEPATH . '../assets/images/slider/' . $filename)) {
            $newname = $filename . "_" . rand(100, 999);
        } else {
            $newname = $filename;
        }
        $this->load->library('upload');
        $config['allowed_types'] = "gif|png|jpg|jpeg";
        $config['upload_path'] = BASEPATH . '../assets/images/slider/';
        $config['file_name'] = $newname;
        $config['max_size'] = '204800';
        $config['max_width'] = '3000';
        $config['max_height'] = '3000';
        $this->upload->initialize($config);

        if (!$this->upload->do_upload($image)) {

            $error = array('error' => $this->upload->display_errors());
            return $error;
        } else {
            $data = $this->upload->data();
            $get_thumb = $this->crud_model->select('*', 'tbl_thumbnail', array('manager' => $manager));
            if ($get_thumb) {
                $ext = '';
                $img_filename = '';
                if ($data['file_name'] . $data['file_ext'] != $old_filename && $old_filename != '') {
                    list($img_filename, $ext) = explode(".", $old_filename);
                    @unlink($config['upload_path'] . $old_filename);
                }
                $this->load->library('image_lib');
                foreach ($get_thumb as $thumb):
                    $new_image = $data['file_name'] . '_' . $thumb->type . '_' . $thumb->width . '_' . $thumb->height . $data['file_ext'];
                    $old_image = $old_filename . '_' . $thumb->type . '_' . $thumb->width . '_' . $thumb->height . "." . $ext;
                    $config = array(
                        // 'image_library'=> 'gd2',

                        'source_image' => $data['full_path'],
                        // 'maintain_ration' => true,
                        // 'create_thumb'=> true,
                        'width' => $thumb->width,
                        'height' => $thumb->height,
                        'new_image' => $new_image
                    );

                    $this->image_lib->initialize($config);

                    $this->image_lib->fit();

                    if ($new_image != $old_image && $old_image != '' && file_exists(BASEPATH . '../assets/images/slider/' . $old_image)) {

                        @unlink(BASEPATH . '../assets/images/slider/' . $old_image);
                    }

                endforeach;
            }

            return $data['file_name'];
        }
    }

    public function detail_slider() {
        $data['title'] = 'Detail Slider List';
        $data['slider_row'] = $this->crud_model->select('*', 'tbl_slider', array('type' => 'listing'));
        $data['template'] = ('admin/slider/detail_slider_view');
        $this->load->view('template_view', $data);
    }

    public function change_status() {
        $status = $this->crud_model->status('tbl_slider');
        echo $status;
    }

}
