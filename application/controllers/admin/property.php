<?php

class Property extends CI_controller {

    public function __construct() {

        parent::__construct();
        $this->load->library("pagination");
        $this->load->library("misc_library");
        $this->load->library('form_validation');
        $this->load->model('crud_model');
        $this->load->helper('url');
        $this->load->helper('misc_helper');

        if (!$this->misc_library->is_logged()) {

            redirect('admin/login');
        }
    }

    public function index() {
        $data['title'] = 'Property ';
        $data["all_property"] = $this->crud_model->select(array("id", "p_name", "p_location", "p_image", "is_active","city"), "tbl_property");
        $data['template'] = 'admin/property/property_list_view';
        $this->load->view('template_view', $data);
    }

    public function add_edit($feature='') {
        
        $id = $this->input->post('id');
        if (!$id) {
            $data['title'] = 'Add Property';
        } else {
            $data['title'] = 'Edit Property';
        }


        $this->load->library('ckeditor');
        $this->load->library('ckFinder');
        $this->ckeditor->basePath = base_url() . 'assets/ckeditor/';
        $this->ckeditor->config['language'] = 'en';
        $this->ckeditor->config['width'] = '100%';
        $this->ckeditor->config['height'] = '100%';
        //configure ckfinder with ckeditor config
        $this->ckfinder->SetupCKEditor($this->ckeditor, '../../assets/ckfinder');
        $id = $this->input->post('id');

        $data['property_info'] = $this->crud_model->select_row('*', 'tbl_property', array('id' => $id));
        $type = 0;
        $image = '';


        $basic_args = array();
        if ($this->input->post('btn_property')) {
            $data['title'] = 'Edit property';
            $form_data = $this->input->post();
            $config = array(
                array(
                    'field' => 'p_name',
                    'label' => 'Title',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'slug',
                    'label' => 'Slug',
                    'rules' => ''
                ),
                array(
                    'field' => 'p_location',
                    'label' => 'Location',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'city',
                    'label' => 'City',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'short_desc',
                    'label' => ' Short Description',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'p_description',
                    'label' => 'Description',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'p_featured',
                    'label' => 'Featured',
                    'rules' => ''
                ),
                array(
                    'field' => 'p_image',
                    'label' => 'Image',
                    'rules' => ''
                ),
                array(
                    'field' => 'meta_title',
                    'label' => 'Meta Title',
                    'rules' => 'max_length[55]'
                ),
                array(
                    'field' => 'meta_keyword',
                    'label' => 'Meta Keyword',
                    'rules' => ''
                ),
                array(
                    'field' => 'meta_description',
                    'label' => 'Meta Description',
                    'rules' => 'max_length[115]'
                ),
                array(
                    'field' => 'is_active',
                    'label' => 'Status',
                    'rules' => ''
                ),
                array(
                    'field' => 'id',
                    'label' => 'id',
                    'rules' => ''
                ),
                 array(
                    'field' => 'lat',
                    'label' => 'Latitude',
                    'rules' => ''
                ),
                  array(
                    'field' => 'long',
                    'label' => 'Longitude',
                    'rules' => ''
                ),
            );
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == FALSE) {
                $data['error_msg'] = validation_errors();
                $data['template'] = 'admin/property/form_view';
                $this->load->view('template_view', $data);return;
            } else {
                foreach ($form_data as $key => $value):

                    if ($key != 'btn_property') {
                        $basic_args[$key] = $value;
                    }
                    if (!$this->input->post('p_featured')) {
                        $basic_args['p_featured'] = 0;
                    }
                endforeach;
                $slug = ($form_data['slug'] == "" ? $form_data['p_name'] : $form_data['slug']);
                $basic_args['slug'] = $this->misc_library->check_if_slug_exists(slugify($slug), 'tbl_property', $id);
                if ($_FILES['p_image']['name']) {

                    //for uploading image
                    $newname = 'file_' . time();
                    $config['allowed_types'] = "gif|png|jpg|jpeg";
                    $config['upload_path'] = BASEPATH . '../assets/images/property/';
                    $config['file_name'] = $newname;
                    $config['max_size'] = '204820482048';
                    $config['max_width'] = '3000';
                    $config['max_height'] = '3000';
                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('p_image')) {
                        $data['error_msg'] = array('error' => $this->upload->display_errors());
                        $data['template'] = 'admin/property/form_view';
                        $this->load->view('template_view', $data);return;
                    } else {
                        $data = $this->upload->data();

                        $get_thumb = $this->crud_model->select('*', 'tbl_thumbnail', array('manager' => 'property', 'type !=' => 'detailslider', 'type !=' => 'detaillisting'));

                        if ($get_thumb) {
                            $ext = '';
                            $img_filename = '';
                            if ($data['file_name'] . $data['file_ext'] != $image && $image != '') {
                                list($img_filename, $ext) = explode(".", $image);
                                @unlink($config['upload_path'] . $image);
                            }
                            $this->load->library('image_lib');
                            foreach ($get_thumb as $thumb):

                                $new_image = $data['file_name'] . '_' . $thumb->type . '_' . $thumb->width . '_' . $thumb->height . $data['file_ext'];
                                $old_image = $image . '_' . $thumb->type . '_' . $thumb->width . '_' . $thumb->height . "." . $ext;
                               
                                list($width, $height) = getimagesize($data['full_path']);
                                $resize_detail=(array)$thumb;
                                $resize_detail['crop']="true";
                                list($resize_width, $resize_height) = $this->misc_library->get_resized_width_height($width, $height,$resize_detail);
                                //echo $resize_width." ".$resize_height;die;
                                $config = array(
                                    // 'image_library'=> 'gd2',
                                    'source_image' => $data['full_path'],
                                    'quality'=>'90',
                                    'width' => $resize_width,
                                    'height' => $resize_height,
                                    'new_image' => $new_image
                                );
                                $config['maintain_ratio']=true;
                                
                                $this->image_lib->initialize($config);
                                $this->image_lib->resize();
                                $this->image_lib->clear();
                                /* if($thumb->type=='listing'){
                                list($width, $height) = getimagesize(BASEPATH."/../assets/images/property/".$new_image); echo $new_image.">>".$width." ".$height;
                                } */    
                                $y_axis=0;$x_axis=0;
                                if($resize_width == $thumb->width){
                                    $y_axis = ($resize_height - $thumb->height)/2;
                                }else{
                                    $x_axis = ($resize_width - $thumb->width)/2;
                                }   
                                
                                //crop image to get the exact size   
                                //load image library for cropping
                                $config = array(
                                    'image_library'=> 'gd2',
                                    'source_image' => BASEPATH."/../assets/images/property/".$new_image,
                                    'x_axis' => $x_axis,
                                    'y_axis' => $y_axis,
                                    'width' => $thumb->width,
                                    'height' => $thumb->height,
                                    'quality'=>'90',
                                    'maintain_ratio'=>FALSE
                                    
                                );
                                $this->image_lib->initialize($config);
                                if(!$this->image_lib->crop()){
                                    //echo $this->image_lib->display_errors();
                                }
                                $this->image_lib->clear();

                                /* if($thumb->type=='listing'){
                                    echo "<pre>";print_r($config);
                                    list($w, $h) = getimagesize(BASEPATH."/../assets/images/property/".$new_image); echo $new_image.">>".$w." ".$h;
                                } */    
                                if ($new_image != $old_image && $old_image != '' && file_exists(BASEPATH . '../assets/images/property/' . $old_image)) {
                                    @unlink(BASEPATH . '../assets/images/property/' . $old_image);
                                }
                            endforeach;
                        }
                        $basic_args['p_image'] = $data['file_name'];
                    }
                }
                if ($id == '') {

                    $this->crud_model->insert('tbl_property', $basic_args);
                    $last_inserted_id = mysql_insert_id();
                    $insert_prop = $this->crud_model->select_row('*', 'tbl_property', array('id' => $last_inserted_id));
                    if ($insert_prop->p_featured == 1) {
                        $this->session->set_flashdata('msg', 'Successfully Added Featured Property');
                        redirect(base_url('admin/property/list_featured_property/'));
                    } else {
                        $this->session->set_flashdata('msg', 'Successfully Added');
                        redirect(base_url('admin/property/index/'));
                    }
                } else {

                    $this->crud_model->update('tbl_property', $basic_args, array('id' => $id));
                    $update_prop = $this->crud_model->select_row('*', 'tbl_property', array('id' => $id));
                    if ($update_prop->p_featured == 1) {

                        $this->session->set_flashdata('msg', 'Successfully Updated Featured Property');
                        redirect(base_url('admin/property/list_featured_property/'));
                    } else {

                        $this->session->set_flashdata('msg', 'Successfully Updated ');

                        redirect(base_url('admin/property/index/'));
                    }
                }
            }
        }
        $data['template'] = 'admin/property/form_view';
        $this->load->view('template_view', $data);
    }

    public function delete() {
        $id = $this->input->post('id');

        $property_row = $this->crud_model->select_row('*', 'tbl_property', array('id' => $id));
        $property_images = $this->crud_model->select('*', 'tbl_property_images', array('property_id' => $id));
        $thumb_detail = $this->crud_model->select('*', 'tbl_thumbnail', array('manager' => 'property'));
              if ($this->crud_model->delete('tbl_property', array('id' => $id))) {
            if (!empty($property_row->p_image)):
                $file_name = explode('.', $property_row->p_image);
                $get_ext = $file_name[1];
                $this->crud_model->img_delete($thumb_detail, $property_row->p_image, 'property', $get_ext);
            endif;
            if($property_images){
            foreach ($property_images as $prop):
                if ($this->crud_model->delete('tbl_property_images', array('property_id' => $id))) {


                    if (!empty($prop->image)):
                        $file_name = explode('.', $prop->image);
                        $file = $file_name[0];
                        $get_ext = $file_name[1];
                        $this->crud_model->img_delete($thumb_detail, $file, 'property', $get_ext, true);
                    endif;
                }
            endforeach;
              }
            $this->session->set_flashdata('msg', 'Successfully deleted.');
            redirect('admin/property/index/');
        } else {
            $this->session->set_flashdata('error', 'Cannot deleted');
            redirect('admin/property');
        }
    }

    public function addQuickProperty() {
         //$id = $this->input->post('id');
       // $data['property_info'] = '';
        if ($this->input->post()) {
            $form_data = $this->input->post();
            $config = array(
                array(
                    'field' => 'p_name',
                    'label' => 'Title',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'p_location',
                    'label' => 'Location',
                    'rules' => ''
                ),
                array(
                    'field' => 'p_description',
                    'label' => 'Description',
                    'rules' => ''
                ),
                array(
                    'field' => 'short_desc',
                    'label' => ' Short Description',
                    'rules' => ''
                ),
                array(
                    'field' => 'p_bathroom',
                    'label' => 'Bathroom',
                    'rules' => ''
                ),
                array(
                    'field' => 'p_bedroom',
                    'label' => 'Bedroom',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'p_lot_size',
                    'label' => 'Lot Size',
                    'rules' => 'required'
                ),
                  array(
                    'field' => 'p_image',
                    'label' => 'Image',
                    'rules' => ''
                ),
                array(
                    'field' => 'is_active',
                    'label' => 'Status',
                    'rules' => ''
                ),
                array(
                    'field' => 'id',
                    'label' => 'id',
                    'rules' => ''
                ),
            );
             $image = '';
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error_msg', validation_errors());
                redirect('admin/dashboard');
                return false;
            } else {
                foreach ($form_data as $key => $value):
                    $basic_args[$key] = $value;
                endforeach;
                $slug = ($form_data['slug'] == "" ? $form_data['p_name'] : $form_data['slug']);
                $basic_args['slug'] = $this->misc_library->check_if_slug_exists(slugify($slug), 'tbl_property', $id);
                  if ($_FILES['p_image']['name']) {

                    //for uploading image
                    $newname = 'file_' . time();
                    $config['allowed_types'] = "gif|png|jpg|jpeg";
                    $config['upload_path'] = BASEPATH . '../assets/images/property/';
                    $config['file_name'] = $newname;
                    $config['max_size'] = '20482048';
                    $config['max_width'] = '2048';
                    $config['max_height'] = '1200';
                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('p_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        // print_r($error);
                        // die;

                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        redirect(base_url('admin/dashboard'));
                    } else {
                        $data = $this->upload->data();

                        $get_thumb = $this->crud_model->select('*', 'tbl_thumbnail', array('manager' => 'property', 'type !=' => 'detailslider', 'type !=' => 'detaillisting'));

                        if ($get_thumb) {
                            $ext = '';
                            $img_filename = '';
                            if ($data['file_name'] . $data['file_ext'] != $image && $image != '') {
                                list($img_filename, $ext) = explode(".", $image);
                                @unlink($config['upload_path'] . $image);
                            }
                            $this->load->library('image_lib');
                            foreach ($get_thumb as $thumb):
                                /*$new_image = $data['file_name'] . '_' . $thumb->type . '_' . $thumb->width . '_' . $thumb->height . $data['file_ext'];
                                $old_image = $image . '_' . $thumb->type . '_' . $thumb->width . '_' . $thumb->height . "." . $ext;
                                $config = array(
                                    'source_image' => $data['full_path'],
                                    'width' => $thumb->width,
                                    'height' => $thumb->height,
                                    'new_image' => $new_image
                                );
                                $this->image_lib->initialize($config);
                                $this->image_lib->fit();

                                if ($new_image != $old_image && $old_image != '' && file_exists(BASEPATH . '../assets/images/property/' . $old_image)) {
                                    @unlink(BASEPATH . '../assets/images/property/' . $old_image);
                                }*/


                                $new_image = $data['file_name'] . '_' . $thumb->type . '_' . $thumb->width . '_' . $thumb->height . $data['file_ext'];
                                $old_image = $image . '_' . $thumb->type . '_' . $thumb->width . '_' . $thumb->height . "." . $ext;
                               
                                list($width, $height) = getimagesize($data['full_path']);
                                $resize_detail=(array)$thumb;
                                $resize_detail['crop']="true";
                                list($resize_width, $resize_height) = $this->misc_library->get_resized_width_height($width, $height,$resize_detail);
                                //echo $resize_width." ".$resize_height;die;
                                $config = array(
                                    // 'image_library'=> 'gd2',
                                    'source_image' => $data['full_path'],
                                    'quality'=>'90',
                                    'width' => $resize_width,
                                    'height' => $resize_height,
                                    'new_image' => $new_image
                                );
                                $config['maintain_ratio']=true;
                                
                                $this->image_lib->initialize($config);
                                $this->image_lib->resize();
                                $this->image_lib->clear();
                                /* if($thumb->type=='listing'){
                                list($width, $height) = getimagesize(BASEPATH."/../assets/images/property/".$new_image); echo $new_image.">>".$width." ".$height;
                                } */
                                $y_axis=0;$x_axis=0;
                                if($resize_width == $thumb->width){
                                    $y_axis = ($resize_height - $thumb->height)/2;
                                }else{
                                    $x_axis = ($resize_width - $thumb->width)/2;
                                }    
                                
                                //crop image to get the exact size   
                                //load image library for cropping
                                $config = array(
                                    'image_library'=> 'gd2',
                                    'source_image' => BASEPATH."/../assets/images/property/".$new_image,
                                    'x_axis' => $x_axis,
                                    'y_axis' => $y_axis,
                                    'width' => $thumb->width,
                                    'height' => $thumb->height,
                                    'quality'=>'90',
                                    'maintain_ratio'=>FALSE
                                    
                                );
                                $this->image_lib->initialize($config);
                                if(!$this->image_lib->crop()){
                                    //echo $this->image_lib->display_errors();
                                }
                                $this->image_lib->clear();

                                /* if($thumb->type=='listing'){
                                    echo "<pre>";print_r($config);
                                    list($w, $h) = getimagesize(BASEPATH."/../assets/images/property/".$new_image); echo $new_image.">>".$w." ".$h;
                                } */    
                                if ($new_image != $old_image && $old_image != '' && file_exists(BASEPATH . '../assets/images/property/' . $old_image)) {
                                    @unlink(BASEPATH . '../assets/images/property/' . $old_image);
                                }
                            endforeach;
                        }
                        $basic_args['p_image'] = $data['file_name'];
                    }
                }//print_r($basic_args);die;
                $property_id = $this->crud_model->insert('tbl_property', $basic_args);
                $this->session->set_flashdata('msg', 'Successfully Added');
                redirect(base_url('admin/property/index/'));
            }
        } else {
            show_404();
        }
    }

    public function list_featured_property() {
        $data['title'] = 'Featured Properties';
        $data['list_featured_property'] = $this->crud_model->select('*', 'tbl_property', array('p_featured' => 1));

        $data['template'] = ('admin/property/featured_property_view');
        $this->load->view('template_view', $data);
    }

    public function change_status() {
        $status = $this->crud_model->status('tbl_property');
        echo $status;
    }

    public function property_open($id) {
        $name = $this->crud_model->select_row('p_name', 'tbl_property', array('id' => $id))->p_name;
        $data['title'] = 'Images of ' . $name;
        $data['old_image'] = $this->crud_model->select_by_order('*', 'tbl_property_images','display_order','ASC', array('property_id' => $id));
        $data['template'] = ('admin/property/property_open_view');
        $this->load->view('template_view', $data);
    }

    public function do_upload() {

        // Detect form submission.
        if ($this->input->post('submit')) {
            $path = BASEPATH . '../assets/images/property';
            $this->load->library('upload');
            $this->upload->initialize(array(
                "upload_path" => $path,
                "allowed_types" => "gif|jpg|png",
                "max_size" => '102400',
                "max_width" => '1600',
                "max_height" => '1600'
            ));

            if ($this->upload->do_multi_upload("uploadfile")) {

                $upload_data = $this->upload->get_multi_upload_data();


                echo json_encode($upload_data);
                // echo '<p class = "bg-success">' . count($upload_data) . 'File(s) successfully uploaded.</p>';
            } else {

// Output the errors
                $errors = array('error' => $this->upload->display_errors('<p class = "bg-danger">', '</p>'));

                foreach ($errors as $k => $error) {
                    echo $error;
                }
            }
        } else {
            echo '<p class = "bg-danger">An error occured, please try again later.</p>';
        }
        // Exit to avoid further execution
        exit();
    }

    public function add_images($id) {
        $basic_args = array();
        $images = $this->input->post('imagecap');
        $upload = explode(",", $images);
        $image = '';
        foreach ($upload as $upl):
            $get_thumb = $this->crud_model->select('*', 'tbl_thumbnail', array('manager' => 'property'));
            if ($get_thumb) {
                $ext = '';
                $img_filename = '';
                if ($upl != $image && $image != '') {
                    list($img_filename, $ext) = explode(".", $image);
                    @unlink($config['upload_path'] . $image);
                }
                $this->load->library('image_lib');
                $image_info = explode('.', $upl);
                $img_upl = $image_info[0];
                $ext_upl = $image_info[1];

                list($width, $height, $type, $attr) = getimagesize(BASEPATH . '../assets/images/property/' . $upl);
                foreach ($get_thumb as $thumb):
                    /*$new_image = $img_upl . '_' . $thumb->type . '_' . $thumb->width . '_' . $thumb->height . "." . $ext_upl;
                    $old_image = $img_upl . '_' . $thumb->type . '_' . $width . '_' . $height . "." . $ext_upl;

                    $config = array(
                        'source_image' => BASEPATH . '../assets/images/property/' . $upl,
                        'width' => $thumb->width,
                        'height' => $thumb->height,
                        'new_image' => $new_image
                    );
                    $this->image_lib->initialize($config);
                    $this->image_lib->fit();
                    if ($new_image != $old_image && $old_image != '' && file_exists(BASEPATH . '../assets/images/property/' . $old_image)) {
                        @unlink(BASEPATH . '../assets/images/property/' . $old_image);
                    }*/

                    $new_image = $img_upl . '_' . $thumb->type . '_' . $thumb->width . '_' . $thumb->height . "." . $ext_upl;
                    $old_image = $img_upl . '_' . $thumb->type . '_' . $width . '_' . $height . "." . $ext_upl;
                   
                    $resize_detail=(array)$thumb;
                    $resize_detail['crop']="true";
                    list($resize_width, $resize_height) = $this->misc_library->get_resized_width_height($width, $height,$resize_detail);
                    //echo $resize_width." ".$resize_height;die;
                    $config = array(
                        // 'image_library'=> 'gd2',
                        'source_image' => BASEPATH . '../assets/images/property/' . $upl,
                        'quality'=>'90',
                        'width' => $resize_width,
                        'height' => $resize_height,
                        'new_image' => $new_image,
                        'maintain_ratio'=>TRUE
                    );
                    
                    $this->image_lib->initialize($config);
                    $this->image_lib->resize();
                    $this->image_lib->clear();
                    
                    $y_axis=0;$x_axis=0;
                    if($resize_width == $thumb->width){
                        $y_axis = ($resize_height - $thumb->height)/2;
                    }else{
                        $x_axis = ($resize_width - $thumb->width)/2;
                    }   
                    //crop image to get the exact size   
                    //load image library for cropping
                    $config = array(
                        'image_library'=> 'gd2',
                        'source_image' => BASEPATH."/../assets/images/property/".$new_image,
                        'x_axis' => $x_axis,
                        'y_axis' => $y_axis,
                        'width' => $thumb->width,
                        'height' => $thumb->height,
                        'quality'=>'90',
                        'maintain_ratio'=>FALSE
                        
                    );
                    $this->image_lib->initialize($config);
                    if(!$this->image_lib->crop()){
                        //echo $this->image_lib->display_errors();
                    }
                    $this->image_lib->clear();

                    /* if($thumb->type=='listing'){
                        echo "<pre>";print_r($config);
                        list($w, $h) = getimagesize(BASEPATH."/../assets/images/property/".$new_image); echo $new_image.">>".$w." ".$h;
                    } */    
                    if ($new_image != $old_image && $old_image != '' && file_exists(BASEPATH . '../assets/images/property/' . $old_image)) {
                        @unlink(BASEPATH . '../assets/images/property/' . $old_image);
                    }
                endforeach;
            }
            $basic_args['image'] = $upl;
            // print_r($basic_args);die;
            $basic_args['property_id'] = $id;
            $max_order = $this->crud_model->get_max('display_order','max_order','tbl_property_images',array('property_id'=>$id));
            if($max_order){
                $max_order++;
            }else{
                $max_order =1;
            }
            
            $basic_args['display_order'] = $max_order;
            $this->crud_model->insert('tbl_property_images', $basic_args);
        endforeach;
        // echo 'yes';die;
        $this->session->set_flashdata('msg', 'Successfully inserted ');
        redirect('admin/property/property_open/'.$id);
    }

    public function delete_image() {
        $data = $this->input->post('image_name'); 
        $filename = BASEPATH . '../assets/images/property/' . $data;
        if (file_exists($filename)) {
            @unlink($filename);
            echo $data;
            // echo "The file $filename exists";
        } else {
            echo 0;
        }
    }

    public function delete_old_image() {
        $data = $this->input->post('name'); 
        $this->crud_model->delete('tbl_property_images',array('image'=>$data));
        $filename = BASEPATH . '../assets/images/property/' . $data;
        if (file_exists($filename)) {
            @unlink($filename);
            echo $data;
            //echo "The file $filename exists";
        } else {
            echo 0;
        }
    }


  /* public function delete_old_image() {
        $data = $this->input->post('name'); 
        $property_images=$this->crud_model->select_row('*','tbl_property_images',array('image'=>$data));
        $thumb_detail = $this->crud_model->select('*', 'tbl_thumbnail', array('manager' => 'property'));
        if($property_images){
       if( $this->crud_model->delete('tbl_property_images',array('image'=>$data))){
          
            foreach ($property_images as $prop):
                
                    if (!empty($prop->image)):
                        $file_name = explode('.', $prop->image);
                        $file = $file_name[0];
                        $get_ext = $file_name[1];
                        $this->crud_model->img_delete($thumb_detail, $file, 'property', $get_ext, true);
                    endif;
                
            endforeach;
              }

       }
        $filename = BASEPATH . '../assets/images/property/' . $data;
        if (file_exists($filename)) {
            @unlink($filename);
            echo $data;
            //echo "The file $filename exists";
        } else {
            echo 0;
        }
    } */








    public function change_status_for() {

        if ($this->crud_model->change_status('tbl_property')) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function sort(){
        if (isset($_GET['list'])) {
            $ids=$_GET['list'];
            $ids=explode(',',$ids);
            if(is_array($ids))
            {
                $i = 1;
                foreach ($ids as $id) {
                    $this->crud_model->update('tbl_property_images',array('display_order'=>$i),array('id'=>$id));
                    $i++;
              }
            echo '<div id="successmsg">Successfully saved.</div>';
            //make the success message disappear slowly
            echo '<script type="text/javascript">$(document).ready(function(){ $("#successmsg").fadeOut(2000); });</script>';  
            }
        }
    }
}