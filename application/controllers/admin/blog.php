<?php

class Blog extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('admin/authorization_model');
        $this->load->library('misc_library');
        $this->load->model('crud_model');
        $this->load->helper('url');
        $this->load->helper('misc_helper');
        $this->load->library('form_validation');
        $this->load->library('email');
        if ($this->uri->segment(2) != 'login' && $this->uri->segment(2) != 'forgot-password') {
            if (!$this->misc_library->is_logged()) {
                redirect('admin/login');
            }
        }
    }

    public function index() {
        $data['title'] = 'Blog ';
        $data["all_blog"] = $this->crud_model->select(array("id", "title","author","image","excerpt","is_active"), "tbl_blog");
        $data['template'] = 'admin/blog/blog_list_view';
        $this->load->view('template_view', $data);
    }


    public function add_edit(){
        $id = $this->input->post('id');
        if (!$id) {
            $data['title'] = 'Add Blog';
        } else {
            $data['title'] = 'Edit Blog';
        }
        $this->load->library('ckeditor');
        $this->load->library('ckFinder');
        $this->ckeditor->basePath = base_url() . 'assets/ckeditor/';
        $this->ckeditor->config['language'] = 'en';
        $this->ckeditor->config['width'] = '100%';
        $this->ckeditor->config['height'] = '100%';
        //configure ckfinder with ckeditor config
        $this->ckfinder->SetupCKEditor($this->ckeditor, '../../assets/ckfinder');
        $id = $this->input->post('id');

        $data['blog_info'] = $this->crud_model->select_row('*', 'tbl_blog', array('id' => $id));
        $type = 0;
        $image = '';


        $basic_args = array();
        if ($this->input->post('btn_blog')) {
            $data['title'] = 'Edit Blog';
            $form_data = $this->input->post();
            $config = array(
                array(
                    'field' => 'title',
                    'label' => 'Title',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'slug',
                    'label' => 'Slug',
                    'rules' => ''
                ),
                array(
                    'field' => 'excerpt',
                    'label' => 'Excerpt',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'b_description',
                    'label' => 'Description',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'create_date',
                    'label' => 'Create Date',
                    'rules' => ''
                ),
                array(
                    'field' => 'author',
                    'label' => 'Author',
                    'rules' => ''
                ),
                array(
                    'field' => 'image',
                    'label' => 'Image',
                    'rules' => ''
                ),
                array(
                    'field' => 'meta_title',
                    'label' => 'Meta Title',
                    'rules' => 'max_length[55]'
                ),
                array(
                    'field' => 'meta_keyword',
                    'label' => 'Meta Keyword',
                    'rules' => ''
                ),
                array(
                    'field' => 'meta_description',
                    'label' => 'Meta Description',
                    'rules' => 'max_length[115]'
                ),
                array(
                    'field' => 'is_active',
                    'label' => 'Status',
                    'rules' => ''
                ),
                array(
                    'field' => 'id',
                    'label' => 'id',
                    'rules' => ''
                ),
            );
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error', validation_errors());
              //  redirect('admin/blog/add-edit');
            } else {
                foreach ($form_data as $key => $value):

                    if ($key != 'btn_blog') {
                        $basic_args[$key] = $value;
                    }
                    
                endforeach;
                $slug = ($form_data['slug'] == "" ? $form_data['title'] : $form_data['slug']);
                $basic_args['slug'] = $this->misc_library->check_if_slug_exists(slugify($slug), 'tbl_blog', $id);
                if ($_FILES['image']['name']) {

                    //for uploading image
                    $newname = 'file_' . time();
                    $config['allowed_types'] = "gif|png|jpg|jpeg";
                    $config['upload_path'] = BASEPATH . '../assets/images/blog/';
                    $config['file_name'] = $newname;
                    $config['max_size'] = '20482048';
                    $config['max_width'] = '2048';
                    $config['max_height'] = '1200';
                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('image')) {
                        $error = array('error' => $this->upload->display_errors());
                        // print_r($error);
                        // die;

                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        redirect(base_url('admin/blog/add-edit'));
                    } else {
                        $data = $this->upload->data();

                        $get_thumb = $this->crud_model->select('*', 'tbl_thumbnail', array('manager' => 'blog' ));

                        if ($get_thumb) {
                            $ext = '';
                            $img_filename = '';
                            if ($data['file_name'] . $data['file_ext'] != $image && $image != '') {
                                list($img_filename, $ext) = explode(".", $image);
                                @unlink($config['upload_path'] . $image);
                            }
                            $this->load->library('image_lib');
                            foreach ($get_thumb as $thumb):
                                $new_image = $data['file_name'] . '_' . $thumb->type . '_' . $thumb->width . '_' . $thumb->height . $data['file_ext'];
                                $old_image = $image . '_' . $thumb->type . '_' . $thumb->width . '_' . $thumb->height . "." . $ext;
                                /*$config = array(
                                    'source_image' => $data['full_path'],
                                    'width' => $thumb->width,
                                    'height' => $thumb->height,
                                    'new_image' => $new_image
                                );

                                $this->image_lib->initialize($config);
                                $this->image_lib->fit();*/

                                list($width, $height) = getimagesize($data['full_path']);
                                $resize_detail=(array)$thumb;
                                $resize_detail['crop']="true";
                                list($resize_width, $resize_height) = $this->misc_library->get_resized_width_height($width, $height,$resize_detail);
                                //echo $width.">>".$height.">>".$resize_width." ".$resize_height;die;
                                $config = array(
                                    // 'image_library'=> 'gd2',
                                    'source_image' => $data['full_path'],
                                    'quality'=>'90',
                                    'width' => $resize_width,
                                    'height' => $resize_height,
                                    'new_image' => $new_image,
                                    'maintain_ratio'=>TRUE
                                );
                                
                                $this->image_lib->initialize($config);
                                $this->image_lib->resize();
                                $this->image_lib->clear();
                                $y_axis=0;$x_axis=0;
                                if($resize_width == $thumb->width){
                                    $y_axis = ($resize_height - $thumb->height)/2;
                                }else{
                                    $x_axis = ($resize_width - $thumb->width)/2;
                                }
                                 //crop image to get the exact size   
                                //load image library for cropping
                                $config = array(
                                    'image_library'=> 'gd2',
                                    'source_image' => BASEPATH."/../assets/images/blog/".$new_image,
                                    'x_axis' => $x_axis,
                                    'y_axis' => $y_axis,
                                    'width' => $thumb->width,
                                    'height' => $thumb->height,
                                    'quality'=>'90',
                                    'maintain_ratio'=>FALSE
                                    
                                );
                                $this->image_lib->initialize($config);
                                if(!$this->image_lib->crop()){
                                    echo $this->image_lib->display_errors();
                                }
                                $this->image_lib->clear();

                                if ($new_image != $old_image && $old_image != '' && file_exists(BASEPATH . '../assets/images/blog/' . $old_image)) {
                                    @unlink(BASEPATH . '../assets/images/blog/' . $old_image);
                                }//print_r($config);die;
                            endforeach;
                        }
                        $basic_args['image'] = $data['file_name'];
                    }
                }
               $date=$form_data['create_date'];
               $current_date=date('Y-m-d', strtotime($date));
               $basic_args['create_date']= $current_date;
               $session_id = $this->session->userdata('session_id');

                $basic_args['session_id'] = $session_id;
               // $database_session_id= $this->crud_model->select("session_id",'tbl_blog');
                //print_r ($database_session_id);die; 
                if ($id == '') {

                    $this->crud_model->insert('tbl_blog', $basic_args);
                                  
                    $this->session->set_flashdata('msg', 'Successfully Added Blog');
                    redirect(base_url('admin/blog'));
                   
                } else {
                    // $basic_args['update_date'] = $date;
                    $this->crud_model->update('tbl_blog', $basic_args, array('id' => $id));              
                    $this->session->set_flashdata('msg', 'Successfully Updated Blog');
                    redirect(base_url('admin/blog'));
                   
                }
            }
        }
     $data['template'] = 'admin/blog/form_view';
     $this->load->view('template_view', $data);
    }


    public function delete(){
        $id=$this->input->post('id');
        if($this->crud_model->delete('tbl_blog',array('id'=>$id))){
            $this->session->set_flashdata('msg',"Successfully deleted the blog!");
            redirect('admin/blog');
        }
        else{
            $this->session->set_flashdata('error',"Cannot delete the blog!");
            redirect('admin/blog');
        }
    }
 

      public function change_status() {
        $status = $this->crud_model->status('tbl_blog');
        echo $status;
    }

}