<?php

class Setting extends CI_controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('misc_library');
        $this->load->model('crud_model');
        $this->load->library('form_validation');
        $this->load->helper('misc_helper');
        $this->load->helper('form');
        if (!$this->misc_library->is_logged()) {
            redirect('admin/login');
        }
    }

    public function index() {
        $data['title'] = 'Setting';
        $data['admin_info'] = $this->crud_model->select_row('*', 'tbl_admin', array('id' => $this->session->userdata('is_logged')));
        $data['setting_info'] = $this->crud_model->select_by_order('*', 'tbl_setting', 'order', 'Asc', '');
        $data['template'] = ('admin/setting/admin_setting_view');
        $this->load->view('template_view', $data);
    }

    public function edit() {
        $data['title'] = "Setting";
        $data['admin_info'] = $this->crud_model->select_row('*', 'tbl_admin', array('id' => $this->session->userdata('is_logged')));
        $data['setting_info'] = $this->crud_model->select_by_order('*', 'tbl_setting', 'order', 'Asc', '');
        $id = $this->input->post('id');
        if ($id != '') {
            $data['stng_info'] = $this->crud_model->select_row('*', 'tbl_setting', array('id' => $id));
        }
        if ($this->input->post() && !$this->input->post('edit')) {
            $config = array(
                array(
                    'field' => 'type',
                    'label' => 'Type',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'value',
                    'label' => 'Value',
                    'rules' => 'required'
                )
            );
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == FALSE) {
                $data['template'] = 'admin/setting/admin_setting_view';
                $this->load->view('template_view', $data);
                return false;
            } else {
                $form_data = $this->input->post();
                $insert_type = $form_data['insert_type'];
                $order_num = $form_data['select_type'];
                $order_add_num = $form_data['select_type'] + 1;
                //get maximum order value
                $max_ord = $this->crud_model->get_max('order', 'max_order', 'tbl_setting');
                $insert_max_ord = $max_ord + 1;
                if ($order_num == 0) {
                    if ($id != "") {
                        $slug = ($form_data['slug'] == "" ? $form_data['type'] : $form_data['slug']);
                        $new_slug = $this->misc_library->check_if_slug_exists(slugify($slug), 'tbl_setting', $id);
                        $insert_arr = array(
                            'type' => $form_data['type'],
                            'slug' => $new_slug,
                            'value' => $form_data['value'],
                            'is_permanent' => isset($form_data['is_permanent']) ? $form_data['is_permanent'] : '0'
                        );
                    } else {
                        $slug = ($form_data['slug'] == "" ? $form_data['type'] : $form_data['slug']);
                        $new_slug = $this->misc_library->check_if_slug_exists(slugify($slug), 'tbl_setting');
                        $insert_arr = array(
                            'type' => $form_data['type'],
                            'slug' => $new_slug,
                            'value' => $form_data['value'],
                            'is_permanent' => isset($form_data['is_permanent']) ? $form_data['is_permanent'] : '0',
                            'order' => $insert_max_ord
                        );
                    }
                } else {
                    if ($insert_type == 'before') {
                        $insert_arr = array(
                            'type' => $form_data['type'],
                            'value' => $form_data['value'],
                            'is_permanent' => isset($form_data['is_permanent']) ? $form_data['is_permanent'] : '0',
                            'order' => $order_num
                        );
                        $get_order = $this->crud_model->select_greater_than('*', 'tbl_setting', 'order >=', $order_num);
                        foreach ($get_order as $make_order):
                            $add_order = $make_order->order + 1;
                            $edit_id = $make_order->id;
                            $this->crud_model->update('tbl_setting', array('order' => $add_order), array('id' => $edit_id));
                        endforeach;
                    }else {
                        $insert_arr = array(
                            'type' => $form_data['type'],
                            'value' => $form_data['value'],
                            'is_permanent' => isset($form_data['is_permanent']) ? $form_data['is_permanent'] : '0',
                            'order' => $order_add_num
                        );
                        $get_order = $this->crud_model->select_greater_than('*', 'tbl_setting', 'order >=', $order_add_num);
                        foreach ($get_order as $make_order):
                            $add_order = $make_order->order + 1;
                            $edit_id = $make_order->id . ' ';
                            $this->crud_model->update('tbl_setting', array('order' => $add_order), array('id' => $edit_id));
                        endforeach;
                    }
                }
                if ($id == "") {
                    $this->crud_model->insert('tbl_setting', $insert_arr);
                    $this->session->set_flashdata('msg', 'Successfully Added');
                    redirect('admin/setting');
                } else {
                    $this->crud_model->update('tbl_setting', $insert_arr, array('id' => $id));
                    $this->session->set_flashdata('msg', 'Successfully Updated');
                    redirect('admin/setting');
                }
            }
        }
        $data['template'] = 'admin/setting/admin_setting_view';
        $this->load->view('template_view', $data);
    }

    public function change_admin() {
        $data['title'] = "Setting";
        //echo $this->session->userdata('is_logged');die;
        $password = $this->crud_model->select_row('*', 'tbl_admin', array('id' => $this->session->userdata('is_logged')))->password;

        if ($form_data = $this->input->post()) {

            foreach ($form_data as $key => $value):
                $basic_args[$key] = $value;
            endforeach;

            if ($form_data['password'] != 'password') {
                $basic_args['password'] = sha1($form_data['password']);
            } else {
                $basic_args['password'] = $password;
            }


            if ($this->crud_model->update('tbl_admin', $basic_args, array('id' => $this->session->userdata('is_logged')))) {
                $data['msg'] = "Successfully Updated.";
            } else {
                $data['msg'] = "Unable to Update.";
            }
        }


        $data['admin_info'] = $this->crud_model->select_row('*', 'tbl_admin', array('id' => $this->session->userdata('is_logged')));

        $data['setting_info'] = $this->crud_model->select_by_order('*', 'tbl_setting', 'order', 'Asc', '');
        $data['template'] = 'admin/setting/admin_setting_view';
        $this->load->view('template_view', $data);
    }

    public function delete() {
        $id = $this->input->post('id');
        $type = $this->crud_model->select_row('*', 'tbl_setting', array('id' => $id));
        if ($this->crud_model->delete('tbl_setting', array('id' => $id))) {
            $this->session->set_flashdata('msg', 'Successfully Deleted The ' . $type->type);
            redirect('admin/setting');
        } else {
            $this->session->set_flashdata('msg', 'Cannot Deleted.');
            redirect('admin/setting');
        }
    }

}

?>
