<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Listing extends CI_Controller {

    private $limit;

    public function __construct() {
        parent::__construct();
        $this->load->model('crud_model');
        $this->load->model('main_model');
        $this->load->library('pagination');
        $this->load->library('misc_library');
        $this->load->helper('misc_helper');
        $this->limit = 3;
    }

    public function index() {
        $data['title'] = 'Listing';
        $data['all_listing'] = $this->crud_model->get_all_content_asc($this->limit, 0, 'tbl_property');
        $data['startagain'] = $this->limit;
        $data['meta'] = $this->crud_model->select_row('*', 'tbl_seo', array('page' => 'generic'));
        $data['meta_title'] = $data['meta']->meta_title;
        $data['meta_keywords'] = $data['meta']->meta_keyword;
        $data['meta_description'] = $data['meta']->meta_description;
        $web_info = $this->crud_model->select_where_in('value, slug', 'tbl_setting', 'slug', array('email', 'website-title', 'site-name'));
        foreach ($web_info as $web) {
            $data[str_replace('-', '', $web->slug)] = $web->value;
        }
        $data['template'] = 'listing_view';
        $this->load->view('template_view', $data);
    }

    public function next_listing($startagain) {
        sleep(1);
        $data['next_listing'] = $this->crud_model->get_all_content_asc($this->limit, $startagain, 'tbl_property');
        $data['startagain'] = $this->limit + $startagain;
        $data['meta'] = $this->crud_model->select_row('*', 'tbl_seo', array('page' => 'generic'));
        $data['meta_title'] = $data['meta']->meta_title;
        $data['meta_keywords'] = $data['meta']->meta_keyword;
        $data['meta_description'] = $data['meta']->meta_description;
        $this->load->view('short_listing_view', $data);
    }

    public function listing_detail($slug) {

        $data['detail'] = $this->main_model->getPropertyBySlug($slug);
        $data['title'] = $data['detail']->p_name;
        $id = $this->crud_model->select_row('id', 'tbl_property', array('slug' => $slug))->id;
        $data['images'] = $this->crud_model->select('*', 'tbl_property_images', array('property_id' => $id));
        $data['basic_info'] = $this->crud_model->select('*', 'tbl_admin',array('id'=>'1'));
        $data['meta'] = $this->crud_model->select_row('*', 'tbl_property', array('slug' => $slug));
        $data['meta_title_p'] = $data['meta']->meta_title;
        $data['meta_keywords_p'] = $data['meta']->meta_keyword;
        $data['meta_description_p'] = $data['meta']->meta_description;
        $web_info = $this->crud_model->select_where_in('value, slug', 'tbl_setting', 'slug', array('email', 'website-title', 'site-name'));
        foreach ($web_info as $web) {
            $data[str_replace('-', '', $web->slug)] = $web->value;
        }
        $data['lat']=$data['detail']->lat; //print_r($data['detail']);die;
        $data['long']=$data['detail']->long;
        $data['template'] = 'details_view';
        $this->load->view('template_view', $data);
    }
    
    public function blog(){
         $data['template'] = 'p_blog_view';
        $this->load->view('template_view', $data);
    }

    public function blog_detail(){
        $data['template'] = 'p_blog_detail_view';
        $this->load->view('template_view', $data);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */