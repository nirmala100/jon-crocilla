<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Testimonial extends CI_Controller {

    private $limit;

    public function __construct() {
        parent::__construct();
        $this->load->model('crud_model');
        $this->load->model('main_model');
        $this->load->library('pagination');
        $this->limit = 4;
    }

    public function index() {
        $data['title'] = 'Testimonials';
        $data['all_testimonial'] = $this->crud_model->get_all_content_asc($this->limit, 0, 'tbl_testimonials');
        $data['start'] = $this->limit;
        $data['meta'] = $this->crud_model->select_row('*', 'tbl_seo', array('page' => 'generic'));
        $data['meta_title'] = $data['meta']->meta_title;
        $data['meta_keywords'] = $data['meta']->meta_keyword;
        $data['meta_description'] = $data['meta']->meta_description;
        $web_info = $this->crud_model->select_where_in('value, slug', 'tbl_setting', 'slug', array('email', 'website-title', 'site-name'));
        foreach ($web_info as $web) {
            $data[str_replace('-', '', $web->slug)] = $web->value;
        }
        $data['template'] = 'testimonial_view';
        $this->load->view('template_view', $data);
    }

    public function next_testimonial($start) {
        sleep(1);
        $data['next_testimonial'] = $this->crud_model->get_all_content_asc($this->limit, $start, 'tbl_testimonials');
        $data['start'] = $this->limit + $start;
        $data['meta'] = $this->crud_model->select_row('*', 'tbl_seo', array('page' => 'generic'));
        $data['meta_title'] = $data['meta']->meta_title;
        $data['meta_keywords'] = $data['meta']->meta_keyword;
        $data['meta_description'] = $data['meta']->meta_description;
        $this->load->view('short_testimonial_view', $data);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */