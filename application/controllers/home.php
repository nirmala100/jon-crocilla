<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Home extends CI_Controller { 


   	public function __construct() {
		parent::__construct();
		$this->load->model('crud_model');
		$this->load->library('misc_library');
		$this->load->library('pagination');
		$this->load->library('form_validation');
		$this->load->model('main_model');
		$this->load->helper('misc_helper');
		$data['all_about'] = $this->crud_model->select_row('*', 'tbl_admin', array('id' => 1));
		$web_info = $this->crud_model->select_where_in('value, slug', 'tbl_setting', 'slug', array('email', 'website-title', 'site-name'));
		foreach ($web_info as $web) {
			$data[str_replace('-', '', $web->slug)] = $web->value;
		}
	}

	public function index() {
		$data['title'] = "Home";
		$day_no = date('N');
		$time = (date('a') == 'pm' ? 1 : 0);
		$data['home_image'] = $this->misc_library->get_slider_image_by_day($day_no, $time, 'home');
		$data['property'] = $this->main_model->get_featured_property();

		$data['all_testimonials'] = $this->main_model->get_limited_testimonials();
		$web_info = $this->crud_model->select_where_in('value, slug', 'tbl_setting', 'slug', array('email', 'website-title', 'site-name'));
		foreach ($web_info as $web) {
			$data[str_replace('-', '', $web->slug)] = $web->value;
		}

		$data['meta'] = $this->crud_model->select_row('*', 'tbl_seo', array('page' => 'generic'));
		$data['meta_title'] = $data['meta']->meta_title;
		$data['meta_keywords'] = $data['meta']->meta_keyword;
		$data['meta_description'] = $data['meta']->meta_description;
		$data['template'] = 'index';

		$this->load->view('template_view', $data);
	}

	public function about() {
		$data['title'] = 'About';
		$data['all_about'] = $this->crud_model->select('*', 'tbl_admin'); //echo '<pre>';print_r($data['all_about']);die;
		//$data['why_me'] = $this->crud_model->select('*', 'tbl_why_me');
		$data['why_me_1'] = $this->crud_model->select_row('*', 'tbl_why_me', array('id' => 1));
		$data['why_me_2'] = $this->crud_model->select_row('*', 'tbl_why_me', array('id' => 2));
		$data['why_me_3'] = $this->crud_model->select_row('*', 'tbl_why_me', array('id' => 3));
		$data['why_me_4'] = $this->crud_model->select_row('*', 'tbl_why_me', array('id' => 4));
		$data['why_me_5'] = $this->crud_model->select_row('*', 'tbl_why_me', array('id' => 5));
		$data['all_testimonials'] = $this->main_model->get_limited_testimonials();
		$data['sliders'] = $this->crud_model->select('*', 'tbl_slider', array('type' => 'about'));
		$data['meta'] = $this->crud_model->select_row('*', 'tbl_seo', array('page' => 'generic'));
		$data['meta_title'] = $data['meta']->meta_title;
		$data['meta_keywords'] = $data['meta']->meta_keyword;
		$data['meta_description'] = $data['meta']->meta_description;
		$web_info = $this->crud_model->select_where_in('value, slug', 'tbl_setting', 'slug', array('email', 'website-title', 'site-name'));
		foreach ($web_info as $web) {
			$data[str_replace('-', '', $web->slug)] = $web->value;
		}
		$data['template'] = 'about_us_view';
		$this->load->view('template_view', $data);
	}

	public function contact() {
		$data['title'] = 'Contact';
		$data['basic_info'] = $this->crud_model->select('*', 'tbl_admin');
		$data['meta'] = $this->crud_model->select_row('*', 'tbl_seo', array('page' => 'generic'));
		$data['contactboxs'] = $this->crud_model->select('*', 'tbl_footerbox', array('type' => 'contact'));
		$data['meta_title'] = $data['meta']->meta_title;
		$data['meta_keywords'] = $data['meta']->meta_keyword;
		$data['meta_description'] = $data['meta']->meta_description;
		$web_info = $this->crud_model->select_where_in('value, slug', 'tbl_setting', 'slug', array('email', 'website-title', 'site-name', 'office-latitude', 'office-longitude'));
		foreach ($web_info as $web) {
			$data[str_replace('-', '', $web->slug)] = $web->value;
		}

		if ($this->input->post('submit')) {
			$form_data = $this->input->post();
			$config = array(
				array(
					'field' => 'name',
					'label' => 'Name',
					'rules' => 'required',
				),
				array(
					'field' => 'email',
					'label' => 'Email',
					'rules' => 'required',
				),
				array(
					'field' => 'phone',
					'label' => 'Phone',
					'rules' => '',
				),
				array(
					'field' => 'inquiry_type',
					'label' => 'Inquiry Type',
					'rules' => 'required',
				),
				array(
					'field' => 'message',
					'label' => 'Message',
					'rules' => '',
				),
			);
			$this->form_validation->set_rules($config);

			if ($this->form_validation->run() == FALSE) {

			} else {
				$basic_args = array();
				foreach ($form_data as $key => $value):
					if ($key != 'submit') {
						$basic_args[$key] = $value;
					}
				endforeach;

				$email = $basic_args['email'];
				$message = "<strong>Dear Admin,</strong>";
				$message .= "<br><br>";
				$message .= "You have received a contact request from " . $basic_args['name'] . ". Please check details below:";
				$message .= "<br><br>";
				$message .= "<strong>Name</strong> : " . $basic_args['name'];
				$message .= "<br>";
				$message .= "<strong>Email</strong> : " . $basic_args['email'];
				$message .= "<br>";
				$message .= "<strong>Phone</strong> : " . $basic_args['phone'];
				$message .= "<br><br>";
				$message .= "<strong>Inquiry Type</strong> : " . $basic_args['inquiry_type'];
				$message .= "<br><br>";
				$message .= "<strong>Message</strong> : " . $basic_args['message'];

				$message .= "<br><br> Thank You.";
				$this->load->library('email');
				$send_to = $this->misc_library->setting_links('email', 'value');

				$subject = "Contact Information from JonCrocilla.com";
				$config['mailtype'] = 'html';
				$config['charset'] = 'utf-8';
				$config['wordwrap'] = TRUE;

				// print_r($send_to);die;
				$this->email->initialize($config);
				$this->email->set_mailtype("html");

				$this->email->from('info@joncrocilla.com');
				$this->email->to($send_to);

				$this->email->subject($subject);
				$this->email->message($message);

				if ($this->email->send()):

					$this->crud_model->insert('tbl_contact', $basic_args);
					$this->session->set_flashdata('emailmsg', "Your message has been sent successfully!");
					redirect('contact-confirm');
					//redirect('contact');
				endif;
				$this->session->set_flashdata('error', 'Email Sent Failed!');
				redirect('contact');

				echo $this->email->print_debugger();
				die;
			}
		}

		//  $data['lat'] = '41.8947545';
		// $data['long'] = '-87.6246486';

		$data['template'] = 'contact_us_view';
		$this->load->view('template_view', $data);
	}



	public function questionnaire() {
		$data['title'] = 'Questionnaire';

		$meta = $this->crud_model->select_row(array('meta_title', 'meta_keyword', 'meta_description'), 'tbl_seo', array('page' => 'generic'));
		$data['meta_title'] = $meta->meta_title;
		$data['meta_keywords'] = $meta->meta_keyword;
		$data['meta_description'] = $meta->meta_description;

		$data['questionnaires'] = $this->crud_model->select('*', 'tbl_question', array('is_active' => 1));

		if (!empty($data['questionnaires'])):
			foreach ($data['questionnaires'] as $ques):
				$ques->answers = $this->crud_model->select('answer', 'tbl_answer', array('question_id' => $ques->id));
			endforeach;
		endif;
		$web_info = $this->crud_model->select_where_in('value, slug', 'tbl_setting', 'slug', array('email', 'website-title', 'site-name'));
		foreach ($web_info as $web) {
			$data[str_replace('-', '', $web->slug)] = $web->value;
		}
		$data['template'] = 'questionnaire_view';
		$this->load->view('template_view', $data);
	}

	public function add_questionnaire() {
		if ($this->input->post()) {
			$questions = $this->input->post('question');
			$answers = $this->input->post('answer');
			$basic_args = array();
			foreach ($questions as $ques):
				$ans = each($answers);
				$basic_args[$ques] = $ans['value'];
			endforeach;

			$this->session->set_userdata('questionnaire', $basic_args);
			$this->session->set_userdata('is_questionnaire_filled', true);
			return redirect(base_url('contact-info'));
		}

		$this->session->set_userdata('is_questionnaire_filled', false);
		return redirect(base_url('questionnaire'));
	}

	public function contact_info() {
		if ($this->session->userdata('is_questionnaire_filled') && $this->session->userdata('is_questionnaire_filled') == true):

			$data['title'] = 'Contact Info';

			$meta = $this->crud_model->select_row(array('meta_title', 'meta_keyword', 'meta_description'), 'tbl_seo', array('page' => 'generic'));
			$data['meta_title'] = $meta->meta_title;
			$data['meta_keywords'] = $meta->meta_keyword;
			$data['meta_description'] = $meta->meta_description;
			$web_info = $this->crud_model->select_where_in('value, slug', 'tbl_setting', 'slug', array('email', 'website-title', 'site-name'));
			foreach ($web_info as $web) {
				$data[str_replace('-', '', $web->slug)] = $web->value;
			}
			if ($this->input->post()) {
				$form_data = $this->input->post();
				foreach ($form_data as $key => $val):
					if ($key != 'btn_continue' && $key != 'baseurl') {
						$basic_args[$key] = $val;
					}
				endforeach;

				if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
					$this->session->set_userdata('is_contact_info_filled', true);
					$this->session->set_userdata('contact_info', $basic_args);
					if ($this->session->userdata('contact_info')):
						$status['status'] = 'success';
						$status['message'] = 'Message sent!';
					else:
						$status['status'] = 'failed';
						$status['message'] = 'Something went wrong!';
					endif;

					echo json_encode($status);
					die;
				} else {
				$this->load->library('form_validation');
				$this->form_validation->set_error_delimiters('<label class="error">', '</label>');

				$config = array(
					array(
						'field' => 'first_name',
						'label' => 'First Name',
						'rules' => 'required',
					),
					array(
						'field' => 'last_name',
						'label' => 'Last Name',
						'rules' => 'required',
					),
					array(
						'field' => 'email',
						'label' => 'Email',
						'rules' => 'required|valid_email',
					),
					array(
						'field' => 'phone_no',
						'label' => 'Phone No.',
						'rules' => 'required|integer',
					),
					array(
						'field' => 'address_1',
						'label' => 'Address',
						'rules' => 'required',
					),
					array(
						'field' => 'zip',
						'label' => 'Zip',
						'rules' => 'integer',
					),
				);

				$this->form_validation->set_rules($config);
				if ($this->form_validation->run() == FALSE) {
					$this->session->set_userdata('is_contact_info_filled', false);
					$data['template'] = 'contact_info_view';
					$this->load->view('template_view', $data);
				} else {
					$this->session->set_userdata('is_contact_info_filled', true);
					$this->session->set_userdata('contact_info', $basic_args);
					return redirect(base_url('submission'));
				}
			}
		} else {
			$this->session->set_userdata('is_contact_info_filled', false);
			$data['template'] = 'contact_info_view';
			$this->load->view('template_view', $data);
		} else :
			return redirect(base_url('questionnaire'));
		endif;
	}

	public function submission() {
		if ($this->session->userdata('is_contact_info_filled') && $this->session->userdata('is_contact_info_filled') == true):
			$data['title'] = 'Submission';

			$meta = $this->crud_model->select_row(array('meta_title', 'meta_keyword', 'meta_description'), 'tbl_seo', array('page' => 'generic'));
			$data['meta_title'] = $meta->meta_title;
			$data['meta_keywords'] = $meta->meta_keyword;
			$data['meta_description'] = $meta->meta_description;

			$data['for_short_questions'] = $this->crud_model->select('*', 'tbl_question', array('is_active' => 1));
			$web_info = $this->crud_model->select_where_in('value, slug', 'tbl_setting', 'slug', array('email', 'website-title', 'site-name'));
			foreach ($web_info as $web) {
				$data[str_replace('-', '', $web->slug)] = $web->value;
			}
			$data['template'] = 'submission_view';
			$this->load->view('template_view', $data);
		else:
			return redirect(base_url('contact-info'));
		endif;
	}

	public function add_submission() {
		if ($this->session->userdata('questionnaire') != ''):
			$info_qest = $this->session->userdata('questionnaire');

			if ($this->session->userdata('contact_info') != ''):
				$info_contact = $this->session->userdata('contact_info');
				$email = $info_contact['email'];
				$message = "You got message.";
				$message .= "<table><tr><td>Contact Informations :</td></tr>";
				foreach ($info_contact as $key => $contact):
					$message .= "<tr>";
					$message .= "<td>" . ucwords(str_replace('_', ' ', $key)) . "</td>";
					$message .= "<td>" . $contact . "</td>";
					$message .= "</tr>";
				endforeach;

				$message .= "<tr><td></td></tr>";
				$message .= "<tr><td>Questionnaires :</td></tr>";
				foreach ($info_qest as $key => $qest):
					$message .= "<tr>";
					$message .= "<td>" . ucwords($key) . "</td>";
					$message .= "<td>" . $qest . "</td>";
					$message .= "</tr>";
				endforeach;

				$message .= "</table>";
				//  echo $message;

				$this->load->library('email');
				$send_to = $this->misc_library->setting_links('email', 'value');

				$subject = "Questionnaires For Jon";
				$config['mailtype'] = 'html';
				$config['charset'] = 'utf-8';
				$config['wordwrap'] = TRUE;

				$this->email->initialize($config);
				$this->email->set_mailtype("html");

				$this->email->from($email);
				$this->email->to($send_to);

				$this->email->subject($subject);
				$this->email->message($message);

				if ($this->email->send()):
					$this->session->set_flashdata('email_status', 'Email Sent!');
					$this->session->unset_userdata('questionnaire');
					$this->session->unset_userdata('contact_info');
					redirect(base_url('lending-confirm'));
				endif;
				$this->session->set_flashdata('email_status', 'Email Sent Failed!');
				redirect(base_url('submission'));

//                echo $this->email->print_debugger();
				//            die;

			endif;
		endif;

		return redirect(base_url('submission'));
	}

	public function lending() {
		$data['title'] = 'Lending';
		$web_info = $this->crud_model->select_where_in('value, slug', 'tbl_setting', 'slug', array('email', 'website-title', 'site-name', 'banner-text'));
		foreach ($web_info as $web) {
			$data[str_replace('-', '', $web->slug)] = $web->value;
		}
		$data['meta_title'] = '';
		$data['meta_keywords'] = '';
		$data['meta_description'] = '';
		$data['title'] = 'Lending';
		$data['template'] = 'lending_view';
		$data['lendings'] = $this->crud_model->select('*', 'tbl_footerbox', array('type' => 'lending'));
		$this->load->view('template_view', $data);
	}

	public function blog() {
		$data['title'] = 'Blog';
		//$data['latest'] = $this->crud_model->select_row_by_order("*", 'tbl_blog', "create_date",'DESC',array('is_active'=>1) );
		//print_r($data['latest']);die;
		$offset = $this->uri->segment(2);
		$data['all_blogs'] = $this->crud_model->select_by_order_limit_start("*", "tbl_blog", "create_date", 'DESC', 4, $offset, array('is_active' => 1));
		// echo '<pre>';print_r($data['all_blogs']);die;
		$data['top_stories'] = $this->crud_model->select_by_order_limit("*", "tbl_blog", "total_views", "DESC", 3, array('is_active' => 1));

		$data['all_about'] = $this->crud_model->select_row('*', 'tbl_admin', array('id' => 1));
		$web_info = $this->crud_model->select_where_in('value, slug', 'tbl_setting', 'slug', array('email', 'website-title', 'site-name'));
		foreach ($web_info as $web) {
			$data[str_replace('-', '', $web->slug)] = $web->value;
		}
		/*pagination*/
		$limit = 4;
		$this->load->library('pagination');
		$total = $this->main_model->get_count('tbl_blog', array('is_active' => 1));

		$this->load->library('pagination');
		//$config = $this->misc_library->get_pagination_classes();
		$config['full_tag_open'] = '<div class="pagination-blog">';
		$config['full_tag_close'] = '</div>';
		$config['base_url'] = base_url() . '/blog/';
		$config['total_rows'] = $total;
		$config['per_page'] = $limit;
		$config['uri_segment'] = 2;

		//  $config['full_tag_open'] = '<ul class="pagination">';
		// $config['full_tag_close'] = '</ul>';
		$this->pagination->initialize($config);

		$data['meta'] = $this->crud_model->select_row('*', 'tbl_seo', array('page' => 'generic'));
		$data['meta_title'] = $data['meta']->meta_title;
		$data['meta_keywords'] = $data['meta']->meta_keyword;
		$data['meta_description'] = $data['meta']->meta_description;
		$data['template'] = 'blog_view';
		$this->load->view('template_view', $data);
	}

	public function blog_detail($slug) {
		$data['blog_detail'] = $this->main_model->getBlogBySlug($slug);
		$data['title'] = $data['blog_detail']->title;
		$id = $this->crud_model->select_row('id', 'tbl_blog', array('slug' => $slug))->id;

		$session_id = $this->session->userdata('session_id');

		$data['blog_info'] = $this->crud_model->select_row('*', 'tbl_blog', array('slug' => $slug));
		//  print_r($data['blog_info']);die;

		if ($session_id == $data['blog_info']->session_id) {
			$data['blog_info']->total_views = $data['blog_info']->total_views;

		} else {

			$data['blog_info']->total_views = $data['blog_info']->total_views + 1;
			$this->crud_model->update('tbl_blog', array('total_views' => $data['blog_info']->total_views), array('id' => $id));
		}

		$data['top_stories'] = $this->crud_model->select_by_order_limit("*", "tbl_blog", "total_views", "DESC", 3, array('is_active' => 1));

		$data['all_about'] = $this->crud_model->select_row('*', 'tbl_admin', array('id' => 1));
		$web_info = $this->crud_model->select_where_in('value, slug', 'tbl_setting', 'slug', array('email', 'website-title', 'site-name'));
		foreach ($web_info as $web) {
			$data[str_replace('-', '', $web->slug)] = $web->value;
		}
		$data['meta'] = $this->crud_model->select_row('*', 'tbl_blog', array('slug' => $slug));
		$data['meta_title'] = $data['meta']->meta_title;
		$data['meta_keywords'] = $data['meta']->meta_keyword;
		$data['meta_description'] = $data['meta']->meta_description;
		$data['template'] = "single_blog_view";
		$this->load->view('template_view', $data);
	}

	public function top_stories() {
		$totalStory = $this->input->get('totalStory');
		$data['top_stories'] = $this->crud_model->select_by_order_limit_start("*", "tbl_blog", "total_views", "DESC", 3, $totalStory, array('is_active' => 1));
		$stories = $this->load->view('top_story_view', $data, true);
		echo $stories; 
		exit;
	}


    public function more_info(){
        $status['status'] = 'failed';
        $status['message'] = 'Something went wrong!';
        if ($this->input->post('submit')) {
            $form_data = $this->input->post();
            $config = array(
                 array(
                    'field' => 'first_name',
                    'label' => 'First Name',
                    'rules' => 'required'
                ),
                  array(
                    'field' => 'last_name',
                    'label' => 'Last Name',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'email',
                    'label' => 'Email Address',
                    'rules' => 'required'
                ),
                 array(
                    'field' => 'phone',
                    'label' => 'Phone',
                    'rules' => ''
                ),
                  array(
                    'field' => 'comments',
                    'label' => 'Comments',
                    'rules' => ''
                ),
                   array(
                    'field' => 'see',
                    'label' => 'See',
                    'rules' => ''
                ),
            ); 
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error', validation_errors());
                redirect('/');
                //echo 'error';die;
            } else {
                $basic_args = array();
                foreach ($form_data as $key => $value):
                    if ($key != 'submit'&& $key !='prop_address'&& $key != 'base-url') {
                        $basic_args[$key] = $value;
                    }
                endforeach;
                $message = "<table><tr><td>Get More Information:</td></tr>";
                  foreach ($form_data as $key => $form):
                  if($key !='submit' && $key !='prop_address'&& $key != 'base-url'):
                  $message .= "<tr>";
                  $message .= "<td>" . ucwords(str_replace('_', ' ', $key)) . "</td>";
                  $message .= "<td>" . $form . "</td>";
                  $message .= "</tr>";
                  endif;
                  endforeach; 
                  $message .= "<tr><td></td></tr>";
                  $message .= "</table>";
                

                $this->load->library('email');
				$send_to = $this->misc_library->setting_links('email', 'value');
               
				$subject = $this->input->post('prop_address');
				$config['mailtype'] = 'html';
				$config['charset'] = 'utf-8';
				$config['wordwrap'] = TRUE;

				// print_r($send_to);die;
				$this->email->initialize($config);
				$this->email->set_mailtype("html");
                $from = $this->input->post('email');
				$this->email->from($from);
				$this->email->to($send_to);

				$this->email->subject($subject);
				$this->email->message($message);
                if ($this->email->send()):
                    $this->crud_model->insert('tbl_info',$basic_args);
                   // echo 'An email is sent.';
                    $status['status'] = 'success';
                    $status['message'] = 'Message sent now!';
               
                   else:
                    show_error($this->email->print_debugger());
                   $status['status'] = 'failed';
                   $status['message'] = 'Something went wrong!';

                endif;
            }
        }//post submit
         echo json_encode($status); 
         die;
    }


    public function contact_confirm(){
    	$data['title'] = 'Contact';
		$data['basic_info'] = $this->crud_model->select('*', 'tbl_admin');
		$data['meta'] = $this->crud_model->select_row('*', 'tbl_seo', array('page' => 'generic'));
		$data['contactboxs'] = $this->crud_model->select('*', 'tbl_footerbox', array('type' => 'contact'));
		$data['meta_title'] = $data['meta']->meta_title;
		$data['meta_keywords'] = $data['meta']->meta_keyword;
		$data['meta_description'] = $data['meta']->meta_description;
		$web_info = $this->crud_model->select_where_in('value, slug', 'tbl_setting', 'slug', array('email', 'website-title', 'site-name', 'office-latitude', 'office-longitude'));
		foreach ($web_info as $web) {
			$data[str_replace('-', '', $web->slug)] = $web->value;
		}
		$data['template'] = 'contact_confirm_view';
		$this->load->view('template_view', $data);

    }

    public function lending_confirm(){
    	$data['title'] = 'Lending Confirmation';
		$data['basic_info'] = $this->crud_model->select('*', 'tbl_admin');
		$data['meta'] = $this->crud_model->select_row('*', 'tbl_seo', array('page' => 'generic'));
		$data['contactboxs'] = $this->crud_model->select('*', 'tbl_footerbox', array('type' => 'contact'));
		$data['meta_title'] = $data['meta']->meta_title;
		$data['meta_keywords'] = $data['meta']->meta_keyword;
		$data['meta_description'] = $data['meta']->meta_description;
		$web_info = $this->crud_model->select_where_in('value, slug', 'tbl_setting', 'slug', array('email', 'website-title', 'site-name', 'office-latitude', 'office-longitude'));
		foreach ($web_info as $web) {
			$data[str_replace('-', '', $web->slug)] = $web->value;
		}
		$data['template'] = 'lending_confirm_view';
		$this->load->view('template_view', $data);

    }





   public function get_approved(){
   	if($this->input->post('btn_approved')){ 
   		$form_data=$this->input->post();
   		foreach ($form_data as $key => $val):
					if ($key != 'btn_approved') {
						$basic_args[$key] = $val;
					}
				endforeach;
   		$this->session->set_userdata('contact_info', $basic_args);
   	 redirect ('questionnaire');
   	}
   }


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */