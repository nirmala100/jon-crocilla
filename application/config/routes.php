<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There area two reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router what URI segments to use if those provided
  | in the URL cannot be matched to a valid route.
  |
 */

$route['default_controller'] = "home";
$route['404_override'] = '';


$route['admin'] = 'admin/home';

$route['admin/blog'] ='admin/blog/index';
$route['admin/blog/add-edit'] = 'admin/blog/add_edit';
$route['admin/blog/delete'] = 'admin/blog/delete';
$route['admin/blog/change_status'] = 'admin/blog/change_status';


$route['admin/property'] = 'admin/property';
$route['admin/property/index'] = 'admin/property/index';
$route['admin/property/add-edit'] = 'admin/property/add_edit';
$route['admin/property/add'] = 'admin/property/add';
$route['admin/property/edit'] = 'admin/property/edit';
$route['admin/property/delete'] = 'admin/property/delete'; //to redirect to login method 
//$route['admin/property/delete/(:num)'] = 'admin/property/delete/$1';

$route['admin/property/addQuickProperty'] = 'admin/property/addQuickProperty';
$route['admin/property/change_status'] = 'admin/property/change_status';
$route['admin/property/featured-property'] = 'admin/property/list_featured_property';
$route['admin/property/list_featured_property'] = 'admin/property/list_featured_property';
$route['admin/property/property_open'] = 'admin/property/property_open';
$route['admin/property/(:any)'] = 'admin/property/$1';
$route['admin/property/(:num)'] = 'admin/property';
$route['admin/property/sort'] = 'admin/property/sort';
$route['admin/(:any)'] = 'admin/home/$1';
$route['admin/property/add-edit/featured'] = 'admin/property/add_edit';


$route['admin/testimonial/post_testimonial'] = 'admin/testimonial/post_testimonial';


$route['admin/about/basic'] = 'admin/about/basic';
$route['admin/about/postBasicInfo'] = 'admin/about/postBasicInfo';

$route['admin/about/experience'] = 'admin/about/experience';
$route['admin/about/why-choose-me'] = 'admin/about/WhyChooseMe';
$route['admin/about/why-to-choose-me'] = 'admin/about/postWhyChooseMe';


$route['admin/slider'] = 'admin/slider';
$route['admin/slider/index'] = 'admin/slider/index';
$route['admin/slider/index/listing'] = 'admin/slider/index';
$route['admin/slider/index/about'] = 'admin/slider/index';
$route['admin/slider/about-slider'] = 'admin/slider/about_slider';
$route['admin/slider/home-slider'] = 'admin/slider/home_slider';
$route['admin/slider/detail-slider'] = 'admin/slider/detail_slider';
$route['admin/slider/add_edit'] = 'admin/slider/add_edit';
$route['admin/slider/delete'] = 'admin/slider/delete';
$route['admin/slider/do_upload'] = 'admin/slider/do_upload';
$route['admin/slider/upload/(:any)'] = 'admin/slider/upload/view/$1';
$route['admin/slider/add'] = 'admin/slider/add';
$route['admin/slider/delete_image'] = 'admin/slider/delete_image';
$route['admin/slider/display'] = 'admin/slider/display';
$route['admin/slider/post_home_slider'] = 'admin/slider/post_home_slider';
$route['admin/slider/add-edit'] = 'admin/slider/add_edit';




$route['admin/testimonial'] = 'admin/testimonial';
$route['admin/testimonial/index'] = 'admin/testimonial/index';
//$route['admin/testimonial/add_edit']='admin/testimonial/add_edit';
$route['admin/testimonial/delete'] = 'admin/testimonial/delete';
$route['admin/testimonial/post_testimonial'] = 'admin/testimonial/post_testimonial';



$route['admin/thumbnail'] = 'admin/thumbnail';
$route['admin/thumbnail/add'] = 'admin/thumbnail/add';
$route['admin/thumbnail/edit'] = 'admin/thumbnail/edit';
$route['admin/thumbnail/delete'] = 'admin/thumbnail/delete';
$route['admin/thumbnail/add-edit/(:num)'] = 'admin/thumbnail/add_edit/$1';
$route['admin/thumbnail/change-status'] = 'admin/thumbnail/change_status';
$route['admin/thumbnail/delete/(:num)'] = 'admin/thumbnail/delete/$1';
$route['admin/thumbnail/add-edit'] = 'admin/thumbnail/add_edit';
$route['admin/thumbnail/(:num)'] = 'admin/thumbnail';
$route['admin/thumbnail/(:any)'] = 'admin/thumbnail/$1';
$route['admin/thumbnail/add-edit/(:any)'] = 'admin/thumbnail/add_edit/$1';

$route['admin/seo'] = 'admin/seo';
$route['admin/seo/add-edit'] = 'admin/seo/add_edit';
$route['admin/seo/(:any)'] = 'admin/seo/$1';
$route['admin/seo/updategeneric'] = 'admin/seo/updategeneric';

$route['admin/setting'] = 'admin/setting';
$route['admin/forgot-password'] = 'admin/home/forgot_password';
$route['admin/setting/edit'] = 'admin/setting/edit';
$route['admin/setting/delete'] = 'admin/setting/delete';
$route['admin/setting/change_admin'] = 'admin/setting/change_admin';

$route['admin/questionnaire'] = 'admin/questionnaire/index';
$route['admin/questionnaire/change-status'] = 'admin/questionnaire/change_status_for';
$route['admin/questionnaire/delete'] = 'admin/questionnaire/delete';
$route['admin/questionnaire/delete-ans'] = 'admin/questionnaire/delete_ans';
$route['admin/questionnaire/add-edit'] = 'admin/questionnaire/add_edit';
$route['admin/questionnaire/add'] = 'admin/questionnaire/add';


$route['admin/footerbox'] = 'admin/footerbox';
$route['admin/footerbox/index'] = 'admin/footerbox/index';
$route['admin/footerbox/add-edit'] = 'admin/footerbox/add_edit';

/* frontend */
$route['about-jclp'] = 'home/about';
$route['contact'] = 'home/contact';
$route['lending'] = 'home/lending';
$route['next-testimonial/(:num)'] = 'testimonial/next_testimonial/$1';
$route['next-listing/(:num)'] = 'listing/next_listing/$1';
$route['listing-detail/(:any)'] = 'listing/listing_detail/$1';

$route['questionnaire'] = 'home/questionnaire';
$route['contact-info'] = 'home/contact_info';
$route['submission'] = 'home/submission';
$route['add-questionnaire'] = 'home/add_questionnaire';
$route['add-submission'] = 'home/add_submission';
$route['blog']= 'home/blog';
$route['blog/(:num)'] = 'home/blog/$1';
$route['blog-detail/(:any)'] = 'home/blog_detail/$1';
$route['top-stories'] = 'home/top_stories';
$route['top-stories/(:num)'] = 'home/top_stories/$1';
$route['home/more_info'] = 'home/more_info';
$route['contact-confirm'] = 'home/contact_confirm';
$route['get_approved'] = 'home/get_approved';
$route['lending-confirm'] = 'home/lending_confirm';
/* End of file routes.php */
/* Location: ./application/config/routes.php */