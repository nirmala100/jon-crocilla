<?php include("header-inner.php");?>

<main>
 <div class="container contain contain-white form-submission-page">

  <div class="row">
    <div class="col-md-12 text-center">
      <h1 class="border-bottom">lending questionnaire</h1>
    </div><!-- column -->
  </div><!-- row -->

  <div class="row">
    <div class="col-md-12">    
      <ul class="page-links">
        <li class="active"><a href="questionnaire.php">1.&nbsp;lending questionnaire</a></li>
        <li><a href="contact-info.php">2.&nbsp;contact info</a></li>
        <li><a href="submission.php">3.&nbsp;submission</a></li>
      </ul>
    </div><!-- column -->
  </div><!-- row -->

  <div class="row text-center">
    <div class="col-md-12">
      <p>FILL OUT THE QUESTIONNAIRE TO UNDERSTAND MORE ABOUT YOUR LENDING NEEDS:</p>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">

      <form class="form-horizontal">

        <div class="form-group">
          <label class="col-md-5 col-md-offset-1 control-label">Price range of home you are thinking about?</label>
          <div class="col-md-5">
            <select class="form-control">
              <option>$0000 to $0000</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </select>
          </div>
          <div class="clearfix"></div>
        </div><!-- form-group -->

        <div class="form-group">
          <label class="col-md-5 col-md-offset-1 control-label">How soon do you want to purchase a home?</label>
          <div class="col-md-5">
            <select class="form-control">
              <option>3 MONTHS</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </select>
          </div>
          <div class="clearfix"></div>
        </div><!-- form-group -->

        <div class="form-group">
          <label class="col-md-5 col-md-offset-1 control-label">How much of a down payment for a purchase?</label>
          <div class="col-md-5">
            <select class="form-control">
              <option>$0000</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </select>
          </div>
          <div class="clearfix"></div>
        </div><!-- form-group -->

        <div class="form-group">
          <label class="col-md-5 col-md-offset-1 control-label">How is your credit?</label>
          <div class="col-md-5">
            <select class="form-control">
              <option>$0000</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </select>
          </div>
          <div class="clearfix"></div>
        </div><!-- form-group -->

        <div class="form-group">
          <label class="col-md-5 col-md-offset-1 control-label">Estimated annual income</label>
          <div class="col-md-5">
            <select class="form-control">
              <option>$0000</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </select>
          </div>
          <div class="clearfix"></div>
        </div><!-- form-group -->

        <div class="form-group">
          <label class="col-md-5 col-md-offset-1 control-label">Salary/hourly or self employed?</label>
          <div class="col-md-5">
            <select class="form-control">
              <option>$0000/HR</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </select>
          </div>
          <div class="clearfix"></div>
        </div><!-- form-group -->

        <div class="form-group">
          <label class="col-md-5 col-md-offset-1 control-label">How many years on job?</label>
          <div class="col-md-5">
            <select class="form-control">
              <option>4 YEARS</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </select>
          </div>
          <div class="clearfix"></div>
        </div><!-- form-group -->

        <div class="form-group">
          <label class="col-md-5 col-md-offset-1 control-label">Do you have a home to sell before you
            purchase a new home?</label>
            <div class="col-md-5">
              <select class="form-control">
                <option>YES</option>
                <option>NO</option>
              </select>
            </div>
            <div class="clearfix"></div>
          </div><!-- form-group -->

          <div class="col-md-12 text-center">
            <button class="btn btn-default btn-pattern" type="submit">continue&nbsp;&nbsp;<img src="images/arrow-forward.png" alt=""></button>
          </div>

        </form>

      </div><!-- column -->
    </div><!-- row -->

  </div><!-- contain-white -->
</main>

</div><!-- bg-pattern-grey -->

<?php include("footer.php");?>
