$(document).ready(function(){
	$('.bxslider').bxSlider({
		nextText:'<i class="fa fa-caret-right"></i>',
		prevText:'<i class="fa fa-caret-left"></i>'
	});
	$('a[rel=gallery-images]').colorbox({
		maxWidth: '100%',
		maxHeight: '100%',
		scrolling: false,
	});
	
	$(".customised-select").selectbox();
});