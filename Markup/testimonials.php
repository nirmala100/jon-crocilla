<?php include("header-inner.php");?>

<main>
  <div class="container contain contain-white testimonials">

    <div class="row">
      <div class="col-md-12">
        <h1>testimonails:</h1>
      </div>
    </div><!-- row -->

    <div class="row">
      <div class="col-md-10 col-md-offset-1 text-center">
        
        <div class="testimonials-wrap">
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
          <img src="images/testimonials-comma.jpg" alt="">
        </div>

        <div class="person-details">
          <div class="person-name">
            john s. doe
          </div>
          <div class="person-address">
            xyz, illinois, chicago area
          </div>
        </div><!-- person-details -->
        
      </div><!-- column -->
    </div><!-- row -->

    <div class="row">
      <div class="col-md-10 col-md-offset-1 text-center">
        
        <div class="testimonials-wrap">
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
          <img src="images/testimonials-comma.jpg" alt="">
        </div>

        <div class="person-details">
          <div class="person-name">
            john s. doe
          </div>
          <div class="person-address">
            xyz, illinois, chicago area
          </div>
        </div><!-- person-details -->
        
      </div><!-- column -->
    </div><!-- row -->

    <div class="row">
      <div class="col-md-10 col-md-offset-1 text-center">
        
        <div class="testimonials-wrap">
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
          <img src="images/testimonials-comma.jpg" alt="">
        </div>

        <div class="person-details">
          <div class="person-name">
            john s. doe
          </div>
          <div class="person-address">
            xyz, illinois, chicago area
          </div>
        </div><!-- person-details -->
        
      </div><!-- column -->
    </div><!-- row -->

    <div class="row">
      <div class="col-md-10 col-md-offset-1 text-center">
        
        <div class="testimonials-wrap">
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
          <img src="images/testimonials-comma.jpg" alt="">
        </div>

        <div class="person-details">
          <div class="person-name">
            john s. doe
          </div>
          <div class="person-address">
            xyz, illinois, chicago area
          </div>
        </div><!-- person-details -->
        
      </div><!-- column -->
    </div><!-- row -->

    <div class="row">
      <div class="col-md-10 col-md-offset-1 text-center">
        
        <div class="testimonials-wrap">
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
          <img src="images/testimonials-comma.jpg" alt="">
        </div>

        <div class="person-details">
          <div class="person-name">
            john s. doe
          </div>
          <div class="person-address">
            xyz, illinois, chicago area
          </div>
        </div><!-- person-details -->
        
      </div><!-- column -->
    </div><!-- row -->

    <div class="row">
      <div class="col-md-10 col-md-offset-1 text-center">
        
        <div class="testimonials-wrap">
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
          <img src="images/testimonials-comma.jpg" alt="">
        </div>

        <div class="person-details">
          <div class="person-name">
            john s. doe
          </div>
          <div class="person-address">
            xyz, illinois, chicago area
          </div>
        </div><!-- person-details -->
        
      </div><!-- column -->
    </div><!-- row -->

  </div><!-- container -->
</main>
</div><!-- bg-pattern-grey -->

<?php include("footer.php");?>