<?php include("header-inner.php");?>

<main>
 <div class="container contain contain-white form-submission-page">

  <div class="row">
    <div class="col-md-12 text-center">
      <h1 class="border-bottom">contact info</h1>
    </div><!-- column -->
  </div><!-- row -->

  <div class="row">
    <div class="col-md-12">    
      <ul class="page-links">
        <li class="active"><a href="questionnaire.php">1.&nbsp;lending questionnaire</a></li>
        <li class="active"><a href="contact-info.php">2.&nbsp;contact info</a></li>
        <li><a href="submission.php">3.&nbsp;submission</a></li>
      </ul>
    </div><!-- column -->
  </div><!-- row -->

  <div class="row text-center">
    <div class="col-md-12">
      <p>FILL OUT THE CONTACT DETAILS AND A REPRESENTATIVE WILL FOLLOW UP AS SOON AS POSSIBLE:</p>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">

      <form class="form-horizontal">

        <div class="form-group">
          <label class="col-md-2 col-md-offset-2 control-label">First name<span class="red">*</span></label>
          <div class="col-md-6">
            <input type="text" class="form-control">
          </div>
          <div class="clearfix"></div>
        </div><!-- form-group -->

        <div class="form-group">
          <label class="col-md-2 col-md-offset-2 control-label">Last name<span class="red">*</span></label>
          <div class="col-md-6">
            <input type="text" class="form-control">
          </div>
          <div class="clearfix"></div>
        </div><!-- form-group -->

        <div class="form-group">
          <label class="col-md-2 col-md-offset-2 control-label">Email<span class="red">*</span></label>
          <div class="col-md-6">
            <input type="email" class="form-control">
          </div>
          <div class="clearfix"></div>
        </div><!-- form-group -->

        <div class="form-group">
          <label class="col-md-2 col-md-offset-2 control-label">Phone number<span class="red">*</span></label>
          <div class="col-md-6">
            <input type="tel" class="form-control">
          </div>
          <div class="clearfix"></div>
        </div><!-- form-group -->

        <div class="form-group">
          <label class="col-md-2 col-md-offset-2 control-label">Address 1</label>
          <div class="col-md-6">
            <input type="text" class="form-control">
          </div>
          <div class="clearfix"></div>
        </div><!-- form-group -->

        <div class="form-group">
          <label class="col-md-2 col-md-offset-2 control-label">Address 2</label>
          <div class="col-md-6">
            <input type="text" class="form-control">
          </div>
          <div class="clearfix"></div>
        </div><!-- form-group -->

        <div class="form-group">
          <label class="col-md-2 col-md-offset-2 control-label">City, state, zip</label>
          <div class="col-md-6">

           <div class="row">

            <div class="col-md-4 col-sm-4 col-xs-4">
              <input type="text" class="form-control">
            </div><!-- column -->

            <div class="col-md-4 col-sm-4 col-xs-4">
              <select class="form-control">
                <option>Alabama</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
              </select>
            </div><!-- column -->

            <div class="col-md-4 col-sm-4 col-xs-4">
              <input type="text" class="form-control">
            </div><!-- column -->

          </div><!-- row -->

        </div><!-- column -->
        <div class="clearfix"></div>
      </div><!-- form-group -->

      <div class="form-group">
        <label class="col-md-2 col-md-offset-2 control-label">Best time to reach you<span class="red">*</span></label>
        <div class="col-md-6">
          <select class="form-control">
            <option>12:00AM to 02:00PM</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
          </select>
        </div>
        <div class="clearfix"></div>
      </div><!-- form-group -->

      <div class="col-md-12 text-center">
        <button class="btn btn-default btn-pattern" type="submit">continue&nbsp;&nbsp;<img src="images/arrow-forward.png" alt=""></button>
      </div>

    </form>

  </div><!-- column -->
</div><!-- row -->

</div><!-- contain-white -->
</main>

</div><!-- bg-pattern-grey -->

<?php include("footer.php");?>
