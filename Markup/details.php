<?php include("header-inner.php");?>

<main>
 <div class="container contain contain-white listing-details">

  <div class="row mb27">

    <div class="col-md-8 col-sm-6 col-xs-6">
      <h1>villa solana</h1>
    </div>

    <div class="col-md-4 col-sm-6 col-xs-6 text-right">
      <button class="btn btn-default" type="submit"><i class="fa fa-print"></i></button>
      <button class="btn btn-default ml" type="submit"><i class="fa fa-share-alt"></i></button>
      <button class="btn btn-default ml bg-color" type="submit">request info</button>
    </div>

  </div><!-- row -->

  <div class="row mb27">
    <div class="col-md-12">
      <div id="carousel-example-generic" class="carousel slide details-main-slider" data-ride="carousel">

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <img src="images/gallery-banner1.jpg" alt="...">
          </div>
          <div class="item">
            <img src="images/gallery-banner1.jpg" alt="...">
          </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control details-control" href="#carousel-example-generic" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left left-arrow" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control details-control" href="#carousel-example-generic" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right right-arrow" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div><!-- carousel -->
    </div><!-- column -->
  </div><!-- row -->

  <div class="row border-down">

    <div class="col-md-8 col-sm-12">
      <ul class="details inner-details">
        <li>bedrooms&nbsp;&nbsp;<span class="big-txt">8</span></li>
        <li>bathrooms&nbsp;&nbsp;<span class="big-txt">13</span></li>
        <li>sq.feet&nbsp;&nbsp;<span class="big-txt">22,658</span></li>
        <li>lot size&nbsp;&nbsp;<span class="big-txt">1.29 acres</span></li>
      </ul>
    </div>

    <div class="col-md-4 col-sm-6 property-rate">
      <i class="fa fa-usd"></i>&nbsp;195,000,000
    </div>

  </div><!-- row -->

  <div class="row mb27">

    <div class="col-md-9">

      <div role="tabpanel">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active">
            <a href="#tab1" aria-controls="home" role="tab" data-toggle="tab">property details</a>
          </li>
          <li role="presentation"><a href="#tab2" aria-controls="profile" role="tab" data-toggle="tab">photos</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">

          <div role="tabpanel" class="tab-pane active" id="tab1">
            <p class="big-itl">Villa Solana is one of Santa Barbara's 5 Historic Hilltop Estates—the most prestigious in the area. Built in 1914 by Forest Peabody and designed by Francis T. Underhill, the estate has been beautifully restored to its original neo-classical grandeur with exquisite details and modern amenities.</p><br>
            <p>Resting on a 11.2-acre knoll with 360-degree mountain and ocean views, Villa Solana offers a dramatic backdrop for a family home or lavish event setting. The scale of public rooms—such as an open kitchen and family room, theater and paneled reception room resurrected from a library of French oak boiserie belonging to William Randolph Hearst—balance with the privacy of the owner’s suite, 3 guest suites and 2-bedroom guest house. The estate pays homage to the Santa Barbara lifestyle with loggias, a central courtyard and one of the finest botanical gardens boasting over 200 fruit trees and 500 rose bushes. An estate beyond compare, Villa Solana has seen John F. Kennedy and Martin Luther King as guests.</p>
          </div>

          <div role="tabpanel" class="tab-pane photo-gallery" id="tab2">
            <div id="photogallery" class="carousel slide photo-slider" data-ride="carousel">

              <!-- Wrapper for slides -->
              <div class="carousel-inner" role="listbox">
                <div class="item active">
                  <div class="row">
                    <div class="col-md-6 col-sm-6">
                    <a href="images/photo-gallery.jpg" rel="gallery-images"><img src="images/photo-gallery.jpg" class="img-responsive" alt=""></a>
                      <a href="images/photo-gallery.jpg" rel="gallery-images"><img src="images/photo-gallery.jpg" class="img-responsive" alt=""></a>
                      <a href="images/photo-gallery.jpg" rel="gallery-images"><img src="images/photo-gallery.jpg" class="img-responsive" alt=""></a>
                    </div><!-- tab1 -->
                    <div class="col-md-6 col-sm-6">
                      <a href="images/photo-gallery.jpg" rel="gallery-images"><img src="images/photo-gallery.jpg" class="img-responsive" alt=""></a>
                      <a href="images/photo-gallery.jpg" rel="gallery-images"><img src="images/photo-gallery.jpg" class="img-responsive" alt=""></a>
                      <a href="images/photo-gallery.jpg" rel="gallery-images"><img src="images/photo-gallery.jpg" class="img-responsive" alt=""></a>
                    </div>
                  </div><!-- row -->
                </div><!-- item -->

                <div class="item">
                  <div class="row">
                    <div class="col-md-6 col-sm-6">
                      <a href="images/photo-gallery.jpg" rel="gallery-images"><img src="images/photo-gallery.jpg" class="img-responsive" alt=""></a>
                      <a href="images/photo-gallery.jpg" rel="gallery-images"><img src="images/photo-gallery.jpg" class="img-responsive" alt=""></a>
                      <a href="images/photo-gallery.jpg" rel="gallery-images"><img src="images/photo-gallery.jpg" class="img-responsive" alt=""></a>
                    </div><!-- tab1 -->
                    <div class="col-md-6 col-sm-6">
                      <a href="images/photo-gallery.jpg" rel="gallery-images"><img src="images/photo-gallery.jpg" class="img-responsive" alt=""></a>
                      <a href="images/photo-gallery.jpg" rel="gallery-images"><img src="images/photo-gallery.jpg" class="img-responsive" alt=""></a>
                      <a href="images/photo-gallery.jpg" rel="gallery-images"><img src="images/photo-gallery.jpg" class="img-responsive" alt=""></a>
                    </div>
                  </div><!-- row -->
                </div><!-- item -->
              </div><!-- carousel-inner -->

              <!-- Controls -->
              <a class="left carousel-control" href="#photogallery" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#photogallery" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
          </div><!-- tab2 -->

        </div><!-- tab-content -->
      </div><!-- tabpanel -->
    </div><!-- column -->

    <div class="col-md-3">

      <div class="row">
        <div class="col-md-12">
          <img src="images/map.jpg" class="img-responsive img-thumbnail" alt="">
          <ul class="location">
            <li><img src="images/location.jpg" alt=""></li>
            <li><p>Santa Barbara, CA</p></li>
          </ul>
        </div>
      </div><!-- row -->

      <div class="row">
        <div class="col-md-12">

          <div class="contact-bg">
            <h2>get in touch</h2>
            <div class="contact-info">
              <div class="img-wrap">
                <img src="images/jon.jpg" alt="">
              </div><!-- img-wrap -->
              <div class="txt-wrap">
                <h1>jon crocilla</h1>
                <span class="sm-heading">real estate agent</span>
                <a href=""><img src="images/mail.png" alt="">&nbsp;&nbsp;&nbsp;&nbsp;send email</a>
                <p><img src="images/call.png" alt="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(847) 452-9123</p>
              </div>
            </div><!-- contact-info -->
          </div><!-- contact-bg -->

        </div><!-- column -->
      </div><!-- row -->
      
    </div><!-- column -->

  </div><!-- row -->

</div><!-- contain-white -->
</main>

</div><!-- bg-pattern-grey -->

<?php include("footer.php");?>
