<?php include("header-inner.php");?>

<main>
  <div class="container contain contain-white testimonials">

    <div class="row">
      <div class="col-md-12">
        <h1>testimonails:</h1>
      </div>
    </div><!-- row -->

    <div class="row">
      <div class="col-md-10 col-md-offset-1 text-center">
        
        <div class="testimonials-wrap">
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>

        <div class="person-details">
          <div class="person-name">
            john s. doe
          </div>
          <div class="person-address">
            xyz, illinois, chicago area
          </div>
        </div><!-- person-details -->
        
      </div><!-- column -->
    </div><!-- row -->

<div class="row">
      <div class="col-md-10 col-md-offset-1 text-center">
        
        <div class="testimonials-wrap">
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>

        <div class="person-details">
          <div class="person-name">
            john s. doe
          </div>
          <div class="person-address">
            xyz, illinois, chicago area
          </div>
        </div><!-- person-details -->
        
      </div><!-- column -->
    </div><!-- row -->
    
    <div class="row">
      <div class="col-md-10 col-md-offset-1 text-center">
        
        <div class="testimonials-wrap">
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>

        <div class="person-details">
          <div class="person-name">
            john s. doe
          </div>
          <div class="person-address">
            xyz, illinois, chicago area
          </div>
        </div><!-- person-details -->
        
      </div><!-- column -->
    </div><!-- row -->
    

  </div><!-- container -->
</main>
</div><!-- bg-pattern-grey -->

<footer>

  <div class="bg-black">
   <div class="container contain">
     <div class="row">
       <div class="col-md-12">
         <ul class="nav navbar-nav footer-nav"> 
           <li><a href="">ANDERSONVILLE</a></li>
           <li><a href="">GOLD COAST </a></li>
           <li><a href="">LAKE VIEW</a></li>
           <li><a href="">LINCOLN PARK</a></li>
           <li><a href="">RAVENSWOOD</a></li>
           <li><a href="">WICKER PARK / BUCKTOWN</a></li>
         </ul><!-- nav -->
       </div><!-- column -->
     </div><!-- row -->
   </div><!-- container -->
 </div><!-- bg-black -->

 <div class="bg-grey">
  <div class="container contain">
    <div class="row">
      <div class="col-md-9 col-sm-12">
        <ol class="breadcrumb">
          <li class="active">Home</li>
          <li><a href="#">about jclp</a></li>
          <li><a href="">current listings</a></li>
          <li><a href="">mls search</a></li>
          <li><a href="">testimonials</a></li>
          <li><a href="">contact</a></li>
        </ol>
        <a href=""><img src="images/associated-logo.png" alt=""></a>
      </div><!-- column -->

      <div class="col-md-3 col-sm-12">
        <a href=""><img src="images/footer-logo.png" alt=""></a>
      </div>
    </div><!-- row -->
  </div><!-- container -->
</div><!-- bg-grey -->

<div class="container contain">
  <div class="row">
    <div class="col-md-11 col-sm-10 col-xs-10">
      <p><i class="fa fa-copyright"></i>&nbsp;2015 Jon Crocilla Luxury Properties. All rights reserved. </p>
    </div>
    <div class="col-md-1 col-sm-2 col-xs-2">
      <a href=""> <img src="images/bc-logo.png" alt=""></a>
    </div>
  </div>
</div>
</footer>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.bxslider.min.js"></script>
<script src="js/custom.js"></script>
</body>
</html>
