<?php include("header-inner.php");?>

<main>
  <div class="container contain contain-white lending">

    <div class="row">
      <div class="col-md-12">
        <div class="lending-background">
          <div class="text-top">
            JON CROCILLA <br>
            LUXURY PROPERTIES <br>
            <span class="lght-blue">LENDING</span> 
          </div><!-- text-top -->
          <div class="text-bottom">
            Lorem ipm dolor sit amet, consec tetur adi cing elit, sed do eiusmod tempor idunt ut labore et dolore magna dolore magnamr incididunt ut labore etore dolore magna aliqua. Uquat. uisoreore auirure dehenderit in volupt”
          </div><!-- text-bottom -->
        </div><!-- lending-background -->
      </div><!-- column -->
    </div><!-- row -->

    <div class="row">
      <div class="col-md-5 col-md-offset-1">

        <form class="form-horizontal application-form">
          <h1 class="border-bottom">GET PRE APPROVED NOW!</h1>

          <div class="form-group">
            <label class="col-md-4 control-label">first name:</label>
            <div class="col-md-8">
              <input type="text" class="form-control">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-4 control-label">last name:</label>
            <div class="col-md-8">
              <input type="text" class="form-control">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-4 control-label">phone #:</label>
            <div class="col-md-8">
              <input type="text" class="form-control">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-4 control-label">Email:</label>
            <div class="col-md-8">
              <input type="email" class="form-control">
            </div>
          </div>

          <div class="text-center">
            <button class="btn btn-default btn-pattern" type="submit">apply now!</button>
          </div>

        </form>
      </div><!-- column -->

      <div class="col-md-5 col-md-offset-1">
        <h1 class="large-heading">apply here</h1>

        <div class="row">
          <div class="col-md-12">
            <div class="number-holder">
              1
            </div>
            <div class="text-holder">
              <h4>apply online</h4>
              <p>Fill out the form and you will get a response as soon as possible.</p>
            </div>
          </div><!-- column -->
        </div><!-- row --> 

        <div class="row">
          <div class="col-md-12">
            <div class="number-holder">
              2
            </div>
            <div class="text-holder">
              <h4>contract</h4>
              <p>The process will not take more than
                60 minutes. </p>
              </div>
            </div><!-- column -->
          </div><!-- row --> 

          <div class="row">
            <div class="col-md-12">
              <div class="number-holder no-img-after">
                3
              </div>
              <div class="text-holder">
                <h4>get your money!</h4>
                <p>Once you have signed the contract.
                  money will be transferred to your
                  bank account within few hours or
                  you will be handed over immediately. </p>
                </div>
              </div><!-- column -->
            </div><!-- row --> 

          </div>
        </div><!-- row -->

        <div class="row bg-pattern-brick">

         <div class="col-md-4 col-sm-4 col-xs-12 alter-width-listings">
           <div class="bg-dropshadow">
           <h2>heading title 1</h2>
             <img src="images/lending1.jpg" class="img-responsive" alt="">
             <p>As a leader in Westside real estate, Sacha is always adding new listings. Browse a gallery of Sacha's hottest current listings and be sure to contact her directly to inquire about exclusive pocket listings or upcoming properties not yet on the market.</p>
             <a href="" class="btn btn-default btn-pattern"><span class="after"><img src="images/icon-btn.png" alt=""></span>read more</a>
           </div>
         </div><!-- column -->

         <div class="col-md-4 col-sm-4 col-xs-12 alter-width-listings">
           <div class="bg-dropshadow">
             <h2>heading title 2</h2>
             <img src="images/lending2.jpg" class="img-responsive" alt="">
             <p>From Beverly Hills to Brentwood, Marina del Rey to Malibu, there is simply no shortage of exceptional neighborhoods to explore within the Los Angeles luxury band. Click the button below to begin searching for your next home today.</p>
             <a href="" class="btn btn-default btn-pattern"><span class="after"><img src="images/icon-btn.png" alt=""></span>read more</a>
           </div>
         </div><!-- column -->

         <div class="col-md-4 col-sm-4 col-xs-12 alter-width-listings">
           <div class="bg-dropshadow">
             <h2>heading title 3</h2>
             <img src="images/lending3.jpg" class="img-responsive" alt="">
             <p>Sacha Radford's intimate understanding of Southern California's luxury communities, combined with her undying commitment to meeting her clients' needs, make her the ideal representative in the fierce world of high-end, Los Angeles real estate.</p>
             <a href="" class="btn btn-default btn-pattern"><span class="after"><img src="images/btn-icon-book.png" alt=""></span>read more</a>
           </div>
         </div><!-- column -->

       </div><!-- row -->


     </div><!-- contain-white -->
   </main>

 </div><!-- bg-pattern-grey -->

 <?php include("footer.php");?>
