<?php include("header-inner.php");?>

<main>
 <div class="container contain contain-white form-submission-page">

  <div class="row">
    <div class="col-md-12 text-center">
      <h1 class="border-bottom">submission</h1>
    </div><!-- column -->
  </div><!-- row -->

  <div class="row">
    <div class="col-md-12">    
      <ul class="page-links">
        <li class="active"><a href="questionnaire.php">1.&nbsp;lending questionnaire</a></li>
        <li class="active"><a href="contact-info.php">2.&nbsp;contact info</a></li>
        <li class="active"><a href="submission.php">3.&nbsp;submission</a></li>
      </ul>
    </div><!-- column -->
  </div><!-- row -->

  <div class="row text-center">
    <div class="col-md-12">
      <p>VERIFY THE DETAILS AND PRESS SUBMIT. WE’LL GET BACK TO YOU AS SOON AS POSSIBLE.</p>
    </div>
  </div>

  <div class="row">

    <div class="col-md-5 col-sm-6 col-md-offset-1 customizable-form">

      <div class="heading">
        questionnaire
        <a href=""><img src="images/pencil.png" alt=""></a>
      </div>
      <div class="body">
        <div class="table-responsive">
          <table class="table">
            <tr>
              <td>Price range of home</td>
              <td class="text-right">$0000 TO $0000</td>
            </tr>
            <tr>
              <td>Time to purchase</td>
              <td class="text-right">3 months</td>
            </tr>
            <tr>
              <td>Down payment</td>
              <td class="text-right">$0000</td>
            </tr>
            <tr>
              <td>Credit</td>
              <td class="text-right">$0000</td>
            </tr>
            <tr>
              <td>Annual income</td>
              <td class="text-right">$0000</td>
            </tr>
            <tr>
              <td>Hourly salary/Self-employed?</td>
              <td class="text-right">$0000</td>
            </tr>
            <tr>
              <td>Years on job</td>
              <td class="text-right">4 YEARS</td>
            </tr>
            <tr>
              <td>Home to sell?</td>
              <td class="text-right">NO</td>
            </tr>
          </table>
        </div>
      </div><!-- body -->

    </div><!-- column -->
    <div class="col-md-5 col-sm-6 customizable-form">

      <div class="heading">
        contact info
        <a href=""><img src="images/pencil.png" alt=""></a>
      </div>

      <div class="body">
        <div class="table-responsive">
          <table class="table">
            <tr>
              <td>First name</td>
              <td class="text-right">Brian</td>
            </tr>
            <tr>
              <td>Last name</td>
              <td class="text-right">cozzi</td>
            </tr>
            <tr>
              <td>Email</td>
              <td class="text-right">bc@briancozzi.com</td>
            </tr>
            <tr>
              <td>Phone number</td>
              <td class="text-right">+1 9876543210</td>
            </tr>
            <tr>
              <td>Address 1</td>
              <td class="text-right">xxxxxxxxxxxxxxxxx</td>
            </tr>
            <tr>
              <td>Address 2</td>
              <td class="text-right">xxxxxxxxxxxxxxxxx</td>
            </tr>
            <tr>
              <td>City, state, zip</td>
              <td class="text-right">xxxxxxx, xxxxxxxxxx, xxxxxx</td>
            </tr>
            <tr>
              <td>Best time to reach you</td>
              <td class="text-right">12:00AM TO 02:00PM</td>
            </tr>
          </table>
        </div>
      </div>
    </div><!-- column -->

  </div><!-- row -->

  <div class="row mb27">
   <div class="col-md-12 text-center">
   <button class="btn btn-default btn-pattern" type="submit">submit</button>
  </div>
</div>

</div><!-- contain-white -->
</main>

</div><!-- bg-pattern-grey -->

<?php include("footer.php");?>
