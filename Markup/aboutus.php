<?php include("header-inner.php");?>

<main>
  <div class="container contain contain-white">

    <div class="row mb27">

      <div class="col-md-8 col-sm-12 col-xs-12">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <img src="images/carousel.jpg" alt="">
            </div>
            <div class="item">
             <img src="images/carousel.jpg" alt="">
           </div>
           <div class="item">
             <img src="images/carousel.jpg" alt="">
           </div>
         </div><!-- carousel-inner -->

       </div><!-- carousel -->
     </div><!-- column -->

     <div class="col-md-4 col-sm-12 col-xs-12 margin">
       
       <?php include("testimonials-widget.php");?>

     </div><!-- column -->

   </div><!-- row -->  

   <div class="row mb27">

    <div class="col-md-8 col-xs-12">
      <h1>about jon crocilla</h1>
      
      <div class="about-box">
        <p>Jon Crocilla always finds a way. When it comes to his clients he stops at nothing to help them achieve their goals. His drive to succeed is something he learned early in life, both from his parents, who operate a mortgage firm, and from competing in football and hockey. He is passionate about real estate and about his clients’ welfare. There is simply no more committed individual than Jon.</p><br>
        <p>Jon also understands the value of creating and maintaining relationships.  Real estate is a relationship based enterprise. The power of relationships becomes apparent when you are looking for the right buyer or seller. Being networked with the top producing agents and the clients they represent can mean the difference between a successful or an unsuccessful outcome. Jon’s focus is on success.</p><br>
        <p>He grew up with real estate and is highly knowledgeable about the Chicago market. He also has a strong understanding of the role of marketing technology and how to use it to the advantage of his clients. With 90% of consumers looking for their next home online, Jon has expert knowledge of eMarketing and social media best practices. He can get your home the greatest amount of exposure possible so that it sells quickly and at the best possible price.</p><br>
        <p>Jon is a lifelong resident of Chicago and graduated from Elmhurst College with a BA in Business and History.</p><br>
        <h1 class="heading-tlnt-box">why to choose me?</h1>

        <div class="row">

          <div class="col-md-6">

            <div class="tlnt-box">
              <div class="heading mw180">
                experience
              </div>
              <div class="body">
                <p>Anyone can call themselves a Luxury Real Estate Agent, but typically the proof is in the transactions. Handling a multimillion dollar sale can have some very complicated components, both on the listing side and the selling side. High end buyers and sellers are typically handling large transactions in their daily business lives, and they expect the same business acumen. </p>
                <div class="bg_img">
                  <img src="images/faded-bg.png" alt="">
                </div>
              </div><!-- heading -->
            </div><!-- tlnt-box -->

            <div class="tlnt-box">
              <div class="heading no-img">
                COMMUNITY &amp; NATIONAL AFFILIATIONS
              </div>
              <div class="body">
                <p>Luxury Real Estate Agents are involved in their communities, as well as in national organizations. Many transactions are done off the public MLS, and you want to have an agent that has their ear to the ground. Whether you are a buyer or a seller, you want whoever is representing you to get the phone call when there is a desirable property available. </p>
                <div class="bg_img">
                  <img src="images/faded-bg.png" alt="">
                </div>
              </div><!-- heading -->
            </div><!-- tlnt-box -->

          </div><!-- column -->

          <div class="col-md-6">

            <div class="tlnt-box">
              <div class="heading mw240">
               marketing knowledge
             </div>
             <div class="body">
              <p>A Luxury Broker should be able to speak about their market with utter fluency. If they cannot give you an educated and thoughtful answer, then you probably need to keep looking for a more knowledgeable agent.</p>
              <div class="bg_img">
                <img src="images/faded-bg.png" alt="">
              </div>
            </div><!-- heading -->
          </div><!-- tlnt-box -->

          <div class="tlnt-box">
            <div class="heading mw225">
              NEGOTIATING SKILLS
            </div>
            <div class="body">
              <p>Luxury Transactions typically have some complicated components and require refined negation skills. You're hiring an agent to get you the best value for your hard earned dollar. Make sure they represent you!</p>
              <div class="bg_img">
                <img src="images/faded-bg.png" alt="">
              </div>
            </div><!-- heading -->
          </div><!-- tlnt-box -->

          <div class="tlnt-box">
            <div class="heading mw150">
              refernces
            </div>
            <div class="body">
              <p>Any agent should have a list of references available to you you should you feel the need to perform some due diligence.</p>
              <div class="bg_img">
                <img src="images/faded-bg.png" alt="">
              </div>
            </div><!-- heading -->
          </div><!-- tlnt-box -->

        </div><!-- column -->

      </div><!-- row -->

    </div><!-- about-box -->
  </div><!-- column -->

  <div class="col-md-4 col-sm-12">
    <div class="row">
      <div class="col-md-12 col-sm-6 col-xs-12 fb-lb">
        <img src="images/fb-lb.jpg" class="img-responsive" alt="">
      </div>

      <div class="col-md-12 col-sm-6 col-xs-12 profile">
       <?php include("profile-widget.php");?>
     </div> 
   </div>

 </div><!-- column -->

</div><!-- row -->

</div><!-- container -->
</main>
</div><!-- bg-pattern-grey -->

<?php include("footer.php");?>