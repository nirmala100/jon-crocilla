<footer>

  <div class="bg-black">
   <div class="container contain">
     <div class="row">
       <div class="col-md-12">
         <ul class="nav navbar-nav footer-nav"> 
           <li>ANDERSONVILLE</li>
           <li>GOLD COAST </li>
           <li>LAKE VIEW</li>
           <li>LINCOLN PARK</li>
           <li>RAVENSWOOD</li>
           <li>WICKER PARK / BUCKTOWN</li>
         </ul><!-- nav -->
       </div><!-- column -->
     </div><!-- row -->
   </div><!-- container -->
 </div><!-- bg-black -->

 <div class="bg-grey">
  <div class="container contain">
    <div class="row">
      <div class="col-md-9 col-sm-12">
        <ol class="breadcrumb">
          <li class="active">Home</li>
          <li><a href="#">about jclp</a></li>
          <li><a href="">current listings</a></li>
          <li><a href="">mls search</a></li>
          <li><a href="">testimonials</a></li>
          <li><a href="">contact</a></li>
        </ol>
        <a href=""><img src="images/associated-logo.png" alt=""></a>
      </div><!-- column -->

      <div class="col-md-3 col-sm-12">
        <a href=""><img src="images/footer-logo.png" alt=""></a>
      </div>
    </div><!-- row -->
  </div><!-- container -->
</div><!-- bg-grey -->

<div class="bg-white">
  <div class="container contain">
    <div class="row">
      <div class="col-md-11 col-sm-10 col-xs-10">
        <p><i class="fa fa-copyright"></i>&nbsp;2015 Jon Crocilla Luxury Properties. All rights reserved. </p>
      </div>
      <div class="col-md-1 col-sm-2 col-xs-2">
        <a href=""> <img src="images/bc-logo.png" alt=""></a>
      </div>
    </div>
  </div><!-- container -->
</div><!-- bg-white -->
</footer>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.bxslider.min.js"></script>
<script src="js/jquery.colorbox-min.js"></script>
<script src="js/jquery.selectbox-0.2.js"></script>
<script src="js/custom.js"></script>

</body>
</html>