<?php include("header-inner.php");?>

<main>
 <div class="container contain contain-white">

  <div class="row mb27">
    <div class="col-md-4 col-sm-4 col-xs-4 alter-width">
      <img src="images/jon-big.jpg" class="img-responsive" alt="">
    </div><!-- column -->
    <div class="col-md-8 col-sm-8 col-xs-8 alter-width">
      <form class="contact-form">

        <div class="row">

          <div class="col-md-6 col-sm-6 form-group">
            <label>Name</label>
            <input type="text" class="form-control" placeholder="">
          </div>

          <div class="col-md-6 col-sm-6 form-group">
            <label>Email Address</label>
            <input type="email" class="form-control" placeholder="">
          </div>

        </div><!-- row -->

        <div class="row">

          <div class="col-md-6 col-sm-6 form-group">
            <label>Phone</label>
            <input type="text" class="form-control" placeholder="">
          </div>

          <div class="col-md-6 col-sm-6 form-group">
            <label>Inquiry Type</label>
            <input type="email" class="form-control" placeholder="">
          </div>

        </div><!-- row -->

        <div class="row">
          <div class="col-md-12 col-sm-12">
            <label>Message</label>
            <textarea class="form-control" rows="4"></textarea>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="checkbox">
              <label>
                Subscribe to my future updates:<input type="checkbox">
              </label>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12 col-sm-12">
            <button class="btn btn-default btn-pattern" type="submit">Send Message</button>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12 col-sm-12">
            <em>Your privacy is important to me. Your information will not be shared, sold or exchanged with anyone else.</em>
          </div>
        </div>

      </form>
    </div><!-- column -->
  </div><!-- row -->

  <div class="row mb27">

    <div class="col-md-4 col-sm-4 alter-width jc-details">
      <h2>jon crocilla <br> real estate agent</h2>
      <address>676 N Michigan Ave <br> 
        Chicago, IL 60611</address>
        <div class="contact-info">
          cell: (847) 452-9123 <br>
          Office: (312) 981-5500
        </div>
      </div><!-- column -->
      <div class="col-md-8 col-sm-8 alter-width">
        <img src="images/map-big.jpg" class="img-responsive img-thumbnail" alt="">
      </div><!-- column -->

    </div><!-- row -->

    <div class="row bg-pattern-brick">

     <div class="col-md-4 col-sm-4 col-xs-12 alter-width-listings">
       <div class="bg-dropshadow">
         <h2>current listings</h2>
         <img src="images/listing1.jpg" class="img-responsive" alt="">
         <p>As a leader in Westside real estate, Sacha is always adding new listings. Browse a gallery of Sacha's hottest current listings and be sure to contact her directly to inquire about exclusive pocket listings or upcoming properties not yet on the market.</p>
         <a href="" class="btn btn-default btn-pattern"><span class="after"><img src="images/icon-btn.png" alt=""></span>view listings</a>
       </div>
     </div><!-- column -->

     <div class="col-md-4 col-sm-4 col-xs-12 alter-width-listings">
       <div class="bg-dropshadow">
         <h2>find your next home</h2>
         <img src="images/listing2.jpg" class="img-responsive" alt="">
         <p>From Beverly Hills to Brentwood, Marina del Rey to Malibu, there is simply no shortage of exceptional neighborhoods to explore within the Los Angeles luxury band. Click the button below to begin searching for your next home today.</p>
         <a href="" class="btn btn-default btn-pattern"><span class="after"><img src="images/icon-btn.png" alt=""></span>view listings</a>
       </div>
     </div><!-- column -->

     <div class="col-md-4 col-sm-4 col-xs-12 alter-width-listings">
       <div class="bg-dropshadow">
         <h2>connecting buyers &amp; sellers</h2>
         <img src="images/listing3.jpg" class="img-responsive" alt="">
         <p>Sacha Radford's intimate understanding of Southern California's luxury communities, combined with her undying commitment to meeting her clients' needs, make her the ideal representative in the fierce world of high-end, Los Angeles real estate.</p>
         <a href="" class="btn btn-default btn-pattern"><span class="after"><img src="images/btn-icon-book.png" alt=""></span>read more</a>
       </div>
     </div><!-- column -->

   </div><!-- row -->

 </div><!-- contain-white -->
</main>

</div><!-- bg-pattern-grey -->

<?php include("footer.php");?>