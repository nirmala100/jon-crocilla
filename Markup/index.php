<?php include("header-index.php");?>
<div class="container contain">

  <div class="row mb27 text-center">
    <div class="col-md-12 mt17">
      <img src="images/previews.png" alt="">
    </div><!-- column -->
  </div><!-- row -->

  <div class="row">
    <div class="col-md-8 col-md-offset-2 bg-fade">
      <div class="heading text-center uppercase">
        Making your <br> 
        home buying experience<br>
        <span class="big">MY PRIORITY</span>
      </div>
    </div>
  </div><!-- row -->

  <div class="row">
    <div class="col-md-12 search-bx">

      <div class="search-heading">
        <h3>search for property</h3>
      </div><!-- search-heading -->

      <form class="search-body">

        <div class="row">

          <div class="col-md-4 col-sm-2 col-xs-12">
            <h4>located in:</h4>
            <select class="form-control customised-select" id="state_id">
              <optgroup>
                <option>Chicago</option>
                <option>Alabama</option>
                <option>Alabama</option>
                <option>Alabama</option>
              </optgroup>
            </select>

          </div><!-- column -->

          <div class="col-md-2 col-sm-4 col-xs-12 text-center">
            <h4 class="line">cost between</h4>
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-6" id="min_id">
                <select class="form-control customised-select">
                  <optgroup>
                    <option>Min. Price</option>
                    <option>$2,000,000</option>
                    <option>$2,000,000</option>
                    <option>$2,000,000</option>
                  </optgroup>
                </select>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6" id="max_id">
                <select class="form-control customised-select">
                <option>Max. Price</option>
                  <option>$3,000,000</option>
                  <option>$3,000,000</option>
                  <option>$3,000,000</option>
                </select>
              </div>
            </div>
          </div><!-- column -->

          <div class="col-md-2 col-sm-4 col-xs-12">
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-6">
                <h4># of beds:</h4>
                <select class="form-control customised-select" id="beds_id">
                  <option>&lt;select&gt;</option>
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                </select>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6">
                <h4># of baths:</h4>
                <select class="form-control customised-select" id="baths_id">
                  <option>&lt;select&gt;</option>
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                </select>
              </div>
            </div>
          </div><!-- column -->

          <div class="col-md-4 col-sm-2 col-xs-12 chng-width">
            <button type="submit" class="btn srch-btn"><i class="fa fa-search"></i>&nbsp;&nbsp;search</button>
          </div><!-- column -->

        </div><!-- row -->

      </form><!-- search-body -->

    </div><!-- column -->
  </div><!-- row -->

</div><!-- contain -->

</div><!-- ahead-bg -->

<main>
  <div class="bg-grey">
    <div class="container contain">
      <div class="row">

        <div class="col-md-8 col-sm-8 col-xs-12">
          <h2>featured properties</h2>

          <ul class="bxslider featured-slider">

            <li>

              <a href="">
                <div class="border">
                  <div class="property-box">
                    <div class="img-wrap"><img src="images/properties1.jpg" alt=""></div>                   
                    <h4>SEASIDE HOUSE</h4>
                    <p>Illinois, Chicago</p>                      
                  </div>
                  <div class="features">
                    <p>4 beds + 2 baths + 233 sqft</p>
                  </div>
                </div>
                <div class="rate"><i class="fa fa-usd"></i>&nbsp;2,150,000</div>
              </a>

            </li>

            <li>

              <a href="">
                <div class="border">
                  <div class="property-box">
                    <div class="img-wrap"><img src="images/properties1.jpg" alt=""></div>                    
                    <h4>SEASIDE HOUSE</h4>
                    <p>Illinois, Chicago</p>
                  </div>                   
                  <div class="features">
                    <p>4 beds + 2 baths + 233 sqft</p>
                  </div>
                </div>
                <div class="rate"><i class="fa fa-usd"></i>&nbsp;2,150,000</div>
              </a>

            </li>

            <li>

             <a href="">
              <div class="border">
                <div class="property-box">
                  <div class="img-wrap"><img src="images/properties1.jpg" alt=""></div>                       
                  <h4>SEASIDE HOUSE</h4>
                  <p>Illinois, Chicago</p>
                </div>                   
                <div class="features">
                  <p>4 beds + 2 baths + 233 sqft</p>
                </div>
              </div>
              <div class="rate"><i class="fa fa-usd"></i>&nbsp;2,150,000</div>
            </a>

          </li>

          <li>

            <a href="">
              <div class="border">
                <div class="property-box">
                  <div class="img-wrap"><img src="images/properties1.jpg" alt=""></div>               
                  <h4>SEASIDE HOUSE</h4>
                  <p>Illinois, Chicago</p>                      
                </div>
                <div class="features">
                  <p>4 beds + 2 baths + 233 sqft</p>
                </div>
              </div>
              <div class="rate"><i class="fa fa-usd"></i>&nbsp;2,150,000</div>
            </a>
          </li>
          
          <li>

            <a href="">
              <div class="border">
                <div class="property-box">
                  <div class="img-wrap"><img src="images/properties1.jpg" alt=""></div>    
                  <h4>SEASIDE HOUSE</h4>
                  <p>Illinois, Chicago</p>
                </div>                   
                <div class="features">
                  <p>4 beds + 2 baths + 233 sqft</p>
                </div>
              </div>
              <div class="rate"><i class="fa fa-usd"></i>&nbsp;2,150,000</div>
            </a>

          </li>

          <li>

           <a href="">
            <div class="border">
              <div class="property-box">
                <div class="img-wrap"><img src="images/properties1.jpg" alt=""></div>           
                <h4>SEASIDE HOUSE</h4>
                <p>Illinois, Chicago</p>
              </div>                   
              <div class="features">
                <p>4 beds + 2 baths + 233 sqft</p>
              </div>
            </div>
            <div class="rate"><i class="fa fa-usd"></i>&nbsp;2,150,000</div>
          </a>

        </li>

      </ul>

    </div><!-- column -->

    <div class="col-md-4 col-sm-4 col-xs-12">
      <?php include("profile-widget.php");?>
    </div>

  </div><!-- row -->
</div><!-- container -->
</div><!-- bg-grey -->

<div class="bg-white">
  <div class="bg-img">
    <img src="images/faded-img.jpg" alt="">
  </div>
  <div class="container contain">
    <div class="row">

      <div class="col-md-8 col-sm-8">
        <h1>ABOUT JON CROCILLA:</h1>
        <div class="about-box">
          <p>Jon Crocilla always finds a way. When it comes to his clients he stops at nothing to help them achieve their goals. His drive to succeed is something he learned early in life, both from his parents, who operate a mortgage firm, and from competing in football and hockey. He is passionate about real estate and about his clients’ welfare. There is simply no more committed individual than Jon.</p><br>
          <p>Jon also understands the value of creating and maintaining relationships.  Real estate is a relationship based enterprise. The power of relationships becomes apparent when you are looking for the right buyer or seller. Being networked with the top producing agents and the clients they represent can mean the difference between a successful or an unsuccessful outcome. Jon’s focus is on success.</p>
          <a href="" class="btn btn-style uppercase">learn more</a>
        </div>
      </div><!-- column -->

      <div class="col-md-4 col-sm-4">
        <?php include("testimonials-widget.php");?>
      </div>

    </div><!-- row -->
  </div><!-- container -->
</div><!--bg-white-->
</main>

<?php include("footer.php");?>