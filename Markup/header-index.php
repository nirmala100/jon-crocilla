<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>home</title>

  <!-- Normalize -->
  <link href="css/normalize.css" rel="stylesheet">

  <!-- Bootstrap -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- bx.slider -->
  <link rel="stylesheet" href="css/jquery.bxslider.css">

  <link href="css/jquery.selectbox.css" type="text/css" rel="stylesheet" />

  <!--Style-->
  <link rel="stylesheet" type="text/css" href="css/style.css">

  <!--responsive-->
  <link rel="stylesheet" type="text/css" href="css/responsive.css">

  <!--Fontawesome-4.3.0-->
  <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

  <link href='http://fonts.googleapis.com/css?family=Questrial|Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel='stylesheet' type='text/css'>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>
    <body>

      <div class="bg-img-home">
      <img src="images/main-bg.jpg" alt="">
      </div>

      <header class="push-top">
        <div class="container contain">
          <div class="row">

            <div class="col-md-4 col-sm-4">
              <div class="heading-dtls uppercase">
                GOLD COAST CHICAGO <br>  
                REAL ESTATE PROPERTIES
              </div>
            </div><!-- column-closed -->

            <div class="col-md-4 col-sm-4 text-center logo">
              <a href=""><img src="images/logo.png" alt=""></a>
            </div><!-- column-closed -->

            <div class="col-md-4 col-sm-4 text-right">

              <ul class="social-media">
                <li><a href="" class="circle fb"><i class="fa fa-facebook"></i></a></li>
                <li><a href="" class="circle twt"><i class="fa fa-twitter"></i></a></li>
                <li><a href="" class="circle lin"><i class="fa fa-linkedin-square"></i></a></li>
              </ul>
              <a href="" class="cb-logo"><img src="images/coldwell-banker.png" alt=""></a>

              <div class="sm-txt uppercase"><a href="">book an appointment</a></div>
              <div class="lg-txt">847.452.9123</div>

            </div><!-- column-closed -->

          </div><!-- row -->

          <div class="row">
           <div class="col-md-12">
            <nav class="navbar navbar-default">
              <div class="container-fluid">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                </div><!-- navbar-header -->

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-collapse-1">
                  <ul class="nav navbar-nav">
                    <li><a href="index.php">home</a></li>
                    <li><a href="aboutus.php">about jclp</a></li>
                    <li><a href="listing.php">our listings</a></li>
                    <li><a href="testimonials.php">testimonials</a></li>
                    <li><a href="">lending</a></li>
                    <li><a href="contact-us.php">contact</a></li>
                  </ul><!--nav-closed-->
                </div><!--collapse-->
              </div><!-- container-fluid -->
            </nav>
          </div><!-- column-closed -->
        </div><!-- row-closed -->

      </div><!-- container-closed -->
    </header>
